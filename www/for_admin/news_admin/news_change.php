<?php
require_once ('inc.php');
require_once ('functions.php');


$id = getRequestOrInit('id', '');
$datum = getRequestOrInit('datum', '');
$zeit = getRequestOrInit('zeit', '');
$titel = getRequestOrInit('titel', '');
$text = getRequestOrInit('text', '');
$action = getRequestOrInit('action', '');

if ($action == 'update') {
    $newsItems = new NewsItems();
    $newsItems->update_item($id, $datum, $zeit, $titel, $text);
}
$renderedHTML = <<<EOT
        <br />
        <div class=\"rm_h1\">Eintrag &auml;ndern</div>
        <form>
EOT;

$newsItems = new NewsItems();
$newsItem = $newsItems->item_by_id($id);
$cols = 120;
$rows = 25;
$renderedHTML .= <<<EOT
            <table class=rm_no_border>
                <tr><td>Datum:</td><td><input name="datum" value="{$newsItem['datum']}"></input></td></tr>
                <tr><td>Zeit:</td><td><input name="zeit" value="{$newsItem['zeit']}"></input></td></tr>
                <tr><td>Titel:</td><td><input name="titel"	size="100"	value="{$newsItem['titel']}"></input></td></tr>
                <tr><td valign=top>Text:</td><td><textarea name="text" cols=$cols rows=$rows>{$newsItem['text']}</textarea></td></tr>
            </table>
            <input type="hidden" name="id" value="{$newsItem['id']}"></input>
            <input type="submit" name="action" value=update></input>
        <form>
        <div class=rm_h2>
            <a href=news.php?datum="{$newsItem['datum']}">Zur&uuml;ck</<a>
        </div>
        <br />
EOT;
 
// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('OK - News verwalten', 'page', $renderedHTML);

?>


