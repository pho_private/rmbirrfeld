<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once ('inc.php');
require_once ('helfer_2018_inc.php');
require_once ('dblib.inc.php');
require_once ('functions.php');


function getTimeSlotInfo($date, $shift, $type, $maxEntries)
{
    global $rm_tbl_einsaetze;
    global $errMsg;
    $timeSlot = array();
    $timeSlot['name'] = array();
    $tmp_cnt = 0;
    
    $select = "SELECT vorname as vorname, name as name, wohnort as wohnort, jahrgang as jahrgang 
		FROM {$rm_tbl_einsaetze} WHERE datum = '{$date}' AND schicht = {$shift} AND gebiet = '{$type}' AND deleted is NULL ORDER BY name, vorname";
    $result = DBQuery($select);
    if ($result) 
	{
        $row = DBFetchRow($result);
        while ($row) 
		{
            $tmp_cnt ++;
            // $timeSlot['name'][] = substr($row[0],0,1).".&nbsp;".$row[1];
            $timeSlot['name'][] = $row[0] . "&nbsp;" . $row[1];
            $row = DBFetchRow($result);
        }
    }
    $rest_tmp = $maxEntries - $tmp_cnt;
    if 		($tmp_cnt > 0 && $rest_tmp > 1)		$timeSlot['names'] = implode('<br /> ', $timeSlot['name']) . "<br /><b>noch $rest_tmp freie Eins&auml;tze</b>";
    elseif 	($tmp_cnt > 0 && $rest_tmp > 0)		$timeSlot['names'] = implode('<br /> ', $timeSlot['name']) . "<br /><b>noch $rest_tmp freier Einsatz</b>";
    elseif	($tmp_cnt == 0 && $rest_tmp > 0)	$timeSlot['names'] = "<br /><b>noch $rest_tmp freie Eins&auml;tze</b>";
    else										$timeSlot['names'] = implode('<br /> ', $timeSlot['name']);
    
    if ($maxEntries - count($timeSlot['name']) > 4)			$timeSlot['style'] = 'td_w3';
    elseif ($maxEntries - count($timeSlot['name']) > 2)		$timeSlot['style'] = 'td_w2';
    elseif ($maxEntries - count($timeSlot['name']) > 0)		$timeSlot['style'] = 'td_w1';
    else													$timeSlot['style'] = 'td_ok';
            
    return $timeSlot;
}

$errMsg = '';
$result = DBQuery("SELECT count(*) FROM {$rm_tbl_einsaetze} where deleted is NULL");
$row = DBFetchRow($result);
$total_meldungen = $row[0];
$left = $total_einsaetze - $total_meldungen;

$renderedHTML = <<<EOT
		<div class=big_col_block>
			<div class=rm_h1>Helfereins&auml;tze w&auml;hrend der SM (total Eins&auml;tze: {$total_einsaetze} / bisher gemeldet: {$total_meldungen} / noch ben&ouml;tigt: {$left})</div>
				<table>
					<colgroup>
						<col width='100px'>
						<col width='200px'>
						<col width='200px'>
						<col width='200px'>
						<col width='200px'>
						<col width='200px'>
					</colgroup>
EOT;

// --------------------------------------------------------------------------------
// Tabelle Aufbau

foreach ($einsatz_gebiete as $einsatz_gebiet) if ($einsatz_gebiet['key'] == 'aufbau') break;
if ( $einsatz_gebiet['activ'] == 1)
{ 
$renderedHTML .= <<<EOT
					<tr>
						<th class=th_1>Aufbau</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_aufbau[1]}</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_aufbau[2]}</th>
					</tr>
EOT;

foreach ($aufbau_dates_und_einsatz as $date_und_einsatz)
{
    $timeSlotInfo[1] = getTimeSlotInfo($date_und_einsatz['datum'], 1, 'Aufbau', $date_und_einsatz['einsaetze'][1]);
    $timeSlotInfo[2] = getTimeSlotInfo($date_und_einsatz['datum'], 2, 'Aufbau', $date_und_einsatz['einsaetze'][2]);

    $curDay = new rm_DateTime($date_und_einsatz['datum']);    
    $weekday = substr($curDay->format('l'), 0, 2) . ".";
    $today = $curDay->format('d.m.');
  
    $renderedHTML .= <<<EOT
					<tr>
						<th class=th_1 valign=top>{$weekday} {$today}</th>
						<td class={$timeSlotInfo[1]['style']} valign=top colspan=2>{$timeSlotInfo[1]['names']}</td>
						<td class={$timeSlotInfo[2]['style']} valign=top colspan=2>{$timeSlotInfo[2]['names']}</td>
					</tr>
EOT;
}

$renderedHTML .= <<<EOT
	<tr><td>&nbsp;</td></tr>
EOT;
}

// --------------------------------------------------------------------------------
// Tabelle Eröffnung
foreach ($einsatz_gebiete as $einsatz_gebiet) if ($einsatz_gebiet['key'] == 'eroeffnung') break;
if ( $einsatz_gebiet['activ'] == 1)
{ 
$renderedHTML .= <<<EOT

					<tr>
						<th class=th_1>Er&ouml;ffnungsfeier</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_eroeffnung[1]}</th>
					</tr>

EOT;

foreach ($eroeffnung_dates_und_einsatz as $date_und_einsatz) {
    
    $timeSlotInfo[1] = getTimeSlotInfo($date_und_einsatz['datum'], 1, 'Eroeffnung', $date_und_einsatz['einsaetze'][1]);
    
    $curDay = new rm_DateTime($date_und_einsatz['datum']);    
    $weekday = substr($curDay->format('l'), 0, 2) . ".";
    $today = $curDay->format('d.m.');
    
    $renderedHTML .= <<<EOT
					<tr>
						<th class=th_1 valign=top>{$weekday} {$today}</th>
						<td class={$timeSlotInfo[1]['style']} valign=top colspan=2>{$timeSlotInfo[1]['names']}</td>
					</tr>

EOT;
}

$renderedHTML .= <<<EOT
	<tr><td>&nbsp;</td></tr>
EOT;
}

// --------------------------------------------------------------------------------
// Tabelle RM/SM (Beiz und Flugbetrieb Betrieb)

$renderedHTML .= <<<EOT

					<tr>
						<th class=th_1>$rm_comp_type</th>
						<th class=th_1 valign=top>{$schichtzeiten_beiz[1]} (Beiz)</th>
						<th class=th_1 valign=top>{$schichtzeiten_beiz[2]} (Beiz)</th>
						<th class=th_1 valign=top>{$schichtzeiten_beiz[3]} (Beiz)</th>
						<th class=th_1 valign=top>{$schichtzeiten_betrieb[1]} (Betrieb)</th>
						<th class=th_1 valign=top>{$schichtzeiten_betrieb[2]} (Betrieb)</th>
					</tr>

EOT;

foreach ($beiz_dates_und_einsatz as $date_und_einsatz)
{
    $curDay = new rm_DateTime($date_und_einsatz['datum']);    
    $weekday = substr($curDay->format('l'), 0, 2) . ".";
    $today = $curDay->format('d.m.');

    $timeSlotInfo[1] = getTimeSlotInfo($date_und_einsatz['datum'], 1, 'Beiz', $date_und_einsatz['einsaetze'][1]);
    $timeSlotInfo[2] = getTimeSlotInfo($date_und_einsatz['datum'], 2, 'Beiz', $date_und_einsatz['einsaetze'][2]);
    $timeSlotInfo[3] = getTimeSlotInfo($date_und_einsatz['datum'], 3, 'Beiz', $date_und_einsatz['einsaetze'][3]);

	
	foreach ($betrieb_dates_und_einsatz as $betrieb_date_und_einsatz) if ($betrieb_date_und_einsatz['datum'] == $date_und_einsatz['datum']) break;
	$dates_und_einsatz = $betrieb_date_und_einsatz;
	
    $timeSlotInfo[4] = getTimeSlotInfo($dates_und_einsatz['datum'], 1, 'Betrieb', $dates_und_einsatz['einsaetze'][1]);
    $timeSlotInfo[5] = getTimeSlotInfo($dates_und_einsatz['datum'], 2, 'Betrieb', $dates_und_einsatz['einsaetze'][2]);

    $renderedHTML .= <<<EOT
					<tr>
						<th class=th_1 valign=top>{$weekday} {$today}</th>
						<td class={$timeSlotInfo[1]['style']} valign=top>{$timeSlotInfo[1]['names']}</td>
						<td class={$timeSlotInfo[2]['style']} valign=top>{$timeSlotInfo[2]['names']}</td>
						<td class={$timeSlotInfo[3]['style']} valign=top>{$timeSlotInfo[3]['names']}</td>
						<td class={$timeSlotInfo[4]['style']} valign=top>{$timeSlotInfo[4]['names']}</td>
						<td class={$timeSlotInfo[5]['style']} valign=top>{$timeSlotInfo[5]['names']}</td>
					</tr>
EOT;
}


$renderedHTML .= <<<EOT
	<tr><td>&nbsp;</td></tr>
EOT;

// --------------------------------------------------------------------------------
// Tabelle Abschluss

foreach ($einsatz_gebiete as $einsatz_gebiet) if ($einsatz_gebiet['key'] == 'abschluss') break;
if ( $einsatz_gebiet['activ'] == 1)
{ 
$renderedHTML .= <<<EOT

					<tr>
						<th class=th_1>Abschlusszeremonie</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_abschluss[1]}</th>
					</tr>

EOT;

foreach ($abschluss_dates_und_einsatz as $date_und_einsatz) {
    
    $timeSlotInfo[1] = getTimeSlotInfo($date_und_einsatz['datum'], 1, 'Abschluss', $date_und_einsatz['einsaetze'][1]);
    
    $curDay = new rm_DateTime($date_und_einsatz['datum']);    
    $weekday = substr($curDay->format('l'), 0, 2) . ".";
    $today = $curDay->format('d.m.');
    
    $renderedHTML .= <<<EOT
					<tr>
						<th class=th_1 valign=top>{$weekday} {$today}</th>
						<td class={$timeSlotInfo[1]['style']} valign=top colspan=2>{$timeSlotInfo[1]['names']}</td>
					</tr>

EOT;
}

$renderedHTML .= <<<EOT
	<tr><td>&nbsp;</td></tr>
EOT;
}

// --------------------------------------------------------------------------------
// Tabelle Abbau
foreach ($einsatz_gebiete as $einsatz_gebiet) if ($einsatz_gebiet['key'] == 'abbau') break;
if ( $einsatz_gebiet['activ'] == 1)
{ 
$renderedHTML .= <<<EOT
					<tr>
						<th class=th_1>Abbau</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_abbau[1]}</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_abbau[2]}</th>
					</tr>
EOT;

foreach ($abbau_dates_und_einsatz as $date_und_einsatz)
{
    $timeSlotInfo[1] = getTimeSlotInfo($date_und_einsatz['datum'], 1, 'Abbau', $date_und_einsatz['einsaetze'][1]);
    $timeSlotInfo[2] = getTimeSlotInfo($date_und_einsatz['datum'], 2, 'Abbau', $date_und_einsatz['einsaetze'][2]);

    $curDay = new rm_DateTime($date_und_einsatz['datum']);    
    $weekday = substr($curDay->format('l'), 0, 2) . ".";
    $today = $curDay->format('d.m.');
  
    $renderedHTML .= <<<EOT
					<tr>
						<th class=th_1 valign=top>{$weekday} {$today}</th>
						<td class={$timeSlotInfo[1]['style']} valign=top colspan=2>{$timeSlotInfo[1]['names']}</td>
						<td class={$timeSlotInfo[2]['style']} valign=top colspan=2>{$timeSlotInfo[2]['names']}</td>
					</tr>
EOT;
}
}
// --------------------------------------------------------------------------------
// no output before this line!

// $renderedTable = encodeToUtf8($renderedTable);

$renderedHTML .= <<<EOT
				</table>
				{$errMsg}
		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Helfer - Einsatzplan SM', 'page', $renderedHTML);

?>