<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once ('inc.php');
require_once ('helfer_inc.php');
require_once ('dblib.inc.php');
require_once ('functions.php');


function getTimeSlotInfo($date, $shift, $type, $maxEntries)
{
    global $rm_tbl_einsaetze;
    global $errMsg;
    $timeSlot = array();
    $timeSlot['name'] = array();
    $tmp_cnt = 0;
    
    $select = "SELECT vorname as vorname, name as name, wohnort as wohnort, jahrgang as jahrgang 
		FROM {$rm_tbl_einsaetze} WHERE datum = '{$date}' AND schicht = {$shift} AND gebiet = '{$type}' AND deleted is NULL ORDER BY name, vorname";
    $result = DBQuery($select);
    if ($result) {
        $row = DBFetchRow($result);
        while ($row) {
            $tmp_cnt ++;
            $timeSlot['name'][] = substr($row[0], 0, 1) . ".&nbsp;" . $row[1];
            $row = DBFetchRow($result);
        }
    }
    $rest_tmp = $maxEntries - $tmp_cnt;
    if ($tmp_cnt > 0 && $rest_tmp > 0)
        $timeSlot['names'] = implode(', ', $timeSlot['name']) . " ($rest_tmp)";
    else 
        if ($tmp_cnt == 0 && $rest_tmp > 0)
            $timeSlot['names'] = "($rest_tmp)";
        else
            $timeSlot['names'] = implode(', ', $timeSlot['name']);
    
    $timeSlot['style'] = (count($timeSlot['name']) >= $maxEntries) ? 'td_ok' : 'td_1';
    
    // ob_start();
    // print_r($timeSlot);
    // $errMsg .= '<p />' . ob_get_contents();
    // ob_end_clean();
    
    return $timeSlot;
}

$errMsg = '';
$result = DBQuery("SELECT count(*) FROM {$rm_tbl_einsaetze} where deleted is NULL");
$row = DBFetchRow($result);
$total_meldungen = $row[0];
$left = $total_einsaetze - $total_meldungen;

// --------------------------------------------------------------------------------
// Tabelle Aufbau

$renderedHTML = <<<EOT
		<div class=rm_h1>Einsatzplan f&uuml;r Helfer (total Eins&auml;tze: {$total_einsaetze} / bisher gemeldet: {$total_meldungen} / noch ben&ouml;tigt: {$left})</div>
			<div class=big_col_block>
				<table>
					<colgroup>
						<col width='80px'>
						<col width='200px'>
						<col width='200px'>
						<col width='200px'>
						<col width='200px'>
						<col width='200px'>
					</colgroup>
					<tr>
						<th class=th_1>Aufbau</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_aufbau[1]}</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_aufbau[2]}</th>
					</tr>
EOT;

foreach ($rm_aufbau_dates as $curDate) {
    $einsaetze[1] = $aufbau_einsatz_dates_soll[$curDate][1];
    $einsaetze[2] = $aufbau_einsatz_dates_soll[$curDate][2];
    
    $curDay = new rm_DateTime($curDate);
    $timeSlotInfo[1] = getTimeSlotInfo($curDate, 1, 'Aufbau', $einsaetze[1]);
    $timeSlotInfo[2] = getTimeSlotInfo($curDate, 2, 'Aufbau', $einsaetze[2]);
    
    $weekday = substr($curDay->format('l'), 0, 2) . ".";
    $today = $curDay->format('d.m.');
    
    $renderedHTML .= <<<EOT
					<tr>
						<th class=th_1 valign=top>{$weekday} {$today}</th>
						<td class={$timeSlotInfo[1]['style']} valign=top colspan=2>{$timeSlotInfo[1]['names']}</td>
						<td class={$timeSlotInfo[2]['style']} valign=top colspan=2>{$timeSlotInfo[2]['names']}</td>
					</tr>

EOT;
}

$renderedHTML .= <<<EOT
					<tr>
						<td></td>
					</tr>

EOT;

// --------------------------------------------------------------------------------
// Tabelle Er�ffnung

$renderedHTML .= <<<EOT

					<tr>
						<th class=th_1>Er�ffnung</th>
						<th class=th_1 valign=top colspan=5>{$schichtzeiten_eroeffnung[1]}</th>
					</tr>

EOT;

foreach ($rm_eroeffnung_date as $curDate) {
    $curDay = new rm_DateTime($curDate);
    $timeSlotInfo[1] = getTimeSlotInfo($curDate, 1, 'Eroeffnung', $eroeffnung_einsaetze_soll[1]);
    
    $weekday = substr($curDay->format('l'), 0, 2) . ".";
    $today = $curDay->format('d.m.');
    
    $renderedHTML .= <<<EOT
					<tr>
						<th class=th_1 valign=top>{$weekday} {$today}</th>
						<td class={$timeSlotInfo[1]['style']} valign=top colspan=5>{$timeSlotInfo[1]['names']}</td>
					</tr>

EOT;
}

$renderedHTML .= <<<EOT
					<tr>
						<td></td>
					</tr>

EOT;

// --------------------------------------------------------------------------------
// Tabelle RM/SM (Betrieb)

$renderedHTML .= <<<EOT

					<tr>
						<th class=th_1>$rm_comp_type</th>
						<th class=th_1 valign=top>{$schichtzeiten_beiz[1]} (Beiz)</th>
						<th class=th_1 valign=top>{$schichtzeiten_beiz[2]} (Beiz)</th>
						<th class=th_1 valign=top>{$schichtzeiten_beiz[3]} (Beiz)</th>
						<th class=th_1 valign=top>{$schichtzeiten_betrieb[1]} (Betrieb)</th>
						<th class=th_1 valign=top>{$schichtzeiten_betrieb[2]} (Betrieb)</th>
					</tr>

EOT;

foreach ($rm_dates as $curDate) {
    $einsaetze[1] = $beiz_einsatz_dates_soll[$curDate][1];
    $einsaetze[2] = $beiz_einsatz_dates_soll[$curDate][2];
    $einsaetze[3] = $beiz_einsatz_dates_soll[$curDate][3];
    
    $timeSlotInfo[1] = getTimeSlotInfo($curDate, 1, 'Beiz', $einsaetze[1]);
    $timeSlotInfo[2] = getTimeSlotInfo($curDate, 2, 'Beiz', $einsaetze[2]);
    $timeSlotInfo[3] = getTimeSlotInfo($curDate, 3, 'Beiz', $einsaetze[2]);
    $timeSlotInfo[4] = getTimeSlotInfo($curDate, 1, 'Betrieb', $betrieb_einsaetze_soll[1]);
    $timeSlotInfo[5] = getTimeSlotInfo($curDate, 2, 'Betrieb', $betrieb_einsaetze_soll[2]);
    
    $curDay = new rm_DateTime($curDate);
    $weekday = substr($curDay->format('l'), 0, 2) . ".";
    $today = $curDay->format('d.m.');
    
    $renderedHTML .= <<<EOT
					<tr>
						<th class=th_1 valign=top>{$weekday} {$today}</th>
						<td class={$timeSlotInfo[1]['style']} valign=top>{$timeSlotInfo[1]['names']}</td>
						<td class={$timeSlotInfo[2]['style']} valign=top>{$timeSlotInfo[2]['names']}</td>
						<td class={$timeSlotInfo[3]['style']} valign=top>{$timeSlotInfo[3]['names']}</td>
						<td class={$timeSlotInfo[4]['style']} valign=top>{$timeSlotInfo[4]['names']}</td>
						<td class={$timeSlotInfo[5]['style']} valign=top>{$timeSlotInfo[5]['names']}</td>
					</tr>

EOT;
}

$renderedHTML .= <<<EOT
					<tr>
					</tr>

EOT;

// --------------------------------------------------------------------------------
// Tabelle Abschluss

$renderedHTML .= <<<EOT

					<tr>
						<th class=th_1>Abschluss</th>
						<th class=th_1 valign=top colspan=5>{$schichtzeiten_abschluss[1]}</th>
					</tr>

EOT;

foreach ($rm_abschluss_date as $curDate) {
    $curDay = new rm_DateTime($curDate);
    $timeSlotInfo[1] = getTimeSlotInfo($curDate, 1, 'abschluss', $abschluss_einsaetze_soll[1]);
    
    $weekday = substr($curDay->format('l'), 0, 2) . ".";
    $today = $curDay->format('d.m.');
    
    $renderedHTML .= <<<EOT
					<tr>
						<th class=th_1 valign=top>{$weekday} {$today}</th>
						<td class={$timeSlotInfo[1]['style']} valign=top colspan=5>{$timeSlotInfo[1]['names']}</td>
					</tr>

EOT;
}

$renderedHTML .= <<<EOT
					<tr>
					</tr>

EOT;

// --------------------------------------------------------------------------------
// Tabelle Abbau

$renderedHTML .= <<<EOT

					<tr>
						<th class=th_1>Abbau</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_abbau[1]}</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_abbau[2]}</th>
					</tr>

EOT;

foreach ($rm_abbau_dates as $curDate) {
    $curDay = new rm_DateTime($curDate);
    $timeSlotInfo[1] = getTimeSlotInfo($curDate, 1, 'Abbau', $abbau_einsaetze_soll[1]);
    $timeSlotInfo[2] = getTimeSlotInfo($curDate, 2, 'Abbau', $abbau_einsaetze_soll[2]);
    
    $weekday = substr($curDay->format('l'), 0, 2) . ".";
    $today = $curDay->format('d.m.');
    
    $renderedHTML .= <<<EOT
			<tr>
				<th class=th_1_c valign=top>{$weekday} {$today}</th>
				<td class={$timeSlotInfo[1]['style']} valign=top colspan=2>{$timeSlotInfo[1]['names']}</td>
				<td class={$timeSlotInfo[2]['style']} valign=top colspan=2>{$timeSlotInfo[2]['names']}</td>
			</tr>

EOT;
}

// --------------------------------------------------------------------------------
// no output before this line!

$renderedHTML .= <<<EOT
				</table>
				{$errMsg}
		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Helfer - Einsatzplan RM', 'page', $renderedHTML);

?>
