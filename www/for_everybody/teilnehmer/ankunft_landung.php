<?php
require_once('inc.php');

$renderedHTML = <<<EOT
		<div class=rm_h1>Ankunft und Landung</div>
		<div class=big_col_block>

			<div class=rm_h2>Zielankunft</div>
			<div class=rm_text align="justify">
				Das Ziel ist als Kreis mit einem Radius von 3km definiert, wobei die n&ouml;dliche H&auml;lfte des Kreises aus Sicherheitsgr&uuml;nden nicht verwendet werden soll.
				Die Minimalh&ouml;he betr&auml;gt 596 Meter Meer. Anfl&uuml;ge aus n&ouml;rdlicher Richtung, werden immer &uuml;ber den Kontrollpunkt Schinznach gef&uuml;hrt.
				Bei Anfl&uuml;gen aus westlicher und s&uuml;dlichen Richtungen sind die Teilnehmemenden frei in der Routenwahl.  
				F&uuml;r die Zielankunft ist rechtzeitig auf die Platz-Frequenz zu wechseln. Direktlandungen bitte nur bei Piste 08 einplanen.
				Die Teilnehmenden melden sich 10 und 5 km vor Erreichen und beim Einflug in den Kontrollpunkte bzw. den Zielkreis: <br />
				Z.B: <b>Birrfeld 7L, 10 km bis Zielkreis</b> <br /> 
				Birrfeld quittiert: <b>Piste 08/26 Wind 270 Grad 5 kmh</b>. <br /> 
				Es darf nicht in den Motorflugbereich eingeflogen werden! <br />
				Das &uuml;berfliegen des aktiven Abgaskamins wird nicht empfohlen.
			</div>
			<br /> <img border="0" src="../../resources/graphics/organisation/Folie5.PNG" width="980">

			<div class=rm_h2>Piste 08</div>
			<div class=rm_text align="justify">
				F&uuml;r den Zeitraum der Ank&uuml;nfte wird nach M&ouml;glichkeit die Piste 08 in Betrieb sein. 
				Vor dem Eindrehen in den Endanflug auf direkt landende Flugzeuge achten.
				F&uuml;r die direkt Landenden ist die n&ouml;rdliche H&auml;lfte des Segelflugbereichs reserviert. 
				Auf anfliegende oder abfliegende Motorflugzeuge achten und gen&uuml;gend Distanz halten.
				Auf Flugzeuge im Queranflug 08 achten.</div>
			<br /> <img border="0" src="../../resources/graphics/organisation/Folie6.PNG" width="980">

			<div class=rm_h2>Piste 26</div>
			<div class=rm_text align="justify">
				Ist situationsbedingt die Piste 26 in Betrieb (ab ca. 10 km/h Wind aus westlicher Richtung), sind Direktlandungen nicht erw&uuml;nscht.
				Sie sind in Anbetracht des herrschenden Gegenverkehrs eine unn&ouml;tige Gefahr. 
				Ist eine Direktlandung trotzdem notwendig, ist dies rechtzeitig unter Angabe der Position zu melden: <br />
				Z.B.<b>Birrfeld, 7L, 1 km, tief, direkt 08</b>. Birrfeld quittiert: <b>7L, direkt 08</b>.
			</div>
			<br /> <img border="0" src="../../resources/graphics/organisation/Folie7.PNG" width="980">

			<div class=rm_h2>Anfl&uuml;ge ohne Zeitwertung und &uuml;briger Verkehr</div>
			<div class=rm_text align="justify">
				R&uuml;ckkehrer ohne Zeitwertung (R&uuml;ckschlepp und Fotovache) und der &uuml;brige Platzverkehr machen keine &Uuml;berfl&uuml;ge und keine Direktlandungen, 
				sondern fliegen eine normale Volte. Sie nehmen R&uuml;cksicht auf die anfliegenden Maschinen mit Zeitwertung. 
				Nicht unter 300m AGL (700m MSL) in den Abkreisraum einfliegen.</div>

			<div class=rm_h2>Landung</div>
			<div class=rm_text align="justify">
				Westlich des Flugplatzes verl&auml;uft unmittelbar angrenzend an das Flugplatzgel&auml;nde eine &ouml;ffentliche Strasse. 
				Diese muss in mindestens 5 Meter Grund &uuml;berflogen werden. Bei Massenank&uuml;nften ist der Platz in seiner ganzen L&auml;nge und Breite zu nutzen. 
				Nach der Landung so weit wie m&ouml;glich ans Ende des Platzes rollen. Wenn die Situation es erlaubt, kann und soll	seitlich Richtung S&uuml;den ausgerollt werden. 
				Ist dies nicht m&ouml;glich, muss geradeaus ausgerollt werden. Flugzeuge, welche	nicht an den s&uuml;dlichen Platzrand ausgerollt sind, 
				werden durch mittels Quad oder durch die Helfer zu Fuss geborgen. W&auml;hrend dieser Phase sind keine Privatfahrzeuge auf der Piste zugelassen.</div>

		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Ankunft und Landung', 'page', $renderedHTML);

?>
