<?php
// include für alle Helfer-Standardeinstellungen, die NUR für die Helferverwaltung gebraucht werden.

// zusaetzliche Tage fuer Helfer
$rm_aufbau_dates = array($rm_year . "-06-14", $rm_year . "-06-15");
$rm_abbau_dates = array($rm_year . "-06-25");
// Helfer-Standardgruppe (ÜBlicherweise der Organisator)
$rm_helfer_default_gruppe = $rm_ausrichter;

// ab diesem Datum müssen Änderungen über das OK laufen.
$rm_helfer_no_changes = $rm_year . "-06-01";

// helfer einsaetze
// ========================================================================================
// einsaetze pro schicht, beginnend jeweils mit Element 1 statt 0 ( 1=>)

$rm_gebiete = array(    'aufbau',    'beiz',    'betrieb',    'abbau');
$aufbau_einsaetze_soll = array(    1 => 8,    8);
$betrieb_einsaetze_soll = array(    1 => 5,    5);
$abbau_einsaetze_soll = array(    1 => 8,    8);

$beiz_einsatz_dates_soll = array(
    "2018-06-15" => array(1 => 0,0,8),
    "2018-06-16" => array(1 => 5,6,6),
    "2018-06-17" => array(1 => 6,6,6),
    "2018-06-18" => array(1 => 5,5,5),
    "2018-06-19" => array(1 => 5,5,5),
    "2018-06-20" => array(1 => 5,5,5),
    "2018-06-21" => array(1 => 5,5,5),
    "2018-06-22" => array(1 => 5,5,5),
    "2018-06-23" => array(1 => 6,6,8),
    "2018-06-24" => array(1 => 6,6,6)
);

$aufbau_einsatz_dates_soll = array("2018-06-14" => array(  1 => 5,  5 ), "2018-06-15" => array(  1 => 5,  5 ));
$abbau_einsatz_dates_soll = array( "2018-06-25" => array(  1 => 5,  5 ));

$eroeffnung_einsaetze_soll = array(1 =>  6);
$abschluss_einsaetze_soll = array( 1 => 10);

// summe aller einsaetze pro einsatzart
$schichten_aufbau = count($aufbau_einsaetze_soll);
$schichten_beiz = 3;
$schichten_betrieb = count($betrieb_einsaetze_soll);
$schichten_abbau = count($abbau_einsaetze_soll);
//$schichten_eroeffnung = count($eroeffnung_einsaetze_soll);
//$schichten_abschluss = count($abschluss_einsaetze_soll);

$schichtzeiten_aufbau =  array(   1 => '0800-1200', '1300-1800');
$schichtzeiten_beiz =    array(   1 => '0800-1300', '1100-1800', '1700-2300');
$schichtzeiten_betrieb = array(   1 => '0900-1500', '1300-1900');
$schichtzeiten_abbau =   array(   1 => '0800-1200', '1300-1800');


// welche Schichten ueberschneiden - Index 1 = Beiz, Index 2 = Betrieb, erstes Element = Schicht 1 (Index 0).
$schicht_konflikt_beiz_betrieb = array(
    array( 1, 0 ),
    array( 1, 1 ),
    array( 0, 1 )
);

// anzahl tage pro einsatzart
$anzahl_aufbau 		= count($rm_aufbau_dates);
$anzahl_beiz 		= count($rm_dates);
$anzahl_betrieb 	= count($rm_dates);
$anzahl_abbau 		= count($rm_abbau_dates);
//$anzahl_eroeffnung	= count($rm_eroeffnung_date);
//$anzahl_abschluss	= count($rm_abschluss_date);

// datum des starts pro einsatzart
date_default_timezone_set('Europe/Zurich');


// summe aller einsaetze ueber alle einsatzarten
$total_einsaetze = $anzahl_aufbau * array_sum($abbau_einsaetze_soll);
foreach ($beiz_einsatz_dates_soll as $daily_beiz_soll)
    $total_einsaetze += array_sum($daily_beiz_soll);
    $total_einsaetze += $anzahl_betrieb * array_sum($betrieb_einsaetze_soll);
    $total_einsaetze += $anzahl_abbau * array_sum($aufbau_einsaetze_soll);
//    $total_einsaetze += $anzahl_eroeffnung * array_sum($eroeffnung_einsaetze_soll);
//    $total_einsaetze += $anzahl_abschluss * array_sum($abschluss_einsaetze_soll);
?>
