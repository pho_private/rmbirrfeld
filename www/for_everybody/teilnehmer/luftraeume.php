<?php
require_once ('inc.php');
$renderedHTML = <<<EOT
		<div class=col_block>
		
			<div class=rm_h1>Luftraum</div>
			<div class=rm_h2>Wettbewerbsgebiet</div>
			<div class=rm_text align="justify">
				Das Wettbewerbsgebiet umfasst die Schweiz, sowie Teile im
				osten von Frankreich, im westen von &Ouml;sterreich und im S&uuml;den von
				Deutschland. Es wird vorausgesetzt, dass alle Teilnehmenden
				&uuml;ber eine aktuelle Version der folgenden Karten verf&uuml;gen:
				<ul>
					<li><a href="https://map.geo.admin.ch/?lang=de&topic=ech&bgLayer=ch.swisstopo.pixelkarte-farbe&layers=ch.swisstopo.zeitreihen,ch.bfs.gebaeude_wohnungs_register,ch.bav.haltestellen-oev,ch.swisstopo.swisstlm3d-wanderwege,ch.bazl.segelflugkarte,ch.bazl.luftfahrtkarten-icao&layers_opacity=1,1,1,0.8,1,1&layers_visibility=false,false,false,false,false,true&layers_timestamp=18641231,,,,,&E=2610835.92&N=1214963.70&zoom=2.244592224481682" target="_blank">ICAO Karte Schweiz</a></li>
					<li>ICAO Karte Deutschland, Blatt Stuttgart</li>
					<li><a href="https://map.geo.admin.ch/?lang=de&topic=ech&bgLayer=ch.swisstopo.pixelkarte-farbe&layers=ch.swisstopo.zeitreihen,ch.bfs.gebaeude_wohnungs_register,ch.bav.haltestellen-oev,ch.swisstopo.swisstlm3d-wanderwege,ch.bazl.segelflugkarte&layers_opacity=1,1,1,0.8,1&layers_visibility=false,false,false,false,true&layers_timestamp=18641231,,,,&E=2610835.92&N=1214963.70&zoom=2.244592224481682" target="_blank">Segelflugkarte Schweiz, 1:300'000</a></li>
				</ul>
				Wir bitten alle Teilnehmenden, sich ausf&uuml;hrlich mit der
				Luftraumsituation im Wettbewerbsgebiet auseinander zu setzen.
				Anl&auml;sslich des ersten Briefings werden wir auf relevante Punkte
				eingehen.
			</div>
			
			<div class=rm_h2>Horizontale Definition der Luftr&auml;ume</div>
			<div class=rm_text align="justify">Die Fl&uuml;ge werden mit der
				Auswertungssoftware SeeYou Competition von Naviter mit der aktuellen
				Luftraum Datenbank ausgewertet. Eine entsprechende Datenbank im
				Format OpenAir wird vom SFVS rechtzeitig zur Verf&uuml;gung
				gestellt. Es ist Sache der Teilnehmer, ihre prim&auml;ren
				Navigationssysteme mit korrekten Luftraumdaten zu versehen.</div>
				
			<div class=rm_h2>Vertikale Definition der Luftr&auml;ume</div>
			<div class=rm_text align="justify">
				Es gelten die H&ouml;hen wie sie auf der Segelflugkarte publiziert
				sind. Die Auswertung erfolgt ausschliesslich anhand der
				H&ouml;hensonde des Loggers.<br /> <font color="#cc0033">Achtung:
					Standarddruck H&ouml;hen beachten!</font>
			</div>
			
			
			<div class=rm_h2>Basel</div>
			<div class=rm_text align="justify">Die Chancengleichheit aller
				Teilnehmenden muss gewahrt bleiben. Aus diesem Grund gelten TMA
				Basel T1, T2 und T3 als aktiv und stehen f&uuml;r SM/RM/JSM-Fl&uuml;ge
				nicht zur Verf&uuml;gung! <br>
				Maximale Flugh&ouml;he: T1:1750m AMSL, T2: 2600m STD, T3: 1750m AMSL<br>
				Die Segelflugr&auml;ume Dittingen Ost und West stehen nicht, bzw. nur zum Zwecke der Landung zur Verf&uuml;gung.<br>
				F&uuml;r R&uuml;ckschlepps und jene Teile eines Fluges nach einer virtuellen Aussenlandung d&uuml;rfen T1,T2 und T3 benutzt werden. 
				Es wird ausdr&uuml;cklich darauf hingewiesen, dass die publizierten Verfahren einzuhalten sind.</div>
			
			<div class=rm_h2>Luftraumdaten</div>
			<div class=rm_tex align="justify"t>
				<div class=rm_h2><a href="{$rm_soaringspot_url}/downloads">Siehe Soaringspot Downloads {$rm_name_kurz}</a></div>
			</div>
		</div>
	
		<!--   Beginn zweite Spalte -->

		<div class=lst_col_block>
			<div class=rm_h1>Spezielles</div>
			<div class=rm_h2>
				Segelflugr&auml;ume TMA Z&uuml;rich und Genf 
			</div>
			<div class=rm_text align="justify">Die Benutzung aktiver
				Segelflugr&auml;ume innerhalb der TMA Z&uuml;rich und der TMA Genf
				ist gestattet. Die Wettbewerbsleitung wird versuchen, die
				Aktivierung der ben&ouml;tigten Segelflugr&auml;ume zu veranlassen.
				Es wird ausdr&uuml;cklich darauf hingewiesen, dass die publizierten
				Verfahren einzuhalten sind.</div>
				
			<div class=rm_h2>TMA Z&uuml;rich</div>
			<div class=rm_text align="justify">
                Die TMA Z&uuml;rich ist ein komplexer Luftraum. Je nach Tageszeit und Wochentag gelten andere Regeln, vor allem im S&uuml;den und Osten.
				Wir werden die westlichen Gebiete anl&auml;sslich des ersten Briefing eingehend behandeln. Sollten Aufgaben in den S&uuml;den oder Osten der TMA Z&uuml;rich f&uuml;hren, werden wir an den t&auml;glichen Briefing auf die aktuelle Situation eingehen.  
            </div>
			<div class=rm_h2>Milit&auml;rische TMA und CTR</div>
			<div class=rm_text align="justify">Die Wettbewerbsleitung wird
				Aufgaben durch milit&auml;rische TMA und CTR legen, falls diese
				f&uuml;r den fraglichen Zeitraum freigegeben wurden. F&uuml;r den
				Durchflug gelten jedoch trotzdem die publizierten Verfahren und
				Bestimmungen. Sollte die Dienststelle wider Erwarten antworten, darf
				ein Durchflug erfragt und durchgef&uuml;hrt werden. Rechtzeitige
				Kontaktaufnahme wird empfohlen.</div>				
								
			<div class=rm_h2>Grenchen</div>
			<div class=rm_text>Nur zum Zwecke der Landung. Man beachte die
				Obergrenze von 1350m AMSL!</div>
				
				
			<div class=rm_h2>Bern</div>
			<div class=rm_text align="justify">Nur zum Zwecke der Landung. Man
				beachte die Untergrenzen von Bern TMA1 (1050m AMSL) und TMA2 (1700m
				AMSL).</div>

			<div class=rm_h2>
				CTR Les Eplatures
			</div>
			<div class=rm_text align="justify">
				Die CTR Les Eplatures darf beflogen werden. Die Teilnehmenden sind
				zur individuellen Kontaktaufnahme verpflichtet.<br /> Wir empfehlen,
				keine Durchfl&uuml;ge einzuplanen. Bei Verz&ouml;gerungen kann kein
				Nachteil geltend gemacht werden. Man beachte die Obergrenze von
				2000m AMSL!<br />
			</div>
			
			<div class=rm_h2>Mittagspause der Luftwaffe / Sektor Alpen</div>
			<div class=rm_text align="justify">
				An Tagen mit MIL ON steht der Luftraum zwischen FL130 und FL150 in den Alpen w&auml;hrend der Mittagspause der Luftwaffe (12:05 - 13:15) nicht zur Verf&uuml;gung.
			</div>
			<br>
			<div class=rm_h2>
				<a href="https://www.segelflug.ch/wp-content/uploads/2020/10/23-Luftraum-d-2020.pdf" target="_blank">Siehe auch SM-Reglement, Anhang 3</a>
			</div>
		</div>

EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Luftr&auml;ume', 'page', $renderedHTML, NULL, true);

?>
