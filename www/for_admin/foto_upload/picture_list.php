<?php
require_once ('inc.php');
require_once ('functions.php');


$folder = getRequestOrInit('folder', $rm_root . "/resources/photos/teilnehmer");
$deletefile = getRequestOrInit('deletefile', '');
$action = getRequestOrInit('action', '');
$submitted = getRequestOrInit('submitted', 'no');

$link = str_replace($rm_root, '../..', $folder);

$bodyStart = <<<EOT
<body>
    <script type="text/javascript">
        function DoSubmit() {
            document.anmeldung.submit();
        }
    </script>
EOT;

$renderedHTML = '';

if ($submitted != 'no') {
    $myfile = $_FILES['userfile'];
    
    // upload of the photo specified in field "userfile" using german messages
    $handle = new rm_upload($myfile, 'de_DE');
    if ($handle->file_is_image) {
        if ($handle->uploaded) {
            $handle->generate_image_set($folder);
        }
        // now that we've done all conversions, remove the uploaded source file.
        $handle->clean();
    } else {
        $renderedHTML .= <<<EOT
		<div class="td_e">
    		<b>Unbekanntes Bildformat!</b><br />Bitte w&auml;hle ein JPEG, GIF oder PNG Bild.<br /></p>
		</div>
EOT;
    }
}
$renderedHTML .= <<<EOT
    <div class="rm_h1">Verwaltung der Pilotenbilder</div>
    <form enctype="multipart/form-data" action="picture_list.php" method="POST">
    	<input type="file" name="userfile" size="80" onchange="DoSubmit()">
    	<input type="submit" value="Datei &uuml;bertragen">
    	<input type="hidden" name="folder" value="{$folder}">
    	<input type="hidden" name="submitted" value="true">
    </form>
EOT;

if ($action == 'delete') {
    $lookfor = "/.jpg/";
    $files = filelist($folder, $lookfor);
    foreach ($files as $file) {
        $deletefile = str_replace('_small', '', $deletefile);
        $name = substr($deletefile, 0, strrpos($deletefile, "."));
        if (preg_match("/" . $name . "_mini.jpg/", $file)) {
            if (! unlink($folder . "/" . $file))
                $renderedHTML .= <<<EOT
    <div class=rm_h3>Datei $file nicht gel&ouml;scht</div>
EOT;
        }
        if (preg_match("/" . $name . "_small.jpg/", $file)) {
            if (! unlink($folder . "/" . $file))
                $renderedHTML .= <<<EOT
    <div class=rm_h3>Datei $file nicht gel&ouml;scht</div>
EOT;
        }
        if (preg_match("/" . $name . "_midi.jpg/", $file)) {
            if (! unlink($folder . "/" . $file))
                $renderedHTML .= <<<EOT
    <div class=rm_h3>Datei $file nicht gel&ouml;scht</div>
EOT;
        }
        if (preg_match("/" . $name . "_middle.jpg/", $file)) {
            if (! unlink($folder . "/" . $file))
                $renderedHTML .= <<<EOT
    <div class=rm_h3>Datei $file nicht gel&ouml;scht</div>
EOT;
        }
        if (preg_match("/" . $name . ".jpg/", $file)) {
            if (! unlink($folder . "/" . $file))
                $renderedHTML .= <<<EOT
    <div class=rm_h3>Datei $file nicht gel&ouml;scht</div>
EOT;
        }
    }
}

$lookfor = "/_small.jpg/";

$files = filelist($folder, $lookfor);
$count = count($files);
sort($files);

$renderedHTML .= <<<EOT
        <table>
            <tr>
EOT;
$count = 0;
$col = 5;
$teiln = new Teilnehmer();
foreach ($files as $file) {
    $count ++;
    $filename = str_replace('_small.jpg', '', $file);
    $activ_foto = $teiln->is_foto_in_use($filename);
    
    // $userDaten['foto'];
    if ($count % $col == 1) {
        $renderedHTML .= <<<EOT
            </tr>
            <tr>
EOT;
    }
    $renderedHTML .= <<<EOT
                <td class=\"td_1_c\">
                    <b>$filename</b><br />
                    <img src=$link/$file><br />
EOT;
    if ($activ_foto) {
        $renderedHTML .= <<<EOT
                    <b>Foto verwendet</b>
EOT;
    } else {
        $renderedHTML .= <<<EOT
                    <a href="picture_list.php?action=delete&folder=$folder&deletefile=$file" onclick="return confirm('$file wirklich l&ouml;schen?')">l&ouml;schen</a>
EOT;
    }
    $renderedHTML .= <<<EOT
                </td>
EOT;
}
$renderedHTML .= <<<EOT
            </tr>
        </table>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('OK - Fotoverwaltung', 'page', $renderedHTML);

?>
