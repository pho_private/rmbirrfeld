<?php
require_once ('inc.php');
require_once ('functions.php');


// --- prepare for evaluation ---
// set today's date as the tentative competition day.

$compDateStr = getRequestOrInit('date', '');
$competitionDate = '';

$pageStart = <<<EOT
    <base target="hnav">
EOT;

// if we're still in morning (before 11:00), set yesterday as tentative competition day.
if ($compDateStr == '') {
    if (date('G') <= 11) {
        $compDateStr = date("%Y-%m-%d", strtotime("-1 days"));
    } else {
        $compDateStr = date("%Y-%m-%d", strtotime("today"));
    }
}

// if our date is indeed a competition day, return it.
if (in_array($compDateStr, $rm_dates)) {
    $competitionDate = $compDateStr;
}  // if not, return the last competition date that is less than or equal to the tentative date.
else {
    $competitionDate = $rm_dates[0];
    $compDate = strtotime($compDateStr);
    foreach ($rm_dates as $compDayStr) { // neither elegant nor minimal - but it works.
        if (strtotime($compDayStr) <= $compDate) {
            $competitionDate = $compDayStr;
        }
    }
}


// --- prepare for news ---

$newsItem = new NewsItems();
$lastNewsDate = $newsItem->getLastNewsEntryDate();
if ($lastNewsDate) {
    if ($lastNewsDate < $rm_dates[0]) {
        $lastNewsDate = $rm_new_year;
    }
    $newspage = "news.php?datum=" . $lastNewsDate;
} else {
    $newspage = "news.php?datum=" . $rm_new_year;
}
$newsadmpath = 'news_admin';
$fotoadmpath = 'foto_upload';

$renderedHTML = <<<EOT
    <div class="rm_logo"><a class="non" href="/index.php" target="_top"><img border="0" src="{$rm_logo_ok_url}" width="120"></a></div>

	<div class="sidebar">Kommunikation</div>
	<div class="sidebarlink"><a href="frame_ok.php?hnav=news_admin/news_admin_hnav.php&page=news_admin/{$newspage}">News</a></div>
	<div class="sidebarlink"><a href="frame_ok.php?hnav=../for_everybody/empty_hnav.php&page=teilnehmer_email.php">Mail --&gt; Piloten</a></div>

	<div class="sidebar">WhatsApp beitreten</div>
	<div class="sidebarlink"><a href="{$rm_whatsapp_url}" target="_blank">OK --> TN</a></div>
	<div class="sidebarlink"><a href="{$rm_whatsapp_teilnehmer_url}" target="_blank">Teilnehmer</a></div>
    <div class="sidebarlink"><a href="{$rm_whatsapp_ok_url}" target="_blank">OK intern</a></div>

	<div class="sidebar">Verwaltung</div>
	<div class="sidebarlink"><a	href="frame_ok.php?hnav=checks_hnav.php&page=foto_upload/picture_list.php">Foto Check</a></div>
	<div class="sidebarlink"><a href="frame_ok.php?hnav=checks_hnav.php&page=teilnehmer_check.php">Admin Check</a></div>
	<div class="sidebarlink"><a href="frame_ok.php?hnav=checks_hnav.php&page=auswertung/teilnehmerdaten.php">ID Check</a></div>
	<div class="sidebarlink"><a href="frame_ok.php?hnav=checks_hnav.php&page=teilnehmer_pdf.php">Teilnehmer-PDF</a></div>
	<div class="sidebarlink"><a href="frame_ok.php?hnav=checks_hnav.php&page=teilnehmer_csv.php">Teilnehmer-CSV</a></div>
    <div class="sidebarlink"><a href="frame_ok.php?hnav=checks_hnav.php&page=nichtteilnehmerdaten.php">Ehemalige</a></div>
    <div class="sidebarlink"><a href="frame_ok.php?hnav=checks_hnav.php&page=tracking_reset.php">Reset Tracking</a></div>


	<div class="sidebar">Auswertung</div>
	<div class="sidebarlink"><a href="frame_ok.php?hnav=auswertung/igc_download_hnav.php&page=auswertung/igc_download.php?date={$competitionDate}">IGC Download</a></div>
	<div class="sidebarlink"><a href="frame_ok.php?hnav=checks_hnav.php&page=auswertung/CUC-csv.php">CUC Pilotenliste</a></div>

EOT;

if ($rm_use_helfer) {
    $renderedHTML .= <<<EOT
	<div class="sidebarlink"><a href="helfer_liste.php">Helferliste</a></div>
EOT;
}

$renderedHTML .= <<<EOT
    <div class="sidebar">Web-Master</div>
	<div class="sidebarlink"><a href="adminaccess.php" target=_blank>Admin</a></div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('Admin vNav', 'vnav', $renderedHTML, $pageStart);

?>
