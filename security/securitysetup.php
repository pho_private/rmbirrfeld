<?php
require_once ('inc.php');

// mittels http://www.htaccesstools.com/htpasswd-generator/ ein Login und Passwort erstellen
// der Eintrag wird aus Login, Verschlüsselungsalgorithmus und Passworthash erzeugt, getrennt mit Doppelpunkt
// Bespiel Eintrag: pho:$apr1$e6nwGGK4$t5gSETNyzDmIMI9euPgfq0
//                  UID  SIG   PWH
// UID = User ID = Login = pho
// SIG = Signatur = Kennzeichen für verwendeten Verschlüsselungsalgorithmus = $apr1$
// PWH = Passwort Hash = e6nwGGK4$t5gSETNyzDmIMI9euPgfq0
//
// diese Zeile kann dann im .htpasswd eingetragen werden.
// Die Sicherheit wird aktiviert durch ausführen von http://www.rmbirrfeld.ch/security/securitysetup.php
//
function getFilenameAndPermsString($filename)
{
    return "{$filename} (" . substr(sprintf('%o', fileperms($filename)), - 4) . ")";
}

function message($msg, $success = true)
{
    if ($success) {
        echo "<br /><span style=\"background-color: #00ffdd; \"><b>{$msg}</b>";
    } else {
        echo "<br /><span style=\"background-color: #ffbb00; \"><b>{$msg}</b>";
    }
}

// construct the access management entry for one directory tree $relpath in web site $websiteroot with contents $contents
function createHtaccess($relpath, $websiteroot, $contents)
{
    echo "<p /><hr><p /><b>createHtAccess( " . $relpath . ", " . $websiteroot . ")</b><br />";
    $dirname = $websiteroot . "/" . $relpath;
    $filename = $dirname . '/.htaccess';
    $file = false;

    if (! is_dir($dirname)) {
        message('Verzeichnis ' . $dirname . ' existiert nicht!', false);
        return false;
    }

    if (! is_writable($dirname)) {
        message("Verzeichnis nicht beschreibbar: " . getFilenameAndPermsString($dirname) . ")", false);
        return false;
    }

    if ((is_file($filename) && is_writable($filename)) or (! is_file($filename))) {
        $file = fopen($filename, 'w+');
        if (! $file) {
            message("Konnte Datei nicht &ouml;ffnen: </b> " . getFilenameAndPermsString($filename) . "<b>)", false);
            return false;
        }
    }

    $flength = fwrite($file, str_replace('\n', chr(10), $contents));
    if (! $flength) {
        message("Datei nicht beschreibbar: </b>" . getFilenameAndPermsString($filename) . "<b>)", false);
        fclose($file);
        return false;
    } else {
        echo "<br />habe " . $flength . " bytes in Datei " . $filename . " geschrieben:<br />";
        echo (str_replace("\n", "<br />", htmlspecialchars($contents)) . "</p>");
    }
    if (fclose($file)) {
        message("Habe erstellt: " . $filename);
        return true;
    } else {
        message("Datei konnte nicht geschlossen werden: </b> " . getFilenameAndPermsString($filename) . "<b>)", false);
        return false;
    }
}

$pos = strrpos($_SERVER['SCRIPT_FILENAME'], '/');
$mypath = substr($_SERVER['SCRIPT_FILENAME'], 0, $pos + 1); // include trailing "/"

header('Content-Type: text/html; charset=UTF-8');
echo <<<EOT
<html>
<head>
	<style type="text/css">
	<!--
	@import url(../www/css/rm.css);
	-->
	</style>
</head>
<body>
<h1>Security Verzeichnis Pfad: {$mypath}</h1>

EOT;

// three files, same contents - prevent directory listing of those directories.
$authfilecontents = <<<EOT
Options -Indexes

EOT;

createHtaccess('../security', $rm_root, $authfilecontents);
createHtaccess('../include', $rm_root, $authfilecontents);
createHtaccess('..', $rm_root, $authfilecontents);

$authfilecontents = <<<EOT
Authname "RM Birrfeld Admin"
AuthUserFile {$mypath}.htpasswd
AuthGroupFile {$mypath}.htgroup
AuthType Basic
require valid-user
EOT;
createHtaccess('../ok', $rm_root, $authfilecontents);
createHtaccess('for_admin', $rm_root, $authfilecontents);
createHtaccess('for_admin/foto_upload', $rm_root, $authfilecontents);
createHtaccess('for_admin/news_admin', $rm_root, $authfilecontents);
createHtaccess('for_admin/auswertung', $rm_root, $authfilecontents);

echo <<<EOT
</body>
</html>
EOT;

?>
