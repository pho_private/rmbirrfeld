<?php
header('Content-Type: text/html; charset=UTF-8');
require_once ('inc.php');
require_once ('functions.php');


$renderedHTML = <<<EOT
			<div class="hnav_item"><h2>Teilnehmer</h2></div>
			<div class="hnav_item"><a href=teilnehmerlist.php>alle&nbsp;Klassen</a></div>

EOT;

foreach ( $class_key as $class ) {
	if ($class != 0) {
		$renderedHTML .= <<<EOT
			<div class="hnav_item"><a href="teilnehmerlist_class.php?class=$class">{$class_name[$class]}</a></div>

EOT;
	}
}
/*
$renderedHTML .= <<<EOT
		</div> <!-- hnav_container -->
		<div class="hnav_container">

EOT;
$teilnehmerList = new Teilnehmer ();

foreach ( $class_key as $class ) {
	if ($class == 0)
		continue;

	$teilnehmerList->load_class_by_wk ( $class );
	foreach ( $teilnehmerList->list as $teilnehmer ) {
		$wk = strtoupper ( $teilnehmer ['wk'] );
		$immatrikulation = urlencode ( $teilnehmer ['immatrikulation'] );

		$renderedHTML .= <<<EOT
			<div class="hnav_item"><a href="teilnehmer_detail.php?immatrikulation=$immatrikulation">&nbsp;{$wk}&nbsp;</a></div>

EOT;
	}
}
*/

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('Teilnehmer', 'hnav', $renderedHTML);

?>
