<?php
require_once ('inc.php');

require_once ('functions.php');

// error_reporting(E_ALL);

/*
 * Hochladen der IGC Dateien fuer RM/SM Birrfeld.
 * Der Pilot laedt die Datei hoch - einfach nur Datei auf lokalem System waehlen, dann wird sie hochgeladen.
 * Hochgeladen wird in 1 Verzeichnis pro Jahr: .... /resources/igc/<rm-jahr>
 * Es wird gecheckt, ob
 * - die Datei an einem Wettbewerbstag erstellt wurde
 * - die Datei wahrscheinlich aus dem Wettbewerbsjahr stammt (10 Jahre wrap around)
 * Es werden keine hochgeladenen Dateien angezeigt.
 */

$folder = $rm_igc;
if (!is_dir($folder)) {
	if (!mkdir($folder, 0755, true)) {
		die('Failed to create IGC folder');
	}
}

$renderedHTML = '';
$startBody = false;

// all set - user pressed "Datei uebertragen".
if (isset($_REQUEST['submitted'])) {

    $myfile = $_FILES['userfile'];
    // print_r($myfile);

    // upload of the file specified in field "userfile" using german messages
    $handle = new rm_upload($myfile, 'de_DE');
    $handle->mime_check = false;
    $handle->mime_file = false;
    $handle->mime_fileinfo = false;
    $handle->mime_magic = false;
    if ($handle->uploaded) {
        $handle->check_and_save_igc_file($folder);
    
        if (!$handle->error) {
            $startBody = <<<EOT
<body>
    <div id="rm_status" class="td_o">
        Alles OK - Datei {$myfile['name']} ({$myfile['size']} bytes) hochgeladen
    </div>
EOT;
        }
    }
    if ($handle->error) {
        $startBody = <<<EOT
<body>
    <div id="rm_status" class="td_e">Es trat folgender Fehler auf:<br />{$handle->error}</div>
EOT;
    }
    // remove the uploaded source file.
    $handle->clean();
}

$files = filelist($folder, "/.igc/i");
sort($files);
$teilnehmerListe = new Teilnehmer();
$newest_file = "2000-01-01 00:00:00";
$renderedHTML = '';

if (count($files) != 0) {
    foreach ($files as $file) {
        $timestamp = date("Y-m-d H:i:s", filemtime($folder . "/" . $file));
        if ($newest_file < $timestamp) {
            $newest_file = $timestamp;
        }
    }
}

$renderedHTML = <<<EOT
		<div class=rm_h1>IGC Dateien hochladen f&uuml;r Fl&uuml;ge an der {$rm_name_lang}</div>
		<div class=big_col_block>
			<div class=rm_h3>Datei ausw&auml;hlen</div>
			<div>
				<form enctype="multipart/form-data" action="igc_upload.php" method="POST">
					<table cellspacing="5">
						<tr>
							<td><input type="file" name="userfile"></td>
							<td><input type="submit" value="Datei &uuml;bertragen"></td>
						</tr>
					</table>
					<input type="hidden" name="submitted" value="true">
				</form>
			</div>
		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('IGC Uploads', 'page', $renderedHTML, $startBody);

$startBody = false;

?>
