function closeWindowAfter(seconds) {
	window.setTimeout("window.close()", seconds);
}

function redirect(url, milliseconds) {
	window.setTimeout("window.location.href = url", milliseconds);
}

var popup_window = null;
function popup(status, url, w, h) {
	if (status != 0) {
		if (popup != null) {
			popup.focus();
		} else {
			var x = event.screenX;
			var y = event.screenY;
			var mw = screen.width / 2;
			var mh = screen.height / 2;
			if (x < mw)
				var xpos = mw;
			else
				var xpos = 10;

			var ypos = mh - h / 2;

			attr = "left=" + xpos + ", top=" + ypos + ", width=" + w
					+ ", height=" + h;
			var popup = open(url + "&width=" + w + "&height=" + h, "Popup",
					attr);
			popup.window.name = url;
			popup_window = popup;
		}
	} else {
		if (popup_window != null)
			popup_window.close();
	}
}
