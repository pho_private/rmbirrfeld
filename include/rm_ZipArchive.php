<?php

/**
 * partial wrapper around ZipArchive object to provide up-front checking
 * @author pho
 *
 * When you generate a new ZipArchive from files, the object just collects what you want to do.
 * It does not at this stage check if your input files do exist etc.
 * This leads to "strange" error messages once you try to close the ZipArchive: A "Read Error"
 * materializes from nowhere.
 *
 * This wrapper ensures that the directory where we want to generate the ZIP is writeable, and
 * that the files we add do exist.
 * It returns false and sets the status string in case of an error. It transparently calls the
 * methods of the wrapped class in case that things work out correctly.
 */
class rm_ZipArchive extends ZipArchive
{

    private $statusString = '';

    private $zipFilename = '';

    private $zipBasename = '';

    public function open($filename, $flags = NULL)
    {
        // check if directory is writeable before calling the real zip open.
        $dirname = dirname($filename);
        if (is_dir($dirname)) {
            if (is_writable($dirname)) {
                $this->statusString = '';
                $this->zipFilename = $filename;
                $this->zipBasename = basename($filename);
                return ZipArchive::open($filename, $flags);
            } else {
                $statusString = "Cannot create {$this->zipBasename}: Directory {$dirname} not writeable!";
            }
        } else {
            $statusString = "Cannot create {$this->zipBasename}: Not a directory: {$dirname}";
        }
        return false;
    }

    public function addFile($filename, $localname = '', $start = 0, $length = 0)
    {
        // check if $fileName exists before calling the real ZipArchive::addFile method.
        $dirname = dirname($filename);
        if (is_dir($dirname)) {
            if (is_file($filename)) {
                $this->statusString = '';
                return ZipArchive::addFile($filename, $localname, $start, $length);
            } else {
                $statusString = "Zip archive {$this->zipBasename}: File {$filename} does not exist";
            }
        } else {
            $statusString = "Zip archive {$this->zipBasename}: Cannot add {$filename} - Not a directory - {$dirname}";
        }
        return false;
    }

    public function getStatusString()
    {
        if ($this->statusString != '') {
            $retval = $this->statusString;
            $this->statusString = '';
            return $retval;
        } else {
            return ZipArchive::getStatusString();
        }
    }
}

