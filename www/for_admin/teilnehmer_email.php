<?php
require_once ('inc.php');
require_once ('functions.php');

/**
 * create list of mail recipients as "F.
 * name, " entries, i.e. first char of first name and last name followed by comma space.
 *
 * @param string $participant
 *        	(on, off)
 * @param string $nonparticipant
 *        	(on, off)
 */
function addresseeString($participant, $nonparticipant) {
	$teilnehmer_list = new Teilnehmer ();
	$text = '';
	foreach ( $teilnehmer_list->list as $teilnehmer ) {
		if ($teilnehmer ['email'] == '')
			continue;

		if (($participant == 'on' && $teilnehmer ['rmactiv'] == 'ja') || ($nonparticipant == 'on' && $teilnehmer ['rmactiv'] == 'nein')) {
			$text .= substr ( $teilnehmer ['vorname'], 0, 1 ) . ".&nbsp;{$teilnehmer['name']}, ";
		}
	}
	
	return ($text == '') ? '' : encodeToUtf8 ( "{$text}" );
}

/**
 *
 * @param object $mail
 *        	- PHPMailer object
 * @param string $recipientsString
 * @return boolean
 */
function getAllRecipients($mail) {
	return implode( ', ', $mail->getAllRecipientAddresses());
}

function displayErrorMessage($emsg) {
	return ($emsg == "") ? "" : "<div class=error>$emsg</div>";
}

function emailTable() {
	$emails = new emails ();
	$table = '';

	if (count ( $emails->list ) > 0) {
		$table .= <<<EOT
			<div class=rm_h2>Gespeicherte E-Mails</div>
				<table>
EOT;
		foreach ( $emails->list as $email ) {
			$areaname = "emailtext" . $email ['id'];
			$text = encodeToUtf8 ( $email ['welcome'] . "\n\n{$email['message']}\n\n{$email['greeting']}\n" );
			$table .= <<<EOT
						<tr>
							<td class=td_1 width=30%>
								<a href=teilnehmer_email.php?action=loeschen&id={$email['id']} onclick=\"return confirm('Wirklich l&ouml;schen?')\">l&ouml;schen</a>
							</td>
			  				<td class=td_1 width=70% rowspan=4>
			  					<textarea name=\"{$areaname}\" cols="80" rows="10">$text</textarea>
			  				</td>
			  			</tr>
						<tr>
							<td class=td_1><a href=teilnehmer_email.php?action=kopieren&id={$email['id']} onclick=\"return confirm('Wirklich &uuml;bernehmen?')\">&uuml;bernehmen</a></td>
						</tr>
						<tr>
							<td class=td_1>{$email['timestamp']}</td>
						</tr>
						<tr>
							<td class=td_1>{$email['subject']}</td>
						</tr>
EOT;
		}
		$table .= <<<EOT
				</table>
EOT;
	}
	return $table;
}

// ------------------------------------------------------
// start of main code
// ------------------------------------------------------

$action = getRequestOrInit ( 'action', '' );
$id = getRequestOrInit ( 'id', '' );
$welcome = getRequestOrInit ( 'welcome', 'Liebe Teilnehmende' );
$subject = getRequestOrInit ( 'subject', $rm_name_lang );
$greeting = getRequestOrInit ( 'greeting', "Liebe Gr&uuml;sse, das $rm_comp_type Birrfeld {$rm_year} Team" );
$messageHTML = getRequestOrInit ( 'message', '' );
$mode = getRequestOrInit ( 'mode', '' );
$class = getRequestOrInit ( 'class', '' );
$participant = getRequestOrInit ( 'participant', 'off' );
$nonparticipant = getRequestOrInit ( 'nonparticipant', 'off' );
$ok = getRequestOrInit ( 'ok', 'off' );

$error_message = '';
$tmp_message = "{$welcome}\n\n{$messageHTML}\n";

$emails = new emails ();

$tmp_list_ok = '';

switch ($action) {

	case 'loeschen' :
		$emails->delete ( $id );
		break;

	case 'speichern' :
		$emails->insert ( $subject, $welcome, $messageHTML, $greeting );
		break;

	case 'kopieren' :
		$email = $emails->item_by_id ( $id );
		$subject = $email ['subject'];
		$welcome = $email ['welcome'];
		$messageHTML = $email ['message'];
		$greeting = $email ['greeting'];
		break;

	case 'senden' :
		$newEmail = new PHPMailer ();

		$newEmail->setFrom ( $rm_mail_noreply, $rm_name_lang );

		$newEmail->Subject = $subject;
		$newEmail->Body = $tmp_message;

		// add OK
		if ($ok == 'on') {
			// display OK on output screen
			$tmp_list_ok = $mailto_list_ok;
			
			// add OK list to output
			$recipients = explode ( ', ', $mailto_list_ok );
			foreach ( $recipients as $recipient ) {
				$newEmail->AddAddress ( $recipient );
			}
		}

		// add participants
		$teilnehmer_list = new Teilnehmer ();
		if ($participant == 'on') {
			foreach ( $teilnehmer_list->list as $teilnehmer ) {
				if ($teilnehmer ['email'] == '')
				continue;
				if ($teilnehmer ['rmactiv'] == 'ja') {
					$newEmail->AddAddress ( $teilnehmer ['email'] );
				}			
			}	
		}
		// ... and nonparticipants
		if ($nonparticipant == 'on') {
			foreach ( $teilnehmer_list->list as $teilnehmer ) {
				if ($teilnehmer ['email'] == '')
					continue;
				if ($teilnehmer ['rmactiv'] == 'nein') {
					$newEmail->AddAddress ( $teilnehmer ['email'] );
				}
			}
		}
		
		// send if possible
		if (! $newEmail->Send ()) {
			$error_message = "E-Mails konnten nicht verschickt werden! " . $newEmail->ErrorInfo .  implode( ', ', $newEmail->getAllRecipientAddresses());
		}

		// save mail in database.
		$emails->insert ( $subject, $welcome, $messageHTML, $greeting );
		break;

	case 'testen' :
		$newEmail = new PHPMailer ();

		$newEmail->setFrom ( $rm_mail_noreply, $rm_name_lang );

		$newEmail->Subject = $subject;
		$newEmail->Body = $tmp_message;

		// add the test recipient list
		$recipients = explode ( ', ', $mailto_list_test );
		foreach ( $recipients as $recipient ) {
			$newEmail->AddAddress ( $recipient );
		}

		if (! $newEmail->Send ()) {
			$error_message = "E-Mails konnten nicht verschickt werden! " . $newEmail->ErrorInfo .  implode( ', ', $newEmail->getAllRecipientAddresses());
		}

		// save mail in database.
		$emails->insert ( $subject, $welcome, $messageHTML, $greeting );
		break;
}

$recipients = addresseeString ( $participant, $nonparticipant );
$emsg = displayErrorMessage ( $error_message );
$mailTable = emailTable ();
$okChecked = ($ok == 'on') ? "checked" : "";
$participantChecked = ($participant == 'on') ? "checked" : "";
$nonparticipantChecked = ($nonparticipant == 'on') ? "checked" : "";

if ($ok == 'on') {
	$tmp_list_ok = $mailto_list_ok;
}

$bodyStart = <<<EOT
<body>
    <script LANGUAGE="javascript">
    	function doSubmit() { document.email.submit(); }
    </script>
    <div class=main_block>
EOT;

$renderedHTML = <<<EOT
<div class=rm_h1>E-Mails an die Teilnehmer</div><br />
	<form name="email">
		<table>
			<tr>
				<td class=td_1>
					Mail an OK<input type="checkbox" name="ok" {$okChecked} onchange="doSubmit()">
				</td>
				<td class=td_1>
					Mail an angemeldete Teilnehmer<input type="checkbox" name="participant" {$participantChecked} onchange="doSubmit()">
				</td>
				<td class=td_1>
					Mail an unentschlossene<input type="checkbox" name="nonparticipant" {$nonparticipantChecked} onchange="doSubmit()"></td>
				</td>
			<tr>
		</table>

		<div class=rm_h3>E-Mail geht an folgende Teilnehmer:</div>
		<table>
			<tr>
				<td>
					{$recipients}
				</td>
			</tr>
		</table>

		<div class=rm_h3>E-Mail geht an folgende OK-Teilnehmer:</div>
		<table>
			<tr>
				<td>
					{$tmp_list_ok}
				</td>
			</tr>
		</table>

		<div class=rm_h3>Test geht an:</div>
		<table>
			<tr>
				<td>
					{$mailto_list_test}
				</td>
			</tr>
		</table>

		<table width=1000>
			<tr>
				<td class=td_1>Subject</td>
				<td><input 		name="subject" 	size="50" value="{$subject}"></input></td>
			</tr>
			<tr>
				<td class=td_1>Begr&uuml;ssung</td>
				<td><input name="welcome" 	size="50" value="{$welcome}"></input></td>
			</tr>
			<tr>
				<td class=td_1>Text</td>
				<td><textarea 		name="message" 	cols="80" rows="10">{$messageHTML}</textarea></td>
			</tr>
			<tr>
				<td class=td_1>Gruss</td>
				<td><input 			name="greeting" 	size="50" value="{$greeting}"></input></td>
			</tr>
		</table>

		<input type="submit" name="action" value="senden" 		onclick="return confirm('Wirklich senden?')"></input>
		<input type="submit" name="action" value="testen" 		onclick="return confirm('Wirklich testen?')"></input>
		<input type="submit" name="action" value="speichern" 	onclick="return confirm('Wirklich speichern?')"></input>
	</form>

	{$emsg}
	{$mailTable}
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('KL - Mail an Teilnehmer', 'page', $renderedHTML, $bodyStart);

?>
