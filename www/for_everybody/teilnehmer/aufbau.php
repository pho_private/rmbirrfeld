<?php
require_once('inc.php');

$renderedHTML = <<<EOT
        <div class=rm_h1>Aufbau</div>
        <div class=big_col_block>

		<div class=rm_h2>Montagepl&auml;tze</div>
		<div class=rm_text>
			Die Montagepl&auml;tze befinden sich am s&uuml;dlichen Platzrand. Die Flugzeuge verf&uuml;gen, 
			nach Klassen geordnet, &uuml;ber einen Standplatz f&uuml;r den Anh&auml;nger.
			In diesem Bereich k&ouml;nnen die Flugzeuge auch montiert &uuml;bernachten. 
			Bitte gewährt den Zugang zu den Anh&auml;ngern f&uuml;r die anderen Teilnehmenden.
		</div>
		<p/>

		<table  align="center" border="0" cellspacing="0" 	cellpadding="0">
			<tr valign="middle" align="center">
			<td><img src="../../resources/graphics/organisation/sm_2024_montageaufstellung.jpg" height="240px;" border="0" /></td>
			<td><img src="../../resources/graphics/organisation/sm_2024_montageaufstellung_detail.jpg" height="240px;" border="0" /></td>
			</tr>
		</table>

    
    
    	<div class=rm_h2>Montieren / Aufstellen / Komprimieren</div>
		<div class=rm_text>
			In der Regel wird montiert. Ausnahmen davon teilen wir per WhatsApp und im Internet mit.
			Aufgestellt wird, sobald die Pistenrichtung klar ist.
			Dreissig Minuten vor dem Start wird komprimiert.
		</div>
		<p/>

		<table  align="center" border="0" cellspacing="0" 	cellpadding="0">
			<tr valign="middle" align="center">
			<td><img src="../../resources/graphics/organisation/sm_2024_startaufstellung_08.jpg" height="240px;" border="0" /></td>
			<td><img src="../../resources/graphics/organisation/sm_2024_startaufstellung_26.jpg" height="240px;" border="0" /></td>
			</tr>
		</table>
		<p/>

    	<div class=rm_h2>Wasserballast</div>
    		<div class=rm_text>
    			Wasserballast ist bis zum zugelassenen Gesamtgewicht erlaubt. 
    			Ausnahmen werden rechtzeitig kommuniziert.
    		</div>
    
    
    
    	<div class=rm_h2>Durchsagen der Konkurrenzleitung</div>
    		<div class=rm_text>
    			Durchsagen der Konkurrenzleitung erfolgen auf der Omega-Frequenz. 
    			In bestimmten F&auml;llen werden wir Mitteilungen auch per WhatsApp versenden (z.B. Startverschiebungen).
    		</div>
    
    	<div class=rm_h2>Privatfahrzeuge auf der Piste</div>
    		<div class=rm_text>
    			Privatfahrzeuge sind auf der Piste ausschliesslich f&uuml;r den Transport der Flugzeuge zum Startplatz zugelassen. 
    			W&auml;hrend des Flugbetriebs sind Fahrten auch am Pistenrand nur in Absprache mit dem Flugdienstleiter erlaubt.
    		</div>
    
    	<div class=rm_h2>Fahrzeuge im Flugplatzbereich</div>
    		<div class=rm_text>
    			Bitte beachtet, dass im unmittelbaren Bereich s&uuml;dlich Hangar 1,2,3 zu keiner Zeit 
				Fahrzeuge abgestellt werden d&uuml;rfen.
    			Wir bitten Euch, Eure Fahrzeuge auf dem Parkplatz am Pistenkopf 08 stehen zu lassen 
				und zu Fuss zum Briefing zu kommen.
    		</div>
    
    	</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Helfer - Aufbau', 'page', $renderedHTML);

?>
