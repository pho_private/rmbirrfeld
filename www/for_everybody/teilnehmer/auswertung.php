<?php
require_once ('inc.php');

$renderedHTML = <<<EOT
	<div class=rm_h1>Auswertung</div>
	<div class=col_block>
		<div class=rm_h2>Abgabe der IGC-Dateien</div>
		<div class=rm_text align="justify">
			Das Auslesen des Loggers wird nicht vom Auswertungsteam, sondern von den Piloten ausgef&uuml;hrt.
			Die IGC-Dateien k&ouml;nnen anschliessend auf <div class=normal><a href="../igc/igc_upload.php">dieser Webseite unter IGC Upload</a></div> hochgeladen werden.
			<br /> <b><font color="#FF0000">Achtung: IGC-Datei nach dem Auslesen auf keinen Fall umbenennen!</font></b>
		</div>

		<div class=rm_h2>Identifikation der IGC-Datei</div>
		<div class=rm_text align="justify">
			Damit die IGC-Datei eindeutig dem Piloten zugeordnet werden kann, bitte die <b><font color="#FF0000">LoggerID</font></b>
			(gem&auml;ss untenstehendem Beispiel) bei der Anmeldung vermerken oder nachtr&auml;glich via Email an das OK senden.<br />
			Wir brauchen diese Angabe (ebenso wie die FlarmId) vor dem ersten Start.
		</div>


		<div class=rm_h2>LoggerId im Namen der IGC-Datei</div>
		<div class=rm_text>
			Die Zusammensetzung des Dateinamens wird anhand des folgenden
			Beispiels ersichtlich: <b>15K<font color="#FF0000">FADN</font>1
			</b>.igc<br />

			<table>
				<tr>
					<th class=th_1>Position</th>
					<th class=th_1>Bedeutung</th>
					<th class=th_1>Wert</th>
				</tr>
				<tr>
					<td class=td_1>1</td>
					<td class=td_1>Jahr</td>
					<td class=td_1>1 -&gt; 2021</td>
				</tr>
				<tr>
					<td class=td_1>2</td>
					<td class=td_1>Monat

					<td class=td_1>5 -&gt; Mai</td>
				</tr>
				<tr>
					<td class=td_1>3</td>
					<td class=td_1>Tag

					<td class=td_1>K -&gt; 20.</td>
				</tr>
				<tr>
					<td class=td_1>4-7</td>
					<td class=td_1><b><font color="#FF0000">LoggerId</font></b>

					<td class=td_1><b><font color="#FF0000">FADN</font></b></td>
				</tr>
				<tr>
					<td class=td_1>8</td>
					<td class=td_1>Flug</td>
					<td class=td_1>1 -&gt; 1. Flug</td>
				</tr>
			</table>
		</div>

	</div>
	<div class=lst_col_block>
		<div class=rm_h2>Loggereinstellung</div>
		<div class=rm_text align="justify">Aufzeichnungsintervall auf 1 Sekunde einstellen.</div>
		<div class=rm_text>
			Im Logger sind folgende Angaben zu speichern:
			<ul>
				<li>Pilot</li>
				<li>Kennzeichen</li>
				<li>Wettbewerbsnummer</li>
				<li>Klasse</li>
			</ul>
		</div>

		<div class=rm_h2>Backup-Logger</div>
		<div class=rm_text>
			Als Backup-Logger sind nur noch IGC-Logger und Flarm zugelassen!
		</div>

		<div class=rm_h2>Motorsegler</div>
		<div class=rm_text align="justify">
			Ein Logger mit Motorlaufaufzeichnung ist vorgeschrieben. Siehe SM Reglement 5.1.3.4. f&uuml;r Details.
		</div>

		<div class=rm_h2>IGC Ranking</div>
		<div class=rm_text align="justify">
			Dieser Wettbewerb zählt für das <a href="https://rankingdata.fai.org/">IGC Ranking</a>.
		</div>
	</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Auswertungsregeln', 'page', $renderedHTML);

?>
