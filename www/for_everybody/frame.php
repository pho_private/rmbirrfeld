<?php
require_once ('inc.php');
require_once ('functions.php');

$newsItem = new NewsItems();
$lastNewsDate = $newsItem->getLastNewsEntryDate();
if ($lastNewsDate) {
    $datum = $lastNewsDate;
} else {
    $datum = $rm_new_year;
}

$datum = getRequestOrInit('datum', $datum);
$hnav = getRequestOrInit('hnav', 'news/news_hnav.php');
$page = getRequestOrInit('page', "news/news.php?datum={$datum}");

$frameset = <<<EOT
<frameset rows="90,*" framespacing="2" frameborder="0">
	<frame name="hnav" marginwidth="5" marginheight="2" scrolling="auto" src="{$hnav}" noresize>
	<frame name="page" marginwidth="5" marginheight="2" scrolling="auto" src="{$page}" noresize>
</frameset>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayFrameset('public hFrame', $frameset);
?>
