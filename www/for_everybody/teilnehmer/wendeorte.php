<?php
require_once ('inc.php');
$renderedHTML = <<<EOT
		<div class=rm_h1>Wendepunktkatalog</div>
		<div class=big_col_block>
			<div class=rm_text>Wir legen die Wendepunkte auf Soaringspot ab.</div>
			<div class=rm_text>
				<table width=250>
					<tr>
						<td class=sidebarlink><a href="{$rm_soaringspot_url}/downloads">Soaringspot Downloads {$rm_name_kurz}</a></td>
					</tr>
				</table>
			</div>
		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Wendeorte Download', 'page', $renderedHTML );

?>
