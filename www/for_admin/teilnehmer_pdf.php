﻿<?php
error_reporting(E_ERROR);
require_once ('inc.php');
require_once ('fpdf.php');
require_once ('functions.php');

class PDF extends FPDF
{
    
    function Header() {
        global $rm_name_lang, $rm_logo_png, $klasse, $class_name;
 
        $this->Rect(20, 6, 170, 28);
        $this->Image($rm_logo_png, 21, 7, 25);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->CellNormal(30, 8, '', 0, 0, 'L', 0);
        $this->SetFont('Helvetica', 'B', 22);
        $this->CellNormal(130, 8, $rm_name_lang, 0, 1, 'L', 0);
        $this->SetFont('Helvetica', 'B', 18);
        $this->CellNormal(30, 8, '', 0, 0, 'L', 0);
        $this->CellNormal(130, 8, 'Teilnehmerliste ' . $class_name[$klasse], 0, 1, 'L', 0);
        
        $now_text = date("j.n.Y H:i", time());
        $this->SetFont('Helvetica', '', 8);
        $this->CellNormal(30, 8, '', 0, 0, 'L', 0);
        $this->CellNormal(40, 8, $now_text . ' / Seite ' . $this->PageNo() . '/{nb}', 0, 1, 'L', 0);
        
        $this->Ln(2);
    }
    
    /*
     * convert text so UTF-8 Umlauts get rendered correctly on PDF
     */
    function CellNormal($a, $b, $c, $d, $e, $f, $g = 0) {
        $this->Cell($a, $b, encodeToIso($c), $d, $e, $f, $g);
    }
    
    /**
     * Produce a cell based on a condition: if value is OK, normal font, if value is bad, bold font
     *
     * @param bool $condition
     * @param string $valueOK
     * @param string $valueBad
     * @param int $w
     * @param int $h
     * @param int $border
     * @param int $ln
     * @param string $align
     * @param bool $fill
     * @param string $link
     */
    
    function CellNormalOrBold($condition, $valueOK, $valueBad, $w, $h = 0, $border = 0, $ln = 0, $align = '', $fill = false, $link = '') {
        if ($condition) {
            $this->CellNormal($w, $h, $valueOK, $border, $ln, $align, $fill, $link);
        } else {
            $this->SetFont('Helvetica', 'B', 0); // leave family and size same as before
            $this->CellNormal($w, $h, $valueBad, $border, $ln, $align, $fill, $link);
            $this->SetFont('Helvetica', '', 0); // just turn off bold
        }
    }
    
    function CellNormalOrRed($condition, $valueOK, $valueBad, $w, $h = 0, $border = 0, $ln = 0, $align = '', $fill = false, $link = '') {
        if ($condition) {
            $this->CellNormal($w, $h, $valueOK, $border, $ln, $align, $fill, $link);
        } else {
            $this->SetTextColor(255, 0, 0);
            $this->SetFont('Helvetica', 'B', 0); // leave family and size same as before
            $this->CellNormal($w, $h, $valueBad, $border, $ln, $align, $fill, $link);
            $this->SetFont('Helvetica', '', 0); // just turn off bold
            $this->SetTextColor(0, 0, 0);
        }
    }
    
    function MultiCell($w, $h, $txt, $border = 0, $align = 'J', $fill = false) {
        parent::MultiCell($w, $h, encodeToIso($txt), $border, $align, $fill);
    }
}

// ---------------------------------------------------------------
// Main Code 
// ---------------------------------------------------------------

// get teilnehmerliste and check if we have participants at all.
$teilnehmerliste = new Teilnehmer();
$teilnehmerliste->load_all_by_class();

$filename = "";

// if there are participants ...
if (count($teilnehmerliste->list) != 0) {

    $lh = 4; // number of lines in a participant table entry.

    $pdf = new PDF('P');
    $pdf->AliasNbPages();
    $pdf->SetLeftMargin(20);
    $pdf->SetRightMargin(20);
    $pdf->SetAutoPageBreak(true, 7 * $lh);

    $pdf->SetFillColor(255, 255, 255);
    $pdf->SetTextColor(0, 0, 0);

    $pdf->SetFont('Helvetica', '', 9);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetDrawColor(0, 0, 0);

    $last_class = '';
    $count = 0;

    foreach ($teilnehmerliste->list as $teilnehmer)
    {
        $teilnehmer['telp'] = formattedPhone($teilnehmer['telp']);
        $teilnehmer['mobp'] = formattedPhone($teilnehmer['mobp']);
        $teilnehmer['telg'] = formattedPhone($teilnehmer['telg']);
        $teilnehmer['mobg'] = formattedPhone($teilnehmer['mobg']);
        $klasse = $teilnehmer['klasse'];
        $junior = $teilnehmer['junior'] == 'ja';
        $nur_rm = !$rm_RMSM ? true : preg_match('/nur rm/', strtolower($teilnehmer['angaben']));
        $bar = !($teilnehmer['bezahlt'] != '' && $teilnehmer['bezahlt'] != '0000-00-00');
        $barzuschlag = $nur_rm ? $rm_barzuschlag : $sm_barzuschlag;
        
        if ($last_class != $klasse) {
            $count = 0;
            $pdf->AddPage();
            $last_class = $klasse;
        }
        $count++;
        
        // alternierend dunkle und helle Eintr&auml;ge
        if ($count % 2 == 0) {
            $pdf->SetFillColor(220, 220, 220);
        } else {
            $pdf->SetFillColor(240, 240, 240);
        }
        
        $pdf->SetFont('', '', 9);
        $nenngeld = 0;
        if ( $junior &&  $nur_rm) $nenngeld = $rm_nenngeld_junior;
        if ( $junior && !$nur_rm) $nenngeld = $sm_nenngeld_junior;
        if (!$junior &&  $nur_rm) $nenngeld = $rm_nenngeld_senior;
        if (!$junior && !$nur_rm) $nenngeld = $sm_nenngeld_senior;
        
        $camping = 0;
        if ($teilnehmer['camping'] == 'ja') {
            if ( $junior &&  $nur_rm) $camping = $rm_camping_junior;
            if ( $junior && !$nur_rm) $camping = $sm_camping_junior;
            if (!$junior &&  $nur_rm) $camping = $rm_camping_senior;
            if (!$junior && !$nur_rm) $camping = $sm_camping_senior;
        }
        
        $rueckholen = 0;
        if ($teilnehmer['rueckholen'] == 'ja') {
            if ( $junior &&  $nur_rm) $rueckholen = $rm_rueckholen_junior;
            if ( $junior && !$nur_rm) $rueckholen = $sm_rueckholen_junior;
            if (!$junior &&  $nur_rm) $rueckholen = $rm_rueckholen_senior;
            if (!$junior && !$nur_rm) $rueckholen = $sm_rueckholen_senior;
        }
        $total = $camping + $rueckholen + $nenngeld + (($bar) ? $barzuschlag : 0);
        
        // Zeile 1
        $pdf->CellNormal(60, $lh, $teilnehmer['name'] . ' ' . $teilnehmer['vorname'], 1, 0, 'L', 1);
        if ($nur_rm) {
            $pdf->CellNormal(10, $lh, 'RM', 1, 0, 'C', 1);
        } else {
            $pdf->CellNormal(10, $lh, 'SM', 1, 0, 'C', 1);
        }
        $pdf->CellNormal(20, $lh, 'FAI '.$teilnehmer['sportlizenz'], 1, 0, 'L', 1);
        $pdf->CellNormal(20, $lh, 'IGC '.$teilnehmer['igc_rank_id'], 1, 0, 'L', 1);
        $pdf->CellNormalOrRed(($teilnehmer['ausweise'] == 'ja'), '< geprüft', '< noch prüfen', 30, $lh, 1, 0, 'L', 1);
        $pdf->CellNormal(20, $lh, 'Nenngeld', 1, 0, 'L', 1);
        $pdf->CellNormal(10, $lh, $nenngeld, 1, 1, 'R', 1);
        
        // Zeile 2
        $pdf->CellNormal(70, $lh, $teilnehmer['adresse'] . ', ' . $teilnehmer['plz'] . ' ' . $teilnehmer['wohnort'], 1, 0, 'L', 1);
        $pdf->CellNormal(35, $lh, $teilnehmer['logger1'], 1, 0, 'C', 1);
        $pdf->CellNormalOrRed(($teilnehmer['logger1id'] != '' 	&& $teilnehmer['logger1id'] != '?'), 	strtoupper($teilnehmer['logger1id']), '______', 35, $lh, 1, 0, 'C', 1);
        $pdf->CellNormal(20, $lh, 'Camping', 1, 0, 'L', 1);
        $pdf->CellNormal(10, $lh, $camping, 1, 1, 'R', 1);
        
        // Zeile 3
        $pdf->CellNormal(35, $lh, $teilnehmer['mobp'], 1, 0, 'C', 1);
        $pdf->CellNormal(35, $lh, $teilnehmer['telp'], 1, 0, 'C', 1);
        $pdf->CellNormal(35, $lh, 'Flarm', 1, 0, 'C', 1);
        $pdf->CellNormalOrRed(($teilnehmer['FlarmId'] != '' && $teilnehmer['FlarmId'] != '?'), 	strtoupper($teilnehmer['FlarmId']), '______', 35, $lh, 1, 0, 'C', 1);
        $pdf->CellNormal(20, $lh, 'Rückholen', 1, 0, 'L', 1);
        $pdf->CellNormal(10, $lh, $rueckholen, 1, 1, 'R', 1);
        
        // Zeile 4
        $pdf->CellNormal(70, $lh, $teilnehmer['email'], 1, 0, 'L', 1);
        if ($junior) {
            $pdf->CellNormal(35, $lh, 'Junior', 1, 0, 'C', 1);
        } else {
            $pdf->CellNormal(35, $lh, '', 1, 0, 'L', 1);
        }
        $pdf->CellNormalOrRed(!$bar, 'bezahlt >', 'Zahlung offen >', 35, $lh, 1, 0, 'C', 1);
        $pdf->CellNormalOrRed(!$bar, 'Total', 'Barzuschlag', 20, $lh, 1, 0, 'L', 1);
        $pdf->CellNormalOrRed(!$bar, $total, $barzuschlag, 10, $lh, 1, 1, 'R', 1);
            
        // Zeile 5
        $pdf->CellNormal(70, $lh, strtoupper($teilnehmer['wk'])." / ".$teilnehmer['immatrikulation']." / ".$teilnehmer['flugzeug']." / ".$teilnehmer['indexwert'] , 1, 0, 'L', 1);
        $pdf->CellNormalOrRed($teilnehmer['fsb'] != 0, 'FSB '.$teilnehmer['fsb'], 'FSB ________', 35, $lh, 1, 0, 'L', 1);
        $pdf->CellNormal(35, $lh, 'Visum', 1, 0, 'L', 1);
        $pdf->CellNormalOrRed(!$bar, '', 'Total', 20, $lh, 1, 0, 'L', 1);
        $pdf->CellNormalOrRed(!$bar, '', $total, 10, $lh, 1, 1, 'R', 1);

        // Zeile 6
        $pdf->CellNormal(170, $lh, "Notfall-Kontakt: ".$teilnehmer['notfallkontakt1'] , 1, 1, 'L', 1);

        if ($count >= 10) {
            $count = 0;
            $pdf->AddPage();
        } else {
            $pdf->Ln(3);
        }
   }

   deleteFilesLike("../resources/downloads/teilnehmerliste_*.pdf");

   // define static filename
    $filename = "../resources/downloads/teilnehmerliste_" . date('d-M-Y') . ".pdf";

    // write new file
    $pdf->Output("F", $filename, true);

}
rm_DownloadListPage("Teilnehmer PDF", $filename);
?>
