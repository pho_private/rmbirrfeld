<?php
require_once ('inc.php');
require_once ('dblib.inc.php');

/*
 * glide and seek data generation:
 * creates aircraft and competition data files.
 * aircraft is written after each change of Flarm ID.
 * competition is written once per event (RM/SM) because glide and seek looks up things in the results page.
 */

/*
 * generate the common aircraft data file according to the spec:
 * [
 * {
 * "comp": "<class id>",
 * "name": "<vorname> <name>",
 * "cn": "<wk>",
 * "glider": "<flugzeug>",
 * "hc": true,
 * "flarm": ["<FlarmId>"],
 * "status": null,
 * // following left away as it is optional
 * "launch": "",
 * "start": "",
 * "finish": "",
 * "landout": {
 * "time": "",
 * "pos": [lat, lng],
 * "w3w": ""
 * }
 * },
 * ...
 * ]
 */
function gsTrackingWriteAircraftFileForClass(array $gliders, int $class, bool $overwrite) {
	global $class_tracking_name, $rm_tracking_dir;

	$fileName = $rm_tracking_dir . '/' . getGsClassAircraftFileName ( $class );

	if ($overwrite) {
		if (file_exists ( $fileName )) {
			unlink ( $fileName );
		}
	}

	$singleClass = ($class > 0);
	$rows = count ( $gliders );

	// initialize empty array in case there are no gliders yet. 
	$aircraftdata = array();

	// generate aircraft file for all classes
	for($i = 0; $i < $rows; $i ++) {
		if ($singleClass && $gliders [$i] ['comp'] != $class) {
			continue;
		}
		$aircraft = array (
				'comp' => $gliders [$i] ['comp'],
				'name' => $gliders [$i] ['name'],
				'cn' => $gliders [$i] ['cn'],
				'glider' => $gliders [$i] ['glider'],
				'flarm' => array (
						$gliders [$i] ['flarm']
				),
				'hc' => false,
				'status' => null,
				'launch' => null,
				'start' => null,
				'finish' => null
		);
		$aircraftdata [] = $aircraft;
	}

	try {
		$aircraft_json = json_encode ( $aircraftdata, JSON_UNESCAPED_UNICODE );
	} catch ( JsonException $e ) {
		echo 'caught exception ' . $e->getMessage ();
	}

	// finally, write aircraft data json in file www/trk/aircraft.json

	file_put_contents ( $fileName, $aircraft_json );

	return true;
}

function gsTrackingWriteAircraftFiles(bool $overwrite = false) {
	global $rm_tbl_teilnehmer, $rm_tracking_dir, $class_key;
	// initialize aircraft data array
	$aircraftdata = array ();
	$gliderquery = <<<EOT
	       SELECT
	            klasse as comp
	            , CONCAT( vorname, ' ', name) as name
	            , wk as cn
	            , flugzeug as glider
	            , flarmid as flarm
	        FROM $rm_tbl_teilnehmer WHERE rmactiv = 'ja'
	        ORDER BY wk ASC;
EOT;
	$ResultPointer = DBQuery ( $gliderquery );

	// if we got results, then construct the aircraft data array.
	if ($ResultPointer) {

		$gliderdb = DBFetchAllAssoc ( $ResultPointer );

		// "class" 0 generates the overall file, all other classes generate one aircraft file 'aircraft-<class>.json' per class
		foreach ( $class_key as $class ) {
			$result = gsTrackingWriteAircraftFileForClass ( $gliderdb, $class, $overwrite );
		}
	}
	return true;
}

/*
 * generate the competition data file, according to the spec:
 *
 * {
 * "comps": [
 * {
 * "id": "<class id>",
 * "name": "<class name> - <rm name lang>",
 * "short": "<clas name>",
 * "task": "<SoaringSpot Day's competition URL>",
 * "colour": "#ff0000" // optional
 * }
 * ],
 * "gliders": "Url to an endpoint following the aircraft-data-interface with optional additional attributes defined below"
 * }
 * where asoaringspot url to a task is structured as follows:
 * https://www.soaringspot.com/de/rm-birrfeld-2017/tasks/offene/task-1-on-2017-05-13
 * < site & language ><comp name > <class ><tasknr. ><compdate>
 * - site & language: fixed
 * - comp name: defined in soaringspot admin settings, edit competition, Competition, edit, permalink
 * - class: defined in soaringspot admin settings, edit competition, Classes, edit, class permalink. Use the $class_tracking_name names from inc.php in Soaringspot.
 * - tasknr.: "task-<n>-on-" more or less fixed.
 * - compdate: yyyy-mm-dd. needs to be today's date.
 */
function gsTrackingWriteCompetitionFileForClass(int $classToBuild, bool $overwrite = false) {
	global $class_key, $class_name, $class_tracking_color, $class_tracking_name, $rm_name_kurz, $rm_name_lang, $rm_soaringspot_url, $rm_tracking_dir, $rm_tracking_dir_url, $today;

	$competitionFilename = "{$rm_tracking_dir}/" . getGsCurrentCompetitionFileName ( $classToBuild );

	if ($overwrite) {
		if (file_exists ( $competitionFilename )) {
			unlink ( $competitionFilename );
		}
	}

	if (! file_exists ( $competitionFilename )) {

		$competitions = <<<EOT
{
    "comps":[
EOT;
		$classAircraftFile = getGsClassAircraftFileName ( $classToBuild );

		foreach ( $class_key as $class ) {
			if ($class != 0) {
				// skip other classes if we generate just 1 class
				// $class = 0 means generate one file with all classes
				// all other values require to filter just the participants of that class
				if (($classToBuild != 0) && ($class != $classToBuild)) {
					continue;
				}
				$competitions .= <<<EOT

	    {
	        "id": "{$class}",
	        "name": "{$class_name[$class]} - $rm_name_lang",
	        "short": "{$class_tracking_name[$class]} - $rm_name_kurz",
	        "task": "$rm_soaringspot_url/results/{$class_tracking_name[$class]}",
	        "colour": "{$class_tracking_color[$class]}"
	    },
EOT;
			}
		}

		// now, we have a superfluous "," after the last comp data block - remove it
		$competitions = substr ( $competitions, 0, strlen ( $competitions ) - 1 ) . <<<EOT

    ],
    "gliders": "{$rm_tracking_dir_url}/{$classAircraftFile}"
}

EOT;

		// finally, write competitions data json in file www/trk/competitions.json or competition-<class>.json
		file_put_contents ( $competitionFilename, $competitions );
	}
}
function gsTrackingWriteCompetitionFiles(bool $overwrite = false) {
	global $class_key, $class_name, $class_tracking_color, $class_tracking_name, $rm_name_kurz, $rm_name_lang, $rm_soaringspot_url, $rm_tracking_dir, $rm_tracking_dir_url, $today;

	// if the tracking directory does not exist, then we have no aircraft file. So, we have to create the directory, and the aircraft file.
	if (! is_dir ( $rm_tracking_dir )) {
		if (! mkdir ( $rm_tracking_dir, 0755, true )) {
			die ( 'Failed to create tracking folder' );
		}
	}
	gsTrackingWriteAircraftFiles ();
	foreach ( $class_key as $class ) {
		gsTrackingWriteCompetitionFileForClass ( $class, $overwrite );
	}
}

function getGsClassAircraftFileName(int $class = 0) {
	global $class_tracking_name;
	$fileName = 'aircraft';

	if ($class > 0) {
		$fileName .= '-' . $class_tracking_name [$class];
	}
	return "{$fileName}.json";
}


function getGsCurrentCompetitionFileName(int $class = 0) {
	global $class_tracking_name, $today;
	$fileName = 'competition';

	if ($class == 0) {
		$fileName .= 's';
	} else {
		$fileName .= '-' . $class_tracking_name [$class];
	}
	return "$fileName.json";
}
?>
