<?php
require_once ('inc.php');
require_once ('helfer_inc.php');
require_once ('helfer_2018_inc.php');
require_once ('dblib.inc.php');
require_once ('functions.php');


function personAttributesToString()
{
    global $vorname, $name, $jahrgang, $ort, $gruppe, $mail, $phone;
    return "{$vorname},{$name},{$jahrgang},{$ort},{$gruppe},{$mail},{$phone}";
}
// --------------------------------------------------------------------------------
// Engagement handling:
function getEngagementComponents($boxAddress)
{
    global $rm_year;
    $matches = array();
    $engagement = NULL;
    $pattern = "/(aufbau|beiz|betrieb|abbau|eroeffnung|abschluss)([0-1][0-9])([0-3][0-9])([1-9])/";
    if (preg_match($pattern, $boxAddress, $matches)) {
        // we have an entry for the database - let's check if the same person has a conflicting engagement...
        $engagement['area'] = $matches[1];
        $engagement['date'] = "{$rm_year}-{$matches[2]}-{$matches[3]}";
        $engagement['shift'] = $matches[4];
    }
    return $engagement;
}

/**
 * get all engagements of this particular person.
 * Format of $person: "$vorname,$name,$jahrgang,$ort,$gruppe,..."
 *
 * @param string $vorname            
 * @param string $name            
 * @param int $jahrgang            
 * @param string $ort            
 * @param string $gruppe            
 * @return array() selected boxes - box address formatted table entries.
 */
function getAllEngagementsInDbOf($person)
{
    // print "<br />getAllEngagementsOf($person): <br />";
    $selectedBoxes = Array();
    
    $myPerson = explode(',', $person);
    $helfer = new Helfer();
    $myEngagements = $helfer->load_helfer_einsaetze($myPerson[0], $myPerson[1], $myPerson[2], $myPerson[3], $myPerson[4]);
    
    // now, generate an array of "box addresses" of the form <area><month><day><shift>
    foreach ($myEngagements as $myEngagement) {
        $selectedBoxes[] = $myEngagement['area'] . str_replace('-', '', substr($myEngagement['date'], 5)) . $myEngagement['shift'];
    }
    // printArray($selectedBoxes);
    return $selectedBoxes;
}

/**
 * Vergleich WAR (in DB) mit IST (in Tabelle):
 * 3 F�lle:
 * - IST, ! WAR - neu, anlegen
 * - IST, WAR - blieb, m�glicherweise update der Personendaten
 * - ! IST, WAR - gel�scht, nur falls das zugelassen ist (vor cutoff-date $rm_helfer_no_changes).
 * Falls nicht mehr zugelassen wird's einfach wieder gesetzt.
 *
 * @param string $errMsg
 *            Error Messages string, error messages will be appended to it, thus it is passed by reference.
 * @return boolean TRUE: alles OK, FALSE: mindestens ein Problem beim Schreiben.
 */
function updateEngagementsInDb(&$errMsg)
{
    global $rm_tbl_einsaetze, $rm_helfer_no_changes;
    global $name, $vorname, $jahrgang, $mail, $phone, $ort, $gruppe, $initialPerson;
    global $selectedBoxes;
    $retval = TRUE;
    
    // this is the original person dataset
    $initialComponents = explode(',', $initialPerson);
    
    // compare the original to the new person data set
    $currentPerson = personAttributesToString();
    $updatePersonData = ($currentPerson != $initialPerson);
    
    $DbEngagements = getAllEngagementsInDbOf($initialPerson);
    
    // the current selection is stored in $selectedBoxes
    $newEngagements = array_diff($selectedBoxes, $DbEngagements);
    $deletedEngagements = array_diff($DbEngagements, $selectedBoxes);
    
    // printArray($newEngagements);
    // if there are new engagements, go insert them.
    if (sizeof($newEngagements) > 0) {
        foreach ($newEngagements as $engagement) {
            // $engagement is a single value of the form <area><mmdd><shift>, so go get the values $area, $date (format "YY-mm-dd") and $shift.
            $components = getEngagementComponents($engagement);
            if (is_array($components)) {
                $area = $components['area'];
                $date = $components['date'];
                $shift = $components['shift'];
                $insert = "INSERT INTO {$rm_tbl_einsaetze} (name, vorname, jahrgang, mail, phone, wohnort, gruppe, datum, gebiet, schicht, created, updated, deleted)
	  				VALUES ('{$name}', '{$vorname}', '{$jahrgang}', '{$mail}', '{$phone}', '{$ort}', '{$gruppe}', '{$date}', '{$area}', {$shift}, NOW(), NOW(), NULL)";
                
                if (! DBQuery($insert)) {
                    $update = "UPDATE $rm_tbl_einsaetze SET deleted = NULL, updated = NOW()
								WHERE  gebiet = '{$area}' AND datum = '{$date}' AND schicht = '{$shift}'
	  									AND	vorname = '{$vorname}' AND name = '{$name}' AND jahrgang = '{$jahrgang}'
	  				 					AND wohnort = '{$ort}' AND gruppe = '{$gruppe}'";
                    if (! DBQuery($update)) {
                        $errMsg .= "Konnte Einsatz {$area} Schicht {$shift} am {$date} nicht anlegen:" . DBErrNo() . ": " . DBError() . "<br />";
                        $retval = FALSE;
                    }
                }
            } else {
                $errMsg .= "Konnte neuen Einsatz nicht identifizieren: {$engagement}<br />";
            }
        }
    }
    
    if (! is_array($deletedEngagements)) {
        // if (date('Y-m-d') < $rm_helfer_no_changes)
        if (true) {
            $components = getEngagementComponents($engagement);
            if (is_array($components)) {
                $area = $components['area'];
                $date = $components['date'];
                $shift = $components['shift'];
                $delete = "UPDATE $rm_tbl_einsaetze SET deleted = NOW(), updated = NOW()
							WHERE vorname = '{$initialComponents[0]}' AND name = '{$initialComponents[1]}' AND jahrgang = {$initialComponents[2]}
									AND wohnort = '{$initialComponents[3]}' AND gruppe = '{$initialComponents[4]}'";
                
                if (! DBQuery($delete)) {
                    $errMsg .= "Konnte Einsatz {$area} Schicht {$shift} am {$date} nicht l&ouml;schen:" . DBErrNo() . ": " . DBError() . "<br />";
                    $retval = FALSE;
                }
            }
        } else {
            $errMsg .= "Eins&ouml;tze k&ouml;nnen nicht mehr selbst gel&ouml;scht werden - wenden Sie sich an den OK-Chef!<br />";
        }
    }
    
    // deletions may not be allowed anymore.
    if (sizeof($deletedEngagements) > 0) {
        // if (date('Y-m-d') < $rm_helfer_no_changes)
        if (true) {
            foreach ($deletedEngagements as $engagement) {
                // $engagement is a single value of the form <area><mmdd><shift>, so go get the values $area, $date (format "YY-mm-dd") and $shift.
                $components = getEngagementComponents($engagement);
                if (is_array($components)) {
                    $area = $components['area'];
                    $date = $components['date'];
                    $shift = $components['shift'];
                    $delete = "UPDATE $rm_tbl_einsaetze SET deleted = NOW(), updated = NOW()
								WHERE gebiet = '{$area}' AND datum = '{$date}' AND schicht = '{$shift}'
	  									AND	vorname = '{$initialComponents[0]}' AND name = '{$initialComponents[1]}' AND jahrgang = {$initialComponents[2]}
	  				 					AND wohnort = '{$initialComponents[3]}' AND gruppe = '{$initialComponents[4]}'";
                    
                    if (! DBQuery($delete)) {
                        $errMsg .= "Konnte Einsatz {$area} Schicht {$shift} am {$date} nicht l�schen:" . DBErrNo() . ": " . DBError() . "<br />";
                        $retval = FALSE;
                    }
                } else {
                    $errMsg .= "Konnte zu l&ouml;schenden Einsatz nicht identifizieren: {$engagement}<br />";
                    $retval = FALSE;
                }
            }
        } else {
            $errMsg .= "Eins&auml;tze k&ouml;nnen nicht mehr selbst gel&ouml;scht werden - wenden Sie sich an den OK-Chef!<br />";
        }
    }
    
    if ($updatePersonData) {
        $update = "UPDATE {$rm_tbl_einsaetze}
								SET vorname = '{$vorname}', name = '{$name}', jahrgang = '{$jahrgang}', wohnort = '{$ort}', gruppe = '{$gruppe}', updated = NOW()
	  							WHERE vorname = '{$initialComponents[0]}' AND name = '{$initialComponents[1]}' AND jahrgang = '{$initialComponents[2]}'
	  				 				AND wohnort = '{$initialComponents[3]}' AND gruppe = '{$initialComponents[4]}'";
        if (! DBQuery($update)) {
            $errMsg .= "Konnte Personendaten nicht anpassen von <br />{$initialPerson}<br />zu<br />{$currentPerson}:<br />" . DBErrNo() . ": " . DBError() . "<br />";
            $retval = FALSE;
        }
        
        // update person data to reflect status in database.
        $initialPerson = $currentPerson;
    }
    
    return $retval;
}

function isConflictBeizBetrieb($shiftBeiz, $shiftBetrieb)
{
    global $schicht_konflikt_beiz_betrieb;
    
    // print "<br />isConflictBeizBetrieb($shiftBeiz, $shiftBetrieb)";
    $retval = $schicht_konflikt_beiz_betrieb[$shiftBeiz - 1][$shiftBetrieb - 1];
    // print $retval;
    return $retval;
}

function isConflict($date1, $area1, $shift1, $date2, $area2, $shift2)
{
    // keine Überschneidung über verschiedene Tage...
    if ($date1 != $date2) {
        $retval = false;
    } // ein Gebiet kann für eine Schicht von derselben Person nicht mehrfach gewählt werden.
elseif ($area1 == $area2) {
        $retval = false;
    } // genauere Abklaerung notwendig.
elseif ($area1 == 'betrieb') {
        $retval = isConflictBeizBetrieb($shift2, $shift1);
    } else {
        $retval = isConflictBeizBetrieb($shift1, $shift2);
    }
    return $retval;
}

/**
 * checken auf Konflikte zwischen gesetzten Boxen.
 * f�r Konflikte werden Meldungen an $errMsg angef�gt.
 *
 * @param string $errMsg
 *            Fehlermeldungs-String. Detaillierte Fehlermeldungen werden angeh�ngt.
 * @return boolean TRUE = keine Konflikte.
 */
function engagementsOK(&$errMsg)
{
    global $rm_tbl_einsaetze;
    global $engagements;
    
    // now, check for conflicts between engagements:
    $conflict = false;
    for ($i = 0; $i < count($engagements); $i ++) {
        $area1 = $engagements[$i]['area'];
        $date1 = $engagements[$i]['date'];
        $shift1 = $engagements[$i]['shift'];
        for ($j = $i + 1; $j < count($engagements); $j ++) {
            $area2 = $engagements[$j]['area'];
            $date2 = $engagements[$j]['date'];
            $shift2 = $engagements[$j]['shift'];
            if (isConflict($date1, $area1, $shift1, $date2, $area2, $shift2)) {
                $conflict = true;
                $area1 = ucfirst($area1);
                $area2 = ucfirst($area2);
                $errMsg .= "Konflikt zwischen Eins&auml;tzen am {$date1} in {$area1} Schicht {$shift1} und {$area2} Schicht {$shift2}<br />";
            }
        }
    }
    return ! $conflict;
}

/**
 * Anzahl der geplanten Helfer pro Schicht bestimmen
 * Statt ein komplexes Objekt statisch aufzubauen in inc.php wird hier aus den verschiedenen einzelnen Variablen die korrekte Zahl "uusegrueblet"
 *
 * @param string $area            
 * @param
 *            Datums-String 'Y-m-d' $curDate
 * @param integer $shift            
 * @return integer <number>
 */
function getPlannedNumberOfEngagements($area, $curDate, $shift)
{
    global $aufbau_dates_und_einsatz, $eroeffnung_dates_und_einsatz, $beiz_dates_und_einsatz, $betrieb_dates_und_einsatz, $abbau_dates_und_einsatz, $abbau_dates_und_einsatz, $abschluss_dates_und_einsatz;
    
    switch($area)
	{
        case 'aufbau':		$dates_und_einsatz = $aufbau_dates_und_einsatz; 		break;
        case 'eroeffnung':	$dates_und_einsatz = $eroeffnung_dates_und_einsatz;		break;
        case 'beiz':		$dates_und_einsatz = $beiz_dates_und_einsatz;			break;
        case 'betrieb':		$dates_und_einsatz = $betrieb_dates_und_einsatz;		break;
        case 'abbau':		$dates_und_einsatz = $abbau_dates_und_einsatz;			break;
        case 'abschluss':	$dates_und_einsatz = $abschluss_dates_und_einsatz;		break;
    }
	foreach ($dates_und_einsatz as $date_und_einsatz)
	{
		if ($date_und_einsatz['datum'] == $curDate) 
		{
			return $date_und_einsatz['einsaetze'][$shift];
			break;
		}
	}
	return "??";
}
	
/**
 * load all engagements from $_REQUEST into $selectedBoxes
 * these are the engagements as adapted/changed by the user
 * side effect: it also fills the raw data into $engagements
 *
 * @return array() - array of engagements in box name format (<area><date><shift>)
 */
function getSelectedBoxesFromRequest()
{
    global $engagements;
    $engagements = array();
    $selectedBoxes = array();
    // generate table of all engagements as defined now in $_REQUEST
    foreach ($_REQUEST as $key => $value) {
        if ($value == 'on') {
            $engagement = getEngagementComponents($key);
            if (is_array($engagement)) {
                $engagements[] = $engagement;
                $selectedBoxes[] = $key;
            }
        }
    }
    return $selectedBoxes;
}

function getBookedEngagements($area, $datum, $shift)
{
    global $bookedEngagements;
    $retval = 0;
    
    if (! is_array($bookedEngagements))
        return $retval;
    if (array_key_exists($area, $bookedEngagements)) {
        if (array_key_exists($datum, $bookedEngagements[$area])) {
            if (array_key_exists($shift, $bookedEngagements[$area][$datum])) {
                $retval = (int) $bookedEngagements[$area][$datum][$shift];
            }
        }
    }
    return $retval;
}

/**
 * Aufbau der Helfer-Anmelde-Tabelle:
 * Aus allen Gebieten an den jeweiligen Tagen pro Schicht eine Box generieren, wenn noch nicht genuegend
 * Helfer registriert sind.
 * Sind genuegend Helfer registriert, so wird nur eine Tabellenzelle mit gruenem Hintergrund definiert.
 *
 * @param
 *            array of date strings $dates
 * @param
 *            array of areas $areas
 * @param integer $shifts            
 * @return string (rendered table rows).
 */
function renderBoxes($dates, $areas, $shifts, $selectedBoxes = array())
{
    global $rm_tbl_einsaetze;
    
    $renderedBoxes = '';
    $helfers = new Helfer();
    foreach ($dates as $curDate) {
        $curDay = new rm_DateTime($curDate);
        $weekday = substr($curDay->format('l'), 0, 2) . ".";
        $today = $curDay->format('d.m.');
        
        $renderedBoxes .= <<<EOT
					<tr>
						<th class=th_1>{$weekday} {$today}</th>

EOT;
        $index = $curDay->format('md');
        foreach ($areas as $area) 
		{
			$tmp_shifts = array_pop($shifts[$area]);
			$shift = 0;
			foreach  ($tmp_shifts as $tmp_shift)
			{
				$shift++; 
                $cnt = getBookedEngagements($area, $curDate, $shift);
                
                $soll = getPlannedNumberOfEngagements($area, $curDate, $shift);
                $rest = $soll - $cnt;
                
                $boxname = $area . $index . $shift;
                $checked = ((getRequestOrInit($boxname, 'off') == 'on') || (in_array($boxname, $selectedBoxes, false))) ? 'checked' : '';
                $renderedBoxes .= <<<EOT
						<td class=td_1><input type=checkbox name={$boxname} {$checked}>noch {$rest} ({$soll})</td>
EOT;

            }
        }
        $renderedBoxes .= <<<EOT
					</tr>
EOT;
   }
    return $renderedBoxes;
}

// --------------------------------------------------------------------------------
// main code
//
// init form fields from request headers.
//

$errMsg = '';

// print "<p />\$_REQUEST = ";
// print_r($_REQUEST);

$action = getRequestOrInit('submit');
$name = getRequestOrInit('name');
$vorname = getRequestOrInit('vorname');
$jahrgang = getRequestOrInit('jahrgang');
$mail = getRequestOrInit('mail');
$phone = getRequestOrInit('phone');
$ort = getRequestOrInit('ort');
$gruppe = getRequestOrInit('gruppe', $rm_helfer_default_gruppe);
$helfer = getRequestOrInit('helfer');
$initialPerson = getRequestOrInit('initialPerson');

$thankYouTable = '';
$errorTable = '';
$selectedBoxes = array();
$engagements = array();

$helfers = new Helfer();

// von der Helfer-Wahl
switch ($action) {
    case 'anmelden':
        // all variables have already been assigned above. So, we need to select possibly existing einsaetze for this helfer.
        // record this upon entering this page - will be used to delete the correct entries
        
        $helferlist = $helfers->load_all_helfers();
        foreach ($helferlist as $helper) {
            if ($helper['name'] == $name && $helper['vorname'] == $vorname && $helper['wohnort'] == $ort && $helper['jahrgang'] == $jahrgang) {
                $name = $helper['name'];
                $vorname = $helper['vorname'];
                $jahrgang = $helper['jahrgang'];
                $mail = $helper['mail'];
                $phone = $helper['phone'];
                $ort = $helper['wohnort'];
                $gruppe = $helper['gruppe'];
                break;
            }
        }
        
        $initialPerson = personAttributesToString();
        $selectedBoxes = getAllEngagementsInDbOf($initialPerson);
        
        break;
    
    case 'aendern':
        // we reselect because of character set trouble.
        $choices = $helfers->load_all_helfers();
        $idx = substr($helfer, 0, strpos($helfer, ','));
        $helfer = $choices[$idx];
        
        // this sets the person data in the table at the end of the screen.
        $name = $helfer['name'];
        $vorname = $helfer['vorname'];
        $jahrgang = $helfer['jahrgang'];
        $mail = $helfer['mail'];
        $phone = $helfer['phone'];
        $ort = $helfer['wohnort'];
        $gruppe = $helfer['gruppe'];
        
        $initialPerson = personAttributesToString();
        $selectedBoxes = getAllEngagementsInDbOf($initialPerson);
        
        break;
    
    // checken und speichern aller Einsaetze wenn das Formular "ausgefuellt" wurde
    case 'speichern':
        $complete = true;
        if ($name == "") {
            $errMsg .= "Name fehlt.<br />";
            $complete = false;
        }
        if ($vorname == "") {
            $errMsg .= "Vorname fehlt.<br />";
            $complete = false;
        }
        if ($ort == "") {
            $errMsg .= "Wohnort fehlt.<br />";
            $complete = false;
        }
        if ($mail == '') {
            $errMsg .= "E-Mail fehlt.<br />";
            $complete = false;
        }
        if ($phone == "") {
            $errMsg .= "Telefonnummer fehlt.<br />";
            $complete = false;
        }
        if ($gruppe == "") {
            $errMsg .= "Bitte Gruppe ausw&auml;hlen.<br />";
            $complete = false;
        }
        
        if ($complete) {
            $selectedBoxes = getSelectedBoxesFromRequest();
            if (engagementsOK($errMsg)) {
                
                if (updateEngagementsInDb($errMsg)) {
                    $thankYouTable .= <<<EOT

			<div class="einsatzplan,td_1">
				<img border=0 src=../resources/graphics/icons/green.gif>&nbsp;
				Die Eins�tze sind in die Datenbank eingetragen worden!
			</div>

EOT;
                } else {
                    $errMsg .= "Nichts gespeichert!<br />";
                }
            }
        }
        
        break;
}

$select = "SELECT COUNT(*) FROM {$rm_tbl_einsaetze} WHERE deleted IS NULL";
$result = DBQuery($select);
$row = DBFetchRow($result);
$total_meldungen = $row[0];
$left = $total_einsaetze - $total_meldungen;
$bookedEngagements = $helfers->load_number_of_engagements_per_slot();

$renderedHTML = <<<EOT
	<div class=rm_h1>Anmeldeformular f&uuml;r Helfer (total Eins&auml;tze: {$total_einsaetze} / bisher gemeldet: {$total_meldungen} / noch ben&ouml;tigt: {$left})</div>
	<div class=big_col_block>
		<br />
		<form method=POST action="helfer_einsatz.php">
			<table class="einsatzplan">
				<colgroup>
					<col width='10%'>
					<col width='18%'>
					<col width='18%'>
					<col width='18%'>
					<col width='18%'>
					<col width='18%'>
				</colgroup>
				<table class=einsatzplan>
					<colgroup>
						<col width='15%'>
						<col width='17%'>
						<col width='17%'>
						<col width='17%'>
						<col width='17%'>
						<col width='17%'>
					</colgroup>
					<tr>
						<th class=th_1>Aufbau</th>
						<th class=th_1_c valign=top>{$schichtzeiten_aufbau[1]}</th>
						<th class=th_1_c valign=top>{$schichtzeiten_aufbau[2]}</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
					</tr>
EOT;

$tmp_dates		= array();
$tmp_einsaetze	= array();
foreach($aufbau_dates_und_einsatz as $date_und_einsatz)
{	
	$tmp_dates[] 			= $date_und_einsatz['datum'];
	$tmp_einsaetze[]		= $date_und_einsatz['einsaetze'];
}
$renderedHTML .= renderBoxes($tmp_dates, array('aufbau'), array('aufbau' => $tmp_einsaetze), $selectedBoxes);

$renderedHTML .= <<<EOT
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <th class=th_1>Er&ouml;ffnung</th>
                        <th class=th_1_c valign=top>{$schichtzeiten_eroeffnung[1]}</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>
EOT;

$tmp_dates 		= array();
$tmp_einsaetze	= array();
foreach($eroeffnung_dates_und_einsatz as $date_und_einsatz)
{	
	$tmp_dates[] 			= $date_und_einsatz['datum'];
	$tmp_einsaetze[]		= $date_und_einsatz['einsaetze'];
}
$renderedHTML .= renderBoxes($tmp_dates, array('eroeffnung'), array('eroeffnung' => $tmp_einsaetze), $selectedBoxes);

$renderedHTML .= <<<EOT
					<tr><td>&nbsp;</td></tr>
					<tr>
						<th class=th_1>$rm_comp_type</th>
						<th colspan=3 class=th_1>Beiz</th>
						<th colspan=2 class=th_1>Betrieb</th>
					</tr>
					<tr>
						<td class=th_1>&nbsp;</td>
						<th class=th_1_c valign=top>{$schichtzeiten_beiz[1]}</th>
						<th class=th_1_c valign=top>{$schichtzeiten_beiz[2]}</th>
						<th class=th_1_c valign=top>{$schichtzeiten_beiz[3]}</th>
						<th class=th_1_c valign=top>{$schichtzeiten_betrieb[1]}</th>
						<th class=th_1_c valign=top>{$schichtzeiten_betrieb[2]}</th>
					</tr>
EOT;

$tmp_dates = array();
$tmp_beiz_einsaetze = array();
$tmp_betrieb_einsaetze = array();
foreach($beiz_dates_und_einsatz as $date_und_einsatz)
{	
	foreach($betrieb_dates_und_einsatz as $date_und_einsatz_2) if ($date_und_einsatz['datum'] == $date_und_einsatz_2) break;
	$tmp_dates[] 				= $date_und_einsatz['datum'];
	$tmp_beiz_einsaetze[]		= $date_und_einsatz['einsaetze'];
	$tmp_betrieb_einsaetze[]	= $date_und_einsatz_2['einsaetze'];
}
$renderedHTML .= renderBoxes($tmp_dates, array('beiz', 'betrieb'), array('beiz' => $tmp_beiz_einsaetze, 'betrieb' => $tmp_betrieb_einsaetze), $selectedBoxes);


$renderedHTML .= <<<EOT
                    <tr><td>&nbsp;</td></tr>                    
                    <tr>
                        <th class=th_1>Abschluss</th>
                        <th class=th_1_c valign=top>{$schichtzeiten_abschluss[1]}</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>
EOT;


$tmp_dates 		= array();
$tmp_einsaetze	= array();
foreach($abschluss_dates_und_einsatz as $date_und_einsatz)
{	
	$tmp_dates[] 			= $date_und_einsatz['datum'];
	$tmp_einsaetze[]		= $date_und_einsatz['einsaetze'];
}
$renderedHTML .= renderBoxes($tmp_dates, array('abschluss'), array('abschluss' => $tmp_einsaetze), $selectedBoxes);



$renderedHTML .= <<<EOT
					<tr><td>&nbsp;</td></tr>
					<tr>
						<th class=th_1>Abbau</th>
						<th class=th_1_c valign=top>{$schichtzeiten_abbau[1]}</th>
						<th class=th_1_c valign=top>{$schichtzeiten_abbau[2]}</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
					</tr>
EOT;

$tmp_dates 		= array();
$tmp_einsaetze	= array();
foreach($abbau_dates_und_einsatz as $date_und_einsatz)
{	
	$tmp_dates[] 			= $date_und_einsatz['datum'];
	$tmp_einsaetze[]		= $date_und_einsatz['einsaetze'];
}
$renderedHTML .= renderBoxes($tmp_dates, array('abbau'), array('abbau' => $tmp_einsaetze), $selectedBoxes);

$clubSelection = ausgabeHelferVereinOptionen($gruppe);

if ($errMsg != '') {
    $errorTable = <<<EOT
			<table class="einsatzplan">
				<tr>
					<td class=td_e>
						<img border=0 src=../../resources/graphics/icons/red.gif>
					</td>
					<td class=td_e>
{$errMsg}
					</td>
				</tr>
			</table>

EOT;
}

$renderedHTML .= <<<EOT
			</table>

			<table class="einsatzplan">
				<colgroup>
					<col width='09%'>
					<col width='13%'>
					<col width='04%'>
					<col width='23%'>
					<col width='13%'>
					<col width='18%'>
					<col width=*>
				</colgroup>
				<tr>
					<th class=th_1>Vorname</th>
					<th class=th_1>Name</th>
					<th class=th_1>Jahrgang</th>
					<th class=th_1>E-Mail</th>
					<th class=th_1>Telefon</th>
					<th class=th_1>Wohnort</th>
					<th class=th_1>Gruppe</th>
				</tr>
				<tr>
					<td class=td_1>{$vorname}</td>
					<td class=td_1>{$name}</td>
					<td class=td_1>{$jahrgang}</td>
					<td class=td_1>{$mail}</td>
					<td class=td_1>{$phone}</td>
					<td class=td_1>{$ort}</td>
					<td class=td_1>{$gruppe}</td>
				</tr>
			</table>
			<table>
				<tr>
					<td>
						<input type="hidden" name="vorname" 		value="{$vorname}">
						<input type="hidden" name="name" 			value="{$name}">
						<input type="hidden" name="jahrgang" 		value="{$jahrgang}">
						<input type="hidden" name="mail" 			value="{$mail}">
						<input type="hidden" name="phone" 			value="{$phone}">
						<input type="hidden" name="ort" 			value="{$ort}">
						<input type="hidden" name="gruppe" 			value="{$gruppe}">
						<input type="hidden" name="initialPerson" 	value="{$initialPerson}">
						<input type="submit" name="submit" value="speichern">
					</td>
				</tr>
			</table>
		</form>
{$errorTable}
{$thankYouTable}
		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Helfer Einsatzliste', 'page', $renderedHTML);

?>
