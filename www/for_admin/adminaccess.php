<?php
require_once ('inc.php');
// redirect to admin UI of the hosting / website
// do a simple, immediate (0 sec) redirect to the real URL.
// Parameter to be set in inc.php

// ------------------------------------------------------------------------------------------------
// HTML Output starts here
// ------------------------------------------------------------------------------------------------

header('Content-Type: text/html; charset=UTF-8');
echo <<<EOT
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="REFRESH" content="0; url={$rm_webadmin_url}">
	</head>
/html>
EOT;
?>