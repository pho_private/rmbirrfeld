<?php

require_once ('inc.php');

require_once ('functions.php');

$total = 0;

$renderedHTML = <<<EOT
		<div class=rm_h1>Teilnehmer aller Klassen {$rm_name_lang}</div>
		<div class=big_col_block>
			<table border=0 width=100%>
				<tr>
EOT;

foreach ( $class_key as $class ) {
	if ($class != 0) {
		$klasse = $class_name [$class];
		if ($klasse != $msgSelect) {
			$bgcolor = $class_bgcolor [$class];
			$renderedHTML .= <<<EOT
						<td valign=top align=center>
							<table border=0 bgcolor=$bgcolor>
EOT;
			if ($rm_RMSM) {
				$renderedHTML .= <<<EOT
								<tr><th colspan=7>$class_name[$class]</th></tr>
								<tr><th class=td_1_c>SM/RM</th><th class=td_1>WK</th><th class=td_1>Name</th><th class=td_1>Immatr.</th><th class=td_1>Flugzeug</th><th class=td_1>Index</th></tr>
EOT;
			} else {
				$renderedHTML .= <<<EOT
								<tr><th colspan=6>$class_name[$class]</th></tr>
								<tr><th class=td_1>WK</th><th class=td_1>Name</th><th class=td_1>Immatr.</th><th class=td_1>Flugzeug</th><th class=td_1>Index</th></tr>
EOT;
			}
			$cnt = 0;
			$teilnehmer_list = new Teilnehmer ();
			$teilnehmer_list->load_class_by_wk ( $class );
			foreach ( $teilnehmer_list->list as $teilnehmer ) {
				$cnt ++;
				$total ++;
				// Wettbewerbskennzeichen in Uppercase
				$wk = strtoupper ( $teilnehmer ['wk'] );
				$immatrikulation = urlencode ( $teilnehmer ['immatrikulation'] );

				$teamname = $teilnehmer ['teamname'];
				$index = $teilnehmer ['indexwert'];
				$pilot1 = $teilnehmer ['name'] . " " . $teilnehmer ['vorname'];
				$flugzeug = $teilnehmer ['flugzeug'];
				$name = ($class == $rm_TeamClassIndex) ? $teamname : $pilot1;

				$comment = ($teilnehmer ['junior'] == 'ja') ? 'ja' : '';

				if ($rm_RMSM) {
					if (preg_match ( '/nur rm/', strtolower ( $teilnehmer ['angaben'] ) )) {
						$tmp_image = '<img src=' . $rm_icon_url . '/ag.png width="18">';
						$renderedHTML .= <<<EOT
														<tr>
															<td class=td_1_c>{$tmp_image}</td>
EOT;
					} else {
						$tmp_image = '<img src=' . $rm_icon_url . '/ch.png width="18">';
					}
				} 

				$renderedHTML .= <<<EOT
														<td class=td_1_c>&nbsp;{$wk}&nbsp;</td>
														<td class=td_1>&nbsp;{$name}&nbsp;</td>
														<td class=td_1_c>&nbsp;{$immatrikulation}&nbsp;</td>
														<td class=td_1>&nbsp;{$flugzeug}&nbsp;</td>
														<td class=td_1_r>&nbsp;{$index}&nbsp;</td>
													</tr>
EOT;
			}
			$tnTotal = ($cnt > 0) ? "$cnt Teilnehmer" : "noch niemand";
			$renderedHTML .= <<<EOT
												<tr><th colspan=7>{$tnTotal}</th></tr>
											</table>
										</td>
EOT;
		}
	}
}
$renderedHTML .= <<<EOT
			</tr>
		</table>
		<div class=rm_h2>Total {$total} Teilnehmer</div>
	</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Teilnehmer', 'page', $renderedHTML);

?>


