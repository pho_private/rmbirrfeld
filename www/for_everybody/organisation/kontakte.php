<?php
require_once ('inc.php');

function okEntry(string $title, string $firstname, string $fullname, string $pic_small) {

    return <<<EOT
			<div class="ok_entry">	
				<div class="ok_text">
					<b>{$title}</b><br />{$fullname}
				</div>
				<div class="ok_image">
					<img src="../../resources/photos/organisation/{$pic_small}" alt="{$firstname}">
				</div>
			</div>

	EOT;
}

$renderedHTML = "";

switch ($rm_ausrichter) {
    case "AFG" :
        $renderedHTML .= okEntry( 'Pr&auml;sident',     'Pascal', 'Pascal Schneider', 'pascal_schneider_small.jpg');
        $renderedHTML .= okEntry( 'Konkurrenzleiter',   'Raphael', 'Raphael Zimmermann', 'Raphael_Zimmermann.jpg');
        $renderedHTML .= okEntry( 'Konkurrenzleiter',   'Daniel', 'Daniel M&uuml;ller 1', 'Daniel_Mueller_1.jpg');
        $renderedHTML .= okEntry( 'Meteo',              'Olivier', 'Olivier Liechti', 'Olivier_Liechti_middle.jpg');
        $renderedHTML .= okEntry( 'Schlepp',            'Bruno', 'Bruno Guidi', 'Bruno_Guidi_middle.jpg');
        $renderedHTML .= okEntry( 'Flugbetrieb',        'Alex', 'Alex Zwahlen', 'nopic.jpg');
        $renderedHTML .= okEntry( 'Flugbetrieb',        'Emmanuel', 'Emmanuel Heer', 'Emmanuel_Heer.jpg');
        $renderedHTML .= okEntry( 'Luftraumchef',       'David', 'David Humair', 'David_Humair.jpg');
        $renderedHTML .= okEntry( 'Auswertung',         'Christian', 'Christian Dornes', 'christian_dornes_small.jpg');
        $renderedHTML .= okEntry( 'Auswertung',         'Loïc', 'Loïc Germeau', 'loic_germeau_small.jpg');
        $renderedHTML .= okEntry( 'Restaurant',         'Pascal', 'Pascal Schneider', 'pascal_schneider_small.jpg');
        $renderedHTML .= okEntry( 'Web Portal',         'Peter', 'Peter Hochstrasser', 'Peter_Hochstrasser_middle.jpg');
        $renderedHTML .= okEntry( 'Web Portal',         'Philipp', 'Philipp Lüthi', 'philipp_luethi_small.jpg');
		$renderedHTML .= okEntry( 'Medien / PR',        'Pascal', 'Pascal Schneider', 'pascal_schneider_small.jpg');
        $renderedHTML .= okEntry( 'Sponsoring',         'Pascal', 'Pascal Schneider', 'pascal_schneider_small.jpg');
		$renderedHTML .= okEntry( 'Aussenlandungen',    '+41 79 825 8263', '+41 79 825 8263', 'al-small.jpg');
    	
        break;

    case "SFB" :
    	$renderedHTML .= okEntry( 'Pr&auml;sident',     'Andi', 'Andi Hofer', 'andi-02.jpg');
    	$renderedHTML .= okEntry( 'Konkurrenzleiter',   'Andi', 'Andi Hofer', 'andi-03.jpg');
    	$renderedHTML .= okEntry( 'Meteo',              'Pascal', 'Pascal Wenker', 'p_wenker.jpg');
    	$renderedHTML .= okEntry( 'Auswertung',         'Marcel &amp; Ren&eacute;', 'Marcel Signer<br>Ren&eacute; Hunziker', 'Marcel_Signer_middle.jpg');
    	$renderedHTML .= okEntry( 'Restaurant',         'Ruedi &amp; Walter', 'Ruedi Bieri<br>Walter Spatny', 'RR.jpg');
    	$renderedHTML .= okEntry( 'Restaurant',         'Wolf &amp; Peter', 'Wolf Lobato<br>Peter Stutz', 'nopic.jpg');
    	$renderedHTML .= okEntry( 'Schlepp',            'Bruno', 'Bruno Guidi', 'Bruno_Guidi_middle.jpg');
    	$renderedHTML .= okEntry( 'Flugbetrieb',        '-', '-', 'nopic.jpg');
    	$renderedHTML .= okEntry( 'Web Portal',         'Peter', 'Peter Hochstrasser', 'Peter_Hochstrasser_small.jpg');
    	$renderedHTML .= okEntry( 'Medien',      		'Andi', 'Andi Hofer', 'andi-04.jpg');
    	$renderedHTML .= okEntry( 'Sponsoring',   		'Andi', 'Andi Hofer', 'andi-05.jpg');
		$renderedHTML .= okEntry( 'Aussenlandungen',    '+41 79 xxx xxxx', '+41 79 xxx xxxx', 'al-small.jpg');
    	
        break;

    case "SGL" :
    	$renderedHTML .= okEntry( 'OK Pr&auml;sidium', 	'René', 'René Dubs', 're_dubs_middle.jpg');
		$renderedHTML .= okEntry( 'Konkurrenzleitung',  'Richi', 'Richi H&auml;chler', 'Richard_Haechler_Form_middle.jpg');
		$renderedHTML .= okEntry( 'Auswertung',         'Michi', 'Michi Wegmann', 'Michael_Wegmann_middle.jpg');
		$renderedHTML .= okEntry( 'Auswertung',         'J&uuml;rg', 'J&uuml;rg Keller', 'nopic.jpg');
    	$renderedHTML .= okEntry( 'Meteo',              'Erwin', 'Erwin Villiger', 'nopic.jpg');
    	$renderedHTML .= okEntry( 'Meteo-Pilot',        '?', '?', 'nopic.jpg');
		$renderedHTML .= okEntry( 'Startliste',         'J&ouml;rg', 'J&ouml;rg Muff', 'Joerg_Muff_middle.jpg');
    	$renderedHTML .= okEntry( 'Restaurant',         '?', '?', 'nopic.jpg');
    	$renderedHTML .= okEntry( 'Flugbetrieb',        'Roland', 'Roland Rebmann', 'RebmannRoland_2015_middle.jpg');
		$renderedHTML .= okEntry( 'Luftraum',    	    'Roland', 'Roland Rebmann', 'RebmannRoland_2015_middle.jpg');
    	$renderedHTML .= okEntry( 'Schlepp',            'Bruno', 'Bruno Guidi', 'Bruno_Guidi_middle.jpg');
    	$renderedHTML .= okEntry( 'Infrastruktur',    	'Mark', 'Mark K&auml;ppeli', 'Mark_Kaeppeli_middle.jpg');
    	$renderedHTML .= okEntry( 'Medien',       		'?', '?', 'nopic.jpg');
    	$renderedHTML .= okEntry( 'Sponsoring',       	'Chris', 'Chris Mayer', 'Chris_Mayer_middle.jpg');
		$renderedHTML .= okEntry( 'Marketing',       	'Chris', 'Chris Mayer', 'Chris_Mayer_middle.jpg');
    	$renderedHTML .= okEntry( 'Kasse',    			'Daniel', 'Daniel Blaser', 'BlaserDaniel_2024_middle.jpg');
    	$renderedHTML .= okEntry( 'Web',      			'Peter', 'Peter Hochstrasser', 'Peter_Hochstrasser_small.jpg');
		$renderedHTML .= okEntry( 'Web',  				'Richi', 'Richi H&auml;chler', 'Richard_Haechler_Form_middle.jpg');
		$renderedHTML .= okEntry( 'Aussenlandungen',    'Siehe Aufgabenblatt', 'Siehe Aufgabenblatt', 'al-small.jpg');
    	
        break;
}
    	$renderedHTML .= okEntry( 'Flugplatz Birrfeld', '+41 56 464 4040', '+41 56 464 4040', '../../graphics/sponsoren/logo_red.png');

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('OK - Kontakte', 'ok', $renderedHTML, NULL, TRUE);

?>
