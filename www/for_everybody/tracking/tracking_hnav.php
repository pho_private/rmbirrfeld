<?php
require_once ('inc.php');
require_once ('tracking_glide_and_seek.php');

// create competitions files just in time.
// sub takes care of directory, and checks if files already exist.

gsTrackingWriteCompetitionFiles(false);


$renderedHTML = <<<EOT
            <div class="hnav_item"><a href="https://glideandseek.com" target="_blank"><h2>Glide And Seek</h2></a></div>

EOT;

foreach ($class_key as $class) {
	$compfilename = getGsCurrentCompetitionFileName($class);
	$compname = ($class == 0) ? 'Alle Klassen' : $class_name[$class];
	$renderedHTML .= <<<EOT
            <div class="hnav_item"><a href="http://www.glideandseek.com/?compDataUrl={$rm_tracking_dir_url}/$compfilename" target="_blank">{$compname}</a></div>

EOT;
}
$renderedHTML .= <<<EOT
            <div class="hnav_item"><a href="https://www.glideandseek.com/?compDataUrl=https://www.rmbirrfeld.ch:443/www/trk/competition-test.json" target="_blank">Test</a></div>
EOT;
// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('Live Tracking', 'hnav', $renderedHTML);

?>
