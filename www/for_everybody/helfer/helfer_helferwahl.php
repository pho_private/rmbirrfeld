<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once ('inc.php');
require_once ('helfer_inc.php');
require_once ('dblib.inc.php');
require_once ('functions.php');



// --------------------------------------------------------------------------------
// main code
//

// set values for fields in new helfer registration
// these will usually not be specified in $_REQUEST, but let's just do as if...
$vorname = getRequestOrInit('vorname');
$name = getRequestOrInit('name');
$jahrgang = getRequestOrInit('jahrgang');
$mail = getRequestOrInit('mail');
$phone = getRequestOrInit('phone');
$ort = getRequestOrInit('ort');
$gruppe = getRequestOrInit('gruppe', $rm_helfer_default_gruppe);
$clubSelection = ausgabeHelferVereinOptionen($gruppe);

// --------------------------------------------------------------------------------
// no output above this comment
//
$vorname_class = ($vorname == "") ? "td_e" : "td_1";
$name_class = ($name == "") ? "td_e" : "td_1";
$ort_class = ($ort == "") ? "td_e" : "td_1";
$jahrgang_class = ($jahrgang == "") ? "td_e" : "td_1";
$phone_class = ($phone == "") ? "td_e" : "td_1";
$mail_class = ($mail == "") ? "td_e" : "td_1";

if ($vorname == "" || $name == "" || $ort == "" || $jahrgang == "" || $phone == "" || $mail == "") {
    $link = "helfer_helferwahl.php";
    $text = "pr&uuml;fen";
} else {
    $link = "helfer_einsatz.php";
    $text = "anmelden";
}

$renderedHTML = <<< EOT
		<div class=rm_h1>Einleitung</div>
		<div class=big_col_block>	
			<div class=rm_h2>Liebe Segelflieger/Innen, Liebe Freunde/Innen des Segelfluges</div>
			<div class=rm_text>Vom 16. bis am 24. Juni 2018 finden im Birrfeld die Schweizer und Aargauen Segelflugmeisterschaften statt.  
			Die Veranstaltung soll f&uuml;r die Teilnehmenden, die Zuschauer und die G&auml;ste zum
			unvergesslichen Erlebnis werden. Grossen Wert wird auf aktuelle
			Wettbewerbsinformationen und auf ein attraktives Rahmenprogramm gelegt.
			S&auml;mtliche Informationen, Resultate und Bilder werden laufend auf
			dieser Homepage ver&ouml;ffentlicht. <br />
			<br /> All diese Aktivit&auml;ten sind jedoch nicht ohne Hilfe aller Birrfelder Segelflieger und Freunde des Segelfluges m&oumlglich. Daher bitten wir Dich um tatkr&auml;ftige Unterst&uuml;tzung dieses Anlasses. Es w&uuml;rde uns freuen, Dich als Helfer dabei zu haben. <br />
			<br />
			<div class=rm_h1>Anmeldung</div>
			<div class=big_col_block>
				<div class=rm_text>Hier kannst du dich f&uuml;r Eins&auml;tze melden, bzw. deine Eins&auml;tze &auml;ndern. Bitte beachte, dass bei Anpassungen in alle Felder die selben Werte einzugeben sind, wie bei der Anmeldung.<br/>Der Jahrgang wird ben&ouml;tigt, falls zwei Personen gleichen Namens im selben Ort wohnen; er erscheint nie auf &ouml;ffentlichen Seiten.
					<a href="mailto:ok@smbirrfeld.ch?subject=Aenderung%20Einsatzplan"><br /><br />Bei Schwierigkeit oder Fragen wende die bitte an das OK.</a>
					<br />
					<br />
				</div>
				<div class=rm_text>				
					<form method="POST" action="{$link}">
						<table>
							<colgroup>
								<col width='14%'>
								<col width='14%'>
								<col width='5%'>
								<col width='23%'>
								<col width='14%'>
								<col width='10%'>
								<col width='20%'>
							</colgroup>
							<tr>
								<th class=th_1>Vorname</th>
								<th class=th_1>Name</th>
								<th class=th_1>Jahrgang</th>
								<th class=th_1>E-Mail</th>
								<th class=th_1>Tel.</th>
								<th class=th_1>Wohnort</th>
								<th class=th_1>Gruppe</th>
							</tr>
							<tr>
								<td class="{$vorname_class}"><input type=text size=10 name="vorname" value="{$vorname}"></td>
								<td class="{$name_class}"><input type=text size=15 name="name" value="{$name}"></td>
								<td class="{$jahrgang_class}"><input type=text size=05 name="jahrgang" value="{$jahrgang}"></td>
								<td class="{$mail_class}"><input type=text size=25 name="mail" value="{$mail}"></td>
								<td class="{$phone_class}"><input type=text size=20 name="phone" value="{$phone}"></td>
								<td class="{$ort_class}"><input type=text size=20 name="ort" value="{$ort}"></td>
								<td class="td_1">{$clubSelection}</td>
							</tr>
						</table>
						<table>
							<tr>
								<td>
									<input type=submit name=submit value="{$text}">
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Helfer - Helferauswahl', 'page', $renderedHTML);

?>
