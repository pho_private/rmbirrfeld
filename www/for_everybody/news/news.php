<?php
require_once ('inc.php');
require_once ('functions.php');

if ($rm_use_helfer) {
    require_once ('helfer_2018_inc.php');
}

// Datumsrechnung - wieviele Tage dauert's noch, bis die RM/SM losgeht
$datum = getRequestOrInit('datum', $rm_new_year);
if ($datum < $rm_dates[0])
{
    $datum = $rm_new_year;
}
$isVorbereitung = ($datum == $rm_new_year);


$int_date = strtotime($datum);
$compDay = date("w", $int_date);
$this_year = date("Y", $int_date);
$this_month = date("n", $int_date);

$start_date = strtotime($rm_dates[0]);
$start_year = date("Y", $start_date);
$start_month = date("n", $start_date);
$start_day = date("d", $start_date);

$rest = date("z", mktime(0, 0, 0, $start_month, $start_day, $start_year)) - date("z");
if ($rest < 0) $rest += 365;


$newsDates = new NewsDates($rm_new_year);
$titel = '';
$newsDatePublished = false;
foreach ($newsDates->list as $newsDate) {
    if ($newsDate['datum'] != $datum) {
        continue;
    }
    $titel = $newsDate['titel'];
    $newsDatePublished = ($newsDate['released'] == 'on');
    break;
}

// Birrfeld Roundshot Kamera Einbindung
$renderedHTML = <<<EOT
    <div class="roundshot">
		<script src="https://backend.roundshot.com/js/roundshot_animated_thumbnail.js" type="text/javascript"></script>
		<script type="text/javascript"> roundshot.animate({ picture: "https://backend.roundshot.com/cams/341/thumbnail", link: "https://birrfeld.roundshot.com/", angle: 360, width: 1000, height: 120, bounce: false, open_in_new_tab: true }); </script>
    </div>
EOT;

$newsMsg = "";

if ($isVorbereitung) {
    $result = DBQuery("SELECT COUNT(*) FROM {$rm_tbl_teilnehmer} WHERE rmactiv = 'ja'");
    $row = DBFetchRow($result);
    $total = $row[0];

	if 		($total < 1)	$msgTN = "Es sind noch keine Teilnehmer angemeldet";
	elseif	($total < 2)	$msgTN = "Es ist ein Teilnehmer angemeldet";
	else					$msgTN = "Es sind {$total} Teilnehmer angemeldet";

	$errMsg = '';

	if ($rm_use_helfer) {
	    $result = DBQuery("SELECT count(*) FROM {$rm_tbl_einsaetze} where deleted is NULL");
		 $row = DBFetchRow($result);
	    $total_meldungen = $row[0];
		$left = $total_einsaetze - $total_meldungen;
	    if 		($left < 1)			$msgHLPR = "| Es sind alle Helfereins&auml;tze besetzt! Danke!";
		elseif 	($left < 2)			$msgHLPR = "| Es ist noch 1 Helfereinsatz offen.";
		else						$msgHLPR = "| Es sind noch {$left} Helfereins&auml;tze offen.";
	} else {
		$msgHLPR = '';
	}
	$newsMsg = "Noch $rest Tage | $msgTN $msgHLPR";
} else {
    $newsMsg = "News am " . getWeekdayDateString($datum);
}
$renderedHTML .= <<<EOT
 		<div class="rm_h1">$newsMsg</div>
		<div class=big_col_block>

EOT;


if ($newsDatePublished) {
	$count = 0;
    // freigegebene News-Items für selektiertes Datum holen
    $newsItems = new NewsItems($datum);
    foreach ($newsItems->list as $newsItem)
	{
//		if ($isVorbereitung && $count++ > 3 && !$more) break;
		if ($newsItem['released'] == 'off')		continue;
        $tmp = (($isVorbereitung) ? "" : substr($newsItem['zeit'], 0, 5)." | ");
        $renderedHTML .= <<<EOT
		  	<div class=rm_h2>
                {$tmp}{$newsItem['titel']}
            </div>
			<div class=rm_text>
                {$newsItem['text']}
            </div>

EOT;
    }


}
	// close big_col_block div
	$renderedHTML .= <<<EOT

		</div> <!-- big_col_block -->

EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('News', 'page', $renderedHTML);

?>

