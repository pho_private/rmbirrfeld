<?php
/*
 * inc.php - definition of various technical and application specific settings
 *
 * All helfer relevant parts have been moved to helfer_inc.php in the include directory.
 */

// some more or less static texts:

// Austragungsjahr
$rm_year = '2025';
// für Vorbereitungs-News
$rm_new_year = $rm_year . '-01-01';

// out of popular demand
$today = date('Y-m-d');

// Wettbewerbstage als einzelne Daten der Form "JJJJ-mm-dd", wobei das Jahr aus der Konfig oben genommen wird.
// ACHTUNG: DIESE DATEN MUESSEN AUCH IN DER DATENBANKTABELLE news ERFASST WERDEN!
// Zusaetzlich noch ein Vorfeld-Record, vom 1.1. bis zum Start des Wettbewerbes.
$rm_dates = array(
    $rm_year . '-05-01',
    $rm_year . '-05-02',
	$rm_year . '-05-03',
    $rm_year . '-05-04'
);

// Daten als Text-String
$rm_datum = '1. - 4. Mai ' . $rm_year;

// RM oder SM?
$rm_comp_type = 'RM';
// $rm_comp_type = 'SM';


// wird der Rückholservice angeboten?
$rm_angebot_rueckholer = true;

// RM Preise
$rm_nenngeld_senior = 120;
$rm_nenngeld_junior = 80;
$rm_nenngeld_team = 160;
$rm_rueckholen_senior = 200;
$rm_rueckholen_junior = 100;
$rm_camping_senior = 50;
$rm_camping_junior = 0;
$rm_barzuschlag = 10;

// SM Preise
$sm_nenngeld_senior = 250;
$sm_nenngeld_junior = 150;
$sm_rueckholen_senior = 200;
$sm_rueckholen_junior = 100;
$sm_camping_senior = 50;
$sm_camping_junior = 50;
$sm_barzuschlag = 10;

//
// Konfiguration der Modul-Nutzung
//
// Statt die Module an verschiedensten Stellen ein- und ausblenden zu müssen, wird hier definiert, ob sie verwendet werden oder nicht.
//
// Verwendung der Helfer-Verwaltung - wenn nein, so werden alle entsprechenden Links und Seiten eingebaut, sonst fehlen sie.
$rm_use_helfer = FALSE;

$rm_logo_file = "{$rm_comp_type}_" . $rm_year . '_Logo.png'; // this may be svg, png, gif, jpg
$rm_logo_ok_file = "{$rm_comp_type}_" . $rm_year . '_Logo.png'; // this may be svg, png, gif, jpg
$rm_logo_file_png = "{$rm_comp_type}_" . $rm_year . '_Logo.png'; // this may be png only



$soaringspot_url = 'https://www.soaringspot.com/de';
// diese URL liefert SoaringSpot als PermaLink. Bitte checken dass der Wettbewerb entsprechend benannt wird - oder URL unten eintragen.
$rm_soaringspot_competition_url = 'rm-birrfeld-2025';
$jsm_soaringspot_competition_url = 'jsm-birrfeld-2022';

$rm_soaringspot_url = $soaringspot_url . '/' . $rm_soaringspot_competition_url;
$jsm_soaringspot_url = $soaringspot_url . '/' . $jsm_soaringspot_competition_url;


//WhatsApp Gruppe für OK --> Piloten
$rm_whatsapp_url = 'https://chat.whatsapp.com/L996o7lrzd3Ew1zChR5T6r';

// Die WhatsApp Gruppe für Piloten
$rm_whatsapp_teilnehmer_url = 'https://chat.whatsapp.com/JGONhPtROZaAtsh4qXn2dy'; 

// Die WhatsApp Gruppe für OK unter sich
$rm_whatsapp_ok_url = 'https://chat.whatsapp.com/EXlC78IjKLl9MfIXpnpFJr';

// definieren des Ausrichters, damit die Listen unten richtig gesetzt werden.
// TODO: Verlegen der OK-Ämter in die DB, und Konfiguration via Datenbank.

//$rm_ausrichter = "AFG";
$rm_ausrichter = "SGL";
// $rm_ausrichter = "SFB";

$msgSelect = "Bitte w&auml;hle aus...";
// Klassen name und keys -- Hier bestimmst Du die Klassennamen, und nirgends sonst -- in der DB wird nur eine Zahl abgelegt.
// Achtung: Alle Klassen aus $class_name, die mit Index gewertet werden, müssen in $class_indexed eine 1 gesetzt haben!
// nicht verwendete Klassen bitte nur auskommentieren!
$class_name = array(
    "0" => $msgSelect
    , "1" => "Offene" // ohne Index
    , "2" => "mixed Offene" // mit Index
    , "3" => "20m 2-Sitzer"
    , "4" => "18m"
    , "5" => "15m"
    , "6" => "mixed 15m"
    , "7" => "Standard"
    // , "8" => "Team"
    //, "9" => "Club"
    // , "10" => "JSM"
);
// mit den nächsten zwei Variablen wird die Teamklasse definiert.
$rm_TeamClass = FALSE;
$rm_TeamClassIndex = 8;

// Doppelwettbewerb (RM/SM) oder nur ein Wettbewerb (RM oder SM)?
$rm_RMSM = FALSE;

// ist eine Klasse indiziert oder nicht?
$class_indexed = array(
    "0" => "0",
    "1" => "0",  // offiziell NICHT indexiert
    "2" => "1",  // drum braucht's die, wenn wir verschiedene Flz. in die Klasse legen.
    "3" => "0",
    "4" => "0",
    "5" => "0",
    "6" => "1",
    "7" => "0",
    "8" => "0",
    "9" => "0",
    "10" => "1"
);

$class_bgcolor = array(
    "0" => "0",
    "1" => "LightSalmon",
    "2" => "ff6666",
    "3" => "Plum",
    "4" => "PaleTurquoise",
    "5" => "lightblue",
    "6" => "66ff66",
    "7" => "PaleGreen",
    "8" => "blue",
    "9" => "red",
    "10" => "6666ff"
);

// Colors for track and aircraft flags.
// DO NOT CHANGE THESE COLORS without consulting
// https://glideandseek.com/about/interfaces/comp-data-interface
//
// Failure to comply with the spec above will lead to non-displaying aircraft flags in tracking.
//
$class_tracking_color = array(
    "0" => "0",
    "1" => "#BF211E",  // venetian red
    "2" => "#BF211E",  // venetian red
    "3" => "#F2750B",  // safety orange
    "4" => "#F20B87",  // flickr pink
    "5" => "#89F20B",  // lawn green
    "6" => "#89F20B",  // lawn green
    "7" => "#0074D9",  // bright navy blue
    "8" => "#0BF2EA",  // fluorescent blue
    "9" => "#0B13F2",  // blue
    "10" => "#770BF2"  // electric indigo
);
// names of classes in Soaringspot Competition definition.
// need to match, or tracking and competition scoring will not work.
$class_tracking_name = array(
    "0" => $msgSelect,
    "1" => "offene",
    "2" => "mix-offen",
    "3" => "20m-ds",
    "4" => "18m",
    "5" => "15m",
    "6" => "mix-15m",
    "7" => "std",
    "8" => "team",
    "9" => "club",
    "10" => "jsm"
);

if ($rm_TeamClass) {
    $rm_TeamClassname = $class_name[$rm_TeamClassIndex];
} else {
    unset($class_name[$rm_TeamClassIndex]);
    unset($class_bgcolor[$rm_TeamClassIndex]);
    unset($class_indexed[$rm_TeamClassIndex]);
    $rm_TeamClassname = "";
}

$class_key = array_flip($class_name);

switch ($rm_ausrichter) {
    case "AFG":
        // AFG - Mailers
        // recipients of registration mails. Separate multiple recipients with ', ' (comma space)
        $mailto_list = 'peter.hochstrasser@gmx.net, pascal_schneider@hotmail.com';

        // members of organisation comittee.
        $mailto_list_ok = 'peter.hochstrasser@gmx.net'; // Peter Hochstrasser
        $mailto_list_ok .= ', ' . 'pascal_schneider@hotmail.com'; // Pascal Schneider
        $mailto_list_ok .= ', ' . 'ziraphae@gmail.com'; // Raphael Zimmermann
        // $mailto_list_ok .= ', ' . 'michael.geisshuesler@gmail.com'; // Michael Geisshüsler
        // $mailto_list_ok .= ', ' . 'ofaist@live.com'; // Olivier Faist
        $mailto_list_ok .= ', ' . 'christian.dornes@gmail.com'; // Christian Dornes
        // $mailto_list_ok .= ', ' . 'roger.walt@gmail.com'; // Roger Walt
        $mailto_list_ok .= ', ' . 'alexander.zwahlen@hotmail.com'; // Alex Zwahlen
        $mailto_list_ok .= ', ' . 'rtfm89@gmail.com'; // David Humair
        $mailto_list_ok .= ', ' . 'daniel.schoeneck@hb-536.ch'; // Daniel Schoeneck
        // $mailto_list_ok .= ', ' . 'clemente.dalmagro@acm.org'; // Clemente dal Magro
        $mailto_list_ok .= ', ' . 'OlivierLiechtiAuK@compuserve.com'; // Olivier Liechti
        // $mailto_list_ok .= ', ' . 'max.humbel@hispeed.ch'; // Max Humbel
        // $mailto_list_ok .= ', ' . ''; //
       
        /*
         * Template, for extending the list above
         * $mailto_list_ok .= ', ' . ''; //
         */
        // recipient list of test news etc.
        $mailto_list_test = 'peter.hochstrasser@gmx.net, pascal_schneider@hotmail.com';
        $rm_ausrichter_lang = 'Akademische Fluggruppe Zürich';

        break;

    case "SFB":
        // SFB - Mailers
        $mailto_list = '';

        // recipients of registration mails. Separate multiple recipients with ', ' (comma space)
        $mailto_list_ok = ', andreas.hofer@psi.ch';
        // $mailto_list_ok .= 'marcelsigner@hotmail.com';
        // $mailto_list_ok .= ', markus.berner@mht.ch';
        // $mailto_list_ok .= ', hunzikerrene@bluewin.ch';
        // $mailto_list_ok .= ', waspa@bluewin.ch';
        // $mailto_list_ok .= ', rudolf_hanni.bieri@bluewin.ch';
        // $mailto_list_ok .= ', pascalwenker@gmail.com';
        // $mailto_list_ok .= ', luca.scheuchzer@hispeed.ch';
        // $mailto_list_ok .= ', koswald@bluewin.ch';
        // $mailto_list_ok .= ', schoetti@gmx.ch';
        // $mailto_list_ok .= ', rmarley@hispeed.ch';
        // $mailto_list_ok .= ', peter.keller.sgl@gmx.ch';
        // $mailto_list_ok .= ', kneuss.e@bluewin.ch';
        // $mailto_list_ok .= ', sfschlepp@swissonline.ch';

        $mailto_list_test = 'andreas.hofer@psi.ch';
        $mailto_list_test .= ', peter@hochstrasser.net';

        $rm_ausrichter_lang = 'Segelfluggruppe Birrfeld';
        break;

    case "SGL":
        // SGL - Mailers
        // recipients of registration mails. Separate multiple recipients with ', ' (comma space)
        $mailto_list = 'rihaechler@bluewin.ch';

        $mailto_list_ok = ' thomas.schiesser@toschi-academy.ch';
        $mailto_list_ok .= ',rihaechler@bluewin.ch';
        $mailto_list_ok .= ',olivierliechtiauk@compuserve.com';
        $mailto_list_ok .= ',bdmueller@swissonline.ch';
        $mailto_list_ok .= ',koswald@bluewin.ch';
        $mailto_list_ok .= ',michael.wegmann@hispeed.ch';
        $mailto_list_ok .= ',rmarley@hispeed.ch';
        $mailto_list_ok .= ',koswald@bluewin.ch';
        $mailto_list_ok .= ',markus.romer@greenmail.ch';
        $mailto_list_ok .= ',sfschlepp@swissonline.ch';
        $mailto_list_ok .= ',joerg.muff@swissonline.ch';
        $mailto_list_ok .= ',chris.mayer@silentwings.us';
        $mailto_list_ok .= ',th.naegeli@gmx.ch';
        $mailto_list_ok .= ',mark.kaeppeli@rothpletz.ch';
        $mailto_list_ok .= ',mpanholzer@vmware.com';
        $mailto_list_ok .= ',paul.a.keller@bluewin.ch';
        $mailto_list_ok .= ',waspa@bluewin.ch';
        $mailto_list_ok .= ',kneuss.e@bluewin.ch';
        $mailto_list_ok .= ',kathrin.keller@aikq.org';
        $mailto_list_ok .= ',kaschafroth@bluewin.ch';

        $mailto_list_test = 'rihaechler@bluewin.ch';
        $rm_ausrichter_lang = 'Segelfluggruppe Lenzburg';
        break;
}

// Database Table Names
$rm_table_config = 'config';
$rm_tbl_einsaetze = 'einsaetze';
$rm_tbl_emails = 'emails';
$rm_tbl_helfer = 'helfer';
$rm_tbl_newsDates = 'news';
$rm_tbl_newsItem = 'newsline';
$rm_tbl_ok = 'ok';
$rm_tbl_pwreset = 'passwordreset';
$rm_tbl_teilnehmer = 'teilnehmer';

// -----------------------
// DO NOT CHANGE -- debug standard value
// add a $debug = true; to declaration of the server below where you want debug to be active.
// this way, debug is not running in PRD environment.
// -----------------------
$debug = false;
$dblib_debug = false;
$debug_msg = '';

/*
 * Web site structure constants:
 * site structure is:
 * . the website top directory
 * ./www/ = $rm_root (where the main code is located, below the actual web site top directory)
 * ./www/for_admin = administration code
 * ./www/for_everybody = public code
 * ./www/resources = resources (data, photos, graphics etc.)
 * ./www/trk = location for all tracking data, like aircraft data for glide and seek, various csv's for other tracking sites, etc.
 * ./include/ = this directory (__DIR__)
 * ./security/ = contains all the security definitions of the site.
 * ./ok/ = contains the index.php that fowards to the admin UI of the site.
 * So, admin users (OK, Auswertung, Webmaster) just open
 * https://rmbirrfeld.ch/ok
 * to open the admin pages and log in.
 * Access requires a uid:password line stored in security/.htpasswd
 */

// we use automatic redirect to HTTPS for all non-encrypted requests.
// Therefore, we always add port 443 for all requests.

$rm_protocol = 'https://';

$base = $_SERVER['DOCUMENT_ROOT'];

$rm_domain = $_SERVER['SERVER_NAME'];
$rm_web_url = $rm_domain . ":" . $_SERVER['SERVER_PORT'];

$rm_web_offset = '';

switch ($rm_web_url) { // test domain
    case 'rmbirrfeld.ch:443':
        {
            $rm_web_url = 'www.' . $rm_web_url; // for the shortcut fanatics...
            // this continues to next break!
        }
    case 'www.rmbirrfeld.ch:443':
        { // "the real thing"
            $rm_server = 'localhost';
            $rm_db = 'rmbirrfeld_prod';
            $rm_user = 'rm_prg';
            $rm_password = 'fly4ls4';

            $mailto_list = 'peter@hochstrasser.net'; // recipients of registration mails. Separate multiple recipients with ', ' (comma space)
            $rm_webadmin_url = "Https://jovinus.metanet.ch:8443";
            $rm_web_offset = '/www';
            $rm_pfbc_jsIncludesPath = '/include/includes';

            break;
        }

    case 'smbirrfeld.ch:443':
        {
            $rm_web_url = 'www.' . $rm_web_url; // for the shortcut fanatics...
            // this continues to next break!
        }
    case 'www.smbirrfeld.ch:443':
        { // "the real thing"
            $rm_server = 'localhost';
            $rm_db = 'smbirrfeld_prod';
            $rm_user = 'sm_prg';
            $rm_password = 'Fwfc59~0';

            $mailto_list = 'peter@hochstrasser.net'; // recipients of registration mails. Separate multiple recipients with ', ' (comma space)
            $rm_webadmin_url = "https://smbirrfeld.ch:8443";
            $rm_web_offset = '/www';
            $rm_pfbc_jsIncludesPath = '/include/includes';

            break;
        }

    case 'rm.hochstrasser.net:443':
        {
            $rm_server = 'localhost';
            $rm_db = 'rmbirrfeld_test';
            $rm_user = 'rm_prg_test';
            $rm_password = 'Yck9y4!5';

            $mailto_list = 'peter@hochstrasser.net'; // recipients of registration mails. Separate multiple recipients with ', ' (comma space)
            $rm_webadmin_url = "https://hochstrasser.net:8443";
            $rm_web_offset = '/www';
            $rm_pfbc_jsIncludesPath = '/include/includes';
            $debug = true;
            $dblib_debug = false;

            break;
        }

    case 'sm.hochstrasser.net:443':
        {
            $rm_server = 'localhost';
            $rm_db = 'smbirrfeld_test';
            $rm_user = 'sm_prg_test';
            $rm_password = 'Yt4pf7$9';

            $mailto_list = 'peter@hochstrasser.net'; // recipients of registration mails. Separate multiple recipients with ', ' (comma space)
            $rm_webadmin_url = "https://hochstrasser.net:8443";
            $rm_web_offset = '/www';
            $rm_pfbc_jsIncludesPath = '/include/includes';
            $debug = true;
            $dblib_debug = false;

            break;
        }

    case 'rm':
    case 'rm.local':
    case '127.0.0.1':
    case 'localhost':
    case 'localhost:80':
        { // on XAMPP on my Machine at home
            $rm_server = 'localhost';
            $rm_db = 'rmbirrfeld_dev';
            $rm_user = 'rmbirrfeld_prg';
            $rm_password = 'fly4ls4';

			$mailto_list = 'peter@hochstrasser.net, webmaster@rmbirrfeld.ch'; // recipients of registration mails. Separate multiple recipients with ', ' (comma space)
            $rm_webadmin_url = "http://127.0.0.1:10081/phpmyadmin";
            $rm_web_offset = '/www';
            $rm_pfbc_jsIncludesPath = '/include/includes';

            break;
        }
    default:
        {
            echo 'please define site and database parameters for host "' . $rm_web_url . '" in {__FILE__}';
        }
}
$rm_mail_noreply = "noreply@$rm_domain";
$rm_mail_webmaster = "webmaster@$rm_domain";

$rm_name_kurz = "{$rm_comp_type} " . $rm_year;
$rm_name_lang = "{$rm_comp_type} Birrfeld " . $rm_year;

$rm_root = $base . $rm_web_offset;
$rm_url = $rm_protocol . $rm_web_url;
$rm_root_url = $rm_url . $rm_web_offset;

$link = null;

$rm_resources = $rm_root . "/resources";
$rm_resources_url = $rm_root_url . "/resources";

$rm_tn_photos = $rm_resources . "/photos/teilnehmer";
$rm_tn_photos_url = $rm_resources_url . "/photos/teilnehmer";

$rm_igc = $rm_resources . "/igc/" . $rm_year;
$rm_igc_url = $rm_resources_url . "/igc/" . $rm_year;

$rm_tracking_dir = $rm_root . '/trk'; // here we store tracking related files - tasks, participants.
$rm_tracking_dir_url = $rm_root_url . '/trk';

$rm_graphics = $rm_resources . "/graphics";
$rm_graphics_url = $rm_resources_url . "/graphics";
$rm_icon_url = $rm_graphics_url . "/icons";
$rm_logo_png = $rm_graphics . "/organisation/" . $rm_logo_file_png;
$rm_logo_url = $rm_graphics_url . "/organisation/" . $rm_logo_file;
$rm_logo_ok_url = $rm_graphics_url . "/organisation/" . $rm_logo_ok_file;


$rm_favicon_url = "{$rm_graphics_url}/organisation/favicons/{$rm_ausrichter}";

$rm_sponsors_html = "{$rm_root}/for_everybody/sponsoren/sponsors.html";


$rm_msg = '';
// how many seconds do you want to show error- or success messages (like on IGC upload)
$rm_msg_delay = 3;

/**
 * allow for autoload of classes so we can reduce the number of require_once statements in the source files.
 */
spl_autoload_register(function ($class_name) {
	global $debug, $rm_msg;
	switch ($class_name) {
		case 'rm_upload':
        case 'upload':
 			$class_filename = 'class.' . $class_name;
			break;
		case 'form':
		case 'pfbc':
			$class_filename = 'class.form';
			break;
		case 'emails':
		case 'Helfer':
		case 'IGCFileNameHandler':
		case 'NewsDates':
		case 'NewsItems':
		case 'rm':
		case 'Teilnehmer':
			$class_filename = 'classes';
			break;

		default:
			$class_filename = $class_name;
			break;
	}
	if ($debug) {
		$rm_msg = $rm_msg . "<br />autoloading {$class_name} from {$class_filename}.php";
	}
	require_once ($class_filename . '.php');
});

function varDumpToString($var) {
    ob_start();
    var_dump($var);
    $result = ob_get_clean();
    return $result;
}
/* 
    renders a single sponsor entry as a line for the sponsor table.
    parameters:
    $url - URL of the sponsor's website; https://www.xxx.tld
    $img - the image file name in the <webroot>/resources/graphics/sponsors/ directory (case sensitive)
    $title - name of the sponsors, used as alt (shown when hovering on image)
 */

function sponsorEntry(string $url, string $img, string $title ) {
    global $rm_web_offset;
    $imgpath = "{$rm_web_offset}/resources/graphics/sponsoren/{$img}";
    $renderedHTML = <<<EOT
            <div class="sponsor"><a href="{$url}" target="_blank"><img src="{$imgpath}" title="{$title}"></a></div>
    EOT;
    return $renderedHTML;
}

function getSponsors($sponsors_file) {
    $rows = array_map('str_getcsv', file($sponsors_file));
    $header = array_shift($rows);
    $sponsors = array();
    foreach ($rows as $row) {
        $sponsors[] = array_combine($header, $row);
    }
    // now, we've got the sponsors in an associative array. 
    // we can "rearrange" this array so we have a different order each time.

    return $sponsors;    
}

function getSponsorDiv(string $sponsors_html) {
    $sponsors = [];
    $sponsors = file($sponsors_html, FILE_SKIP_EMPTY_LINES);
    $x = shuffle($sponsors);
    $crlf = chr(13) . chr(10);
    $sponsorsString = implode( "", $sponsors);
    $renderedHTML = <<<EOT
        <div class="sponsor_container">
            {$sponsorsString}
        </div>
    EOT;
    return $renderedHTML;
}

// ------------------------------------------------------------------------------------------------
// HTML Output for most pages - variants with and without Sponsor entries.
// ------------------------------------------------------------------------------------------------

function rm_DownloadListPage(string $listName, string $fileName = "") {

    if ($fileName != "") {
        // display download link
        $message =  "<a href=\"{$fileName}\">hier herunterladen</a>";
    } else { 
        // no participants
        $message = '<b>Keine Teilnehmer - keine Liste</b>';
    }
    $renderedHTML = <<<EOT
        <div class="rm_h1">{$listName}</div>
        <div class="rm_text">{$message}</div>
    EOT;
    return rm_displayPage($listName, 'page', $renderedHTML);
}

function rm_displayPageAndSponsors(string $pageTitle, string $pageType, string $renderedHTML, string $bodyStart = NULL, $with_hs = false) {
    global $rm_sponsors_html;

    $sponsors = getSponsorDiv($rm_sponsors_html);
    return rm_displayPage($pageTitle, $pageType, $renderedHTML, $bodyStart, $with_hs, $sponsors);
}


function rm_displayFrameset(string $pageTitle, string $frameset) {
    global $rm_web_offset, $rm_favicon_url;
    mb_convert_encoding($frameset, "UTF-8", mb_detect_encoding($frameset, "UTF-8, ISO-8859-1, ISO-8859-15", true));;

    $renderedPage = <<<EOT
<!doctype html>
<html>
	<head>
    <title>{$pageTitle}</title>
<!--
	<style type="text/css">
    	@import url({$rm_web_offset}/css/rm.css);
	</style>
-->

    <link rel="apple-touch-icon" sizes="57x57" href="{$rm_favicon_url}/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{$rm_favicon_url}/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{$rm_favicon_url}/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{$rm_favicon_url}/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{$rm_favicon_url}/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{$rm_favicon_url}/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{$rm_favicon_url}/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{$rm_favicon_url}/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{$rm_favicon_url}/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="{$rm_favicon_url}/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{$rm_favicon_url}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{$rm_favicon_url}/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{$rm_favicon_url}/favicon-16x16.png">
    <link rel="manifest" href="{$rm_favicon_url}/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{$rm_favicon_url}/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

	</head>
    {$frameset}
</html>
EOT;

    header('Content-Type: text/html; charset=UTF-8');
    echo $renderedPage;
    return;
}

/**
 *
 * @param string $pageTitle
 *            - the title in the head section
 * @param string $pageType
 *            - vnav, hnav or page
 *            vnav: vertical navigation - menu to the left, targets the right-hand frame
 *            hnav: horizontal navigation - menu at the top, targets the bottom frame
 *            page: a "normal" web page, targets itself ("_self")
 * @param string $renderedHTML
 * @param string $bodyStart
 * @param bool   $with_highslide
 * @param string $bodyEnd
 *            - usually reserved for the sponsors block.
 */
function rm_displayPage(string $pageTitle, string $pageType, string $renderedHTML, string $bodyStart = NULL, bool $with_highslide = false, string $bodyEnd = NULL) {
    global $debug, $debug_msg, $rm_favicon_url, $rm_msg, $rm_msg_delay, $_SESSION;
    $divName = '';

    if ($debug) {
        $debug_msg .= varDumpToString($_SESSION);
    }

    $pageStart = <<<EOT
    <!doctype html>
    <html>
    <head>
        <meta name="page-type" content="{$pageType}">
        <title>{$pageTitle}</title>
        <link rel="stylesheet" href="/www/css/rm.css">

        <link rel="shortcut icon" type="image/x-icon" href="{$rm_favicon_url}/favicon.ico?v=1">
        <link rel="apple-touch-icon" sizes="57x57" href="{$rm_favicon_url}/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="{$rm_favicon_url}/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="{$rm_favicon_url}/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="{$rm_favicon_url}/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="{$rm_favicon_url}/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="{$rm_favicon_url}/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="{$rm_favicon_url}/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="{$rm_favicon_url}/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{$rm_favicon_url}/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="{$rm_favicon_url}/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="{$rm_favicon_url}/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="{$rm_favicon_url}/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="{$rm_favicon_url}/favicon-16x16.png">
        <link rel="manifest" href="{$rm_favicon_url}/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{$rm_favicon_url}/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

    EOT;
    // load javascript helper functions only for pages, not for navigation
    // setMessage is used in onload event to display a success- or error message for $rm_msg_delay seconds
    //
    if ($pageType == 'page') {
    $pageStart .= <<<EOT
        <script type="text/javascript" src="/include/pagehelpers.js"></script>

    EOT;
    }
    // add highslide scripts if required
    if ($with_highslide) {
        $pageStart .= <<<EOT
        <script type="text/javascript"
            src="/include/highslide/highslide.js"></script>
        <link rel="stylesheet" type="text/css"
            href="/include/highslide/highslide.css" />
        <script type="text/javascript">
            hs.graphicsDir = '/include/highslide/graphics/';
            hs.wrapperClassName = 'borderless';
        </script>

    EOT;
    }

    switch ($pageType) {
        case 'hnav':
            $divName ='hnav';
            $pageStart .= <<<EOT
                <base target="page">
            EOT;

            $bodyStart .= <<<EOT
            <body>
                <div class={$divName}>
                    <div class="hnav_container">
            EOT;

            $bodyEnd .= <<<EOT
                    </div> <!-- hnav_container -->
            EOT;
            break;
            
        case 'ok':
            $divName ='ok_block';
            $pageStart .= <<<EOT
                <base target="_self">                
            EOT;

            $bodyStart .= <<<EOT
            <body>
                <div class="rm_h1">Das OK</div>
                <div class={$divName}>
            EOT;

            break;
        case 'page':
            $divName ='main_block';
            $pageStart .= <<<EOT
                <base target="_self">

            EOT;
            break;
        case 'vnav':
            $divName ='vnav';
            $pageStart .= <<<EOT
                <base target="frameset">

            EOT;
            break;
        default:
            echo 'unknown page type in rm_displayPage';
            break;
    }

    $pageStart .= <<<EOT
    </head>
    EOT;
    if (empty($bodyStart)) {
        $pageStart .= <<<EOT
        <body>
            <div id="rm_status"></div>
            <div class={$divName}>
        EOT;
    } else {
        $pageStart .= $bodyStart;
    }

    // add general messaging blocks to page content
    if ($pageType == 'page') {

    }
    // now, finish main division on page
    $renderedHTML .=<<<EOT
        </div> <!-- {$divName} -->

    EOT;
    if (is_null($rm_msg)) $rm_msg = " ";
    if (is_null($debug_msg)) $debug_msg = " ";

    // now, add messages area
    $renderedHTML .= <<<EOT
        <div class="rm_msg">{$rm_msg}</div>
        <div class="rm_debug">{$debug_msg}</div>

    EOT;

    // now add Sponsorenblock outside of main div.
    if (! empty($bodyEnd)) {
        $renderedHTML .= <<<EOT
            {$bodyEnd}
        EOT;
    }

    // include the hide status message code here
    $renderedHTML .= <<<EOT
        <script type="text/javascript">
            clearMessageAfter($rm_msg_delay);
        </script>
    </body>
    </html>

    EOT;

    $renderedHTML = mb_convert_encoding($renderedHTML, "UTF-8", mb_detect_encoding($renderedHTML, "UTF-8, ISO-8859-1, ISO-8859-15", true));

    // write all headers in one place.
    header('Content-Type: text/html; charset=UTF-8');
    header("Content-Security-Policy: default-src https: ; script-src https: 'unsafe-inline' 'unsafe-eval'; ");
    header("Strict-Transport-Security: max-age=31536000");
    header("X-XSS-Protection:1;mode=block");
    header("X-Content-Type-Options: nosniff");
    header("Referrer-Policy: strict-origin");

    echo $pageStart;
    echo $renderedHTML;
    return;
}

?>
