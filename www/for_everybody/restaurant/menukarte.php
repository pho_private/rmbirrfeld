<?php
require_once ('inc.php');
$renderedHTML = <<<EOT
	<div class=rm_h1>Menukarte</div>
	<div class=col_block>
		<div class=rm_h2>Tagesmeu</div>
		<div class=rm_text>
			<table>
				<tr><td class=th_1>Sa. 11. Mai</td><td class=td_1>tbd</td></tr>
				<tr><td class=th_1>So. 12. Mai</td><td class=td_1>tbd</td></tr>
				<tr><td class=th_1>Sa. 18. Mai</td><td class=td_1>tbd</td></tr>
				<tr><td class=th_1>So. 19. Mai</td><td class=td_1>tbd</td></tr>
			</table>
		</div>
		
		

		<div class=rm_h2>Rotweine</div>
		<div class=rm_text>
			<table>
				<tr><td class=th_1>xyz 7dl</td><td class=td_1_r>17.50</td></tr>
			</table>
		</div>

		<div class=rm_h2>Weissweine</div>
		<div class=rm_text>
			<table>
				<tr><td class=th_1>xyz 7dl</td><td class=td_1_r>12.50</td></tr>
			</table>
		</div>

	
		<div class=rm_h2>Getr&auml;nke</div>
		<div class=rm_text>
			<table>
				<tr><td class=th_1>grosses Bier</td><td class=td_1_r>5.00</td></tr>
				<tr><td class=th_1>kleines Bier</td><td class=td_1_r>3.00</td></tr>
				<tr><td class=th_1>Mineral 3dl</td><td class=td_1_r>3.00</td></tr>
				<tr><td class=th_1>Mineral 5dl</td><td class=td_1_r>5.00</td></tr>
				<tr><td class=th_1>Kaffee Creme</td><td class=td_1_r>3.00</td></tr>
			</table>
		</div>

	</div>
	<div class=lst_col_block>

		<div class=rm_h2>Geb&auml;ck</div>
		<div class=rm_text>
			<table>
				<tr><td class=th_1>Gipfeli</td><td class=td_1_r>1.20</td></tr>
				<tr><td class=th_1>Kuchen</td><td class=td_1_r>2.50</td></tr>
				<tr><td class=th_1>Nuss- /Mandelgipfel</td><td class=td_1_r>3.00</td></tr>
			</table>
		</div>
		
		
		<div class=rm_h2>Salat</div>
		<div class=rm_text>
			<table>
				<tr><td class=th_1>kleine Gr&uuml;ner</td><td class=td_1_r>5.00</td></tr>
				<tr><td class=th_1>Buffet kleiner Teller</td><td class=td_1_r>7.50</td></tr>
				<tr><td class=th_1>Buffet grosser Teller</td><td class=td_1_r>12.50</td></tr>
			</table>
		</div>
		
		<div class=rm_h2>Klassiker</div>
		<div class=rm_text>
			<table>
				<tr><td class=th_1>Spaghetti (diverse Saucen)</td><td class=td_1_r>10.00</td></tr>
				<tr><td class=th_1>Pizza</td><td class=td_1_r>10.00</td></tr>
			</table>
		</div>
		
		
		<div class=rm_h2>Fr&uuml;hst&uuml;ck</div>
		<div class=rm_text>
			<table>
				<tr><td class=th_1>Buffet</td><td class=td_1_r>10.00</td></tr>
				<tr><td class=th_1>Spiegelein</td><td class=td_1_r>1.50</td></tr>
			</table>
		</div>

		<div class=rm_h2>Grilladen &amp; Pommes</div>
		<div class=rm_text>
			<table>
				<tr><td class=th_1>Bratwusrt</td><td class=td_1_r>7.50</td></tr>
				<tr><td class=th_1>Servelats</td><td class=td_1_r>5.00</td></tr>
				<tr><td class=th_1>Fl&uuml;&uuml;gersteak</td><td class=td_1_r>10.00</td></tr>
				<tr><td class=th_1>Portion Pommes</td><td class=td_1_r>5.00</td></tr>
			</table>
		</div>

	</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Restaurant - Menu', 'page', $renderedHTML);

?>
