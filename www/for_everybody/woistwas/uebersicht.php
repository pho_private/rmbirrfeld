<?php
require_once ('inc.php');
$renderedHTML = <<<EOT
		<table  align="center" border="0" cellspacing="0" 	cellpadding="0">
			<tr>
					<td class=rm_h1>Zufahrt Birrfeld</td>
					<td class=rm_h1></td>
					<td class=rm_h1>{$rm_comp_type} City</td>
			</tr>
			<tr>
				<td>&nbsp;
			</tr>
			<tr valign="middle" align="center">
				<td><img src="../../resources/graphics/organisation/zufahrt.jpg" height="240px;" border="0" /></td>
				<td><img src="../../resources/graphics/organisation/sm_2024_montageaufstellung.jpg" height="240px;" border="0" /></td>
				<td><img src="../../resources/graphics/organisation/rm-city.png" height="240px;" border="0" /></td>
			</tr>
			<tr>
				<td class=rm_h2 align="center">A1 Ausfahrt M&auml;genwil oder <br />A3 Ausfahrt Lupfig!</td>
				<td class=rm_h2 align="center"></td>
				<td class=rm_h2 align="center">{$rm_comp_type} City</td>
			</tr>
		</table>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Wo ist was?', 'page', $renderedHTML, NULL, TRUE);

?>
