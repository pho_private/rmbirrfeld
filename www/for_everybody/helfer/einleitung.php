<?php
require_once ('inc.php');
$renderedHTML = <<<EOT
		<div class=rm_h1>Einleitung</div>
		<div class=big_col_block>


			<div class=rm_h2>Liebe Segelflieger/Innen, Liebe Freunde/Innen des
				Segelfluges</div>

			<div class=rm_text>
				Am {$rm_datum} wird die {$rm_name_lang} durch die {$rm_ausrichter_lang} ausgerichtet.<br />
				Die Veranstaltung soll f&uuml;r die Teilnehmenden, die Zuschauer und die G&auml;ste 
                zum unvergesslichen Erlebnis werden. Grossen Wert legen wir auf
				aktuelle Wettbewerbsinformationen und ein attraktives Rahmenprogramm.<br /> 
                S&auml;mtliche Informationen, Resultate und Bilder werden laufend auf dieser Homepage ver&ouml;ffentlicht.<p />

				All diese Aktivit&auml;ten sind jedoch nicht ohne Hilfe aller Birrfelder Segelflieger und Freunde des Segelfluges m&ouml;glich. <p />
                Daher bitten wir Dich um tatkr&auml;ftige Unterst&uuml;tzung dieses Anlasses. Es w&uuml;rde uns freuen, Dich als Helfer dabei zu haben. <p />

				Anmeldungen online oder per <a href="mailto:ok@smbirrfeld.ch">E-Mail</a>
				ab sofort.
			</div>
		</div>
		<div class=rm_end></div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Helfer - Einleitung', 'page', $renderedHTML);

?>
