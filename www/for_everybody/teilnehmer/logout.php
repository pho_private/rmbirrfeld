<?php
session_start();
error_reporting(E_ALL);

if ($_SESSION['logged_in']) {
    session_unset();
    session_destroy();
}
header("Location: anmeld-start.php");
?>
