<?php

// TODO - Querchecken mit anderen Berechnungen (Barzahlungs-Zuschlag)
require_once ('inc.php');
require_once ('functions.php');

$action = getRequestOrInit('action');
$login = getRequestOrInit('login');

$teilnehmerList = new Teilnehmer();

function strtochf($value) {
    $strtochf = '';
    if ($value > 0) {
        if ($value > 999)
            $strtochf = floor($value / 1000) . "'";
        $strtochf .= round($value % 1000) . ".00";
    }
    return $strtochf;
}

switch ($action) {
    case 'paid':
        $teilnehmerList->set_paid_by_login($login);
        break;
    case 'notpaid':
        $teilnehmerList->reset_paid_by_login($login);
        break;
    case 'ausweiseok':
        $teilnehmerList->set_ausweise_ok_by_login($login);
        break;
    case 'ausweisenotok':
        $teilnehmerList->reset_ausweise_ok_by_login($login);
        break;
    case 'rueckholen':
        $teilnehmerList->set_rueckholen_by_login($login);
        break;
    case 'notrueckholen':
        $teilnehmerList->reset_rueckholen_by_login($login);
        break;
    case 'gotgift':
        $teilnehmerList->set_gotgift_ok_by_login($login);
        break;
    case 'notgotgift':
        $teilnehmerList->reset_gotgift_ok_by_login($login);
        break;
    case 'camping':
        $teilnehmerList->set_camping_ok_by_login($login);
        break;
    case 'keincamping':
        $teilnehmerList->reset_camping_ok_by_login($login);
        break;
    case 'junior':
        $teilnehmerList->set_junior_ok_by_login($login);
        break;
    case 'keinjunior':
        $teilnehmerList->reset_junior_ok_by_login($login);
        break;
    default:
        break;
}

$renderedHTML = <<<EOT
        <div class="rm_h1">Admin Check - Check In</div>
        <div class="big_col_block">
EOT;

// number of participants having booked camping, rueckholen
$tot_camping = 0;
$tot_rueckholen = 0;

// how much money did we get
$summe_betrag = 0;
$summe_rueckholen = 0;
$summe_nenngeld = 0;
$summe_camping = 0;
$summe_barzuschlag = 0;

$tableStart = <<<EOT

                <colgroup>
                    <col width="50">
                    <col width="30">
                    <col width="180">
                    <col width="80">
                    <col width="35">
                    <col width="35">
                    <col width="70">
                    <col width="70">
                    <col width="70">
                    <col width="70">
                    <col width="30">
                    <col width="50">
                    <col width="40">
                    <col width="40">
                    <col width="80">
                </colgroup>

EOT;

foreach ($class_key as $class) {
    if ($class == 0)
        continue;

    $renderedHTML .= <<<EOT
             <table bgcolor={$class_bgcolor[$class]} border=0">
             {$tableStart}
                <tr><th colspan=15>$class_name[$class]</th></tr>
                <tr>
                    <th class=th_1_c>SM/RM</th>
                    <th class=th_1_c>WK</th>
                    <th class=th_1  >Teilnehmer</th>
                    <th class=th_1_c>Sportlizenz</th>
                    <th class=th_1_c colspan="2">Shirt</th>
                    <th class=th_1_c>Junior</th>
                    <th class=th_1_c>Bezahlt</th>
                    <th class=th_1_c>Total</th>
                    <th class=th_1_c>Nenngeld</th>
                    <th class=th_1_c colspan="2">R&uuml;ckholen</th>
                    <th class=th_1_c colspan="2">Camping</th>
                    <th class=th_1_c>Barzuschlag</th>
                </tr>
    EOT;

    $teilnehmerList->load_class_by_wk($class);
    foreach ($teilnehmerList->list as $teilnehmer) {
        // next line to be replaced by database field for sm or rm participant.
        $junior = ($teilnehmer['junior'] == 'ja');
        $sm_participant = $rm_RMSM ? (preg_match('/nur rm/', strtolower($teilnehmer['angaben'])) ? false : true) : ($rm_comp_type == "SM");
		if ($junior || $class_name[$class] == 'JSM') $sm_participant = true;
        $camping = ($teilnehmer['camping'] == 'ja');
        $rueckholen = ($teilnehmer['rueckholen'] == 'ja');
        $bezahlt = ($teilnehmer['bezahlt'] != '' && $teilnehmer['bezahlt'] != '0000-00-00');
        // Barzuschlag aufzeigen fuer Barzahler - wenn am 1. Wettbewerbstag oder sp&auml;ter bezahlt, dann Barzuschlag
        $bezahlt_bar = ($bezahlt) ? ($teilnehmer['bezahlt'] >= $rm_dates[1]) : true;

        if ($camping)
            $tot_camping ++;
        if ($rueckholen)
            $tot_rueckholen ++;
        if ($bezahlt && ! $bezahlt_bar) {
            $barzuschlag = 0;
        } else {
            $barzuschlag = $sm_participant ? $sm_barzuschlag : $rm_barzuschlag;
        }

        if ($junior) {
            $nenngeld = ($sm_participant) ? $sm_nenngeld_junior : $rm_nenngeld_junior;
            $camping = ($camping) ? ($sm_participant ? $sm_camping_junior : $rm_camping_junior) : 0;
            $rueckholen = ($rueckholen) ? ($sm_participant ? $sm_rueckholen_junior : $rm_rueckholen_junior) : 0;
        } else {
            $nenngeld = ($sm_participant) ? $sm_nenngeld_senior : $rm_nenngeld_senior;
            $camping = ($camping) ? ($sm_participant ? $sm_camping_senior : $rm_camping_senior) : 0;
            $rueckholen = ($rueckholen) ? ($sm_participant ? $sm_rueckholen_senior : $rm_rueckholen_senior) : 0;
        }

        // Total fuer Teilnehmer
        $betrag = $nenngeld + $camping + $rueckholen + $barzuschlag;

        // Summen fuer Totalzeile
        $summe_nenngeld += $nenngeld;
        $summe_rueckholen += $rueckholen;
        $summe_camping += $camping;
        $summe_barzuschlag += $barzuschlag;
        $summe_betrag += $betrag;

        // CHF Strings fuer Teilnehmerzeile
        $nenngeld_str = strtochf($nenngeld);
        $camping_str = strtochf($camping);
        $rueckholen_str = strtochf($rueckholen);
        $barzuschlag_str = strtochf($barzuschlag);
        $betrag_str = strtochf($betrag);
        $tmp_name = $teilnehmer['name'] . ' ' . $teilnehmer['vorname'];

        if ($sm_participant) {
            $tmp_image = '<img src=' . $rm_icon_url . '/ch.png width="18">'; // swiss flag
        } else {
            $tmp_image = '<img src=' . $rm_icon_url . '/ag.png width="18">'; // argovia flag
        }
        $commontext = "&login={$teilnehmer['login']}\" onclick=\"return confirm('Hat {$tmp_name} wirklich ";

        // Wettbewerb (RM oder SM), Wettbewerbskennzeichen, Name
        $renderedHTML .= <<<EOT
            
            <tr>
                <td class=td_1_c>{$tmp_image}</td>
                <td class=td_1_c>{$teilnehmer['wk']}</td>
                <td class=td_1>{$tmp_name}</td>
        EOT;
        // Sportlizenz und Ausweise
        if ($teilnehmer['ausweise'] == 'ja') {
            $renderedHTML .= <<<EOT
                        
                <td class=td_1_c><a href="teilnehmer_check.php?action=ausweisenotok{$commontext}KEINE g&uuml;ltigen Papiere?')"><img src="{$rm_icon_url}/check.png" width="16">&nbsp;</a></td>
            EOT;
        } else {
            $renderedHTML .= <<<EOT
                        
                <td class=td_1_c><a href="teilnehmer_check.php?action=ausweiseok{$commontext}g&uuml;ltige Papiere?')"><img src="{$rm_icon_url}/nav_plain_red.png" width="16"></a></td>
            EOT;
        }

        // Shirt: 1. Spalte Groesse, 2. Spalte bezogen ja/nein.
        $renderedHTML .= <<<EOT
                <td class=td_1_c>{$teilnehmer['gift']}</td>                                    
        EOT;
        if ($teilnehmer['gotgift'] == 'ja') {
            $renderedHTML .= <<<EOT
                <td class=td_1_c><a href="teilnehmer_check.php?action=notgotgift{$commontext}das Mitnahmegeschenk noch nicht bezogen?')"><img src="{$rm_icon_url}/check.png" width="16">&nbsp;</a></td>
            EOT;
        } else {
            $renderedHTML .= <<<EOT
                <td class=td_1_c><a href="teilnehmer_check.php?action=gotgift{$commontext}das Mitnahmegeschenk bezogen?')"><img src="{$rm_icon_url}/nav_plain_red.png" width="16">&nbsp;</a></td>
            EOT;
        }
        
        // Junior oder Senior?
        if ($teilnehmer['junior'] == 'ja') {
            $renderedHTML .= <<<EOT
                <td class=td_1_c><a href="teilnehmer_check.php?action=keinjunior{$commontext}keinen Junior Status?')"><img src="{$rm_icon_url}/check.png" width="16">&nbsp;</a></td>
            EOT;
        } else {
            $renderedHTML .= <<<EOT
                <td class=td_1_c><a href="teilnehmer_check.php?action=junior{$commontext}Junior Status?')">&nbsp;</a></td>
            EOT;
        }      
        
        // Bezahlt- Datum
        if ($barzuschlag == 0) {
            $renderedHTML .= <<<EOT
            
                <td class=td_1_c><a href="teilnehmer_check.php?action=notpaid{$commontext}NICHT bezahlt?')"> {$teilnehmer['bezahlt']}&nbsp;</a></td>
            EOT;
        } else {
            $renderedHTML .= <<<EOT
            
                <td class=td_1_c><a href="teilnehmer_check.php?action=paid{$commontext}bezahlt?')"><img src="{$rm_icon_url}/nav_plain_red.png" width="16">&nbsp;</a></td>
            EOT;
        }
        // Total Betrag, Nengeld
        $renderedHTML .= <<<EOT
                <td class=td_1_r>{$betrag_str}</td>
                <td class=td_1_r>{$nenngeld_str}</td>
        EOT;

        // Rueckholservice ja/nein
        if ($teilnehmer['rueckholen'] == 'ja') {
            $renderedHTML .= <<<EOT
            
                <td class=td_1_c><a href="teilnehmer_check.php?action=notrueckholen{$commontext}den R&uuml;ckholservice abbestellt?')"><img src="{$rm_icon_url}/car_compact_red.png" width="16">&nbsp;</a></td>
            EOT;
        } else {
            $renderedHTML .= <<<EOT
            
                <td class=td_1_c><a href="teilnehmer_check.php?action=rueckholen{$commontext}den R&uuml;ckholservice gebucht?')">&nbsp;</a></td>
            EOT;
        }
        // Rueckholservice Preis
        $renderedHTML .= <<<EOT
                <td class=td_1_r>{$rueckholen_str}</td>
        EOT;

        // Camping ja/nein
        if ($teilnehmer['camping'] == 'ja') {
            $renderedHTML .= <<<EOT
            
                <td class=td_1_c><a href="teilnehmer_check.php?action=keincamping{$commontext}keine Campingabsicht?')"><img src="{$rm_icon_url}/zelt.png" width="30">&nbsp;</a></td>
            EOT;
        } else {
            $renderedHTML .= <<<EOT
            
                <td class=td_1_c><a href="teilnehmer_check.php?action=camping{$commontext}Campingabsicht?')">&nbsp;</a></td>
            EOT;
        }

        // Camping Betrag und Barzuschlag Betrag
        $renderedHTML .= <<<EOT
                <td class=td_1_r>{$camping_str}</td>       
                <td class=td_1_r>{$barzuschlag_str}</td>                       
            </tr>
        EOT;
    }

    $renderedHTML .= <<<EOT
        </table><br />
    EOT;
}
$summe_nenngeld_str = strtochf($summe_nenngeld);
$summe_camping_str = strtochf($summe_camping);
$summe_rueckholen_str = strtochf($summe_rueckholen);
$summe_barzuschlag_str = strtochf($summe_barzuschlag);
$summe_betrag = $summe_nenngeld + $summe_camping + $summe_rueckholen + $summe_barzuschlag;
$summe_betrag_str = strtochf($summe_betrag);

$renderedHTML .= <<<EOT
            <table bgcolor="lightgrey" border=0">
            {$tableStart}
            <tr>
                <th class=th_1></th>
                <th class=th_1></th>
                <th class=th_1>Total</th>
                <th class=th_1_c></th>
                <th class=th_1_c></th>
                <th class=th_1_c></th>
                <th class=th_1_c></th>
                <th class=th_1_c></th>
                <th class=th_1_r>{$summe_betrag_str}</th>
                <th class=th_1_r>{$summe_nenngeld_str}</th>
                <th class=th_1_c>{$tot_rueckholen}</th>
                <th class=th_1_r>{$summe_rueckholen_str}</th>
                <th class=th_1_r>{$tot_camping}</th>
                <th class=th_1_r>{$summe_camping_str}</th>
                <th class=th_1_r>{$summe_barzuschlag_str}</th>
            </tr>
        </table>
    </div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('KL - Teilnehmer-Check', 'page', $renderedHTML);

?>
