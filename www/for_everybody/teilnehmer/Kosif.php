<?php
require_once ('inc.php');
$bodyStart = <<<EOT
<body>
	<script type="text/javascript">

function DateAdd(objDate, strInterval, intIncrement) {
		if (strInterval != "M" && strInterval != "D" && strInterval != "Y"
				&& strInterval != "h" && strInterval != "m"
				&& strInterval != "uM" && strInterval != "uD"
				&& strInterval != "uY" && strInterval != "uh"
				&& strInterval != "um" && strInterval != "us") {
			throw ("DateAdd: Second parameter must be M, D, Y, h, m, uM, uD, uY, uh, um or us");
		}

		if (typeof (intIncrement) != "number") {
			throw ("DateAdd: Third parameter must be a number");
		}

        switch(strInterval)
        {
            case "M":
            objDate.setMonth(parseInt(objDate.getMonth()) + parseInt(intIncrement));
            break;
 
            case "D":
            objDate.setDate(parseInt(objDate.getDate()) + parseInt(intIncrement));
            break;
 
            case "Y":
            objDate.setYear(parseInt(objDate.getYear()) + parseInt(intIncrement));
            break;
 
            case "h":
            objDate.setHours(parseInt(objDate.getHours()) + parseInt(intIncrement));
            break;
 
            case "m":
            objDate.setMinutes(parseInt(objDate.getMinutes()) + parseInt(intIncrement));
            break;
 
            case "s":
            objDate.setSeconds(parseInt(objDate.getSeconds()) + parseInt(intIncrement));
            break;
 
            case "uM":
            objDate.setUTCMonth(parseInt(objDate.getUTCMonth()) + parseInt(intIncrement));
            break;
 
            case "uD":
            objDate.setUTCDate(parseInt(objDate.getUTCDate()) + parseInt(intIncrement));
            break;
 
            case "uY":
            objDate.setUTCFullYear(parseInt(objDate.getUTCFullYear()) + parseInt(intIncrement));
            break;
 
            case "uh":
            objDate.setUTCHours(parseInt(objDate.getUTCHours()) + parseInt(intIncrement));
            break;
 
            case "um":
            objDate.setUTCMinutes(parseInt(objDate.getUTCMinutes()) + parseInt(intIncrement));
            break;
 
            case "us":
            objDate.setUTCSeconds(parseInt(objDate.getUTCSeconds()) + parseInt(intIncrement));
            break;
        }
        return objDate;
    }

	function openKosif( intOffset ) {
		//base url
		//      https://www.skybriefing.com/portal/home?p_p_id=dabsportlet_WAR_ibsportletdabs&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=APP&p_p_cacheability=cacheLevelPage&p_p_col_id=column-3&p_p_col_count=3&_dabsportlet_WAR_ibsportletdabs_v-resourcePath=%2FAPP%2Fconnector%2F0%2F2%2Fhref%2Fdabs-2017-01-02.pdf
		//      https://www.skybriefing.com/portal/home;jsessionid=AF714B9DC1ED5123986836B9750A5E11?p_p_id=dabsportlet_WAR_ibsportletdabs&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=APP&p_p_cacheability=cacheLevelPage&p_p_col_id=column-3&p_p_col_count=3&_dabsportlet_WAR_ibsportletdabs_v-resourcePath=%2FAPP%2Fconnector%2F0%2F2%2Fhref%2Fdabs-2016-05-01.pdf
		// found on skyguide: http://www.skyguide.ch/fileadmin/dabs-today/DABS_20130109.pdf
		//                    http://www.skyguide.ch/fileadmin/dabs-tomorrow/DABS_20130110.pdf
		var baseURL = "https://www.skybriefing.com/portal/home?p_p_id=dabsportlet_WAR_ibsportletdabs&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=APP&p_p_cacheability=cacheLevelPage&p_p_col_id=column-3&p_p_col_count=3&_dabsportlet_WAR_ibsportletdabs_v-resourcePath=%2FAPP%2Fconnector%2F0%2F2%2Fhref%2Fdabs-########.pdf";
			// "https://www.skybriefing.com/portal/home;jsessionid=AF714B9DC1ED5123986836B9750A5E11?p_p_id=dabsportlet_WAR_ibsportletdabs&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=APP&p_p_cacheability=cacheLevelPage&p_p_col_id=column-3&p_p_col_count=3&_dabsportlet_WAR_ibsportletdabs_v-resourcePath=%2FAPP%2Fconnector%2F0%2F2%2Fhref%2Fdabs-########.pdf";
		if (intOffset == 1 ) baseURL = "https://www.skybriefing.com/portal/de/home?p_p_id=dabsportlet_WAR_ibsportletdabs&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=APP&p_p_cacheability=cacheLevelPage&p_p_col_id=column-3&p_p_col_count=3&_dabsportlet_WAR_ibsportletdabs_v-resourcePath=%2FAPP%2Fconnector%2F1%2F10%2Fhref%2Fdabs-########.pdf";
		
		

		// format YYYYMMDD date
		var today = new Date();
		var targetDate = DateAdd(today, "D", intOffset);
		var y = targetDate.getFullYear();
		var m = (((targetDate.getMonth() + 1) < 10) ? "0" + String(targetDate.getMonth() + 1) : String(targetDate.getMonth() + 1));
		var d = ((targetDate.getDate() < 10) ? "0" + String(targetDate.getDate()) : String(targetDate.getDate()));
		var dateString = y + m + d;
		// link
		window.open(baseURL.replace("########", dateString), 'Kosif','menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no');
	}
</script>

	<div class="main_block">
EOT;

$renderedHTML = <<<EOT
		<div class="rm_h1">DABS Informationen f&uuml;r heute und morgen</div>
		<div class="big_col_block">

			<div class="rm_text">
				<table>
					<tr>
						<td class="sidebarlink"><b>DABS</b> (Daily Airspace Bulletin
							Switzerland) zum downloaden f&uuml;r <a class="link"
							href="javascript:openKosif(0)">heute</A></td>
					</tr>
					<tr>
						<td class="sidebarlink"><a class="link"
							href="javascript:openKosif(1)">morgen&nbsp;&nbsp;&nbsp;(ab 13 Uhr
								verf&uuml;gbar)</A></td>
					</tr>
					<tr>
						<td class="sidebarlink"><a
							href="https://www.skybriefing.com/portal/" target="_blank">DABS
								allgemein</a></td>
					</tr>
					<tr>
						<td class="sidebarlink"><a href="http://www.homebriefing.com"
							target="_blank">www.homebriefing.com (kostenpflichtig!)</a></td>
					</tr>
					<tr>
						<td><b><big>skybriefing Helpdesk 24/7 </big></b><br /> DABS per
							Telefon +41 43 931 61 61 (Deutsch, Englisch)<br /> DABS per
							Telefon +41 43 931 62 03 (Franz&ouml;sisch)</td>
					</tr>
				</table>
			</div>

		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Kosif', 'page', $renderedHTML, $bodyStart);

?>

