<?php
// session_start();
error_reporting(E_ALL);
require_once ('dblib.inc.php');

$message = array(' ');
$msgs = '';
$debug_msg = '';
$redirect = false;

// grab the token from the email link (from the URL)
$token = getRequestOrInit('token');

// first round: get the token from the URL parameter
if ($token != '') {
    $result = DBQuery("SELECT email FROM {$rm_tbl_pwreset} WHERE token='$token'");
    if (!$result) {
        array_push($message, 'Keinen passenden Passwort-&auml;nderungsantrag gefunden!');        
        $redirect = true;
    } else {
        $email = DBFetchAssoc($result)['email'];
        $debug_msg .= "<br />email = '$email'";
    }
} else {
    array_push($message, 'Diese Seite kann nur per Mail Link ge&ouml;ffnet werden!');
    $redirect = true;
}
if (isset($email)) {
    $debug_msg .= "<br />token = '$token'<br />email = '$email'";
    $action = getPostOrInit('action');
    
    if ($action == 'resetpw') {    
        $new_pass = escapeString( $_POST['new_pass']);
        $new_pass_c = escapeString($_POST['new_pass_c']);
        $debug_msg .= "<br />new passwords: '{$new_pass}' '{$new_pass_c}'";
            
        if (empty($new_pass) || empty($new_pass_c)) {
            array_push($message, 'Passwort leer!');
        }
        if ($new_pass != $new_pass_c) {
            array_push($message, 'Passworte stimmen nicht &uuml;berein!');
        }
        if (count($message) < 2) {
            $new_pass = hashPassword($new_pass);
            $debug_msg .= "<br />password hash = '$new_pass'";
            // save the new password hash
            $results = DBQuery("UPDATE {$rm_tbl_teilnehmer} SET passwort='$new_pass' WHERE email='$email'");
            if (! $results) {
                $dberr = DBErrList();
                $debug_msg .= '<br />cannot save password: <pre>' . print_r($dberr, true) . '</pre>';
                array_push ($message, 'Kann Passwort nicht speichern');
            } else {
                $debug_msg .= "<br />password saved";                
                // if successully updated, delete the token entry in password_reset table
                $results = DBQuery("DELETE FROM {$rm_tbl_pwreset} WHERE email='$email'");
                $debug_msg = '<pre>' . print_r($results, true) . '</pre>';
                if (! $results) {
                    $dberr = DBErrList();
                    $debug_msg .= "<br />cannot delete {$rm_tbl_pwreset} entry: <pre>" . print_r($dberr, true) . '</pre>';
                } else {
                    $debug_msg .= "<br />password_reset entry deleted";
                    array_push ($message, 'Passwort erfolgreich gesetzt');
                    header( "refresh: 5; url='$rm_url'");
                    $redirect = true;
                }
            }
        } else {
            $debug_msg .= '<br />errors found <pre>' . print_r($message, true) .'</pre>';
        }
    }
}
if (count($message) > 1) {
    $msgs = '<br /><span style=color:red>';
    foreach ($message as $m) {
        $msgs .= $m . '<br />';
    }
    $msgs .= '</span>';
}
$renderedHTML = <<<EOT
<div class=body>
	<div class=rm_h1>Setze ein neues Passwort f&uuml;r {$email}</div>				
	<form method="POST" name="newpw" action="{$_SERVER['SCRIPT_NAME']}">
		<input type="hidden" name="action" value="resetpw">
        <input type="hidden" name="token" value="{$token}">
        <input type="hidden" name="email" value="{$email}">
<!--
		<input type="hidden" name="{session_name()}" value="{session_id()}">
-->		
		<table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><b>{$msgs}&nbsp;</b></td>
            </tr>
			<tr>
				<td width="170px"><img src="{$rm_icon_url}/key1.png" width="15" height="11" border="0" alt=""> &nbsp;Neues Passwort:</td>
				<td><input type="password" name="new_pass" value=""></td>
			</tr>
			<tr>
				<td width="170px"><img src="{$rm_icon_url}/key1.png" width="15" height="11" border="0" alt=""> &nbsp;Wiederholung:</td>
				<td><input type="password" name="new_pass_c" value=""></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" value="Speichern"></td>
			</tr>
		</table>
		<br />
		<hr />						
		<br />
		<br />
	</form>
</div>
EOT;
if ($redirect) {
    header( "refresh: 5; url='$rm_url'");
}
// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Passwort setzen', 'page', $renderedHTML);


?>
