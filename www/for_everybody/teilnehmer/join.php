<?php
session_start();
error_reporting(E_ALL);

require_once ('inc.php');
require_once ('dblib.inc.php');

$nachricht = "";
$login = "";
$passwort = "";
$pass2 = "";

if (isset($_POST['logged_in']) && $_SESSION['logged_in']) {
    $nachricht = "Du bist bereits eingeloggt!";
    header("Location: anmeld-start.php");
}
$action = getPostOrInit('aktion');

if ($action == 'join') {
    $login = trim($_POST['login']);
    $passwort = trim($_POST['passwort']);
    $pass2 = trim($_POST['pass2']);
    
    if (empty($login))
        $nachricht .= "F&uuml;lle Login aus!<br />\n";
    if (empty($passwort))
        $nachricht .= "F&uuml;lle Passwort aus<br />\n";
    if (empty($pass2))
        $nachricht .= "F&uuml;lle Passwort best&auml;tigen aus!<br />\n";
    if ($passwort != $pass2)
        $nachricht .= "Die beiden Passworteingaben waren nicht identisch. Bitte wiederholen.<br />\n";
    
    if (getRowByKeyValuePair($rm_tbl_teilnehmer, "login", $login)) {
        $nachricht .= "Login <span style=color:red>" . $login . "</span> ist leider schon vergeben. Versuche bitte ein anderes.<br />\n";
    }
    if ($nachricht == "") { // keine Fehler. create user, init session, and go to userdata mask
        $id = createParticipant($login, $passwort);
        initSession($id, $login, $passwort);
        header("Location: update-form.php");
        exit();
    }
}

$renderedHTML = <<<EOT
	<table class="body" cellspacing="0" cellpadding="0">
		<tr valign="middle" align="center">
			<td>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<div class=rm_h1>Registrierung / Anmeldung <?php print $rm_name_lang; ?>&nbsp;&nbsp;&nbsp;&nbsp;<small>(Schritt
									2 von 3)</small>
							</div> <br />
							Um Dich f&uuml;r die <?php echo $rm_name_kurz ?> einschreiben zu k&ouml;nnen, musst Du dich zuerst einmalig <b>registrieren</b>.<br />
							Das Feld <b>Login</b> kannst du frei w&auml;hlen, z.B. aus dem Vor- und Nachnamen.<p /> 
							Diese Daten werden gespeichert und sind <b>f&uuml;r sp&auml;tere RM's und SM's</b> falls n&ouml;tig wieder abruf- und ver&auml;nderbar!<p /> 
							
							W&auml;hle ein eigenes Passwort aus. Notiere Dir <b>Login + Passwort</b>. Es wird Dir <b>nicht</b> zugeschickt!<p />
							
							Sp&auml;ter kannst Du mit diesem Login/Passwort Deine Angaben jederzeit &auml;ndern.
EOT;
if ($nachricht != '') {
    $renderedHTML .= <<<EOT
                            <b class="nachricht">$nachricht</b>
EOT;
}
$renderedHTML .= <<<EOT
				            <form method="POST" name="join"
								action="{$_SERVER['SCRIPT_NAME']}">
								<input type="hidden" name="aktion" value="join"> 
                                <input type="hidden" name="{session_name()}" value="{session_id()}">

								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="170px"><img src="../../resources/graphics/icons/mem.gif" width="9" height="13" border="0" alt=""> Login:</td>
										<td><input type="text" name="login" value="{$login}"></td>
									</tr>
									<tr>
										<td width="170px"><img src="../../resources/graphics/icons/key.gif" width="9" height="15" border="0" alt=""> Passwort:</td>
										<td><input type="password" name="passwort" value="{$passwort}"></td>
									</tr>
									<tr>
										<td width="170px"><img src="../../resources/graphics/icons/key.gif" width="9" height="15" border="0" alt=""> Passwort wiederholen:</td>
										<td><input type="password" name="pass2" value=""></td>
									</tr>
									<tr>
										<td colspan="2"><br /> <input type="submit" value="Registrieren"></td>
									</tr>
								</table>
							</form>
						</td>
					</tr>
				</table>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align=center><input type="button" name="goback" value="zur&uuml;ck zur Auswahl" onClick="self.location.href='./anmeld-start.php'"></td>
					</tr>
				</table>

			</td>
		</tr>
	</table>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Registrieren', 'page', $renderedHTML);

?>

