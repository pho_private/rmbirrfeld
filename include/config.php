<?php
require_once ('inc.php');
require_once ('dblib.inc.php');


final class config {

    private string $area;
    private string $name;
    private string $value;
    private string $type;

    private array $configs;

    function __construct(string $myArea = null) {
        global $rm_tbl_config, $rm_year, $rm_debug, $debug_msg;

        $whereClause = "WHERE year = {$rm_year}" . ($myArea ? " AND area = '$myArea'" : "");

        $result = DBQuery("SELECT * FROM {$rm_tbl_config} {$whereClause}");
        if ($result) {
            $this->configs = DBFetchAllAssoc($result);
            if ($rm_debug) {
                $debug_msg .= '<pre>' . print_r($this->configs, true) . '</pre>';
            }
        } else {
            unset($this->configs);
        }
    }

    function getConfig(string $area, string $name) {
        if (isset($this->configs)) {
            $value = $this->configs["$area"]["$name"]["value"];
            $type = $this->configs["$area"]["$name"]["type"];
            switch ($type) {
                 case 'int':
                    return intval($value);
                    break;
                // case 'float': return number($value);
                // case 'timestamp': return intval($value);
                // case 'date': return date('Y-m-d', $value);
                default:
                    return $value;
                    break;
            }
        } else {
        	return NULL;
        }
    }
	function setConfig(string $area, string $name, string $value, string $type) {
		global $rm_year, $rm_tbl_config;
		if (isset ( $this->configs )) {
			if (isset ( $this->configs ["$area"] ["$name"] ["value"] )) {
				// update if value has changed
				$oldvalue = $this->configs ["$area"] ["$name"] ["value"];
				if ($oldvalue != $value) {
					$this->configs ["$area"] ["$name"] ["value"] = $value;

					$query = "UPDATE $rm_tbl_config SET value = '$value', type = '$type' WHERE year = '$rm_year' AND area = '$area' AND name = '$name'";
					$result = DBQuery ( $query );
				}
				return isset($result);
			}
		}
		// in both remaining cases, we need to insert a new value into the configuration table.
		$this->configs ["$area"] ["$name"] ["value"] = $value;

		$query = "INSERT year, area, name, value, type INTO $rm_tbl_config VALUES ('$rm_year','$area','$name','$value','$type')";
		$result = DBQuery ( $query );

		return isset ($result );
	}

	function getConfigArray() {
		return $this->configs;
	}
}

