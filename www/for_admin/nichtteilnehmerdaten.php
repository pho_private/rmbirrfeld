<?php
require_once ('inc.php');
require_once ('functions.php');


$teilnehmerListe = new Teilnehmer();
$cnt = 0;
$renderedHTML = <<<EOT
		<div class=rm_h1>Ehemalige Teilnehmer</div>
		<div class=big_col_block>
			<table>
				<tr>
					<td valign=top>
    					<table border=0>
    						<tr>
    							<th class=th_1>Teilnehmer</td>
    							<th class=th_1>Adresse</td>
    							<th class=th_1>E-Mail</td>
    							<th class=th_1>Mobile P</td>
    						</tr>
EOT;

foreach ($teilnehmerListe->list as $teilnehmer) 
{
	if ($teilnehmer['rmactiv'] == 'ja') continue;
	$pilot 					= $teilnehmer['name'] . " " . $teilnehmer['vorname'];
	$adresse 				= $teilnehmer['adresse'].", " .$teilnehmer['plz']." ".$teilnehmer['wohnort'];
	$email		 			= $teilnehmer['email'];
	$mobil_p		 		= $teilnehmer['mobp'];
	
	$renderedHTML .= <<<EOT
    						<tr>
    							<form method="POST" name="speichern" action="teilnehmerdaten.php">
    								<td class=td_1>{$pilot}</td>
    								<td class=td_1>{$adresse}</td>
    								<td class=td_1>{$email}</td>
    								<td class=td_1>{$mobil_p}</td>
    							</form>
    						</tr>
EOT;
}

$renderedHTML .= <<<EOT
    					</table>
    					<br />
                    </td>
				</tr>
			</table>
		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('OK - Mail an Nichtteilnehmer', 'page', $renderedHTML);

?>
