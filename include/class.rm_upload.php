<?php
require_once 'inc.php';
require_once 'class.upload.php';

class rm_upload extends Verot\Upload\Upload {

	function resize_image($location, $max_x, $max_y, $filename_add = '', $newname = '') {
		if (is_dir($location)) {
			if (is_writable($location)) {
				$this->file_overwrite = true;
				$this->image_resize = true;
				$this->image_ratio_no_zoom_in = true; // do not upscale a smaller image
				$this->image_x = $max_x;
				$this->image_y = $max_y;
				if ($newname != '') {
					$this->file_new_name_body = $newname . $filename_add;
				} else {
					$this->file_name_body_add = $filename_add;
				}
				$this->process($location);
				if ($this->processed) {
					// echo "<br />saved file '" . $this->file_dst_pathname . "'";
					return true;
				} else {
					echo "error: {$this->error}";
					return false;
				}
			} else {
				$this->error = 'Verzeichnis ' . $location . ' nicht beschreibbar!';
			}
		} else {
			$this->error = 'Verzeichnis ' . $location . ' existiert nicht!';
		}
	}

	// generate the pilot's photo set in various sizes
	function generate_image_set($location, $newname = '') {

		// all errors are handled inside the called methods.
		$ok = $this->resize_image($location, 800, 600, "_middle", $newname);
		$ok = $ok && $this->resize_image($location, 400, 300, "_midi", $newname);
		$ok = $ok && $this->resize_image($location, 200, 150, "_small", $newname);
		$ok = $ok && $this->resize_image($location, 40, 30, "_mini", $newname);
		$ok = $ok && $this->resize_image($location, 1600, 1200, "", $newname); // moved this to end to make sure we can reference it after the upload.
		return $ok;
	}

	/**
	 * erstellt aus einem IGC Dateinamen den Datumsstring in der Form YYYY-MM-DD.
	 *
	 * @return string
	 */
	function getIGCfilenameComponents($igcFilename) {
		if (preg_match('/^([0-9])([1-9abc])([1-9a-v])([0-9a-z]{4})([0-9a-z])\.igc$/', $igcFilename, $igcfn) != 1) {
			$this->error = "Dateiname " . $igcFilename . " entspricht nicht den IGC Konventionen - bitte korrekt benennen!";
			return '';
		}
		return $igcfn;
	}

	function getDateString() {
		// IGC Dateinamen enthalten das Datum, die Logger-Seriennummer/Bezeichnung, und eine Laufnummer:
		//
		// <jahr(1)><monat(1)><tag(1)><hersteller(1)><serno(3)><laufno(1)>.igc
		// wobei
		// jahr = 0..9
		// monat = 1..9,a,b,c (1-12)
		// tag = 0..9, a..v (0..31)
		// hersteller = 0..9, a..z von IGC vergeben.
		// serno = 3 buchstaben/zahlen
		// laufno = 1..9 wenn an einem Tag mit demselben Logger mehrere Aufzeichnungen gemacht werden.
		global $rm_year;

		$dateString = '';

		$igcFilename = strtolower($this->file_src_name);

		$igcfn = $this->getIGCfilenameComponents($igcFilename);

		if ($igcfn == '') {
			return '';
		}
		if ($igcfn[1] == substr($rm_year, - 1, 1)) {
			$dateString = $rm_year . '-' . (($igcfn[2] <= "9") ? ('0' . $igcfn[2]) : (ord($igcfn[2]) - ord('a') + 10)) . '-' . (($igcfn[3] <= "9") ? ('0' . $igcfn[3]) : (ord($igcfn[3]) - ord('a') + 10));
		} else {
			$this->error = "Datei " . $igcFilename . " stammt nicht aus diesem Jahr...";
		}
		return $dateString;
	}

	function check_and_save_igc_file($location) {
		global $rm_dates, $rm_comp_type;

		$filedate = $this->getDateString();

		if ($filedate != "") {
			// check if date is among competition dates.
			if (! in_array($filedate, $rm_dates)) {
				$this->error = 'Meines Erachtens ist der ' . $filedate . " kein $rm_comp_type Wettbewerbstag!";
			} else {
				if (is_dir($location)) {
					if (is_writable($location)) {
						$this->file_new_name_ext = $this->file_src_name_ext;
						$this->file_new_name_body = $this->file_src_name_body;
						$this->mime_check = false;
						$this->file_auto_rename = false;
						$this->file_overwrite = true;
						$this->process($location);
						return ($this->processed);
					} else {
						$this->error = 'Verzeichnis ' . $location . ' nicht beschreibbar!';
					}
				} else {
					$this->error = 'Verzeichnis ' . $location . ' existiert nicht!';
				}
			}
		}
		return false;
	}
}
