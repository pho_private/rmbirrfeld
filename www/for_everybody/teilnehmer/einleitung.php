<?php
require_once ('inc.php');

$renderedHTML = <<<EOT
	<div class=rm_h1>Willkommen</div>
	<div class=big_col_block>

 		<div class=rm_h2>Willkommen auf dem Birrfeld zur {$rm_name_kurz}</div>
		<div class=rm_text>
			Die {$rm_ausrichter_lang} heisst die Piloten und ihre Helfer herzlich willkommen.<br />
			Wir w&uuml;nschen uns einen spannenden Wettbewerb auf hohem sportlichen Niveau mit interessanten und lehrreichen Fl&uuml;gen.</br>
			Sicherheit ist unser h&ouml;chstes Gebot.</br>
			Bitte unterst&uuml;tzt uns in diesem Bestreben!
		</div>
		
		<div class=rm_h2>Festwirtschaft</div>
		<div class=rm_text>
			In der Festwirtschaft gibt es von Morgens bis Abends viele K&ouml;stlichkeiten f&uuml;r Piloten und Besucher. <br />
			Vom Fr&uuml;hst&uuml;ck &uuml;ber Grillspezialit&auml;ten und t&auml;glich wechselnden Tagesmen&uuml;s bis zum Kuchenbuffet 
			ist gesorgt.
		</div>

		<div class=rm_h2>Campieren an der $rm_comp_type</div>
		<div class=rm_text>
			W&auml;hrend des Wettbewerbes besteht die M&ouml;glichkeit auf dem Flugplatz zu campieren.<br />
			Es ist nicht geplant, elektrische Energie bereitzustellen.<br />
			Sanit&auml;re Einrichtungen befinden sich im {$rm_comp_type}-Dorf.
		</div>
		
		<div class=rm_h2>Fahrzeuge im Flugplatzbereich</div>
		<div class=rm_text>
			Stellt bitte niemals Fahrzeuge im unmittelbaren Bereich der s&uuml;dlichen Hangars 1,2,3 ab!<br />
			Lasst Eure Fahrzeuge auf dem Parkplatz s&uuml;dlich Pistenkopf 08 stehen und kommt zu Fuss ins {$rm_comp_type}-Dorf.
		</div>
		
		<div class=rm_h2>Zufahrt zum Segelflugbereich</div>
		<div class=rm_text>
			Der Segelflugbereich kann nur noch via westliche Seite des Platzes und Pistenkopf 08 erreicht werden.<br />
			Der Weg s&uuml;dlich des Platzes ist die einzige Verbindung zu Pistenkopf 26.
		</div>
EOT;
if ($rm_angebot_rueckholer) {
    $renderedHTML .= <<<EOT
		<div class=rm_h2>R&uuml;ckholservice</div>
		<div class=rm_text>
			Wir bieten einen R&uuml;ckholservice an. Autoschl&uuml;ssel beim Infocorner abgeben und bei Bedarf anrufen.<br />
			Wer diesen Dienst beanspruchen m&ouml;chte, bezahlt pauschal und im Voraus. Es fallen keine weiteren Kosten an.<br />
			Wurde der Dienst w&auml;hrend der {$rm_comp_type} nie ben&ouml;tigt, wird der halbe Preis zur&uuml;ckerstattet.<br />
			Wir vermitteln nur den R&uuml;ckholer. Das Fahrzeug stellt Ihr. Es muss in einwandfreiem Zustand sein und gen&uuml;gend Energievorrat haben.<br />
			Die Versicherung ist Eure Sache. Wir lehnen jede Haftung f&uuml;r Sch&auml;den im Zusammenhang mit dem Service ab.
		</div>
EOT;
}

$renderedHTML .= <<<EOT
		</div>
EOT;



// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Einleitung f&uuml;r Teilnehmer', 'page', $renderedHTML);

?>
