<?php
require_once ('inc.php');
$renderedHTML = <<<EOT
		<div class=rm_h1>Einleitung</div>
		<div class=big_col_block>

			<div class=rm_h2>Willkommen bei der {$rm_name}!</div>
			<div class=rm_text>
				Vom {$rm_datum} finden die Schweizer Meisterschaften im Segelfliegen im Birrfeld statt. 
				Wir heissen Sie als Besucher herzlich willkommen. <br />
				Besuchen Sie uns im Birrfeld, lernen Sie den Segelflugsport etwas n&auml;her kennen!<br /> 
				Lassen Sie sich von der eleganten Erscheinung der Segelflugzeuge beeindrucken, erleben Sie die spezielle Atmosph&auml;re die nur an Wettbewerben entsteht!</div>

			<div class=rm_h2>Tagesablauf</div>
			<div class=rm_text>
				Bei gutem Wetter werden ab ca. 9:00 die Flugzeuge montiert und aufgestellt.<br />
				Das Briefing, an dem die Tagesaufgabe bekannt gegeben wird, findet jeweils um 10:00 statt. <br />
				Anschliessend an das Briefing wird der Schleppbetrieb gestartet. <br />
                Je nach Wetter und Aufgabe k&ouml;nnen die Abfl&uuml;ge der Piloten beobachtet werden.<br />
				Ziel&uuml;berfl&uuml;ge &uuml;ber dem Flugplatz sind jeweils ab 16 Uhr zu erwarten.<br />
                Zudem sind die Positionen der Teilnehmenden im <a href="../frame.php?hnav=tracking/tracking_hnav.php&page=sponsoren/sponsoren.php" target="frameset">Live Tracking</a> jederzeit sichtbar.<br />
				Das Live Tracking ist im Briefingraum aufgeschaltet.
            </div>

			<div class=rm_h2>Festwirtschaft</div>
			<div class=rm_text>
				In der Festwirtschaft gibt es von Morgens bis Abends viele K&ouml;stlichkeiten f&uuml;r Piloten und Besucher. <br />
				Vom Fr&uuml;hst&uuml;ck &uuml;ber Grillspezialit&auml;ten und t&auml;glich wechselnden Tagesmen&uuml;s bis zum Kuchenbuffet 
				ist gesorgt.
            </div>

<!-- 
            <div class=rm_h2>Schnupperfl&uuml;ge</div>
            <div class=rm_text>Sie k&ouml;nnen bei uns auch selbst ins Cockpit
                steigen und das lautlose Gleiten in einem Segelflugzeug erleben. Wir
                f&uuml;hren Schnupperfl&uuml;ge mit doppelsitzigen Segelflugzeugen durch. Die
                Fl&uuml;ge werden von unseren erfahrenen Fluglehrern durchgef&uuml;hrt. <br />
                <b>Lassen Sie sich diese einmalige Gelegenheit nicht entgehen!</b> <br />
                Die Schnupperfl&uuml;ge finden voraussichtlich jeweils am Nachmittag statt,
                nachdem die Wettbewerbsteilnehmer abgeflogen sind. Weitere Ausk&uuml;nfte und
                Ticket's f&uuml;r Schnupperfl&uuml;ge k&ouml;nnen am Infostand bezogen werden. Die
                Preise stehen noch nicht fest, die Angaben sind <b>Richtwerte</b>:
                <div class=rm_text>
                <table>
                	<tr>
                		<td class=th_1>Segelflug</td>
                		<td class=th_1>Dauer ca. 20 - 30 min.</td>
                		<td class=th_1_r>80.- CHF</td>
                	</tr>
                	<tr>
                		<td class=th_1>Akroflug</td>
                		<td class=th_1>Dauer ca. 15 min.</td>
                		<td class=th_1_r>150.- CHF</td>
                	</tr>
                </table>
            </div>
 -->
			<br />
			
			<div class=rm_text>
				<b>Egal ob Sie:</b><br />

				<ul>
					<li>die eleganten Segler aus der N&auml;he sehen m&ouml;chten</li>
<!--
					<li>selbst das lautlose Gleiten in einem Segelflugzeug erleben m&ouml;chten</li>
-->
					<li>sich in unserer Festwirtschaft preiswert und gut verpflegen m&ouml;chten</li>
				</ul>
				<b>... ein Besuch der Segelflugmeisterschaft im Birrfeld lohnt sich in jedem Fall!</b><br />
			</div>


			<div class=rm_h2>Sicherheit</div>
			<div class=rm_text>
                Bitte betreten Sie zu Ihrer Sicherheit und jene der Piloten nie den Tarmac! Diese geteerte Fl&auml;che ist f&uuml;r die Flugzeuge bestimmt. 
                &Uuml;berschreiten Sie keine der Pisten! Benutzen Sie den Weg auf der Westseite des Flugplatzes.
                Im Zweifelsfalle wenden Sie sich bitte an einen der zahlreichen Funktion&auml;re.
			</div>

			<br />
			<div class=rm_h2>Besucher-Parkpl&auml;tze</div>

        	<div class=rm_text>
        		Stellen Sie Ihr Fahrzeug auf den gekennzeichneten Parkpl&auml;tzen - Besten Dank. 
        	</div>
			
			<div class=rm_h2>Pl&auml;ne und Karten</div>
			<div class="sidebarlink"><b><a	href="../woistwas/uebersicht.php" target="_self">Was Ist Wo&nbsp;</a></b></div>

		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Besucher - Einleitung', 'page', $renderedHTML);

?>
