<?php
require_once ('inc.php');


$page = "news.php?datum=" . $rm_new_year;

$newsItem = new NewsItems();
$lastNewsDate = $newsItem->getLastNewsEntryDate();
if ($lastNewsDate) {
    if ($lastNewsDate < $rm_dates[0]) {
        $lastNewsDate = $rm_new_year;
    }
    $page = "news.php?datum=" . $lastNewsDate;
}

$frameset = <<<EOT
    <frameset rows="60,*" framespacing="2" frameborder="0">
		<frame name="hnav" marginwidth="5" marginheight="2" scrolling="no" noresize src="news_admin_hnav.php">
		<frame name="page" marginwidth="5" marginheight="2" scrolling="auto" noresize src="{$page}">
	</frameset>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayFrameset('admin news hFrame', $frameset);
?>
