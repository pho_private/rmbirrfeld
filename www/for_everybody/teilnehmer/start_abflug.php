<?php
require_once ('inc.php');
$renderedHTML = <<<EOT
		<div class=rm_h1>Start und Abflug</div>
		<div class=col_block>
			<div class=rm_h2>Funkfrequenzen {$rm_name_kurz}</div>
			<div class=rm_text>
				<table width="100%">
					<tr>
						<th class=th_1>Bezeichnung/Verwendung</th>
						<th class=th_1_c>Frequenz [MHz]</th>
					</tr>
					<tr>
						<td class=td_1>Schlepp und Absaufen (LSZF/M)</td>
						<td class=td_1_c>123.555</td>
					</tr>
					<tr>
						<td class=td_1>Campo & Omega Birrfeld (LSZF/S)</td>
						<td class=td_1_c>119.830</td>
					</tr>
					<tr>
						<td class=td_1>Luft-Luft Nord</td>
						<td class=td_1_c>123.580</td>
					</tr>
					<tr>
						<td class=td_1>Luft-Luft West</td>
						<td class=td_1_c>125.030</td>
					</tr>
					<tr>
						<td class=td_1>Luft-Luft Alpen</td>
						<td class=td_1_c>123.680</td>
					</tr>
				</table>
			</div>
			<div class=rm_h2>Funkfrequenzen SFR TMA Z&uuml;rich</div>
			<div class=rm_text>
				<table width="100%">
					<tr>
						<th class=th_1>Bezeichnung/Verwendung</th>
						<th class=th_1_c>Frequenz [MHz]</th>
					</tr>
					<tr>
						<td class=td_1>ATIS</td>
						<td class=td_1_c>120.880</td>
					</tr>
					<tr>
						<td class=td_1>H&ouml;rbereitschaft</td>
						<td class=td_1_c>122.305</td>
					</tr>
				</table>
			</div>
			<div class=rm_h2>Funkfrequenzen SFR TMA Genf</div>
			<div class=rm_text>
				<table width="100%">
					<tr>
						<th class=th_1>Bezeichnung/Verwendung</th>
						<th class=th_1_c>Frequenz [MHz]</th>
					</tr>
					<tr>
						<td class=td_1>ATIS</td>
						<td class=td_1_c>124.775</td>
					</tr>
					<tr>
						<td class=td_1>Aktivierung (Geneva Delta)</td>
						<td class=td_1_c>119.175</td>
					</tr>
					<tr>
						<td class=td_1>H&ouml;rbereitschaft</td>
						<td class=td_1_c>125.030</td>
					</tr>
				</table>
			</div>
			<div class=rm_h2>Funkfrequenzen SFR TMA Stuttgart</div>
			<div class=rm_text>
				<table width="100%">
					<tr>
						<th class=th_1>Bezeichnung/Verwendung</th>
						<th class=th_1_c>Frequenz [MHz]</th>
					</tr>
					<tr>
						<td class=td_1>ATIS </td>
						<td class=td_1_c>119.325</td>
					</tr>
					<tr>
						<td class=td_1>Aktivierung (Langen Info)</td>
						<td class=td_1_c>128.950</td>
					</tr>
					<tr>
						<td class=td_1>H&ouml;rbereitschaft</td>
						<td class=td_1_c>134.500</td>
					</tr>
				</table>
			</div>
			
			
			<div class=rm_h2>Start / Schlepp</div>
			<div class=rm_text>
				Nicht vergessen den Logger einzuschalten. W&auml;hrend des Schlepps muss die Schlepp-Frequenz gerastet sein.
				Nach dem Klinken auf Platz-Frequenz wechseln.
			</div>
		</div>
		<div class=lst_col_block>
		
			<div class=rm_h2>Landungen w&auml;hrend der Startphase</div>
			<div class=rm_text>
				Rastet die Schlepp-Frequenz. Fr&uuml;hzeitig &uuml;ber Funk melden.
				Ohne andere Anweisungen: Lange Landung auf dem n&ouml;rdlichen Teil des Segelflugbereichs (ausrollen bis ans Ende!).
				Der R&uuml;cktransport zum Startplatz muss mit dem Flugdienstleiter koordiniert werden.
			</div>
			
			
			<div class=rm_h2>Schleppvolten</div>
			<div class=rm_text>
				F&uuml;r jeden Klinkraum sind Schleppvolten definiert. Diese m&uuml;ssen eingehalten werden.
				Teilnehmende mit eigenstartf&auml;higen Flugzeugen melden sich beim	Schleppchef.
			</div>
			
			
			<div class=rm_h2>Klinkr&auml;ume</div>
			<div class=rm_text>
				In der Regel wird an den Chestenberg geschleppt. In Ausnahmef&auml;llen wird an die Gisliflue geschleppt.
				Klinkh&ouml;he gem&auml;ss Aufgabenblatt.
			</div>
			
			<div class=rm_h2>Alternative Klinkr&auml;ume</div>
			<div class=rm_text>
				Wenn im Birrfeld keine fliegbaren Bedingungen vorliegen, k&ouml;nnen eine oder mehrere Klassen in einen alternativen Klinkraum geschleppt werden.
				Sollte das der Fall sein, werden wir am Briefing die notwendigen Informationen abgeben.
			</div>
			
			
			<div class=rm_h2>Abflug</div>
			<div class=rm_text>
				Die Abflugfreigabe erfolgt auf der Platz-Frequenz.
			</div>
			
		</div>
		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Start und Abflug', 'page', $renderedHTML);

?>
