<?php
require_once ('inc.php');
require_once ('dblib.inc.php');
require_once ('functions.php');

// SeeYou Competition export of competitor's data. This data can be cut & pasted into each classes' .CUC file as its [Pilots] section.

$query = <<<EOT
  SELECT 
	  vorname 
    , name 
    , UPPER(CONCAT(logger1id, ',')) AS logger 
    , flugzeug 
    , immatrikulation 
    , wk 
    , CASE
    	WHEN (klasse BETWEEN 1 AND 10) THEN klasse
       	ELSE 'unbekannte Klasse!'
      END as klasse  
    , indexwert 
    , gruppe 
    , CASE 
    	WHEN immatrikulation LIKE 'HB-2%' THEN 0
      	WHEN immatrikulation LIKE 'D-K%' THEN 0
      	ELSE 1
      END AS pureglider
 FROM {$rm_tbl_teilnehmer} 
	WHERE rmactiv='ja' 
  ORDER BY klasse, name, vorname;
EOT;

$fileName = "";

$ResultPointer = DBQuery($query);

if ($ResultPointer) {
    $newLine = chr(13) . chr(1);
    $ExportHeader = 'first_name,last_name,flight_recorders,aircraft_model,aircraft_registration,contestant_number,class,handicap,club,pure_glider';
    $ExportHeader .= $newLine;

    $renderedHTML = '';
    $lastClass = '';

    for ($i = 0; $i < mysqli_num_rows($ResultPointer); $i ++) {
        $Daten = DBFetchRow($ResultPointer);
        $Daten[6] = $class_name[$Daten[6]]; // convert class number to class name.
        if ($Daten[6] != $lastClass) { // insert a class title to simplify the handling
            if ($lastClass != '') { // if not first entry, add trailing line
                $renderedHTML .= $newLine;
            }
            $lastClass = $Daten[6];
            $renderedHTML .= $newLine . $ExportHeader;
        }
        foreach ($Daten as $data) {
            $renderedHTML .= "\"" . str_replace("\"", "\"\"", $data) . "\",";
        }
        $renderedHTML = substr($renderedHTML, 0, strlen($renderedHTML) - 1) . $newLine;
    }
    
    // delete existing old versions of list
    deleteFilesLike("../../resources/downloads/cuc_teilnehmer_*.txt");

    // define static filename
    $filename = "../../resources/downloads/cuc_teilnehmer_" . date('d-M-Y') . ".txt";

    // write file contents 
    file_put_contents($filename, $renderedHTML, LOCK_EX);
}

rm_DownloadListPage("CUC CSV", $filename);

?>

