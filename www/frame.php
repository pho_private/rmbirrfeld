<?php
require_once ('inc.php');

// TODO unify column framesets into one with session attribute control

$frameset = <<<EOT
<frameset cols="125,*" framespacing="2" frameborder="0">
	<frame name="vnav"  marginwidth="2" marginheight="2" scrolling="auto" src="for_everybody/vnav_public.php" noresize />
	<frame name="frameset" marginwidth="2" marginheight="2" scrolling="auto" src="for_everybody/frame.php" />
</frameset>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayFrameset("{$rm_name_lang}", $frameset);
?>
