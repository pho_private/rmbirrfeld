<?php
require_once ('inc.php');
require_once ('dblib.inc.php');
require_once ('tracking_glide_and_seek.php');

class rm
{

    function load_table($tableName, $whereClause = '', $orderClause = '')
    {
        $table = array();
        $result = DBQuery("SELECT * FROM {$tableName} {$whereClause} {$orderClause}");
        if ($result) {
            $row = DBFetchAssoc($result);
            while ($row) {
                $table[] = $row;
                $row = DBFetchAssoc($result);
            }
        }
        reset($table);
        return $table;
    }
}

class Helfer
{
    function queryResult($query)
    {
        $table = array();
        $dbobject = DBQuery($query);
        if ($dbobject) {
            $row = DBFetchAssoc($dbobject);
            while ($row) {
                array_push($table, $row);
                $row = DBFetchAssoc($dbobject);
            }
        }
        // print "<br />queryResult($query):<br />";
        // print_r($table);
        return $table;
    }

    function load_all_helfers()
    {
        global $rm_tbl_einsaetze;
        $select = "SELECT DISTINCT name, vorname, jahrgang, mail, phone, wohnort, gruppe FROM {$rm_tbl_einsaetze} WHERE deleted <> NULL ORDER BY name, vorname, wohnort, gruppe";
        return $this->queryResult($select);
    }

    function load_number_of_engagements_per_slot()
    {
        $select = "SELECT gebiet,datum,schicht, COUNT(*) AS anzahl FROM einsaetze WHERE deleted <> NULL GROUP BY datum, gebiet, schicht";
        $tempTbl = $this->queryResult($select);
        // now create a value associative array instead of column name associative as delivered by MySQL.
        $retval = array();
        foreach ($tempTbl as $assignment) {
            $retval["{$assignment['gebiet']}"]["{$assignment['datum']}"]["{$assignment['schicht']}"] = $assignment['anzahl'];
        }
        return $retval;
    }

    function load_helfer_einsaetze($vorname, $name, $jahrgang, $ort, $gruppe)
    {
        global $rm_tbl_einsaetze;
        $select = "SELECT gebiet AS area, datum AS date, schicht AS shift FROM $rm_tbl_einsaetze WHERE vorname = '{$vorname}' AND name = '{$name}' AND wohnort = '{$ort}' AND jahrgang = '{$jahrgang}' AND gruppe = '{$gruppe}' AND deleted <=> NULL GROUP BY gebiet, datum, schicht ORDER BY gebiet, datum, schicht";
        $retval = $this->queryResult($select);
        // print "<br />load_helfer_Einsaetze($vorname, $name, $jahrgang, $ort, $gruppe):<br />";
        // print_r($retval);
        return $retval;
    }

    function load_list()
    {
        global $rm_tbl_einsaetze;
        $einsatze_activ = array();
        $einsatze_inactiv = array();
        $select = "SELECT gruppe, name, vorname, wohnort, jahrgang, mail, phone, 'aktiv' as activ, COUNT(schicht) AS einsaetze, created, updated, deleted FROM $rm_tbl_einsaetze WHERE deleted is NULL GROUP BY gruppe, name, vorname, mail, phone ORDER BY gruppe, name, vorname";
        $einsatze_activ = $this->queryResult($select);
        $select = "SELECT gruppe, name, vorname, wohnort, jahrgang, mail, phone, 'gelöscht' as activ, COUNT(schicht) AS einsaetze, created, updated, deleted  FROM $rm_tbl_einsaetze WHERE deleted is not NULL GROUP BY gruppe, name, vorname, mail, phone ORDER BY gruppe, name, vorname";
        $einsatze_inactiv = $this->queryResult($select);
        $einsaetze = array_merge($einsatze_activ, $einsatze_inactiv);
        return $einsaetze;
    }

    function load_gruppe()
    {
        $einsatze_activ = array();
        $einsatze_inactiv = array();
        global $rm_tbl_einsaetze;
        $select = "SELECT gruppe, 'aktiv' as activ, COUNT(schicht) AS einsaetze FROM $rm_tbl_einsaetze WHERE deleted is NULL GROUP BY gruppe ORDER BY gruppe";
        $einsatze_activ = $this->queryResult($select);
        $select = "SELECT gruppe, 'gelöscht' as activ, COUNT(schicht) AS einsaetze FROM $rm_tbl_einsaetze WHERE deleted is NOT NULL GROUP BY gruppe ORDER BY gruppe";
        $einsatze_inactiv = $this->queryResult($select);
        $einsaetze = array_merge($einsatze_activ, $einsatze_inactiv);
        return $einsaetze;
    }
}

class Teilnehmer
{

    public $list;

    private $tn_table;

    // constructor
    function __construct($class = '')
    {
        global $rm_tbl_teilnehmer;
        $this->tn_table = $rm_tbl_teilnehmer;
        $this->load($class);
    }

    function load($class = '')
    {
        $rm = new rm();
        if ($class == '') {
            $this->list = $rm->load_table($this->tn_table, "WHERE name != ''", "ORDER BY name, vorname");
        } else {
            $this->list = $rm->load_table($this->tn_table, "WHERE rmactiv = 'ja' AND name != '' AND klasse = '$class'", "ORDER BY name, vorname");
        }
    }

    function load_all_by_class()
    {
        $rm = new rm();
        $this->list = $rm->load_table($this->tn_table, "WHERE name != '' AND rmactiv = 'ja'", "ORDER BY klasse, wk");
    }

    function load_class_by_wk($class)
    {
        $rm = new rm();
        $this->list = $rm->load_table($this->tn_table, "WHERE name != '' AND rmactiv = 'ja' AND klasse = '$class'", "ORDER BY wk");
    }

    function numactiv()
    {
        $result = DBQuery("SELECT COUNT(*) as num FROM " . $this->tn_table . " WHERE rmactiv = 'ja' AND name != ''");
        if ($result) {
            $row = DBFetchAssoc($result);
            if ($row) {
                return $row['num'];
            }
        }
        return 0;
    }

    function item_by_loggerid($loggerid)
    {
        foreach ($this->list as $line) {
            $loggerIdArray = explode(",", strtoupper($line['logger1id']));           
            if (in_array(strtoupper($loggerid), $loggerIdArray)) {
                return $line;
            }
        }
    }

    function item_by_wk($wk)
    {
        foreach ($this->list as $line) {
            if ($line['rmactiv'] != 'ja')
                continue;
            if ($line['wk'] == $wk)
                return $line;
        }
    }

    function item_by_account($account)
    {
        foreach ($this->list as $line) {
            if ($line['account'] == $account)
                return $line;
        }
    }

    function is_foto_in_use($foto)
    {
        foreach ($this->list as $tn) {
            if ($tn['foto'] == $foto)
                return true;
            if ($tn['foto2'] == $foto)
                return true;
            if ($tn['foto3'] == $foto)
                return true;
            if ($tn['foto4'] == $foto)
                return true;
            if ($tn['foto5'] == $foto)
                return true;
        }
        return false;
    }

    private function update_db($login, $key, $value)
    {
        $query = "UPDATE " . $this->tn_table . " SET {$key}=" . (($value == 'NULL') ? "NULL" : "'{$value}'") . " WHERE login='{$login}'";
        // print $query;
        if (! DBQuery($query)) {
            print "<p /> error updating database: " . DBErrNo() . ": " . DBError();
        }
        // after update, if the Flarm ID has changed, write the aircraft file for tracking.
        if (strtolower($key) == 'flarmid') {
            gsTrackingWriteAircraftFiles(true);
        }
        
    }

    // $number = Pilotennummer (Teamklasse)
    function set_foto_by_login($login, $fotoname, $number = '')
    {
        if ($number == 1) {
            $number = '';
        }
        $fotoname = str_replace('.jpg', '', strtolower($fotoname));
        $this->update_db($login, 'foto' . $number, $fotoname);
    }

    function set_sportlizenz_by_login($login, $index)
    {
        $this->update_db($login, 'sportlizenz', $index);
    }
    
    function set_igc_id_by_login($login, $index)
    {
        $this->update_db($login, 'igc_rank_id', $index);
    }

    function set_index_by_login($login, $index)
    {
        $this->update_db($login, 'indexwert', $index);
    }

    function set_fsb_by_login($login, $fsb)
    {
        $this->update_db($login, 'fsb', $fsb);
    }

    function set_id_by_login($login, $id)
    {
        $this->update_db($login, 'logger1id', $id);
    }

    function set_flarmid_by_login($login, $flarmid)
    {
        $this->update_db($login, 'FlarmId', $flarmid);
    }

    function set_paid_by_login($login)
    {
        global $today;
        $this->update_db($login, 'bezahlt', $today);
    }

    function reset_paid_by_login($login)
    {
        $this->update_db($login, 'bezahlt', 'NULL');
    }

    function set_ausweise_ok_by_login($login)
    {
        $this->update_db($login, 'ausweise', 'ja');
    }

    function set_rueckholen_by_login($login)
    {
        $this->update_db($login, 'rueckholen', 'ja');
    }

    function reset_ausweise_ok_by_login($login)
    {
        $this->update_db($login, 'ausweise', 'nein');
    }

    function reset_rueckholen_by_login($login)
    {
        $this->update_db($login, 'rueckholen', 'nein');
    }

    function set_gotgift_ok_by_login($login)
    {
        $this->update_db($login, 'gotgift', 'ja');
    }

    function reset_gotgift_ok_by_login($login)
    {
        $this->update_db($login, 'gotgift', 'nein');
    }

    function set_camping_ok_by_login($login)
    {
        $this->update_db($login, 'camping', 'ja');
    }

    function reset_camping_ok_by_login($login)
    {
        $this->update_db($login, 'camping', 'nein');
    }

    function set_junior_ok_by_login($login)
    {
        $this->update_db($login, 'junior', 'ja');
    }

    function reset_junior_ok_by_login($login)
    {
        $this->update_db($login, 'junior', 'nein');
    }

    function load_gruppe()
    {
        $table = array();
        $result = DBQuery("SELECT gruppe, COUNT(name) AS anzahl FROM " . $this->tn_table . " WHERE rmactiv = 'ja' AND name != '' GROUP BY gruppe ORDER BY anzahl DESC");
        if ($result) {
            $row = DBFetchAssoc($result);
            while ($row) {
                $table[] = $row;
                $row = DBFetchAssoc($result);
            }
        }
        return $table;
    }
}

class emails
{

    var $list;

    function __construct()
    {
        $this->load();
    }

    function load()
    {
        global $rm_tbl_emails;
        $rm = new rm();
        $this->list = $rm->load_table($rm_tbl_emails, "", "ORDER BY timestamp DESC");
    }

    function item_by_id($id)
    {
        foreach ($this->list as $line) {
            if ($line['id'] == $id)
                return $line;
        }
    }

    function delete($id)
    {
        global $rm_tbl_emails;
        DBQuery("DELETE FROM " . $rm_tbl_emails . " WHERE id={$id}");
    }

    function insert($subject, $welcome, $message, $greeting)
    {
        global $rm_tbl_emails;
        $timestamp = date("Y-m-d H:i:s");
        $statement = "INSERT INTO " . $rm_tbl_emails . " (subject, welcome, message, greeting, timestamp) VALUES ('{$subject}', '{$welcome}', '{$message}', '{$greeting}', '{$timestamp}')";
        $result = DBQuery($statement);
    }
}

class NewsDates
{
    var $list;

    function __construct($datum = NULL)
    {
        $this->load($datum);
    }

    function load($datum = NULL)
    {
        global $rm_tbl_newsDates;
        global $rm_dates;
        global $rm_new_year;
        global $rm_msg;
        global $debug;
        $rm = new rm();

        $whereClause = '';
        if (! is_null($datum)) {
            if ($datum < $rm_dates[0]) {
                $whereClause = "WHERE datum >= '{$rm_new_year}' AND datum < '{$rm_dates[0]}'";
            } else {
                $whereClause = "WHERE datum = '{$datum}'";
            }
        } else {
            # only retrieve dates from current $rm_year
            $lastDay = end($rm_dates);
            $whereClause = "WHERE datum >= '{$rm_new_year}' AND datum <= '{$lastDay}'";
        }
        if ($debug) {
            $rm_msg = $rm_msg . "<br />news retrieved with {$whereClause}";
        }
        $this->list = $rm->load_table($rm_tbl_newsDates, $whereClause, "");
    }

    function last()
    {
        return array_shift($this->list);
    }

    function numreleased()
    {
        $cnt = 0;
        foreach ($this->list as $news) {
            if ($news['released'] == 'on')
                $cnt ++;
        }
        return $cnt;
    }

    function isreleased($datum)
    {
        global $rm_tbl_newsDates;
        $result = DBQuery("SELECT * FROM $rm_tbl_newsDates WHERE datum='{$datum}' AND released = 'on'");

        return mysqli_num_rows($result) == 1;
    }

    function lock($datum)
    {
        global $rm_tbl_newsDates;
        DBQuery("UPDATE $rm_tbl_newsDates SET released='off' WHERE datum='{$datum}'");
    }

    function release($datum)
    {
        global $rm_tbl_newsDates;
        DBQuery("UPDATE $rm_tbl_newsDates SET released='on' WHERE datum='{$datum}'");
    }
}

class NewsItems
{

    var $list;

    function __construct($datum = NULL)
    {
        $this->load($datum);
    }

    function load($datum = NULL)
    {
        global $rm_tbl_newsItem;
        global $rm_dates;
        global $rm_new_year;
        $rm = new rm();
        $whereClause = "";
        if (! is_null($datum)) {
            if ($datum < $rm_dates[0]) {
                $whereClause = "WHERE datum >= '{$rm_new_year}' AND datum < '{$rm_dates[0]}'";
            } else {
                $whereClause = "WHERE datum = '{$datum}'";
            }
        }
        $this->list = $rm->load_table($rm_tbl_newsItem, $whereClause, "ORDER BY datum DESC, zeit DESC");
    }

    function item_by_id($id)
    {
        foreach ($this->list as $item) {
            if ($item['id'] == $id)
                return $item;
        }
    }

    function item_by_date_time($datum, $zeit)
    {
        foreach ($this->list as $item) {
            if ($item['datum'] == $datum && $item['zeit'] == $zeit)
                return $item;
        }
    }

    function create_item($datum, $zeit)
    {
        global $rm_tbl_newsItem;
        global $rm_new_year;
        // Spezialfall News Vorbereitung: Falls das gelieferte Datum Neujahr ist, so wird der NewsItem am heutigen Datum angelegt.
        if ($datum == "{$rm_new_year}") {
            $datum == date("Y-M-d");
        }
        $statement = "INSERT INTO $rm_tbl_newsItem (datum, zeit, released) VALUES ('{$datum}', '{$zeit}', 'off')";
        return DBQuery($statement);
    }

    // copies item between NewsDates
    function copy_item($id, $datum)
    {
        global $rm_tbl_newsItem;
        global $rm_new_year;
        $item = $this->item_by_id($id);

        // Spezialfall News Vorbereitung: Falls das gelieferte Datum Neujahr ist, so wird der NewsItem am heutigen Datum angelegt.
        if ($datum == "{$rm_new_year}") {
            $datum == date("Y-M-d");
        }
        $statement = "INSERT INTO {$rm_tbl_newsItem} (datum, zeit, titel, text, released) VALUES ('{$datum}', '{$item['zeit']}', '{$item['titel']}', '{$item['text']}', 'off')";
        $result = DBQuery($statement);
        if (mysqli_error()) {
            return "Dieser Eintrag existiert schon. Bitte vor dem Kopieren l&ouml;schen!";
        }
        return $result;
    }

    // copies item within NewsDate
    function duplicate_item($id, $zeit)
    {
        global $rm_tbl_newsItem;
        $line = $this->item_by_id($id);
        $statement = "INSERT INTO {$rm_tbl_newsItem} (datum, zeit, titel, text, released) VALUES ('{$line['datum']}', '{$zeit}', '{$line['titel']}', '{$line['text']}', 'off')";
        $result = DBQuery($statement) or $error = mysqli_error();
        if ($error)
            return $error;
        return $result;
    }

    function delete_item($id)
    {
        global $rm_tbl_newsItem;
        DBQuery("DELETE FROM $rm_tbl_newsItem WHERE id=$id");
    }

    function update_item($id, $datum, $zeit, $titel, $text)
    {
        global $rm_tbl_newsItem;
        global $rm_year;
        // Spezialfall News Vorbereitung: Falls das gelieferte Datum Neujahr ist, so wird der NewsItem am heutigen Datum angelegt.
        if ($datum == "{$rm_year}-01-01") {
            $result = DBQuery("SELECT datum FROM {$rm_tbl_newsItem} WHERE id='{$id}'");
            if ($result) {
                $value = DBFetchRow($result);
                $datum = $value[0];
            }
        }
        // echo $datum;
        DBQuery("UPDATE $rm_tbl_newsItem SET datum='{$datum}', zeit='{$zeit}', titel='{$titel}', text='{$text}' WHERE id='{$id}'");
    }

    function lock_item($id)
    {
        global $rm_tbl_newsItem;
        DBQuery("UPDATE $rm_tbl_newsItem SET released='off' WHERE id='{$id}'");
    }

    function release_item($id)
    {
        global $rm_tbl_newsItem;
        DBQuery("UPDATE $rm_tbl_newsItem SET released='on' WHERE id='{$id}'");
    }

    function getLastNewsEntryDate()
    {
        // in order to prevent showing prematurely released future news, this function ALWAYS returns a date less than the current date.
        global $rm_tbl_newsItem;
        // $result = DBQuery("SELECT MAX(datum) FROM {$rm_tbl_newsItem} WHERE released = 'on' AND datum <= DATE(NOW())");
        $result = DBQuery("SELECT MAX(datum) FROM {$rm_tbl_newsItem} WHERE released = 'on' ");
        if ($result) {
            $value = DBFetchRow($result);
            return $value[0];
        }
        return false;
    }
}

class IGCFileNameHandler
{

    private $igcFilename = '';

    private $igcfn = '';

    // structure containing all parts of the file name
    private $completeYear;

    // the guessed 1-digit year of the file name. We assume that flights are not older than 10 years.
    private $year;

    // file name parts
    private $month;

    private $day;

    private $manufacturer;

    private $serial;

    private $sequenceNo;

    private $error = '';

    private function convertToInt($charsoup)
    {
        return (is_int($charsoup)) ? $charsoup : (ord($charsoup) - ord('a') + 10);
    }

    function setIgcFilename($igcFilename)
    {
        $this->igcFilename = $igcFilename;
        // tricky thing here: manufacturer code ist most often 1 character, however, it may also be 3 characters - so, filename length is variable.
        if (preg_match('/^([0-9])([1-9abc])([1-9a-v])([0-9a-z]|[0-9a-z]{3})([0-9a-z]{3})([1-9a-z])\.igc$/', $igcFilename, $this->igcfn) == 1) 
        {
            // set components
            $this->setComponents();
            return true;
        } else {
            $this->error .= "Dateiname $igcFilename entspricht nicht den IGC Konventionen - bitte nicht umbenennen!<br />";
            return false;
        }
    }

    private function setComponents()
    {
        $this->year = $this->igcfn[1];
        $this->month = $this->convertToInt($this->igcfn[2]);
        $this->day = $this->convertToInt($this->igcfn[3]);
        $this->manufacturer = $this->igcfn[4];
        $this->serial = $this->igcfn[5];
        $this->sequenceNo = $this->igcfn[6];
        // calculate a year up to 9 years back
        $thisYear = date('Y');
        $digit = $thisYear % 10;
        $this->completeYear = ($digit < $this->year) ? (10 * (($thisYear / 10) - 1) + $this->year) : (10 * ($thisYear / 10) + $this->year);
    }

    function getIgcFilenameComponents($igcFilename)
    {
        if ($this->setIgcFilename($igcFilename)) {
            return $this->igcfn;
        } else {
            return false;
        }
    }

    function getError()
    {
        return $this->error;
    }

    function setYear($year)
    {
        if (is_numeric($year)) {
            $this->year = substr($year, - 1, 1);
            $this->completeYear = $year;
            return true;
        } else {
            $this->error .= "year {$year} not numeric.<br />";
        }
        return false;
    }

    function setMonth($month)
    {
        if (is_numeric($month)) {
            if ($month > 0 && $month < 13) {
                $this->month = $month;
                return true;
            } else {
                $this->error .= "month {$month} outside range 1..12.<br />";
            }
        } else {
            $this->error .= "month {$month} not numeric.<br />";
        }
        return false;
    }

    function setDay($day)
    {
        if (is_numeric($day)) {
            if ($day > 0 && $day < 32) {
                $this->day = $day;
                return true;
            } else {
                $this->error .= "day {$day} outside range 1..31.<br />";
            }
        } else {
            $this->error .= "day {$day} not numeric.<br />";
        }
        return false;
    }

    function setDate($datestring)
    {
        $dateObj = new DateTime($datestring);
        if ($datestring == $dateObj->format('Y-m-d')) {
            $this->setYear($dateObj->format('Y'));
            $this->setMonth($dateObj->format('m'));
            $this->setDay($dateObj->format('d'));
            return true;
        } else {
            $this->error .= "date $datestring not understandable; use 'Y-m-d' convention.<br />";
            return false;
        }
    }

    function getIgcDate()
    {
        if (isset($this->year) && isset($this->month) && isset($this->day)) {
            $this->igcfn[1] = $this->year;
            $this->igcfn[2] = ($this->month < 10) ? (substr((string) $this->month, - 1, 1)) : (chr(ord('a') + $this->month - 10));
            $this->igcfn[3] = ($this->day < 10) ? (substr((string) $this->day, - 1, 1)) : (chr(ord('a') + $this->day - 10));
            return $this->igcfn[1] . $this->igcfn[2] . $this->igcfn[3];
        } else {
            $this->error .= "year, month or day not set. Set those components first.\n";
            return false;
        }
    }

    function getDateString()
    {
        return $this->completeYear . "-" . substr("0" . $this->month, - 2, 2) . "-" . substr("0" . $this->day, - 2, 2);
    }
}
?>
