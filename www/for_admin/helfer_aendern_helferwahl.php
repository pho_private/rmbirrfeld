<?php
require_once ('inc.php');
require_once ('dblib.inc.php');
require_once ('functions.php');

$renderedHTML = <<<EOT
		<div class=rm_h1>&Auml;ndern von Einsatzdaten</div>
		<br />
		<div class=rm_text>
		W&auml;hle die Person aus, f&uuml;r welche du eine &Auml;nderung vornehmen m&ouml;chstest.
		</div>
		<br />
		<form method="POST" action="../for_everybody/helfer/helfer_einsatz.php">
			<select size=1 name="helfer">
EOT;
$select = "SELECT DISTINCT name, vorname, jahrgang, mail, phone, wohnort, gruppe FROM {$rm_tbl_einsaetze} ORDER BY name, vorname, wohnort, gruppe";
$result = DBQuery($select);
$row = DBFetchRow($result);
$i = 0;
while ($row) {
    $renderedHTML .= "<option>" . "{$i},{$row[0]},{$row[1]},{$row[5]},{$row[6]}" . "</option>\n";
    $row = DBFetchRow($result);
    $i += 1;
}
$renderedHTML .= <<<EOT
			</select>
			<input type=hidden name=submit value="aendern">
			<input type=submit name=aendern value="&auml;ndern">
		</form>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Helfer - &Auml;nderung', 'page', $renderedHTML);

?>
