<?php
require_once ('inc.php');
require_once ('tracking_glide_and_seek.php');

// ------------------------------------------------------
// start of main code
// ------------------------------------------------------

$overwrite = TRUE;
$result = gsTrackingWriteCompetitionFiles($overwrite);

// display link to download new file
$renderedHTML = <<<EOT
    <div class="rm_h1">Tracking Reset</div>
    <div class="rm_text">Die Trackingdaten wurden neu erzeugt.</div>
EOT;

rm_displayPageAndSponsors("Tracking-Reset", "page", $renderedHTML);



?>
