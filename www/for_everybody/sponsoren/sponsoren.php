<?php
require_once ('inc.php');
$renderedHTML = <<<EOT
	<div class="rm_h1">Unsere Sponsoren</div>
	<div class=big_col_block>
		<div class="rm_text">
			Wir m&ouml;chten uns bei allen Unternehmen, Institutionen und Privatpersonen bedanken, welche uns unterst&uuml;tzen.<br />
			Solche Anl&auml;sse sind undenkbar ohne eine entsprechende Unterst&uuml;tzung.
		</div>
	</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Unsere Sponsoren', 'page', $renderedHTML);

?>
