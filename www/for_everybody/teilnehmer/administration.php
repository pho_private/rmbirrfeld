<?php
require_once ('inc.php');
require_once ('functions.php');

$openingDay = getWeekdayDateString($rm_dates[0]);
$secondDay  = getWeekdayDateString($rm_dates[1]);
$finalDay    = getWeekdayDateString(end($rm_dates));
$renderedHTML = <<<EOT
		<div class=rm_h1>Administration</div>
		<div class=col_block>
			<div class=rm_h2>Termine</div>
			<div class=rm_text>
				<table>
					<tr>
						<th class=th_1>Anmeldeschluss</th>
						<td class=td_1 colspan=2><b>Am Er&ouml;ffnungsbriefing</b></td>
					</tr>
					<tr>
						<th class=th_1>Er&ouml;ffnungsbriefing</th>
						<td class=td_1>{$openingDay}</td>
						<td class=td_1_r>09:30</td>
					</tr>
                    <tr>
						<th class=th_1>T&auml;gliches Briefing</th>
						<td class=td_1>ab {$secondDay}</td>
						<td class=td_1_r>10:00</td>
					</tr>
					<tr>
						<th class=th_1>Wettbewerbstage</th>
						<td class=td_1>{$rm_datum}</td>
						<td class=td_1_r></td>
					</tr>
					<tr>
						<th class=th_1>Siegerehrung</th>
						<td class=td_1>{$finalDay}</td>
						<td class=td_1_r>ca. 18:00</td>
					</tr>
				</table>
			</div>
			
			<div class=rm_h2>Preise</div>

			<div class=rm_text>
				<table>
						<th class=th_1>&nbsp;</th>
						<td class=td_1_r>Senior</td>
						<td class=td_1_r>Junior</td>
EOT;
if ($rm_comp_type == 'RM') {
    $renderedHTML .= <<<EOT
					<tr>
						<th class=th_1>Nenngeld</th>
						<td class=td_1_r>CHF {$rm_nenngeld_senior}.-</td>
						<td class=td_1_r>CHF {$rm_nenngeld_junior}.-</td>
					</tr>
					<tr>
						<th class=th_1>Camping</th>
						<td class=td_1_r>CHF {$rm_camping_senior}.-</td>
						<td class=td_1_r>CHF {$rm_camping_junior}.-</td>
					</tr>
EOT;
} else {
    $renderedHTML .= <<<EOT
					<tr>
						<th class=th_1>Nenngeld</th>
						<td class=td_1_r>CHF {$sm_nenngeld_senior}.-</td>
						<td class=td_1_r>CHF {$sm_nenngeld_junior}.-</td>
					</tr>
					<tr>
						<th class=th_1>Camping</th>
						<td class=td_1_r>CHF {$sm_camping_senior}.-</td>
						<td class=td_1_r>CHF {$sm_camping_junior}.-</td>
					</tr>
EOT;

}

$preiskategorien = 'Nenngeld, Camping';

if ($rm_angebot_rueckholer) {

	if ($rm_comp_type == 'RM') {
    
		$renderedHTML .= <<<EOT
					<tr>
						<th class=th_1>R&uuml;ckholservice</th>
						<td class=td_1_r>CHF {$rm_rueckholen_senior}.-</td>
						<td class=td_1_r>CHF {$rm_rueckholen_junior}.-</td>
					</tr>
EOT;
	} else {

		$renderedHTML .= <<<EOT
		<tr>
			<th class=th_1>R&uuml;ckholservice</th>
			<td class=td_1_r>CHF {$sm_rueckholen_senior}.-</td>
			<td class=td_1_r>CHF {$sm_rueckholen_junior}.-</td>
		</tr>
EOT;

	}
    $preiskategorien .= ', R&uuml;ckholservice';
}

$renderedHTML .= <<<EOT
                </table>
			</div>
EOT;

if ($rm_TeamClass) {
	$renderedHTML .= <<<EOT
	<div class=rm_h2>Teamklasse</div>
	<div class=rm_text>
		Teams bezahlen ein Nenngeld von CHF {$rm_nenngeld_team}.-.
		Der Rückholservice und Camping sind für auch Teams verfügbar.
	</div>
EOT;
}

$renderedHTML .= <<<EOT
		<div class=rm_h2>&Uuml;berweisen statt Barzahlung</div>
		<div class=rm_text>
			Bitte die Betr&auml;ge f&uuml;r {$preiskategorien} im Vorfeld &uuml;berweisen.<br />
			F&uuml;r Barzahlung erheben wir eine Umtriebsentsch&auml;digung von CHF {$rm_barzuschlag}.-<br />
			Die Kosten f&uuml;r Schlepps werden den Teilnehmenden von der Flugschule Birrfeld AG in Rechnung gestellt.<br />
			Teilnehmer aus dem Ausland sollen die Betr&auml;ge in Bar mitbringen, da sonst unn&ouml;tige Kosten f&uuml;r die &Uuml;berweisung anfallen.
		</div>			
EOT;
switch ($rm_ausrichter) {
    
    case "AFG":
        $renderedHTML .= <<<EOT
			<div class=rm_h2>&Uuml;berweisung</div>
			<div class=rm_text>
				Einzahlung f&uuml;r: <br /> <b>Akademische Fluggruppe<br />8000 Z&uuml;rich</b>
				Vermerk: <b>{$rm_name_kurz} sowie Name &amp; WKZ</b>
				IBAN: <b>CH23 0900 0000 1626 6766 5</b>
				BIC: <b>POFICHBEXXX</b>
			</div>
EOT;
        break;
    case "SGL":
        $renderedHTML .= <<<EOT
			<div class=rm_h2>&Uuml;berweisung</div>
			<table>
				<tr>
					<th class=th_1>Einzahlung f&uuml;r</th>
					<td class=td_1>Segelfluggruppe Lenzburg<br />5600 Lenzburg</td>
				</tr>
				<tr>
					<th class=th_1>Bank</th>
					<td class=td_1>Hypothekarbank Lenzburg AG</td>
				</tr>
				<tr>
					<th class=th_1>Clearing-Nummer</th>
					<td class=td_1>8307</td>
				</tr>
				<tr>
					<th class=th_1>Swift-Code</th>
					<td class=td_1>HYPLCH22</td>
				</tr>
				<tr>
					<th class=th_1>IBAN</th>
					<td class=td_1>CH98 0830 7000 0512 2740 1</td>
				</tr>
				<tr>
					<th class=th_1>Vermerk</th>
					<td class=td_1>{$rm_comp_type} {$rm_year} + WKZ</td>
				</tr>
			</table>
EOT;
        break;
    case "SFB":
        $renderedHTML .= <<<EOT
			<div class=rm_h2>&Uuml;berweisung</div>
			<div class=rm_text>
				Einzahlung f&uuml;r: <br /> 
				<b>Segelfluggruppe Birrfeld<br /> 
				Flugplatz Birrfeld<br /> 
				5242 Lupfig
				</b>
			</div>
			<div class=rm_text>
				Bank: <b>Postfinance</b>
			</div>
			<div class=rm_text>
				IBAN: <b>CH46 0900 0000 8005 6214 3</b>
			</div>
			<div class=rm_text>
				Vermerk: {$rm_comp_type} {$rm_year} + WKZ
			</div>
EOT;
        break;
        
    default:
        echo "<div class=error>Ausrichter nicht korrekt gesetzt in inc.php - nicht gefunden in administration.php</div>";
        break;
        
}
$renderedHTML .= <<<EOT
		</div>
		<div class=lst_col_block>
			<div class=rm_h2>Dokumentenkontrolle</div>
			<div class=rm_text>
				Wir setzen voraus, dass ihr g&uuml;ltige Ausweise besitzt, &uuml;ber ein angemessenes Training verf&uuml;gt und dass alle notwendigen Papiere 
				f&uuml;r das Flugzeug vorhanden sind. Die Verantwortung liegt bei den Teilnehmenden.
				Wir werden im Vorfeld nach M&ouml;glichkeit mit dem Sekretariat SFVS die G&uuml;ltigkeit eurer Sportlizenz &uuml;berpr&uuml;fen.
			</div>
			<div class=rm_h2>Reglemente</div>
			<div class=rm_text>Der Wettbewerb wird nach dem aktuellsten Reglement f&uuml;r MEISTERSCHAFTEN IM STRECKENSEGELFLUG des SFVS und dessen Anh&auml;nge 1,2 und 3 
			durchgef&uuml;hrt.
			Bitte beachtet, dass die Reglemente gelegentlich leicht angepasst werden. Auf wichtige Anpassungen werden wir am Briefing hinweisen.
			</div>
			<div class="sidebarlink"><b><a	href="https://www.segelflug.ch/sport/reglemente/" target="_blank">SM Reglemente</a></b></div>
			<div class=rm_h2>Aussenlandem&ouml;glichkeiten</div>
			<div class=rm_text>
			    Wir bitten Euch, Euch mit den Aussenlandem&ouml;glichkeiten im Wettbewerbsgebiet vertraut zu machen.
				Wir empfehlen die bekannten Dokumentationen des s&uuml;dlichen und n&ouml;rdlichen Schwarzwaldes sowie der Alpen und Voralpen.
				Wir weisen auf die Unverbindlichkeit dieser Dokumentationen hin.
			</div>

			<div class=rm_h2>Kontakte</div>
			<div class=rm_text>
				<table width=100%>
					<tr>
						<td class=td_1_c><b>Organisation</b></td>
						<td class=td_1_c><b>Konkurrenzleitung</b></td>
						<td class=td_1_c><b>Auswertung</b><br/></td>
					</tr>
					<tr>
						<td class=td_1_c>ok@rmbirrfeld.ch</td>
						<td class=td_1_c>kl@rmbirrfeld.ch</td>
						<td class=td_1_c>auswertung@rmbirrfeld.ch</td>
					</tr>
				</table>
			</div>
			<div class=rm_h2>Kommunikation - WhatsApp</div>
			<div class=rm_text>
				<table class="rm_no_borders">
					<colgroup>
						<col width="40%">
						<col width="10%">
						<col width="50%">
					</colgroup>
					<tr>
						<td class="td_1_r"><strong>Konkurrenzleitung -> Teilnehmer</strong></td>
						<td class="td_1_l"><a href="{$rm_whatsapp_url}" target="_blank">beitreten</a></td>
						<td class="td_1_l">Infos der Konkurrenzleitung an die Teilnehmenden</td>
					</tr>
					<tr>
						<td class="td_1_r"><strong>Teilnehmende unter sich</strong></td>
						<td class="td_1_l"><a href="{$rm_whatsapp_teilnehmer_url}" target="_blank">beitreten</a></td>
						<td class="td_1_l">Infos der Teilnehmenden</td>
					</tr>
				</table>
			</div>

		</div>
	</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Teilnehmer - Administration', 'page', $renderedHTML);

?>
