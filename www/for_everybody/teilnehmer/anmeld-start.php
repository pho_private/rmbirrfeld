<?php

error_reporting(E_ALL);
session_start();

require_once ('inc.php');
require_once ('dblib.inc.php');
require_once ('functions.php');

global $nachricht;

/**
 * Personennamen in Dateinamen (ohne Typ/Endung) umwandeln.
 * Alles gut und recht, wenn wir bloss keine Umlaute und Accents haetten...
 * Der Pilotenname ist bereits in der Datenbank, und wir nehmen an, dass er bereits korrekt in UTF-8 umgesetzt worden ist.
 * Der resultierende Dateiname soll den ASCII Konventionen entsprechen - keine Umlaute, keine Accents etc,.
 * Erst setzen wir unser Locale(de_CH), dann konvertieren wir zu transliteriertem ASCII.
 * Alle Umlaute und nicht-ASCII Zeichen werden einfach weggelassen. Bye bye Sorgen!
 *
 * @param string $pilotname
 * @param integer $id
 * @return string
 */
function makePhotoFilename($pilotname, $id)
{
    setlocale(LC_CTYPE, 'de_CH.utf8');
    $parts = explode(" ", strtolower(iconv('UTF-8', 'ASCII//IGNORE', $pilotname)));
    $fotoname = '';
    for ($i = 0; $i < count($parts) - 1; $i ++) {
        $fotoname .= substr($parts[$i], 0, 1) . "_";
    }
    return $fotoname . "{$parts[count($parts) - 1]}_{$id}";
}

/**
 * Return input element for pilot name - fixed (disabled) text box (single pilot) or selector (Team)
 *
 * @param object $userDaten
 * @return string
 */
function getPilotnameInput($userDaten)
{
    global $rm_TeamClassIndex;
    $pilotname = "{$userDaten['vorname']} {$userDaten['name']}";
    if ($userDaten['klasse'] == $rm_TeamClassIndex) {
        
        // insert selector for pilots of the team
        $html = "				<select name=\"pilotname\" class=\"rm_entry\" >\n";
        $selection = array(
            $userDaten['vorname'] . " " . $userDaten['name'],
            $userDaten['pilot2'],
            $userDaten['pilot3'],
            $userDaten['pilot4'],
            $userDaten['pilot5']
        );
        foreach ($selection as $key => $value) {
            if ($value != "") {
                $html .= "					<option name=\"pilotname\" value=\"$value\"";
                if ($pilotname == $value) {
                    $html .= " SELECTED";
                }
                $html .= ">$value</option>\n";
            }
        }
        return $html . "				</select>\n";
        
    } else { // for "normal" classes, just return label - the pilot's name.
        return "			<input type='text' readonly name='pilotname' value='{$pilotname}'>\n";
    }
}

// generates the HTML for photo file name, photo, and pilot name label(s). if multiple labels, then
// it also generates calls to change the photo.
function photoLabel($pilotNr, $filename, $setfile, $userDaten)
{
    $filename = strtolower($filename);
    
    if ($pilotNr == 1) {
        $pilotText = "";
        $pilotname = $userDaten['vorname'] . " " . $userDaten['name'];
    } else {
        $pilotText = (string) $pilotNr;
        $pilotname = $userDaten["pilot{$pilotText}"];
    }
    if ($pilotname == "") { // no use producing empty labels...
        return "";
    }
    if (strtolower($userDaten["foto{$pilotText}"]) == $filename) {
        $html = "<br /><b>{$pilotname}</b>";
    } else {
        $html = "<br /><a href=anmeld-start.php?setfoto{$pilotNr}={$setfile}>{$pilotname}?";
    }
    return $html;
}

function checkSetFoto($login) {
    for ($i = 1; $i <= 5; $i ++) {
        $fn = getRequestOrInit("setfoto{$i}", '');
        if ($fn != '') {
            // if filename present, a new photo has been chosen.
            $tnl = new Teilnehmer();
            $tnl->set_foto_by_login($login, $fn, $i);
            header('Location: anmeld-start.php');
        }
    }
    // this function returns only if no photo file name is present.
    return false;
}

// this function generates a table with all already stored photos for all pilots of an entry.
function generatedPhotoDivision($folder, $userDaten) {
    global $rm_tn_photos_url, $rm_TeamClassIndex;
    
    // find the photo(s) of this pilot or team
    $files = array();
    $upperLimit = ($userDaten['klasse'] == $rm_TeamClassIndex) ? 5 : 1;
    $pilotnameInput = getPilotnameInput($userDaten);
    
    // Suchen aller gespeicherten Fotonamen
    for ($i = 1; $i <= $upperLimit; $i ++) {
        $index = ($i == 1) ? 'foto' : 'foto' . $i;
        if ($userDaten[$index] != '') {
            $files = array_merge($files, filelist($folder, "/" . $userDaten[$index] . "_small.jpg/i"));
        }
    }
    
    $nof = count($files);
    
    if ($nof != 0) {
        sort($files);
        $LeadIn = ($nof == 1) ? 'Gespeichertes Photo' : 'Gespeicherte Photos';
        
        $generatedHTML = <<<EOT
            <tr>
				<td class="td_1_r">
				    <strong>{$LeadIn}</strong>
				</td>
EOT;
        
        for ($i = 1; $i <= $nof; $i ++) {
            if ($i = 1) {
                $fotoname = $userDaten["foto"];
            } else {
                $fotoname = $userDaten["foto{$i}"];
            }
            $foto = "{$fotoname}.jpg";
            $small = "{$fotoname}_small.jpg";
            $hash = filemtime($folder . "/" . $small); // this "hash" prevents the browser from caching old versions of the file, but the current version is taken from cache.
            $generatedHTML .= <<<EOT
				<td class=\"td_1_c\"><b>{$fotoname}</b><br />
					<a href="{$rm_tn_photos_url}/{$foto}" class="highslide"  onclick="return hs.expand(this)">
					<img src="{$rm_tn_photos_url}/{$small}?hash={$hash}" height="90"></a>
			    </td>
EOT;
        }
    } else {
        $generatedHTML = <<<EOT
            <tr>
				<td class="td_1_r"><strong>Bitte lade ein Photo von Dir hoch!</strong></td>
				
EOT;
    }
    
    return $generatedHTML . <<<EOT
                </tr>
				<tr>
					<form enctype="multipart/form-data" action="anmeld-start.php" method="POST">
					<input type="hidden" name="folder" value="{$folder}">
					<input type="hidden" name="{$_SESSION['logged_in']}">
					<td class="td_1_r"><strong>&auml;ndern</strong> f&uuml;r Pilot</td>
					<td>{$pilotnameInput}</td>
					<td colspan=3>
						<input class="rm_entry" name="userfile" type="file" style="width: 100%;"></br>
						<input type=submit value="Datei hochladen">
					</td>
					</form>
				</tr>
EOT;
    
}

// -------------------------------------------------------------------------------------
// begin of code body.
// -------------------------------------------------------------------------------------


$id = 0;
$errorMsg = '';
$pwdidx = 'passwort';
$action_login = 'login';
$renderedHTML = '';
$login = '';

$action = getPostOrInit('aktion');
$logged_in = getSessionOrInit('logged_in', false); 

if ($logged_in) {
    
    $id = getSessionOrInit('id');
    $userDaten = getParticipantById($id);
    
    $vorname = $userDaten['vorname'];
    $name = $userDaten['name'];
    $login = $userDaten['login'];
    $pilot1 = $vorname . " " . $name;
    $pilot2 = $userDaten['pilot2'];
    $pilot3 = $userDaten['pilot3'];
    $pilot4 = $userDaten['pilot4'];
    $pilot5 = $userDaten['pilot5'];
    $nachricht = "Du bist eingeloggt als " . $pilot1 . " <small>(ID = $id)</small>";
    checkSetFoto($login);
    // display error messages if present
    if ($nachricht != "") {
        $renderedHTML .= <<<EOT
	   <div class=rm_h3>{$nachricht}</div>
EOT;
    }
    // Kopf der Pilotentabelle mit den Photos.
    $renderedHTML .= <<<EOT
    	<div class=rm_text>
			Bitte sicherstellen, dass alle Angaben vollst&auml;ndig sind. Insbesondere Notfallkontakte, 
            FSB-Nummer, IGC-Rank ID, ID des prim&auml;ren Logger und Flarm-ID nicht vergessen. Fehlenden Angaben 
            f&uuml;hren zu unn&ouml;tigen R&uuml;ckfragen. 
    		<table class="rm_no_borders">
    			<colgroup>
    				<col width="20%">
    				<col width="20%">
    				<col width="50%">
            </colgroup>
    			<tr>
    				<td class="td_1_r"><strong>&Auml;ndern</strong></td>
    				<td class="td_1_l"><input type="button" value="Daten anpassen" onClick="self.location.href='update-form.php'"></td>
    				<td class="td_1_l">Hier kannst Du Deine Daten erfassen oder anpassen.</td>
    			</tr>
    			<tr>
    				<td class="td_1_r"><strong>WhatsApp Konkurrenzleitung -> Teilnehmer</strong></td>
    				<td class="td_1_l"><a href="{$rm_whatsapp_url}" target="_blank">beitreten</a></td>
    				<td class="td_1_l">Infos der Konkurrenzleitung an die Teilnehmenden</td>
    			</tr>
    			<tr>
    				<td class="td_1_r"><strong>WhatsApp Teilnehmende unter sich</strong></td>
    				<td class="td_1_l"><a href="{$rm_whatsapp_teilnehmer_url}" target="_blank">beitreten</a></td>
    				<td class="td_1_l">Infos der Teilnehmenden</td>
    			</tr>
    			<tr>
    				<td class="td_1_r"><strong>Ausloggen</strong></td>
    				<td class="td_1_l"><input type="button" value="Ausloggen" onClick="self.location.href='logout.php'"></td>
    				<td class="td_1_l">Wenn Du alles korrekt erfasst hast...</td>
    			</tr>
    			<tr>
    				<td>&nbsp;</td>
    				<td>&nbsp;</td>
    				<td>&nbsp;</td>
    			</tr>
EOT;
    
    $folder = $rm_tn_photos . '/';
    
    if (isset($_FILES['userfile'])) { // ok, the user has pressed the upload button.
        
        // new naming scheme: photo = <firstname(1)>_<lastname>_<id>.jpg
        // id included for uniqueness, so we can have two or more Hans Meier.
        
        $originalFilename = $_FILES['userfile'];
        
        // upload of the photo specified in field "userfile" using german messages
        $handle = new rm_upload($originalFilename, 'de_DE');
        if ($handle->file_is_image) {
            if ($handle->uploaded) {
                $pilotname = $_REQUEST['pilotname'];
                $photoFilename = makePhotoFilename($pilotname, $id);
                $handle->generate_image_set($folder, $photoFilename);
                
                // assign to correct pilot
                $nr = 0;
                $teilnehmerListe = new Teilnehmer();
                switch ($pilotname) {
                    case $pilot1:
                        $nr = 1;
                        break;
                    case $pilot2:
                        $nr = 2;
                        break;
                    case $pilot3:
                        $nr = 3;
                        break;
                    case $pilot4:
                        $nr = 4;
                        break;
                    case $pilot5:
                        $nr = 5;
                        break;
                    default:
                        $errorMsg = "Gew&auml;hlter Pilot: '{$pilotname}'<br />kann nicht zugeordnet werden...<br />Datei {$handle->file_dst_name_body}</p>";
                        break;
                }
                if ($nr != 0) {
                    $teilnehmerListe->set_foto_by_login($login, $photoFilename, $nr);
                }
            }
            // now that we've done all conversions, remove the uploaded source file.
            $handle->clean();
        } else {
            $errorMsg .= <<<EOT
            <div class="td_e">
				<b>Unbekanntes Bildformat!</b><br />Bitte w&auml;hle ein JPEG, GIF oder PNG Bild.</p>
			</div>"
EOT;
        }
    }
    
    $folder = $rm_tn_photos;
    $generatedPhotoDivision = generatedPhotoDivision($folder, $userDaten);
    $errorMsg = (! isset($errorMsg)) ? "" : $errorMsg;
    $errorMsgBlock = ($errorMsg != '') ? encodeToUtf8("<div class=\"td_e\">{$errorMsg}</div>") : ('');
    
    $renderedHTML .= <<<EOT
			{$generatedPhotoDivision}
    		</table>
    	</div>
	{$errorMsgBlock}
EOT;
	
} else {
    if ($action == $action_login) {
        $_POST['form']['login'] = trim($_POST['form']['login']);
        $_POST['form'][$pwdidx] = trim($_POST['form'][$pwdidx]);
        if (empty($_POST['form']['login']) || empty($_POST['form'][$pwdidx])) {
            $nachricht .= "Du musst alle Felder ausf&uuml;llen.<br />\n";
        }
        if ($nachricht == '') {
            $id = checkPassword($_POST['form']['login'], $_POST['form']['passwort']);
            initSession($id['id'], $_POST['form']['login'], $_POST['form']['passwort']);
            header('Location: join.php');
            exit();
        }
    }
    $renderedHTML .= <<<EOT
        <div class=rm_h1>Registrierung / Anmeldung / Anpassen</div>
        <div class=big_col_block>
			<div class=rm_text>
				<table class="rm_no_borders">
					<colgroup>
						<col width="20%">
						<col width="20%">
						<col width="60%">
					</colgroup>
					<tr>
						<td class="td_1_r"><strong>Noch nicht registriert?</strong></td>
						<td class="td_1_l"><input type="button" name="Verweis" value="Registrieren" onClick="self.location.href='join.php'"></td>
						<td class="td_1_l">Um dich anmelden zu k&ouml;nnen, musst Du Dich <b>einmalig</b> registrieren.</td>
					</tr>
					<tr>
						<td class="td_1_r"><strong>Einloggen</strong></td>
						<td class="td_1_l"><input type="button" name="Verweis" value="Einloggen" onClick="self.location.href='login.php'"></td>
						<td class="td_1_l">Danach kannst Du jederzeit einloggen und Deine Daten anpassen!</td>
					</tr>
					<tr>
						<td class="td_1_r"><strong>Passwort vergessen?</strong></td>
						<td class="td_1_l"><input type="button" name="Verweis" value="Passwort &auml;ndern" onClick="self.location.href='pwchangemail.php'"></td>
						<td class="td_1_l">e-mail auch gewechselt? <a href="mailto:{$rm_mail_webmaster}?subject=$rm_comp_type Birrfeld:%20Account-Zugriff">Mitteilung</a> an den Webmaster!</td>
					</tr>
				</table>
			</div>
		</div>
EOT;
}

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Teilnehmer - Anmeldung', 'page', $renderedHTML, NULL, TRUE);

?>
