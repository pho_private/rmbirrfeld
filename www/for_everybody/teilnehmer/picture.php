<?php
require_once ('functions.php');

$folderUrl = getRequestOrInit('url', '');
$width = getRequestOrInit('width', '');
$height = getRequestOrInit('height', '');

$sbodyStart = <<<EOT
<body topmargin=0 leftmargin=0 onLoad="closeWindowAfter(5000)">
    <div class=main_block>
EOT;

$renderedHTML = <<<EOT
        <img src=$folderUrl border=0 width=$width height=$height>
    </div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('Bild', 'page', $renderedHTML, $bodyStart, TRUE);

?>
