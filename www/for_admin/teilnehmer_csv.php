<?php
require_once ('inc.php');
require_once ('dblib.inc.php');
require_once ('functions.php');

$query = <<<EOT
    SELECT 
        fsb, vorname, name, adresse , plz , wohnort, telp, telg, mobp, mobg, 
        immatrikulation, wk, klasse, teamname, 
        gift, flugzeug, gruppe, email, 
        notfallkontakt1, logger1, logger1id, foto, 
        pilot2, pilot3, pilot4, pilot5, sportlizenz, camping, rueckholen, flarm, angaben   
    FROM {$rm_tbl_teilnehmer}
    WHERE rmactiv='ja' order by name, vorname
EOT;

$filename = "";
$ResultPointer = DBQuery($query);

if ($ResultPointer) {
    if ($debug) {
        $debug_msg .= "<br/>--- Results found:";
    }
    $newLine = chr(13) . chr(10);
    $renderedHTML = "\xEF\xBB\xBF"; // BOM for UTF-8
    $renderedHTML .= "FSB-Nr;Vorname;Name;Strasse;PLZ;Ort;TelP;TelG;HandyP;HandyG;Immatrikulation;WK;Klasse;Teamname;T-Shirt;Flugzeug;Gruppe;e-Mail;Notfall-Kontakt;Logger;LoggerID;Foto;Pilot2;Pilot3;Pilot4;Pilot5;Sportliz;Camping;Rueckholen;Flarm;Angaben";
    $renderedHTML .= $newLine;
    if ($debug) {
        $debug_msg .= "<br/>" . $renderedHTML . "<p/>";
    }

    for ($i = 0; $i < mysqli_num_rows($ResultPointer); $i ++) {
        // do not use the DBFetchRow here since we want utf-8 characters, not html codes.
        $Daten = mysqli_fetch_row($ResultPointer);
        $csvLine = "";
        foreach ($Daten as $data) {
            $csvLine .= "\"" . str_replace("\"", "\"\"", $data) . "\";";
        }
        $csvLine = substr($csvLine, 0, strlen($csvLine)-1); // remove ";" at end
        $renderedHTML .=  $csvLine . $newLine; // add newline
        if ($debug) {
            $debug_msg .= $csvLine . "<br/>";
        }
    }
    // $renderedHTML = iconv("UTF-8","ISO-8859-1",$renderedHTML); // removed because it caused file write errors.
  
    // delete existing old versions of list
    deleteFilesLike("../resources/downloads/teilnehmerliste_*.csv");

    // define static filename
    $filename = "../resources/downloads/teilnehmerliste_" . date('d-M-Y') . ".csv";
    
    // write file contents 
    $result = file_put_contents($filename, $renderedHTML, LOCK_EX);    
    if ($result == false) {
        $rm_msg = "cannot write file $filename";
    } else {
        $rm_msg = "$result bytes in $filename wait to be downloaded...";
    }
}

rm_DownloadListPage("Teilnehmer CSV", $filename);

?>

