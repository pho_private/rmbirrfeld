<?php

// 32 bit
echo '<h1>32bit</h1>';

$large_number = 2147483647;
var_dump($large_number);                     // int(2147483647)
echo '<br />';

$large_number = 2147483648;
var_dump($large_number);                     // float(2147483648)
echo '<br />';

$million = 1000000;
$large_number =  50000 * $million;
var_dump($large_number);                     // float(50000000000)
echo '<br />';


// 64 bit
echo '<h1>64bit</h1>';

$large_number = 9223372036854775807;
var_dump($large_number);                     // int(9223372036854775807)
echo '<br />';

$large_number = 9223372036854775808;
var_dump($large_number);                     // float(9.2233720368548E+18)
echo '<br />';

$million = 1000000;
$large_number =  50000000000000 * $million;
var_dump($large_number);                     // float(5.0E+19)

?>