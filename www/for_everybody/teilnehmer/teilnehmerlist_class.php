<?php
session_start();
error_reporting(E_ALL);

require_once ('inc.php');
require_once ('functions.php');

// main code ...

// need to rewrite such that Index yes/no comes from DB instead of from hardcoded logic.
$class = getRequestOrInit('class', '');
$showIndex = '';

$nextClassUrl = '';
if ($class > 0 && $class < count($class_name)) {
    $nextClassUrl = '?class=' . (string)((int)$class + 1);
}

if ($class_indexed[$class] == '1') {
    $showIndex = ' (Index)';
}



$tnlist = new Teilnehmer($class);

/*
a {
	color: #000000;
	background-color: #f1f1f1;
	display: block;
	padding: 2px 2px;
	margin: 0 0 2px;
	text-decoration: none;
	line-height: 15px;
	font-size: 13px;
}
 */

$renderedHTML = <<<EOT
		<div class=rm_h1>Teilnehmer der Klasse "{$class_name[$class]}" {$rm_name_lang}</div>
		<div class=big_col_block>
			<div class="tn">
EOT;

foreach ($tnlist->list as $tn) {
    if ($tn['name'] != '') {
        $immatrikulation = $tn['immatrikulation'];
        $websafe_immatrikulation = urlencode($immatrikulation);
        $name = $tn['vorname'] . " " . $tn['name'];
		if (preg_match('/nur rm/', strtolower($tn['angaben']))) $name .= ' (nur RM)';
        $pilots = $name;
        if ($class == $rm_TeamClassIndex) {
            $teamname = $tn['teamname'];
            $pilots .= ($tn['pilot2'] == '') ? '' : ("<br />" . $tn['pilot2']);
            $pilots .= ($tn['pilot3'] == '') ? '' : ("<br />" . $tn['pilot3']);
            $pilots .= ($tn['pilot4'] == '') ? '' : ("<br />" . $tn['pilot4']);
            $pilots .= ($tn['pilot5'] == '') ? '' : ("<br />" . $tn['pilot5']);
            $indexwert = ' ( ' . $tn['indexwert'] . ')';
        } elseif ($class_indexed[$class] == '1') {
            $indexwert = ' (' . $tn['indexwert'] . ')';
        } else {
            $indexwert = '';
        }
     
        $renderedHTML .= <<<EOT
    			<a class="tn_popup" href=teilnehmer_detail.php?immatrikulation={$websafe_immatrikulation}>{$name} / {$immatrikulation} / {$tn['flugzeug']} / {$tn['wk']} $indexwert
    				<span>
    					<table class=tn_tbl>
EOT;
        
        if ($class == $rm_TeamClassIndex) {
            $renderedHTML .= <<<EOT
    						<tr><td class=th_1_tn>Team</td><td class=td_1>{$teamname}</td></tr>
    						<tr><td class=th_1_tn>Piloten</td><td class=td_1>{$pilots}</td></tr>
EOT;
        } else {
            $renderedHTML .= <<<EOT
    						<tr><td class=th_1_tn>Pilot</td><td class=td_1>{$name}</td></tr>
EOT;
        }
        $renderedHTML .= <<<EOT
    						<tr><td class=th_1_tn>Wohnort</td><td class=td_1>{$tn['wohnort']} </td></tr>
    						<tr><td class=th_1_tn>Gruppe</td><td class=td_1>{$tn['gruppe']} </td></tr>
    						<tr><td class=th_1_tn>Flugzeug</td><td class=td_1>{$tn['immatrikulation']} / {$tn['flugzeug']} / {$tn['wk']}</td></tr>
EOT;
        
        for ($i = 1; $i <= 5; $i ++) {
            $idx = ($i == 1) ? '' : (string) $i;
            if ($tn["foto{$idx}"] != "") {
                $foto = $tn["foto{$idx}"];
                $renderedHTML .= <<<EOT
    						<tr><td class=td_1 colspan=2 align=center><img border=0 src={$rm_tn_photos_url}/{$foto}_midi.jpg></td></tr>
EOT;
            }
        }
        $renderedHTML .= <<<EOT
    					</table>
    				</span>
    			</a><br />
EOT;
    }
}

$renderedHTML .= <<<EOT
			</div>
		</div>
    </div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('Teilnehmer pro Klasse', 'page', $renderedHTML, NULL, TRUE);

?>
