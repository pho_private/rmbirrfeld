<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once ('inc.php');
require_once ('helfer_inc.php');
require_once ('dblib.inc.php');
require_once ('functions.php');


function getTimeSlotInfo($date, $shift, $type, $maxEntries)
{
    global $rm_tbl_einsaetze;
    $timeSlot = array();
    $timeSlot['name'] = array();
    $tmp_cnt = 0;
    
    $select = "SELECT vorname as vorname, name as name, wohnort as wohnort, jahrgang as jahrgang 
		FROM {$rm_tbl_einsaetze} WHERE datum = '{$date}' AND schicht = {$shift} AND gebiet = '{$type}' AND deleted is NULL ORDER BY name, vorname";
    $result = DBQuery($select);
    if ($result) {
        $row = DBFetchRow($result);
        while ($row) {
            $tmp_cnt ++;
            $timeSlot['name'][] = substr($row[0], 0, 1) . ".&nbsp;" . $row[1];
            $row = DBFetchRow($result);
        }
    }
    $rest_tmp = $maxEntries - $tmp_cnt;
    if ($tmp_cnt > 0 && $rest_tmp > 0)
        $timeSlot['names'] = implode(', ', $timeSlot['name']) . " ($rest_tmp)";
    else 
        if ($tmp_cnt == 0 && $rest_tmp > 0)
            $timeSlot['names'] = "($rest_tmp)";
        else
            $timeSlot['names'] = implode(', ', $timeSlot['name']);
    
    $timeSlot['style'] = (count($timeSlot['name']) >= $maxEntries) ? 'td_ok' : 'td_1';
    
    // ob_start();
    // print_r($timeSlot);
    // $errMsg .= '<p />' . ob_get_contents();
    // ob_end_clean();
    
    return $timeSlot;
}

$errMsg = '';
$result = DBQuery("SELECT count(*) FROM {$rm_tbl_einsaetze} where deleted is NULL");
$row = DBFetchRow($result);
$total_meldungen = $row[0];
$left = $total_einsaetze - $total_meldungen;

// --------------------------------------------------------------------------------
// Tabelle Aufbau

$renderedHTML = <<<EOT
		<div class=rm_h1>Helfereins�tze Auf- und Abbau</div>
			<div class=big_col_block>
				<table>
					<colgroup>
						<col width='80px'>
						<col width='200px'>
						<col width='200px'>
						<col width='200px'>
						<col width='200px'>
						<col width='200px'>
					</colgroup>
					<tr>
						<th class=th_1>Aufbau</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_aufbau[1]}</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_aufbau[2]}</th>
					</tr>
EOT;

foreach ($rm_aufbau_dates as $curDate) {
    $einsaetze[1] = $aufbau_einsatz_dates_soll[$curDate][1];
    $einsaetze[2] = $aufbau_einsatz_dates_soll[$curDate][2];
    
    $curDay = new rm_DateTime($curDate);
    $timeSlotInfo[1] = getTimeSlotInfo($curDate, 1, 'Aufbau', $aufbau_einsaetze_soll[1]);
    $timeSlotInfo[2] = getTimeSlotInfo($curDate, 2, 'Aufbau', $aufbau_einsaetze_soll[2]);
    
    $weekday = '?'; // substr($curDay->format('l'),0,2).".";
    $today = $curDay->format('d.m.');
    
    $renderedHTML .= <<<EOT
					<tr>
						<th class=th_1 valign=top>{$weekday} {$today}</th>
						<td class={$timeSlotInfo[1]['style']} valign=top colspan=2>{$timeSlotInfo[1]['names']}</td>
						<td class={$timeSlotInfo[2]['style']} valign=top colspan=2>{$timeSlotInfo[2]['names']}</td>
					</tr>

EOT;
}

$renderedHTML .= <<<EOT
                    <tr><td></td></tr>
                    <tr><td></td></tr>
EOT;

// --------------------------------------------------------------------------------
// Tabelle Abbau

$renderedHTML .= <<<EOT

					<tr>
						<th class=th_1>Abbau</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_abbau[1]}</th>
						<th class=th_1 valign=top colspan=2>{$schichtzeiten_abbau[2]}</th>
					</tr>

EOT;

foreach ($rm_abbau_dates as $curDate) {
    $curDay = new rm_DateTime($curDate);
    $timeSlotInfo[1] = getTimeSlotInfo($curDate, 1, 'Abbau', $abbau_einsaetze_soll[1]);
    $timeSlotInfo[2] = getTimeSlotInfo($curDate, 2, 'Abbau', $abbau_einsaetze_soll[2]);
    
    $weekday = '?'; // substr($curDay->format('l'),0,2).".";
    $today = $curDay->format('d.m.');
    
    $renderedHTML .= <<<EOT
        			<tr>
        				<th class=th_1 valign=top>{$weekday} {$today}</th>
        				<td class={$timeSlotInfo[1]['style']} valign=top colspan=2>{$timeSlotInfo[1]['names']}</td>
        				<td class={$timeSlotInfo[2]['style']} valign=top colspan=2>{$timeSlotInfo[2]['names']}</td>
        			</tr>

EOT;
}

$renderedHTML .= <<<EOT
				</table>
				{$errMsg}
    		</div>
    	</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Helfer - Einsatzplan Aufbau / Abbau', 'page', $renderedHTML);

?>
