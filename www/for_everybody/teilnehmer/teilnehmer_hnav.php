<?php
require_once ('inc.php');

$renderedHTML = <<<EOT
			<div class="hnav_item"><h2>TN-Infos</h2></div>
			<div class="hnav_item"><a href="einleitung.php">Willkommen</a></div>
			<div class="hnav_item"><a href="../woistwas/uebersicht.php">Was Ist Wo&nbsp;</a></div>
			<div class="hnav_item"><a href="administration.php">Admin.</a></div>
			<div class="hnav_item"><a href="sicherheit.php">Sicherheit</a></div>
			<div class="hnav_item"><a href="aufbau.php">Aufbau</a></div>
			<div class="hnav_item"><a href="start_abflug.php">Start</a></div>
			<div class="hnav_item"><a href="ankunft_landung.php">Landung</a></div>
			<div class="hnav_item"><a href="luftraeume.php">Luftraum</a></div>
			<div class="hnav_item"><a href="wendeorte.php">Wendepunkte</a></div>
			<div class="hnav_item"><a href="auswertung.php">Auswertung</a></div>
			<div class="hnav_item"><a href="al_deutschland.php">Aussenlandung in D</a></div>

EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('Infos f&uuml;r Teilnehmer', 'hnav', $renderedHTML);

?>
