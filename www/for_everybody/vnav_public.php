<?php
require_once ('inc.php');

$renderedHTML = <<<EOT
    <div class="rm_logo"><a class="non" href="../../index.php" target="_top"><img border="0" src="{$rm_logo_url}" width="120"></a></div>
	<div class="sidebar">Allgemein</div>
	<div class="sidebarlink"><a href="frame.php?hnav=news/news_hnav.php&page=news/news.php">News</a></div>
	<div class="sidebarlink"><a href="frame.php?hnav=empty_hnav.php&page=sponsoren/sponsoren.php">Sponsoren</a></div>
	<div class="sidebarlink"><a	href="frame.php?hnav=empty_hnav.php&page=organisation/kontakte.php">Das OK</a></div>
	<div class="sidebarlink"><a	href="frame.php?hnav=empty_hnav.php&page=woistwas/uebersicht.php">Lageplan</a></div>
<!--
    <div class="sidebarlink"><a	href="frame.php?hnav=empty_hnav.php&page=restaurant/einleitung.php">Restaurant</a></div>
-->
	<div class="sidebarlink"><a class="external_link" href="http://piwigo.rmbirrfeld.ch" target="_blank">Fotos</a></div>
	<div class="sidebar">Anmelden</div>
	<div class="sidebarlink"><a	href="frame.php?hnav=empty_hnav.php&page=teilnehmer/anmeld-start.php">Piloten</a></div>
EOT;

if ($rm_use_helfer) {
	$renderedHTML .= <<<EOT
    <div class="sidebarlink"><a href="frame.php?hnav=helfer/helfer_hnav.php&page=helfer/einsatzplan_sm.php">Helfer</a></div>
EOT;
}

$renderedHTML .= <<<EOT
	<div class="sidebar">Briefing</div>
	<div class="sidebarlink"><a	class="external_link" href="https://www.skybriefing.com/portal/de" target="_blank">DABS</a></div>
	<div class="sidebarlink"><a	class="external_link" href="http://notaminfo.com/switzerlandmap" target="_blank">Notam CH</a></div>
	<div class="sidebarlink"><a	class="external_link" href="http://notaminfo.com/germanymap" target="_blank">Notam D</a></div>
	<div class="sidebarlink"><a	class="external_link" href="http://notaminfo.com/francemap" target="_blank">Notam F</a></div>
	<div class="sidebarlink"><a	class="external_link" href="https://www.sia.aviation-civile.gouv.fr/schedules" target="_blank">Tiefflugstrecken F</a></div>

	<div class="sidebar">Informationen</div>
	<div class="sidebarlink"><a	href="frame.php?hnav=teilnehmer/teilnehmer_hnav.php&page=teilnehmer/einleitung.php">f&uuml;r Teilnehmer</a></div>
<!--
	<div class="sidebarlink"><a	href="frame.php?hnav=empty_hnav.php&page=besucher/einleitung.php">f&uuml;r Besucher</a></div>
-->

	<div class="sidebar">Wettbewerb</div>
	<div class="sidebarlink"><a href="frame.php?hnav=teilnehmer/teilnehmerlist_hnav.php&page=teilnehmer/teilnehmerlist.php">Teilnehmer</a></div>
	<div class="sidebarlink"><a	class="external_link" href="{$rm_soaringspot_url}/results" target="_blank">Task/Result $rm_comp_type</a></div>

    <div class="sidebarlink"><a href="frame.php?hnav=tracking/tracking_hnav.php&page=sponsoren/sponsoren.php">Live Tracking</a></div>
	<div class="sidebarlink"><a href="igc/igc_upload.php" target="_blank">IGC Upload</a></div>
EOT;

/*
$renderedHTML .= <<<EOT
	<div class="sidebar">WhatsApp beitreten</div>
	<div class="sidebarlink"><a href="{$rm_whatsapp_url}" target="_blank">OK Infos </a></div>
	<div class="sidebarlink"><a href="{$rm_whatsapp_teilnehmer_url}" target="_blank">Teilnehmende</a></div>
EOT;
*/

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('Main Menu', 'vnav', $renderedHTML);

?>
