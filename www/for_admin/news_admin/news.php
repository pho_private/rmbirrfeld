<?php
error_reporting(E_ALL);
require_once ('inc.php');
require_once ('functions.php');


$id =     getRequestOrInit('id', '');
$datum =  getRequestOrInit('datum', "{$rm_new_year}"); // Init als Vorfeldnews
$zeit =   getRequestOrInit('zeit', date("H:i:s"));
$action = getRequestOrInit('action', '');

$result = '';

$isVorbereitung = ($datum == "{$rm_new_year}");

switch ($action) {
    case 'create':
        $newsItems = new NewsItems();
        $newsItems->create_item($datum, $zeit);
        break;
    case 'delete':
        $newsItems = new NewsItems();
        $newsItems->delete_item($id);
        break;
    case 'release':
        $newsDates = new NewsDates();
        $newsDates->release($datum);
        break;
    case 'lock':
        $newsDates = new NewsDates();
        $newsDates->lock($datum);
        break;
    case 'release_item':
        $newsItems = new NewsItems();
        $newsItems->release_item($id);
        break;
    case 'lock_item':
        $newsItems = new NewsItems();
        $newsItems->lock_item($id);
        break;
    case 'copy_item':
        $newsItems = new NewsItems();
        $result = $newsItems->copy_item($id, $datum);
        break;
    case 'duplicate_item':
        $newsItems = new NewsItems();
        $result = $newsItems->duplicate_item($id, $zeit);
        break;
}

// create readable date
$int_date = strtotime($datum);
$this_year = date("Y", $int_date);
$this_month = date("n", $int_date);

if ($isVorbereitung) {
    $newsHeader = 'Vorbereitungs-News';
} else {
    $newsHeader = 'News am ' . getWeekdayDateString($datum);
}

$renderedHTML = '';

// read all News Dates - prep period ($rm_new_year), competition days
$newsDates = new NewsDates();
foreach ($newsDates->list as $newsDate) {
    if ($newsDate['datum'] != $datum) continue;
    
    // create headers and show news items pertaining to this NewsDate.
    if ($newsDate['released'] == 'on') {
        $renderedHTML .= <<<EOT
        <br />
         <div class="rm_h1">{$newsHeader} (publiziert) verwalten</div>
        <div class=rm_h2>
            <a href=news.php?action=lock&datum={$datum}>nicht publizieren</<a>
        </div>
EOT;
    } else {
        $renderedHTML .= <<<EOT
        <br />
        <div class="rm_h1">{$newsHeader} (nicht publiziert) verwalten</div>
        <div class=rm_h2>
            <a href=news.php?action=release&datum={$datum}>publizieren</<a>
        </div>
EOT;
    }
    break;
}
$renderedHTML .= <<<EOT
        <div class=rm_h2>
            <a href=news.php?action=create&datum={$datum}>neuer Eintrag</<a>
        </div>
EOT;

// retrieve newsItems pertaining to selected newsDate.
$newsItems = new NewsItems($datum);
foreach ($newsItems->list as $newsItem) {
    $renderedHTML .= <<<EOT
        <table>
            <tr>
EOT;
    if ($newsItem['released'] == 'on'){
        $renderedHTML .= <<<EOT
                <td class=th_1 width=120><a href=news.php?action=lock_item&id={$newsItem['id']}&datum={$datum}>nicht publizieren</a></td>
                <td class=th_1 width=100>{$newsItem['datum']}</td><td class=th_1 width=100>{$newsItem['zeit']}</td><td class=th_1>{$newsItem['titel']}</td>
EOT;
    } else {
        $renderedHTML .= <<<EOT
                <td class=th_1 width=120><a href=news.php?action=release_item&id={$newsItem['id']}&datum={$datum}>publizieren</a></td>
                <td class=td_e width=100>{$newsItem['datum']}</td><td class=td_e width=100>{$newsItem['zeit']}</td><td class=td_e>{$newsItem['titel']}</td>
                <td class=th_1 width=120><a href=news.php?action=delete&id={$newsItem['id']}&datum={$datum} onclick="return confirm('Wirklich l&ouml;schen?')">l&ouml;schen</a></td>
EOT;
    }
    $css_class = ($newsItem['released'] == 'on') ? 'td_1' : 'td_e';
    
    $renderedHTML .= <<<EOT
            </tr>
            <tr>
                <td class=th_1 width=60>
                    <a href=news_change.php?id={$newsItem['id']}>&auml;ndern</a>
                </td>
                <td class={$css_class} colspan=3 width=700>
                    {$newsItem['text']}
                </td>
            </tr>
        </table>
        
       <table>
            <tr>
                <td class=th_1>Diesen Eintrag an einem anderen Tag nochmals verwenden</td>
EOT;

foreach ($newsDates->list as $newsDate) {
    if ($newsDate['datum'] == $datum) {
        $renderedHTML .= <<<EOT
                <td class=td_1></td>
EOT;
    } else {
        $renderedHTML .= <<<EOT
                <td class=td_1>
                    <a href=news.php?action=copy_item&id={$newsItem['id']}&datum={$newsDate['datum']}>{$newsDate['datum']}</a>
                </td>
EOT;
    }
}
$renderedHTML .= <<<EOT
            </tr>
            <tr>
                <td class=th_1>Diesen Eintrag an gleichen Tag nochmals verwenden (um $zeit)</td>
EOT;

foreach ($newsDates->list as $newsDate) {
    if ($newsDate['datum'] == $datum) {
        $renderedHTML .= <<<EOT
                <td class=td_1>
                    <a href=news.php?action=duplicate_item&id={$newsItem['id']}&zeit={$zeit}>{$newsItem['datum']}</a>
                </td>
EOT;
    } else {
        $renderedHTML .= <<<EOT
                <td class=td_1>
                    &nbsp;
                </td>
EOT;
    }
}
$renderedHTML .= <<<EOT
            </tr>
        <table>
EOT;
}

$renderedHTML .= <<<EOT
        <div class=rm_h2>
            {$result}
        </div>
EOT; 
            
// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('OK - Newseditor', 'page', $renderedHTML);
                       
?>
        