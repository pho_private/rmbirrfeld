<?php

function encodeHTML($myVar) {
    // print ("<br />encodeHTML('$myVar') = ");
    if (is_string($myVar)) {
        $myVar = encodeToUtf8(htmlentities($myVar));
        // print("'$myVar'");
    } elseif (is_array($myVar)) {
        foreach ($myVar as $key => $value) {
            $myVar[$key] = encodeHTML($value);
        }
    }
    return $myVar;
}

function getSponsors(string $sponsors_csv) {
    $rows = array_map('str_getcsv', file($sponsors_csv));
    $header = array_shift($rows);
    $sponsors = array();
    foreach ($rows as $row) {
        $sponsors[] = array_combine($header, $row);
    }
    return $sponsors;    
}

function getSponsorHtmlDivs(string $sponsors_html) {
    $sponsors = [];
    $sponsors = file($sponsors_html, FILE_SKIP_EMPTY_LINES);
    $x = shuffle($sponsors);
    return $sponsors;
}

function printArray(array $array) {
    foreach($array as $key => $value) {
      echo $key." |". encodeHTML($value) . "|";
    }    
}


$base = $_SERVER['DOCUMENT_ROOT'];
$rm_web_offset = '/www';
$rm_root = $base . $rm_web_offset;
$crlf = chr(13) . chr(10);

// $csvFile = "{$rm_root}/for_everybody/sponsoren/sponsoren.csv";
$htmlFile = "{$rm_root}/for_everybody/sponsoren/sponsors.html";


$sponsors = [];

for ( $i = 0; $i < 10; $i+=1 ) {
    $sponsors = getSponsorHtmlDivs($htmlFile);
    printArray($sponsors);
    echo "$crlf";
}

?>