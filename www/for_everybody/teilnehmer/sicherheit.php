<?php
require_once ('inc.php');
$renderedHTML = <<<EOT
		<div class=rm_h1>Sicherheit</div>
		<div class=col_block>		
			<div class=rm_h2>Wenige und einfache Standardverfahren</div>
			<div class=rm_text>
				In Platzn&auml;he gelten einfache und bew&auml;hrte Standardverfahren.
				Damit k&ouml;nnen &Uuml;berfl&uuml;ge und Landungen ohne Gefahren durchgef&uuml;hrt werden.
				Diese gelten in der Regel; es kann und soll davon abgewichen werden, falls die Sicherheit dies erfordert.
				Unn&ouml;tiges Abweichen von den Standardverfahren ist unerw&uuml;nscht!
			</div>
			
			<div class=rm_h2>Direktlandungen Piste 08 bei aktiver Piste 26</div>
			<div class=rm_text>
				Direktlandung entgegen der aktiven Pistenrichtung sind im Reglement von einer Bestrafung ausgenommen.
				Damit soll vermieden werden, dass unn&ouml;tige Risiken eingegangen werden, um in korrekter Richtung landen zu k&ouml;nnen.
				F&uuml;r diesen Notfall sind klare Verfahren publiziert und der Pilot kann mit jeder ben&ouml;tigten Unterst&uuml;tzung rechnen.
				Provozierte Opositelandungen werden von der Wettbewerbsleitung anhand der Loggerfiles einzeln beurteilt.
				Es ist mit Zeitstrafen zu rechnen.
			</div>
			
			<div class=rm_h2>Sauerstoff</div>
			<div class=rm_text>
				Ab dem Birrfeld sind auch Fl&uuml;ge in die Voralpen oder Alpen m&ouml;glich. Bitte stellt sicher, dass ihr in so einem Fall
				eine Sauerstoffanlage zur Verf&uuml;gung habt. Nur in Einzelf&auml;llen oder bei St&ouml;rungen k&ouml;nnen wir aushelfen.
			</div>
			
			<div class=rm_h2>Spuren hinterlassen</div>
            <div class="rm_text">
				Im Falle eines Falles kann es wichtig sein, dass man euch schnell findet. Dabei helfen eingeschaltete Handies, ein SPOT
                und vor allem Eure Registrierung in der <a href="http://wiki.glidernet.org/ddb" target="_blank">Device Database von glidernet.org</a>.<br>
                Wir empfehlen auch, Eure FLARM ID <ul>
                  <li>auf Eure ICAO Aircraft ID zu setzen</li>
                  <li>in der <a href="https://www.flarmnet.org/flarmnet/" target="_blank">Flarmnet Datenbank</a> zu registrieren</li>
                  <li>Euren Rechner mit der tagesaktuellen Flarmnet Datenbank auszur&uuml;sten</li>
			</ul></div>
		</div>
		<div class=lst_col_block>
		
			<div class=rm_h2>Vermeidung von Kollisionen</div>
			<div class=rm_text align="justify">
				Aufgrund der hohen Anzahl Flugzeuge, welche identische oder	&auml;hnliche Aufgaben zu erf&uuml;llen haben, besteht eine
				erh&ouml;hte Kollisionsgefahr, sowohl unterwegs, als auch beim Auskreisen von Thermik und bei allf&auml;lligem Hangsegeln.
				Wir bitten die Teilnehmenden:
				<ul>
					<li>die Wolkenabst&auml;nde zu respektieren.</li>
					
					<li>sowohl im Geradeausflug als auch beim Kreisen pl&ouml;tzliche
						und grosse Richtungs&auml;nderungen zu unterlassen. Diese kommen
						f&uuml;r die anderen grunds&auml;tzlich unerwartet.</li>
						
					<li>sich wieder einmal mit den Hangflugregeln vertraut zu machen,
						und bei allf&auml;lligem Hangflug umzusetzen.</li>
				</ul>
			</div>
			
			<div class=rm_h2>FLARM ist Pflicht</div>
			<div class=rm_text>
				Alle Flugzeuge m&uuml;ssen mit FLARM ausger&uuml;stet sein.
				Wir m&ouml;chten aber betonen, dass ein FLARM nur ein Hilfmittel ist und nicht von der Verpflichtung entbindet,
				den Luftraum mit der der Situation entsprechenden Priorit&auml;t zu &uuml;berwachen.
			</div>
			
			<div class=rm_h2>Verantwortung f&uuml;r die sichere Durchf&uuml;hrung der Fl&uuml;ge</div>
			<div class=rm_text>
				Die Teilnehmenden tragen die Verantwortung f&uuml;r die sichere Durchf&uuml;hrung der Fl&uuml;ge.
				Durchsagen der Organisatoren und ihrer Organe haben in diesem Zusammenhang nur den Charakter eines Wunsches.
				Diesen soll nachgekommen werden, wenn die Sicherheit es erlaubt.
			</div>
			
		</div>
		<div class=big_col_block>
			<div class=rm_h1>besondere Gefahren in Platznähe</div>
			
			<div class=rm_text>
				<img border="0" src="../../resources/graphics/organisation/Folie4.PNG">
			</div>
			
			<div class=rm_h2>Hochspannungsleitung</div>
			<div class=rm_text>
				Entlang des nord-&ouml;stlichen Fusses des Chestenbergs verl&auml;uft eine Hochspannungsleitung,
				welche sowohl s&uuml;d-&ouml;stlich als auch n&ouml;rdlich weiterf&uuml;hrt. Die Masth&ouml;hen betragen bis zu hundert Meter.
			</div>
			
			<div class=rm_h2>Pr&uuml;fanlage f&uuml;r Gasturbinen</div>
			<div class=rm_text>
				1.5 km SW der Flugplatzmitte befindet sich eine Pr&uuml;fanlage f&uuml;r Gasturbinen.
				Der Betrieb wird mit einem orangen Drehlicht signalisiert. Es k&ouml;nnen grosse Turbulenzen auftreten.
			</div>
			
			<div class=rm_h2>Motorflugbetrieb</div>
			<div class=rm_text>
				Im Birrfeld herrscht t&auml;glich reger Motorflugbetrieb. Der gr&ouml;sste Teil
				Verkehrs wird &uuml;ber den Sektor West abgewickelt (Einflug > 4000 ft; Ausflug nach Leistung des Flugzeuges). Bitte auf diesen Verkehr achten.
				Das Einfliegen in den Bereich der Motorflugvolten ist unter allen Umst&auml;nden zu unterlassen.
			</div>
			
		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Sicherheit', 'page', $renderedHTML);

?>
