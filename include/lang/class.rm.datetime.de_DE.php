<?php
// +------------------------------------------------------------------------+
// | class.rm.datetime.de_DE.php |
// +------------------------------------------------------------------------+
// | Copyright (c) Peter Hochstrasser 2011. All rights reserved. |
// | Version 0.1 |
// | Last modified 12.03.2011 |
// | Email peter@hochstrasser.net |
// | Web http://www.hochstrasser.net |
// +------------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify |
// | it under the terms of the GNU General Public License version 2 as |
// | published by the Free Software Foundation. |
// | |
// | This program is distributed in the hope that it will be useful, |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the |
// | GNU General Public License for more details. |
// | |
// | You should have received a copy of the GNU General Public License |
// | along with this program; if not, write to the |
// | Free Software Foundation, Inc., 59 Temple Place, Suite 330, |
// | Boston, MA 02111-1307 USA |
// | |
// | Please give credit on sites that use class.upload and submit changes |
// | of the script so other people can use them as well. |
// | This script is free to use, don't abuse. |
// +------------------------------------------------------------------------+
$this->translation = array();
$this->translation['sunday'] = 'Sonntag';
$this->translation['monday'] = 'Montag';
$this->translation['tuesday'] = 'Dienstag';
$this->translation['wednesday'] = 'Mittwoch';
$this->translation['thursday'] = 'Donnerstag';
$this->translation['friday'] = 'Freitag';
$this->translation['saturday'] = 'Samstag';
$this->translation['goodfriday'] = 'Karfreitag';
$this->translation['easter'] = 'Ostern';
$this->translation['eastermonday'] = 'Ostermontag';
$this->translation['ascension'] = 'Auffahrt';
$this->translation['whitsunday'] = 'Pfingsten';
$this->translation['whitsunmonday'] = 'Pfingstmontag';
