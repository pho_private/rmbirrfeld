<?php
require_once ('inc.php');
require_once ('functions.php');


error_reporting(E_ALL);

/**
 * from a date 'yyyy-mm-dd' generate an IGC Date String 'YMD'.
 *
 * @param string $day
 */
function getIgcDateString($day)
{
    $igcDate = new IGCFileNameHandler();
    $igcDate->setDate($day);
    return $igcDate->getIgcDate();
}

function finishTable()
{
    return <<<EOT
			</table>

EOT;
}

// ------------------------------------------------------------------------------------------
// code starts here
// ------------------------------------------------------------------------------------------

// get url commands
$action = getRequestOrInit('action');
$competitionDate = getRequestOrInit('date', $rm_dates[1]);

$formattedCompetitionDate = getWeekdayDateString($competitionDate);

$folder = $rm_igc;
if (!is_dir($folder)) {
    if (!mkdir($folder, 0755, true)) {
        die('Failed to create IGC folder');
    }
}
$folderUrl = $rm_igc_url;

$errorMsg = '';

// generate compressed archive (per competition day)

if ($action == 'Dateien komprimieren') {

    // logger files are in one directory per year
    // thus, get file list per competition day, and generate zip per competition day, if necessary.
    // necessary = there is at least one logger file of the day that has been modified after the day's archive has been created.

    foreach ($rm_dates as $compDay) {

        // get all logger files of specified day in directory
        $igcDateString = getIgcDateString($compDay);

        // $files contains no paths!
        $files = filelist($folder, "/" . $igcDateString . ".*igc|" . $igcDateString . ".*lxn/i");

        if (count($files) > 0) {
            $zipFilename = "flights-{$compDay}.zip";
            $zip = new rm_ZipArchive();
            $tmp = $zip->open($folder . "/" . $zipFilename, ZipArchive::OVERWRITE);
            if ($tmp) {
                sort($files);
                reset($files);

                foreach ($files as $file) {

                    // pretty important to add the path here in the first parameter and ...
                    // to add the "local name" as the second parameter. Otherwise, the whole path gets saved in the ZIP!
                    $tmp = $zip->addFile($folder . "/" . $file, $file);
                    if (! $tmp) {
                        $zipError = $zip->getStatusString();
                        $errorMsg .= "<div class=\"errormessage\">failed to add {$file} to {$zipFilename}:{$zipError}</div>";
                    }
                }
                $tmp = $zip->close();
                if (! $tmp) {
                    $zipError = $zip->getStatusString();
                    $errorMsg .= "<div class=\"errormessage\">failed to close {$zipFilename}:{$zipError}</div>";
                }
            } else {
                $zipError = $zip->getStatusString();
                $errorMsg .= "<div class=\"errormessage\">failed to open {$zipFilename}:{$zipError}</div>";
            }
        }
    }
}
$igcDateString = getIgcDateString($competitionDate);

$files = filelist($folder, "/{$igcDateString}.*\.igc|{$igcDateString}.*\.lxn/i");
sort($files);
reset($files);

$list = array();
$teilnehmerListe = new Teilnehmer();
$teilnehmerListe->load_all_by_class();
$newestFile = "01-01 00:00";

// generate list of known loggers and their files
foreach ($teilnehmerListe->list as $teilnehmer) {
    $id = strtoupper($teilnehmer['logger1id']);

    // handle multiple loggers per participant:
    // generate array with one loggerid per array element. In most cases, the array has 1 element only.
    $ids = preg_split("/[,\.][ ]{0,1}/", $id);

    $found = false;
    $fileid = "";
    foreach ($files as $file) {
        $timestamp = date("m-d H:i", filemtime($folder . "/" . $file));
        if ($newestFile < $timestamp) {
            $newestFile = $timestamp;
        }
        // IGC FileName = <year><month><day><manufacturer><logger_serial_3chars><sequence_number>.igc where
        // year = 0-9
        // month = 1-9,a-c (1..12)
        // day = 1-9,a-u (1..31)
        // manufacturer = a..z, 0..9
        // logger_serial_3chars = 3 character long serial ID consisting of numbers and characters
        // sequence_number = flight record on this date with this logger (1..9)
        // we pick manufacturer + logger_serial_3chars
        $fileid = strtoupper(substr($file, 3, 4));
        $found = (in_array($fileid, $ids));
        if ($found)
            break;
    }
    $entry = array();
    $entry['file'] = ($found) ? $file : "";
    $entry['zeit'] = ($found) ? $timestamp : "";
    $entry['id'] = $id;
    $entry['wk'] = $teilnehmer['wk'];
    $entry['pilot'] = $teilnehmer['vorname']." ". $teilnehmer['name'];
    $entry['klasse'] = $teilnehmer['klasse'];
    $list[] = $entry;
}

// generate list of non-assigned loggers
foreach ($files as $file) {
    $timestamp = date("m-d H:i", filemtime($folder . "/" . $file));
    $fileid = strtoupper(substr($file, 3, 4));
    $found = false;
    foreach ($teilnehmerListe->list as $teilnehmer) {
        $id = strtoupper($teilnehmer['logger1id']);

        // handle multiple loggers per participant
        $ids = preg_split("/[,\.][ ]{0,1}/", $id);

        if (in_array($fileid, $ids)) {
            $found = true;
            break;
        }
    }
    if (! $found) {
        $entry = array();
        $entry['file'] = $file;
        $entry['zeit'] = $timestamp;
        $entry['id'] = $fileid;
        $entry['wk'] = "";
        $entry['pilot'] = "";
        $entry['klasse'] = "";
        $list[] = $entry;
    }
}

$last_class = '';
$cnt = 0;
$renderedHTML = <<<EOT
    <div class=main_block>
        <div class=rm_h1>IGC Dateien f&uuml;r Fl&uuml;ge von {$formattedCompetitionDate}</div>
        <div class=big_col_block>
EOT;

// compress button etc.
$renderedHTML .= <<<EOT
			<table class="rm_todo">
				<colgroup>
					<col width="20%">
					<col width="40%">
					<col width="40%">
				</colgroup>
				<tr>
					<th class=th_1>
						<form>
							<input type="hidden" name="date" value="{$competitionDate}">
							<input type="submit" name="action" value="Dateien komprimieren">
						</form>
					</th>
					<th class=th_1_c>Anzahl Dateien</td>
					<th class=th_1_c>erstellt</td>
					<th class=th_1_c>aktuell?</td>
				</tr>
EOT;

// now, list all zip files.
$files = filelist($folder, "/.zip/i");
// ... if there are any
if (count($files) > 0) {
    sort($files);
    reset($files);

    foreach ($files as $file) {
        $filepath = $folder . "/" . $file;
        $timestamp = date("m-d H:i", filemtime($filepath));
        $zip = new ZipArchive();
        $zip->open($filepath);
        $num = $zip->numFiles;
        $actualityMsg = "<img src=\"{$rm_icon_url}/" . (($newestFile < $timestamp) ? "nav_plain_green.png" : "nav_plain_red.png") . "\" width=15 height=15>";
        $timestamp = htmlentities($timestamp);
        $renderedHTML .= <<<EOT
					<tr>
						<td class="td_1">
							<a href="{$folderUrl}/{$file}">{$file}<a>
						</td>
						<td class="td_1_c">{$num}</td>
						<td class="td_1_r">{$timestamp}</td>
						<td class="td_1_c">{$actualityMsg}</td>
					</tr>
EOT;
    }
}
$renderedHTML .= finishTable();

// the unknown logger files

$renderedHTML .= <<<EOT
			<table class="rm_todo">
				<colgroup>
					<col width="20%">
					<col width="40%">
					<col width="40%">
				</colgroup>
				<tr>
					<th class=th_1>nicht registrierte Logger</th>
					<th class=th_1_c>geladen</th>
					<th class=th_1_c>ID (name)</th>
					<th class=th_1_c>ID (Inhalt)</th>
				</tr>
EOT;

foreach ($list as $item) {
    if ($item['klasse'] == "") {

        $fileName = htmlentities(strtolower($item['file']));
        $fileUrl = $rm_igc_url . "/" . $item['file'];

        $inhalt = file($folder . "/" . $item['file']);
        $info_text = "<div class=tooltip><table width=100%>";
        $tmp_id = substr($inhalt[0], 1, 1) . substr($inhalt[0], 4, 3);
        for ($pnt = 0; $pnt < 15; $pnt ++) {
            $tmp_txt = substr($inhalt[$pnt], 0, - 2);
            $info_text .= "<tr><td class=th_1>" . htmlentities($tmp_txt) . "</td></tr>";
        }
        $info_text .= "</table></div>";
        $renderedHTML .= <<<EOT
				<tr>
					<td class=td_1><a href="{$fileUrl}" onMouseover="showtip('{$info_text}')" onMouseout="hidetip()">{$fileName}</a></td>
					<td class=td_1_c>{$item['zeit']}</td>
					<td class=td_1_c>{$item['id']}</td>
					<td class=td_1_c>{$tmp_id}</td>
				</tr>
EOT;
        $cnt ++;
    }
}
$renderedHTML .= finishTable();
$renderedHTML .= <<<EOT
            <div id="dhtmltooltip"></div>
            <script type="text/javascript" src='tooltips.js'></script>
EOT;

// render a table for each class
foreach ($class_key as $class) {
    if ($class != 0) {
        $klasse = $class_name[$class];
        $bgcolor = $class_bgcolor[$class];
        $renderedHTML .= <<<EOT
			<table bgcolor={$bgcolor} width="100%" border=0>
				<colgroup>
					<col width="20%">
					<col width="10%">
					<col width="20%">
					<col width="10%">
					<col width="40%">
				</colgroup>
				<tr><th colspan=5>{$klasse}</th<tr>
				<tr>
					<th class=th_1_c>Datei</td>
					<th class=th_1_c>geladen</td>
					<th class=th_1_c>Id</td>
					<th class=th_1_c>WZ</td>
					<th class=th_1>TeilnehmerIn</td>
				</tr>
EOT;
        // now, generate the table entries
        foreach ($list as $item) {
            $fileUrl = $folderUrl . "/" . $item['file'];
            $fileName = strtolower($item['file']);
            $uploadTime = (($item['zeit'] == $newestFile) ? "<b>{$item['zeit']}</b>" : $item['zeit']);
            $loggerid = $item['id'];

            if ($item['klasse'] != $class)
                continue;
            $renderedHTML .= <<<EOT
				<tr>
					<td class=td_1><a href="{$fileUrl}">{$fileName}</a></td>
					<td class=td_1_c>{$uploadTime}</td>
					<td class=td_1_c>{$loggerid}</td>
					<td class=td_1_c>{$item['wk']}</td>
					<td class=td_1>{$item['pilot']}</td>
				</tr>
EOT;
        }
        $renderedHTML .= finishTable();
        $cnt ++;
    }
}
$renderedHTML .= <<<EOT
		</div>
EOT;

$errorMsgBlock = ($errorMsg != '') ? encodeToUtf8("<div class=\"td_e\">{$errorMsg}</div>") : ('');
// used to be 	<body onload="redirect('igc_download.php?date={$competitionDate}', 15000)">
$bodyStart = <<<EOT
	<body onload="setTimeout("location.reload(true);", 15000)">
{$errorMsgBlock}
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Auswertung - IGC Downloads', 'page', $renderedHTML, $bodyStart);

?>
