<?php
require_once ('inc.php');
require_once ('helfer_inc.php');
require_once ('functions.php');


// get date and class selected by the user
$action = getRequestOrInit('action', '');
$name = getRequestOrInit('name');
$vorname = getRequestOrInit('vorname');
$jahrgang = getRequestOrInit('jahrgang');
$ort = getRequestOrInit('ort');

if ($action == 'delete') {
    $delete = "DELETE FROM einsaetze WHERE deleted is NOT NULL AND vorname = '{$vorname}' AND name = '{$name}' AND jahrgang = '{$jahrgang}' AND wohnort = '{$ort}'";
    DBQuery($delete);
}

// header
$renderedTable = <<<EOT
		<div class=rm_h1>Eins&auml;tze pro Helfer</div>
        <table>
        <tr>
        	<th class=th_1>Gruppe</td>
        	<th class=th_1>Helfer</td>
        	<th class=th_1>E-Mail</td>
        	<th class=th_1>Telefon</td>
        	<th class=th_1>Eins&auml;tze</td>
        	<th class=th_1></td>
        	<th class=th_1></td>
        	<th class=th_1>Letzte &Auml;</td>
        </tr>

EOT;

$tot = 0;
$helferlist = new Helfer();
$helfer_liste = $helferlist->load_list();
foreach ($helfer_liste as $helfer) {
    $phone = $helfer['phone'];
    if ($helfer['activ'] == 'aktiv') {
        $tot += $helfer['einsaetze'];
        $renderedTable .= <<<EOT
		<tr>
			<td class=td_1>{$helfer['gruppe']}</td>
			<td class=td_1>{$helfer['name']} {$helfer['vorname']}, {$helfer['wohnort']} ,{$helfer['jahrgang']} </td>
			<td class=td_1>{$helfer['mail']}</td>
			<td class=td_1>{$helfer['phone']}</td>
			<td class=td_1>{$helfer['einsaetze']} ({$helfer['activ']})</td>
			<td class=td_1>
				<a href="helfer_einsatz.php?submit=anmelden&name={$helfer['name']}&vorname={$helfer['vorname']}&jahrgang={$helfer['jahrgang']}&ort={$helfer['wohnort']}&mail={$helfer['mail']}&phone={$helfer['phone']}">
				<img src="{$rm_icon_url}/edit.png" width="18"></td>
			<td class=td_1>
			</td>
			<td class=td_1>{$helfer['updated']}</td>
		</tr>
EOT;
    } else {
        $renderedTable .= <<<EOT
			<tr>
			<td class=td_1>{$helfer['gruppe']}</td>
			<td class=td_1>{$helfer['name']} {$helfer['vorname']}, {$helfer['wohnort']} ,{$helfer['jahrgang']} </td>
			<td class=td_1>{$helfer['mail']}</td>
			<td class=td_1>{$phone}</td>
			<td class=td_1>{$helfer['einsaetze']} ({$helfer['activ']})</td>
			<td class=td_1>
				<a href="helfer_einsatz.php?submit=anmelden&name={$helfer['name']}&vorname={$helfer['vorname']}&jahrgang={$helfer['jahrgang']}&ort={$helfer['wohnort']}&mail={$helfer['mail']}&phone={$helfer['phone']}">
				<img src="{$rm_icon_url}/edit.png" width="18" title="alle Eins�tze l�schen"></a>
			</td>
			<td class=td_1>
				<a href="helfer_liste.php?action=delete&name={$helfer['name']}&vorname={$helfer['vorname']}&jahrgang={$helfer['jahrgang']}&ort={$helfer['wohnort']}">
				<img src="{$rm_icon_url}/delete.png" width="18" title="Eins�tze l�schen"></a>
			</td>
			<td class=td_1>{$helfer['updated']}</td>
		</tr>
EOT;
    }
}

$renderedTable .= <<<EOT
			<tr>
				<th class=th_1 colspan=8>Total aktive Eins&auml;tze {$tot}</th>
			</tr>
		</table>
		<div class=rm_h1>Eins&auml;tze pro Gruppe</div>
		<table>
			<tr>
				<th class=th_1>Gruppe</td>
				<th class=th_1>Eins&auml;tze</td>
			</tr>
EOT;

$tot = 0;
$helferlist = new Helfer();
$helfer_liste = $helferlist->load_gruppe();
foreach ($helfer_liste as $helfer) {
    $renderedTable .= <<<EOT
			<tr>
				<td class=td_1>{$helfer['gruppe']}</td>
				<td class=td_1>{$helfer['einsaetze']} ({$helfer['activ']})</td>
			</tr>
EOT;
    if ($helfer['activ'] == 'aktiv')
        $tot += $helfer['einsaetze'];
}
$renderedTable .= <<<EOT
			<tr>
				<th class=th_1 colspan=2>Total aktive Eins&auml;tze {$tot}</td>
			</tr>
		</table>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Helfer - Helferliste', 'page', $renderedHTML);

?>
