<?php
require_once ('inc.php');

$renderedHTML = <<<EOT
			<div class="hnav_item"><h2>Checks und Administration</h2></div>
	        <div class="hnav_item"><a href="foto_upload/picture_list.php">Foto Verwaltung</a></div>
            <div class="hnav_item"><a href="teilnehmer_check.php">Admin Check</a></div>
            <div class="hnav_item"><a href="auswertung/teilnehmerdaten.php">ID Check</a></div>
            <div class="hnav_item"><a href="teilnehmer_pdf.php">TN PDF</a></div>
            <div class="hnav_item"><a href="teilnehmer_csv.php">TN CSV</a></div>
            <div class="hnav_item"><a href="auswertung/CUC-csv.php">CUC CSV</a></div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('Admin Checks', 'hnav', $renderedHTML);

?>

