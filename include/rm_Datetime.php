<?php

class rm_DateTime extends DateTime
{

    /**
     * Array of translated messages
     *
     * By default, the language is english (en_GB)
     * Translations can be in separate files, in a lang/ subdirectory
     *
     * @access public
     * @var array
     */
    var $translation;

    /**
     * Language selected for the translations
     *
     * By default, the language is english ("en_US")
     *
     * @access public
     * @var array
     */
    var $language;

    var $weekdaysLong;

    /*
     * (non-PHPdoc)
     * @see DateTime::__construct()
     */
    public function __construct($time = null, $timezone = null, $language = 'de_DE')
    {
        // sets default language messages
        $this->translation = array();
        $this->translation['sunday'] = 'Sunday';
        $this->translation['monday'] = 'Monday';
        $this->translation['tuesday'] = 'Tuesday';
        $this->translation['wednesday'] = 'Wednesday';
        $this->translation['thursday'] = 'Thursday';
        $this->translation['friday'] = 'Friday';
        $this->translation['saturday'] = 'Saturday';
        $this->translation['goodfriday'] = 'Good Friday';
        $this->translation['easter'] = 'Easter';
        $this->translation['eastermonday'] = 'Easter Monday';
        $this->translation['ascension'] = 'Ascension';
        $this->translation['whitsunday'] = 'Whitsunday';
        $this->translation['whitsunmonday'] = 'Whitsun Monday';
        
        // determines the language
        $this->language = $language;
        if ($this->language != 'en_US' && file_exists(dirname(__FILE__) . '/lang') && file_exists(dirname(__FILE__) . '/lang/class.rm.datetime.' . $language . '.php')) {
            $translation = null;
            include (dirname(__FILE__) . '/lang/class.rm.datetime.' . $language . '.php');
            if (is_array($translation)) {
                $this->translation = array_merge($this->translation, $translation);
            } else {
                $this->language = 'en_US';
            }
        }
        $this->weekdaysLong = array(
            $this->translation['sunday'],
            $this->translation['monday'],
            $this->translation['tuesday'],
            $this->translation['wednesday'],
            $this->translation['thursday'],
            $this->translation['friday'],
            $this->translation['saturday'],
            $this->translation['goodfriday'],
            $this->translation['easter'],
            $this->translation['eastermonday'],
            $this->translation['ascension'],
            $this->translation['whitsunday'],
            $this->translation['whitsunmonday']
        );
        
        // finally, construct base object.
        if ($timezone == NULL) {
            $timezone = new DateTimeZone('Europe/Zurich');
        }
        if ($time == NULL) {
            $time = 'now';
        }
        DateTime::__construct($time, $timezone);
    }

    private function getSpecialWeekday()
    {
        
        // calculate specially named weekdays for all easter-related holidays.
        $easter = easter_date(); // this year's easter date
        
        /*
         * if (phpversion() >= '5.3.0')
         * {
         *
         * $diffToEaster = $this->diff(new DateTime($easter)); // returns a DateInterval object of $this - $easter - i.e. dates before easter have a negative difference.
         * $daysAfterEaster = - $diffToEaster->format('%r%a'); // returns the number of days in the date interval, including sign.
         * }
         * else
         */
        {
            $today = localtime(time(), true);
            $easter = localtime($easter, true);
            $daysAfterEaster = $today['tm_yday'] - $easter['tm_yday'];
        }
        
        switch ($daysAfterEaster) {
            case - 2:
                $compDay = 7; // Karfreitag
                break;
            case 0:
                $compDay = 8; // Ostern
                break;
            case 1:
                $compDay = 9; // Ostermontag
                break;
            case 39:
                $compDay = 10; // Auffahrt
                break;
            case 49:
                $compDay = 11; // Pfingsten
                break;
            case 50:
                $compDay = 12; // Pfingstmontag
                break;
            default: // just leave as is.
                $compDay = $this->format('w');
                break;
        }
        return $this->weekdaysLong[$compDay];
    }

    /*
     * trap "w" (weekday literal) formatting to add support for special weekdays:
     * easter-related:
     * good friday
     * easter monday
     * ascension
     * whitsun monday
     *
     * @see DateTime::format()
     */
    public function format($format)
    {
        // replace all occurrences of the weekday indicator in the search string with a token that is passed on verbatim
        // this ensures that weekdays are not replaced by the built-in format, and
        // that our replacement strings are not taken for format commands/selectors
        $modifiedFormat = str_replace('l', '!!', $format);
        // now, format everything else with the standard method
        $result = DateTime::format($modifiedFormat);
        if ($format != $modifiedFormat) {
            // finally, replace all occurrences of our token with our formatting
            $result = str_replace('!!', $this->getSpecialWeekday(), $result);
        }
        return $result;
    }
}
?>