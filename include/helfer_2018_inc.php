<?php
// include für Helferverwaltung gebraucht werden.
// ==============================================
// Neue Version 2018-12-02 / Richi Hächler
//
//
// Datum des starts pro einsatzart
date_default_timezone_set('Europe/Zurich');

// Helfer-Standardgruppe (Üblicherweise der Organisator)
$rm_helfer_default_gruppe 	= $rm_ausrichter;

// ab diesem Datum müssen Änderungen über das OK laufen.
$rm_helfer_no_changes 		= $rm_year . "-06-01";

// mögliche Einsatzgebiete (nun activ 0/1 modifizieren)
$einsatz_gebiete = array(
	array('key' =>	'aufbau',		'text' => 'Aufbau',		'activ'	=> 1),
	array('key' =>	'eroeffnung',	'text' => 'Eroeffnung',	'activ'	=> 1),
	array('key' =>	'beiz',			'text' => 'Beiz',		'activ'	=> 1),
	array('key' =>	'betrieb',		'text' => 'Betrieb',	'activ'	=> 1),
	array('key' =>	'abschluss',	'text' => 'Abschluss',	'activ'	=> 0),
	array('key' =>	'abbau',		'text' => 'Abbau',		'activ'	=> 1)
	);

	
// Einsatzzeiten
$schichtzeiten_aufbau 		= array(1 => '0800-1200', '1300-1800');
$schichtzeiten_eroeffnung 	= array(1 => '1600-2230');
$schichtzeiten_beiz 		= array(1 => '0800-1300', '1100-1800', '1700-2300');
$schichtzeiten_betrieb 		= array(1 => '1000-1600', '1200-1800');
$schichtzeiten_abschluss 	= array(1 => '0800-1400');
$schichtzeiten_abbau 		= array(1 => '0800-1200', '1300-1800');

	
// Daten und Einsätze
$aufbau_dates_und_einsatz 			= array();
$eroeffnung_dates_und_einsatz 		= array();
$beiz_dates_und_einsatz 			= array();
$betrieb_dates_und_einsatz 			= array();
$abschluss_dates_und_einsatz		= array();
$abbau_dates_und_einsatz			= array();


$aufbau_dates_und_einsatz[]			= array('datum' => $rm_year . "-06-14", 'einsaetze' => array(1 => 8,8 ));
$aufbau_dates_und_einsatz[]			= array('datum' => $rm_year . "-06-15", 'einsaetze' => array(1 => 8,8 ));
	
$eroeffnung_dates_und_einsatz[]		= array('datum' => $rm_year . "-06-15", 'einsaetze' => array(1 => 8));

$beiz_dates_und_einsatz[]			= array('datum' => $rm_year . "-06-16", 'einsaetze' => array(1 => 5, 5, 4));
$beiz_dates_und_einsatz[]			= array('datum' => $rm_year . "-06-17", 'einsaetze' => array(1 => 5, 5, 4));
$beiz_dates_und_einsatz[]			= array('datum' => $rm_year . "-06-18", 'einsaetze' => array(1 => 4, 4, 4));
$beiz_dates_und_einsatz[]			= array('datum' => $rm_year . "-06-19", 'einsaetze' => array(1 => 4, 4, 4));
$beiz_dates_und_einsatz[]			= array('datum' => $rm_year . "-06-20", 'einsaetze' => array(1 => 5, 5, 4));
$beiz_dates_und_einsatz[]			= array('datum' => $rm_year . "-06-21", 'einsaetze' => array(1 => 4, 4, 4));
$beiz_dates_und_einsatz[]			= array('datum' => $rm_year . "-06-22", 'einsaetze' => array(1 => 4, 4, 4));
$beiz_dates_und_einsatz[]			= array('datum' => $rm_year . "-06-23", 'einsaetze' => array(1 => 5, 5, 6));
$beiz_dates_und_einsatz[]			= array('datum' => $rm_year . "-06-24", 'einsaetze' => array(1 => 5, 5, 4));

$betrieb_dates_und_einsatz[]		= array('datum' => $rm_year . "-06-16", 'einsaetze' => array(1 => 5, 5));
$betrieb_dates_und_einsatz[]		= array('datum' => $rm_year . "-06-17", 'einsaetze' => array(1 => 5, 5));
$betrieb_dates_und_einsatz[]		= array('datum' => $rm_year . "-06-18", 'einsaetze' => array(1 => 5, 5));
$betrieb_dates_und_einsatz[]		= array('datum' => $rm_year . "-06-19", 'einsaetze' => array(1 => 5, 5));
$betrieb_dates_und_einsatz[]		= array('datum' => $rm_year . "-06-20", 'einsaetze' => array(1 => 5, 5));
$betrieb_dates_und_einsatz[]		= array('datum' => $rm_year . "-06-21", 'einsaetze' => array(1 => 5, 5));
$betrieb_dates_und_einsatz[]		= array('datum' => $rm_year . "-06-22", 'einsaetze' => array(1 => 5, 5));
$betrieb_dates_und_einsatz[]		= array('datum' => $rm_year . "-06-23", 'einsaetze' => array(1 => 5, 5));
$betrieb_dates_und_einsatz[]		= array('datum' => $rm_year . "-06-24", 'einsaetze' => array(1 => 5, 5));

$abschluss_dates_und_einsatz[]		= array('datum' => $rm_year . "-06-24", 'einsaetze' => array(1 => 0));

$abbau_dates_und_einsatz[]			= array('datum' => $rm_year . "-06-25", 'einsaetze' => array( 1 => 8, 8));

// summe aller einsaetze ueber alle einsatzarten
$total_einsaetze = 0;
foreach ($aufbau_dates_und_einsatz 		as $dates_und_einsatz)		$total_einsaetze += array_sum($dates_und_einsatz['einsaetze']);
foreach ($eroeffnung_dates_und_einsatz 	as $dates_und_einsatz)		$total_einsaetze += array_sum($dates_und_einsatz['einsaetze']);
foreach ($beiz_dates_und_einsatz 		as $dates_und_einsatz)		$total_einsaetze += array_sum($dates_und_einsatz['einsaetze']);
foreach ($betrieb_dates_und_einsatz 	as $dates_und_einsatz)		$total_einsaetze += array_sum($dates_und_einsatz['einsaetze']);
foreach ($abschluss_dates_und_einsatz 	as $dates_und_einsatz)		$total_einsaetze += array_sum($dates_und_einsatz['einsaetze']);
foreach ($abbau_dates_und_einsatz	 	as $dates_und_einsatz)		$total_einsaetze += array_sum($dates_und_einsatz['einsaetze']);


// welche Schichten ueberschneiden - Index 1 = Beiz, Index 2 = Betrieb, erstes Element = Schicht 1 (Index 0).
$schicht_konflikt_beiz_betrieb = array(
    array( 1, 0 ),
    array( 1, 1 ),
    array( 0, 1 )
);
?>
