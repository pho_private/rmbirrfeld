<?php
require_once ('inc.php');
require_once ('functions.php');


$newsDates = new NewsDates($rm_new_year);
$renderedHTML = <<<EOT
			<div class="hnav_item"><h2>Liveticker verwalten</h2></div>
EOT;
foreach ($newsDates->list as $newDate)
{
    $renderedHTML .= <<<EOT
			<div class="hnav_item"><a href=news.php?datum={$newDate['datum']}>{$newDate['titel']}</a></div>

EOT;
}

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('Newsverwaltung', 'hnav', $renderedHTML);

?>
