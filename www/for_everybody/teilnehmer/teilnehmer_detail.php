<?php
session_start();
error_reporting(E_ALL);

require_once ('inc.php');
require_once ('dblib.inc.php');
require_once ('functions.php');

function displayFotoFor($pilotNr, $pilotData)
{
    global $rm_tn_photos_url;
    $pilot = $pilotData["pilot{$pilotNr}"];
    
    if ($pilot != '') {
        $foto = $pilotData["foto{$pilotNr}"];
        if ($foto != '') {
            $renderedHTML = <<<EOT
                <td valign=top align=center width=300><img src={$rm_tn_photos_url}/{$foto}_midi.jpg width=300><br />{$pilot}</td>
EOT;
        } else {
            $renderedHTML = <<<EOT
                <td valign=top align=center width=300>{$pilot}<br />Bitte ein Foto hochladen!</td>
EOT;
        }
        return true;
    }
    return false;
}

$renderedHTML = <<<EOT
	<div class=big_col_block>
EOT;

$immatrikulation = getRequestOrInit('immatrikulation', '');

$pilotData = getParticipantsFor("rmactiv = 'ja' AND immatrikulation = '{$immatrikulation}' ORDER BY klasse, name, vorname");

if ($pilotData['klasse'] == $rm_TeamClassIndex) {
    $renderedHTML .= <<<EOT
		<div class=rm_h1>{$pilotData['teamname']} ({$class_name[$pilotData['klasse']]})</div>
		<table>
			<tr>
				<td valign=top width=300>
					<table>
						<tr>
							<td style=text-transform:uppercase class=wk_pd colspan=2>{$pilotData['wk']}</td>
						</tr>
						<tr>
							<td class=th_pd>Pilot 1</td>
							<td class=td_pd>{$pilotData['vorname']} {$pilotData['name']}</td>
						</tr>
EOT;
    
    if ($pilotData['pilot2'] != '') {
        $renderedHTML .= <<<EOT
						<tr>
							<td class=th_pd>Pilot 2</td>
							<td class=td_pd>{$pilotData['pilot2']}</td>
						</tr>
EOT;
    }
    if ($pilotData['pilot3'] != '') {
        $renderedHTML .= <<<EOT
						<tr>
							<td class=th_pd>Pilot 3</td>
							<td class=td_pd>{$pilotData['pilot3']}</td>
						</tr>
EOT;
    }
    if ($pilotData['pilot4'] != '') {
        $renderedHTML .= <<<EOT
						<tr>
							<td class=th_pd>Pilot 4</td>
							<td class=td_pd>{$pilotData['pilot4']}</td>
						</tr>
EOT;
    }
    if ($pilotData['pilot5'] != '') {
        $renderedHTML .= <<<EOT
						<tr>
							<td class=th_pd>Pilot 5</td>
							<td class=td_pd>{$pilotData['pilot5']}</td>
						</tr>
EOT;
    }
    $renderedHTML .= <<<EOT
						<tr>
							<td class=th_pd>Flugzeug</td>
							<td class=td_pd>{$pilotData['immatrikulation']} / {$pilotData['flugzeug']}</td>
						</tr>
						<tr>
							<td class=th_pd>Gruppe</td>
							<td class=td_pd>{$pilotData['gruppe']}</td>
						</tr>
					</tr>
				</table>
			</td>
			<td>
				<table>
					<tr>
EOT;
    
    if ($pilotData['foto'] != '') {
        $renderedHTML .= <<<EOT
						<td valign=top align=center width=300><img src={$rm_tn_photos_url}/{$pilotData['foto']}_midi.jpg width=300><br />{$pilotData['vorname']} {$pilotData['name']}</td>
EOT;
    } else {
        $renderedHTML .= <<<EOT
						<td align=center width=300>{$pilotData['vorname']} {$pilotData['vorname']} <br />Bitte ein Foto hochladen!</td>
EOT;
    }
    $fotos = 1;
    for ($i = 2; $i <= 5; $i ++) {
        if (displayFotoFor($i, $pilotData)) {
            $fotos += 1;
            if ($fotos == 2) {
                $renderedHTML .= <<<EOT
					</tr>
				</table>
			</td>
		</tr>
		<table>
			<tr>
				<td>
					<table>
EOT;
            }
        }
    }
    if ($pilotData['angaben'] != "")
        $renderedHTML .= <<<EOT
						<tr>
							<td class=td_pd colspan=2 width=900><textarea name=\"angaben\" cols=\"124\" rows=\"10\" readonly>{$pilotData['angaben']}</textarea></td>
						</tr>
EOT;
    else
        $renderedHTML .= <<<EOT
						<tr>
							<td class=td_pd colspan=2 width=900>Bitte noch ein paar Angaben eingeben!</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
EOT;
} else {
    
    // Beginn Einzelpilot
    $renderedHTML .= <<<EOT
		<table>
			<tr>
				<td valign=top width=450>
					<table>
						<tr><td class=wk_pd colspan=2>{$pilotData['wk']}</td></tr>
						<tr><th class=th_pd width=200>TeilnehmerIn</th><td class=td_pd>{$pilotData['vorname']} {$pilotData['name']}</td></tr>
						<tr><th class=th_pd>Immatrikulation</th><td class=td_pd>{$pilotData['immatrikulation']}</td></tr>
						<tr><th class=th_pd>Type</th><td class=td_pd>{$pilotData['flugzeug']}</td></tr>
						<tr><th class=th_pd>Index</th><td class=td_pd>{$pilotData['indexwert']}</td></tr>
						<tr><th class=th_pd>Gruppe</th><td class=td_pd>{$pilotData['gruppe']}</td></tr>
						<tr><th class=th_pd>Wohnort</th><td class=td_pd>{$pilotData['wohnort']}</td></tr>
					</table>	
				</td>
				<td>
					<table>
						<tr>
EOT;
    
    if ($pilotData['foto'] != '') {
        $renderedHTML .= <<<EOT
							<td valign=top align=center width=500><img src={$rm_tn_photos_url}/{$pilotData['foto']}_midi.jpg><br /></td>
EOT;
    } else {
        $renderedHTML .= <<<EOT
							<td align=center width=500><br /><h1>Bitte ein Foto hochladen!</h1></td>
EOT;
    }
    $renderedHTML .= <<<EOT
						</tr>
EOT;
    if ($pilotData['angaben'] != '')
        $renderedHTML .= <<<EOT
						<tr>
							<td class=td_pd colspan=2><textarea name="angaben" width = 100% rows="10" readonly style="width:500px; height:206px; ">{$pilotData['angaben']}</textarea></td>
						</tr>
EOT;
    else
        $renderedHTML .= <<<EOT
						<tr>
							<td class=td_pd colspan=2>Bitte noch ein paar Angaben eingeben!</td>
						</tr>
EOT;
}
$renderedHTML .= <<<EOT
					</table>
				</td>
			</tr>
		</table>
	</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('Teilnehmerdetails', 'page', $renderedHTML);

?>
