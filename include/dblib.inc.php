<?php
require_once ('inc.php');
require_once ('functions.php');
require_once ('tracking_glide_and_seek.php');

global $db_link;

function initSession($id, $login, $pass) {
    global $debug, $debug_msg, $dblib_debug;
    $_SESSION ['id'] = $id;
    $_SESSION ['login'] = $login;
    $_SESSION ['passwort'] = $pass;
    $_SESSION ['logged_in'] = true;

    if ($debug && $dblib_debug) {
        $debug_msg .= '<br />$_SESSION = ' . print_r($_SESSION, true);
    }
}
function connectToDB() {
    global $rm_server, $rm_user, $rm_password, $rm_db, $db_link;

    if (! isset ( $db_link )) {
        $db_link = mysqli_connect ( $rm_server, $rm_user, $rm_password, $rm_db );
        if (! $db_link) {
            die ( $rm_server . " - " . $rm_user . " - " . $rm_db . " - Keine Verbindung zur Datenbank!" );
        }
        mysqli_set_charset ( $db_link, "utf8mb4" );
        mysqli_select_db ( $db_link, $rm_db ) or die ( "Konnte DB " . $rm_db . " nicht &ouml;ffnen: " . mysqli_error ( $db_link ) );
        mysqli_query ( $db_link, "SET NAMES 'utf8mb4'" );
    }
    return (isset ( $db_link ));
}
function DBQuery($query) {
    global $db_link, $debug, $debug_msg, $dblib_debug;
    connectToDB ();

    $retval = mysqli_query ( $db_link, $query );
    if ($debug && $dblib_debug) {
        $debug_msg .= "<br /><pre>DBQuery({$query}) = \r\n" . print_r($retval, true) . '</pre>';
        if (!$retval) {
            $debug_msg .= DBErrNo() . ' ' . DBError();
        }
    }
    return $retval;
}
function DBFetchAllArray($dbobject) {
    return mysqli_fetch_all ( $dbobject, MYSQLI_NUM );
}
function DBFetchAllAssoc($dbobject) {
    return mysqli_fetch_all ( $dbobject, MYSQLI_ASSOC );
}
function DBFetchArray($dbobject) {
    return mysqli_fetch_array ( $dbobject );
}
function DBFetchAssoc($dbobject) {
    return mysqli_fetch_assoc ( $dbobject );
}
function DBFetchRow($dbobject) {
    return mysqli_fetch_row ($dbobject );
}
function DBErrList() {
    global $db_link;
    return mysqli_error_list ( $db_link );
}
function DBErrNo() {
    global $db_link;
    return encodeHTML ( mysqli_errno ( $db_link ) );
}
function DBError() {
    global $db_link;
    return encodeHTML ( mysqli_error ( $db_link ) );
}
function DBAffectedRows() {
    global $db_link;
    return mysqli_affected_rows ( $db_link );
}
function escapeString($str) {
    global $db_link;
    if (!$db_link) {
        connectToDB();
    }
    return mysqli_real_escape_string( $db_link, $str );
}
/*
 * hash the password so we don't know it.
 */
function hashPassword($password) {
    global $debug, $debug_msg, $dblib_debug;
    $pwdHash = '$h$' . md5($password);
    if ($debug && $dblib_debug) {
        $debug_msg .= "<br /><h3>hashPassword('{$password}') = '{$pwdHash}</h3>" ;
    }
    return $pwdHash;
}

function checkPassword($login, $password) {
    global $rm_tbl_teilnehmer, $debug, $debug_msg, $dblib_debug;
    if ($debug && $dblib_debug) {
        $debug_msg .= "<br /><h3>checkPassword('{$login}', '{$password}')</h3>" ;
    }
    $pwd = hashPassword($password);
    $ergebnis = DBQuery ( "SELECT id, login, passwort FROM $rm_tbl_teilnehmer WHERE login ='{$login}' and passwort in ('{$pwd}','{$password}')" );
    if (! $ergebnis)
        die ( "Fehler bei der Passwortkontrolle: " . DBError () );
        elseif (mysqli_num_rows ( $ergebnis )) {
            $retval = DBFetchArray ( $ergebnis );
            if ($debug && $dblib_debug) {
                $debug_msg .= "<br />'checkPassword({$login}', '{$password}') = " . print_r($retval, true);
            }
            return $retval;
        }
        return false;
}

function setPasswordReset($email, $token) {
    global $rm_tbl_pwreset, $debug, $debug_msg;
    if ($debug) {
        $debug_msg .= "<br /><h3>setPasswordReset('{$email}', '{$token}')</h3>" ;
    }
    // make sure we have only 1 record using this email.
    $result = DBQuery ( "DELETE FROM {$rm_tbl_pwreset} WHERE email = '{$email}'" );
    if ($result) {
        if ($debug) {
            $debug_msg .= "<br />password reset cleaned" ;
        }
        $result = DBQuery ( "INSERT INTO {$rm_tbl_pwreset}(email, token) VALUES ('{$email}', '{$token}')" );
        if ($debug) {
            if ($result) {
                $debug_msg .= "<br />password reset saved" ;
            } else {
                $debug_msg .= "<br />password reset failed" ;
            }
        }
    }
    return $result;
}


function createParticipant($login, $pass) {
    global $db_link, $rm_tbl_teilnehmer;
    $ergebnis = DBQuery ( "INSERT INTO $rm_tbl_teilnehmer ( login, passwort) VALUES( '$login', '$pass')", $db_link );
    return mysqli_insert_id ( $db_link );
}

function getRowByKeyValuePair($rm_tbl, $fnm, $fval) {
    global $db_link;
    $ergebnis = DBQuery ( "SELECT * FROM {$rm_tbl} WHERE {$fnm}='{$fval}'", $db_link );
    if (! $ergebnis)
        die ( "Konnte Datensatz nicht holen: " . DBError () );
        return encodeHTML ( mysqli_fetch_array ( $ergebnis ) );
}

function getParticipantById($id, $returnOnFailure = false) {
    global $db_link, $rm_tbl_teilnehmer;
    $query = "SELECT * FROM {$rm_tbl_teilnehmer} WHERE id = '{$id}'";
    $ergebnis = DBQuery ( $query, $db_link );
    if (! $ergebnis)
        if ($returnOnFailure)
            return false;
            else
                die ( "Konnte die Member-Daten nicht holen: " . "'" . $query . "'\n" . mysqli_error ( $db_link ) );
                return DBFetchArray ( $ergebnis );
}

function getParticipantsFor($whereClause, $returnOnFailure = false) {
    global $db_link, $rm_tbl_teilnehmer;
    $query = html_entity_decode ( "SELECT * FROM {$rm_tbl_teilnehmer} WHERE {$whereClause};" );
    $ergebnis = DBQuery ( $query, $db_link );
    if (! $ergebnis)
        if ($returnOnFailure)
            return false;
            else
                die ( "Konnte die Member-Daten nicht holen: '{$query}'\n" . mysqli_error ( $db_link ) );
                return DBFetchAssoc ( $ergebnis );
}

/**
 * update member data with minimal query - update just those data items that have changed.
 *
 * @param object $newMemberData
 * @return boolean
 */
function saveMemberData($newMemberData) {
    global $db_link, $rm_tbl_teilnehmer;

    $flarmIdChanged = false;
    $id = $newMemberData ['id'];
    $currentMemberData = getParticipantById ( $id, true );
    $newMember = ($currentMemberData == false);

    $queryFields = "";
    foreach ( $newMemberData as $key => $newValue ) {
        if ($newMember || ($currentMemberData [$key] != $newValue)) {
            $queryFields .= "$key = '$newValue', ";
            $flarmIdChanged = (strtolower($key) == 'flarmid');
        }
    }
    if ($queryFields != "") {
        $query = "UPDATE $rm_tbl_teilnehmer SET "; // start of UPDATE query
        $query .= substr ( $queryFields, 0, strlen ( $queryFields ) - 2 ); // remove last ", " from fields
        $query .= " WHERE id = '$id'"; // add WHERE clause

        // $query = html_entity_decode(htmlentities($query, ENT_COMPAT, "UTF-8")); // this is safe since we do not have the html entities outside of the data

        if (! DBQuery ( $query )) {
            die ( "Konnte die Mitglieder-Daten nicht speichern: " . mysqli_error ( $db_link ) );
        } else {
            // if FlarmID has changed, update the aircraft file
            if ($flarmIdChanged) {
                gsTrackingWriteAircraftFile(true);
            }
            return true;
        }
    } else { // no fields updated - let's just do as if...
        return true;
    }
}

?>
