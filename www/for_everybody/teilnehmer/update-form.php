<?php

session_start();
error_reporting(E_ALL);
require_once ('inc.php');
require_once ('dblib.inc.php');
require_once ('class.form.php');
require_once ('functions.php');

$class = '';

$userDaten = array();

function getClassSelections()
{
    global $class_name;
    global $msgSelect;
    global $rm_TeamClassIndex;
    
    foreach ($class_name as $key => $value) {
        $classes[] = $value;
    }
    
    return $classes;
}

function getMemberData()
{
    global $userDaten;
    global $msgSelect;
    global $class;
    global $class_name;
    global $class_key;
	global $rm_TeamClass;
    global $rm_TeamClassName;
    
    $userDaten = getParticipantById($_SESSION['id']);
    
    // manage special cases:
    $userDaten['wk'] = strtoupper($userDaten['wk']);
    
    $class = $userDaten['klasse'];
    
    // map klasse from DB to screen naming
    if (! in_array($class, $class_key, false)) {
        $class = $msgSelect;
    } elseif ((! $rm_TeamClass) && ($class == $rm_TeamClassName)) {
 		$class = $msgSelect;				
 	} else {
        $class = $class_name[$class];
    }
}

function updateMemberData()
{
    global $class_key;
    global $class_name;
    global $rm_TeamClassIndex;
    global $msgSelect;
    
    // die Daten sitzen im Array $_POST.
    if ((! in_array($_POST['klasse'], $class_name)) || (($_POST['klasse'] == $msgSelect) && ($_POST['rmactiv'] == 'ja'))) { // if the pilot wants to fly, he better select a class!
        return false;
    }
    $userDaten['klasse'] = $class_key[$_POST['klasse']];
    
    $userDaten['id'] = $_SESSION['id'];
    $userDaten['rmactiv'] = $_POST['rmactiv'];
    $userDaten['vorname'] = $_POST['vorname'];
    $userDaten['name'] = $_POST['name'];
    $userDaten['adresse'] = $_POST['adresse'];
    $userDaten['plz'] = $_POST['plz'];
    $userDaten['wohnort'] = $_POST['wohnort'];
    $userDaten['telp'] = formattedPhone($_POST['telp']);
    $userDaten['telg'] = formattedPhone($_POST['telg']);
    $userDaten['mobp'] = formattedPhone($_POST['mobp']);
    $userDaten['mobg'] = formattedPhone($_POST['mobg']);
    $userDaten['email'] = $_POST['email'];
    $userDaten['notfallkontakt1'] = $_POST['notfallkontakt1'];
    $userDaten['gruppe'] = htmlentities($_POST['gruppe']);
    $userDaten['sportlizenz'] = $_POST['sportlizenz'];
    $userDaten['igc_rank_id'] = $_POST['igc_rank_id'];
    $userDaten['flugzeug'] = $_POST['flugzeug'];
    $userDaten['flarm'] = $_POST['flarm'];
    $userDaten['immatrikulation'] = strtoupper($_POST['immatrikulation']);
    $userDaten['indexwert'] = $_POST['indexwert'];
    $userDaten['wk'] = strtoupper($_POST['wk']);
    $userDaten['logger1'] = $_POST['logger1'];
    $userDaten['logger1id'] = $_POST['logger1id'];
    $userDaten['FlarmId'] = $_POST['FlarmId'];
    $userDaten['fsb'] = $_POST['fsb'];
    $userDaten['gift'] = $_POST['gift'];
    $userDaten['camping'] = $_POST['camping'];
    $userDaten['rueckholen'] = $_POST['rueckholen'];
    $userDaten['angaben'] = $_POST['angaben'];
    
    // Clean out team data if not flying in a team.
    if ($userDaten['klasse'] == $rm_TeamClassIndex) {
        $userDaten['teamname'] = $_POST['teamname'];
        $userDaten['pilot2'] = $_POST['pilot2'];
        $userDaten['pilot3'] = $_POST['pilot3'];
        $userDaten['pilot4'] = $_POST['pilot4'];
        $userDaten['pilot5'] = $_POST['pilot5'];
        $userDaten['foto2'] = $_POST['foto2'];
        $userDaten['foto3'] = $_POST['foto3'];
        $userDaten['foto4'] = $_POST['foto4'];
        $userDaten['foto5'] = $_POST['foto5'];
    } else {
        $userDaten['teamname'] = '';
        $userDaten['pilot2'] = '';
        $userDaten['pilot3'] = '';
        $userDaten['pilot4'] = '';
        $userDaten['pilot5'] = '';
        $userDaten['foto2'] = '';
        $userDaten['foto3'] = '';
        $userDaten['foto4'] = '';
        $userDaten['foto5'] = '';
    }
    
    return (saveMemberData($userDaten));
}

// Falls es jemand mit einem Hack fertig braechte, diese Seite zu laden und nicht mit korrektem Login daherkommt,
// dann wird er zum login.php verwiesen, wo die ganze Plausi gemacht werden muss.
if ($_SESSION['logged_in'] != 1) {
    header("Location: login.php");
    exit();
}

if (isset($_POST["aktion"])) {
    if ($_POST["aktion"] == "update") {
        
        // die Form-Informationen sind noch in der Session. Wir machen das Formular nur nochmals greifbar.
        $form = new form("update");
        // wenn alles OK gibts eine Best�tigung, und sonst Fehlermeldungen.
        if ($form->validate()) 
		{
            // Daten "einsammeln" und speichern
            if (updateMemberData()) {
                header("Location: " . $_SERVER['PHP_SELF'] . "?errormsg_0=" . urlencode("Danke! Deine Angaben sind OK und wurden gespeichert."));
            } 
			else 
                if (($_POST['klasse'] != $msgSelect) && ($_POST['rmactiv'] == 'ja')) 
				{
                    header("Location: " . $_SERVER['PHP_SELF'] . "?errormsg_0=" . urlencode("Bitte eine Klasse ausw&auml;hlen!"));
                } 
				else 
				{
                    header("Location: " . $_SERVER['PHP_SELF'] . "?errormsg_0=" . urlencode("Datenbankprobleme verhinderten die Speicherung Deiner Daten. Bitte versuche es nochmals."));
                }
        } 
		else 
		{
            header("Location: " . $_SERVER['PHP_SELF']);
        }
        exit();
    }
}

// der Einstieg: Aufbau des gesamten Formulares.
elseif (! isset($_REQUEST["aktion"])) {
    echo <<<EOT
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Teilnehmerdaten anpassen - {$rm_comp_type} Birrfeld</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/> 
		<link href="../../css/rm.css" rel="stylesheet" type="text/css"/>
	</head>
	<body>
		<div class="rm_h1">Eingabe / Anpassen Deiner Daten f&uuml;r die SM/RM/JSM</div>
		<br/>
		<div id="pfbc_content">
EOT;
    
    $auswahl_ja_nein = array(
        "nein",
        "ja"
    );
    
    getMemberData();
    
    $form = new form("update");
    $form->setAttributes(array(
        "width" => "1024",
        "labelwidth" => "128",
        "labelrightalign" => "1", // layout options 1 oder 2 Felder pro Zeile
        "jsIncludesPath" => "$rm_pfbc_jsIncludesPath",
        "map" => array( // fields per line on this form (not team_form!)
            2, // Teilnahme & Klasse
                // Pilot
            2, // Vorname, Name
            1, // Adresse
            2, // PLZ, Wohnort
            2, // T-P, -G
            2, // H-P, -G
            2, // e-Mail, FSB
            2, // Gruppe, Notfallkontakt
            2, // Sportlizenz, IGC Rank ID
                // Flugzeug
            2, // Flz-Typ, Index
            2, // Immatrikulation, WBK
            2, // Logger-Typ, -ID
                // Flarm
            2, // Flarm, -ID
                // hier liegt das Team-Formular
                // Verschiedenes
            1, // T-Shirts
            2, // Campieren, Rueckholer
            1  // Infos
        )
    ) 

    );
    
    if (! empty($_GET["errormsg_0"]))
        $form->errorMsg = filter_var(stripslashes($_GET["errormsg_0"]), FILTER_SANITIZE_SPECIAL_CHARS);
    
    $form->addHidden("aktion", "update");
    
    $form->openFieldset("Teilnahme");
    $form->addSelect("Teilnahme:", "rmactiv", $userDaten['rmactiv'], $auswahl_ja_nein, array(
        "required" => "1"
    ));
    
    if ($rm_TeamClass) {
        $form->addSelect("Klasse:", "klasse", $class, getClassSelections(), array(
            "required" => "1",
            "onblur" => "toggleTeam(this.value);"
        ));
    } else {
        $form->addSelect("Klasse:", "klasse", $class, getClassSelections(), array(
            "required" => "1"
        ));
    }
    $form->closeFieldset();
    
    $form->openFieldset("Pilot");
    $form->addTextbox("Vorname:", "vorname", $userDaten['vorname'], array(
        "required" => 1
    ));
    $form->addTextbox("Name:", "name", $userDaten['name'], array(
        "required" => 1
    ));
    $form->addTextbox("Adresse:", "adresse", $userDaten['adresse'], array(
        "required" => 1,
        "tooltip" => "Strasse und Nummer"
    ));
    $form->addTextbox("PLZ:", "plz", $userDaten['plz'], array(
        "required" => 1
    ));
    $form->addTextbox("Wohnort:", "wohnort", $userDaten['wohnort'], array(
        "required" => 1
    ));
    $form->addTextbox("Telefon P:", "telp", $userDaten['telp'], array(
        "tooltip" => "Format: +41 12 345 67 89"
    ));
    $form->addTextbox("Telefon G:", "telg", $userDaten['telg'], array(
        "tooltip" => "Format: +41 12 345 67 89"
    ));
    $form->addTextbox("Handy P:", "mobp", $userDaten['mobp'], array(
        "required" => 1,
        "tooltip" => "Format: +41 12 345 67 89"
    ));
    $form->addTextbox("Handy G:", "mobg", $userDaten['mobg'], array(
        "tooltip" => "Format: +41 12 345 67 89"
    ));
    $form->addTextbox("e-Mail:", "email", $userDaten['email'], array(
        "required" => 1
    ));
    $form->addTextbox("FSB#:", "fsb", $userDaten['fsb'], array(
        "required" => 1,
        "tooltip" => "Deine Kunden-Nummer der FSB, falls vorhanden."
    ));
    $form->addSelect("Gruppe:", 'gruppe', $userDaten['gruppe'], auswahlVereine(), array(
        "required" => 1,
        "tooltip" => "Suche deine Fluggruppe"
    ));
    $form->addTextbox("Notfallkontakt:", "notfallkontakt1", $userDaten['notfallkontakt1'], array(
        "required" => 1,
        "tooltip" => "Name, Telefonnummer, Adresse"
    ));
    $form->addTextbox("Sportlizenz:", "sportlizenz", $userDaten['sportlizenz'], array(
        "tooltip" => "CH-Piloten: 5-stellige Zahl, siehe R&uuml;ckseite AeCS Member Card"
    ));
    $form->addTextbox("IGC Rank ID:", "igc_rank_id", $userDaten['igc_rank_id'], array(
        "tooltip" => 'von <a target="_blank" href="https://igcr.fai.org/">IGC Ranking Site</a>'
    ));
    $form->closeFieldset();
    
    $form->openFieldset("Flugzeug");
    $form->addTextbox("Flugzeug-Typ:", "flugzeug", $userDaten['flugzeug'], array(
        "required" => 1
    ));
    $form->addTextbox("Index:", "indexwert", $userDaten['indexwert'], array(
        "required" => 1
    ));
    $form->addTextbox("Immatrikulation:", "immatrikulation", $userDaten['immatrikulation'], array(
        "required" => 1
    ));
    $form->addTextbox("Wettbew.-Zeichen:", "wk", $userDaten['wk'], array(
        "required" => 1,
    ));
    $form->addTextbox("Logger-Typ:", "logger1", $userDaten['logger1'], array(
        "required" => 1,
    ));
    $form->addTextbox("Logger ID:", "logger1id", $userDaten['logger1id'], array(
        "required" => 1,
        "tooltip" => "Die Buchstaben 4-7 des Dateinamens: <br />Bei 14uzd641.igc ist das zd64.<br />Noch nicht fix --> XXXX - Danke!<br />mehrere Logger mit Komma (,) trennen."
    ));
    $form->closeFieldset();
    
    $form->openFieldset("Flarm");
    $form->addSelect("Flarm eingebaut?", "flarm", $userDaten['flarm'], $auswahl_ja_nein, array(
        "required" => 1,
        "tooltip" => "Flarm wird f&uuml;r's live tracking genutzt."
    ));
    $form->addTextbox("Flarm Id:", "FlarmId", $userDaten['FlarmId'], array(
        "required" => 1,
        "tooltip" => "Die ICAO Aircraft Adress oder ein anderer 6-stelliger Hex Code (0-9 und A-F als Zeichen)"
    ));
    $form->closeFieldset();
    
    if ($rm_TeamClass) {
        // Team defined as subform as per the example of php-form-builder-class.
        
        $team_form = new form('Team');
        $team_form->setAttributes(
            array(
                "width" => '1024',
                "labelWidth" => '128',
                "labelRightAlign" => '1', // layout options
                "jsIncludesPath" => "$rm_pfbc_jsIncludesPath",
                "map" => array(
                    1, // Team-Name
                    2, // Pilot 2&3
                    2  // Pilot 4&5
                )
            )
        );
        
        $team_form->openFieldset('Team');
        $team_form->addTextbox('Team-Name:', "teamname", $userDaten['teamname'], array("required" => 1)); // a team has to have a name ...
        $team_form->addTextbox("Pilot 2:", "pilot2", $userDaten['pilot2'], array("required" => 1));       // and consists of at least two pilots
        $team_form->addTextbox("Pilot 3:", "pilot3", $userDaten['pilot3']);
        $team_form->addTextbox("Pilot 4:", "pilot4", $userDaten['pilot4']);
        $team_form->addTextbox("Pilot 5:", "pilot5", $userDaten['pilot5']);
        $team_form->closeFieldset();
        
        // this element links the team into the form.
        $form->addHTML("<div id=\"team_section\" style=\"display: none;\">" . $team_form->elementsToString() . "</div>");
    }
    
    $form->openFieldset("Verschiedenes");
    $form->addTextbox("T-Shirts:", "gift", $userDaten['gift'], array("tooltip" => "Anzahl (1 pro Pilot)<br />Gr&ouml;sse (S,M,L,XL)"));
    $form->addSelect("M&ouml;chte Campieren:", "camping", $userDaten['camping'], $auswahl_ja_nein, array("required" => 1));
    $form->addSelect("Brauche R&uuml;ckholer:", "rueckholen", $userDaten['rueckholen'], $auswahl_ja_nein, array("required" => 1));
    $form->addTextarea("Angaben zu Dir:", 'angaben', $userDaten['angaben'], 
        array("tooltip" => "Deine Erfolgsgeschichten, Segelflieger-Karriere, Heldengeschichten, Motivation, Statements - was immer Dir wichtig ist!"));
    $form->closeFieldset();
    
    $form->addButton("Zur&uuml;ck", "button", array("onclick" => "self.location.href='anmeld-start.php'"));
//    $form->addButton("Teilnehmerliste", "button", array("onclick" => "self.location.href='teilnehmerlist.php'"));
    $form->addButton("Speichern", "submit");
    
    // this binds the subform Team into the form.
    if ($rm_TeamClass) {
        $form->bind($team_form, "document.getElementById('update').klasse == '{$rm_TeamClassname}'", "\$_POST['klasse'] == '" . $rm_TeamClassname . "'");
    }
    
    $form->render();
}

// Funktion, die den Teamklasse-Block ein- und ausblendet.
if ($rm_TeamClass) {
	
echo <<<EOT
<script type="text/javascript">
    function toggleTeam(klasse) {
        if(klasse != "{$rm_TeamClassname}") {
            document.getElementById("team_section").style.display = "none";
        } else {
            document.getElementById("team_section").style.display = "block";
        }
    }
    toggleTeam(document.getElementById("update").klasse.value);
</script>

		</div>
EOT;
};
?>
		<div class=rm_end></div>

<?php
include '../sponsoren/sponsorenblock.txt';
?>

</div>

</body>
</html>

