function closeWindowAfter(seconds) {
    window.setTimeout("window.close()", 1000 * seconds);
}
function redirect(url, seconds) {
    window.setTimeout(function(url){
        window.location.href = url
    }, 1000 * seconds);
}
function setMessage(msgStr, msgStyle) {
    document.getElementById("rm_status").setAttribute("innerHTML", msgStr);
    document.getElementById("rm_status").setAttribute("class", msgStyle);
}
function clearMessage() {
    document.getElementById("rm_status").innerHTML = "";
    document.getElementById("rm_status").setAttribute("class", "");
}
function clearMessageAfter(seconds) {
    setTimeout("clearMessage();", 1000 * seconds);
}