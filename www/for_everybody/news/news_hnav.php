<?php
// header('Content-Type: text/html; charset=UTF-8');
require_once ('inc.php');
require_once ('functions.php');


$newsDates = new NewsDates();
$renderedHTML = <<<EOT
			<div class="hnav_item"><h2>News</h2></div>

EOT;

foreach ($newsDates->list as $newsDate) {
    $renderedHTML .= <<<EOT
    		<div class="hnav_item"><a href=news.php?datum={$newsDate['datum']}>{$newsDate['titel']}</a></div>

EOT;
}

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('News hnav', 'hnav', $renderedHTML);

?>
