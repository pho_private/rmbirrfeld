<?php
require_once ('inc.php');
require_once ('functions.php');

$menuEntries = $rm_dates;

$renderedHTML = <<<EOT
            <div class="hnav_item"><h2>IGC Downloads</h2></div>

EOT;

foreach ($menuEntries as $menuEntry) {

    $formattedDate = getWeekdayDateString($menuEntry, true);
    $renderedHTML .= <<<EOT
			<div class="hnav_item"><a href="igc_download.php?date={$menuEntry}">{$formattedDate}</a></div>

EOT;
}

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPage('Auswertung - IGC Downloads', 'hnav', $renderedHTML);

?>
