<?php
session_start();
error_reporting(E_ALL);

require_once ('inc.php');
require_once ('dblib.inc.php');

$msgHTML = '';
$login = '';
$passwort = '';
$debugmsg = '';

if (isset($_POST['logged_in']) && $_SESSION['logged_in']) {
    $msgHTML = 'Du bist bereits eingeloggt!';
}

$action = getPostOrInit('aktion');
if ($action == 'login') {
    $login = trim($_POST['login']);
    $passwort = trim($_POST['passwort']);
    
    if (empty($login) || empty($passwort)) {
        $msgHTML = 'Du musst alle Felder ausf&uuml;llen.<br />';
    } else {
        $teilnehmer = checkPassword($login, $passwort);
        
        if (! $teilnehmer) {
            $msgHTML = 'Falsches Login/Passwort. Versuche es nochmals!<br />';
        }
        
        if ($msgHTML == '') { // keine Fehler
            initSession($teilnehmer['id'], $login, $passwort);
            header("Location: anmeld-start.php");
            exit();
        }
    }
}
$msgHTML = "";
if ($msgHTML != "") {
    $msgHTML = "			<b class=\"nachricht\">$$msgHTML</b>";
}
if ($debug && ($debugmsg != '')) {
    $msgHTML .= <<<EOT
            <br />
			<b class="nachricht">DEBUG: $debugmsg</b>";
EOT;
    
}

$renderedHTML = <<<EOT
		<div class=rm_h1>Login zum Update der Pilotendaten {$rm_name_lang}</div>
		<div class=big_col_block><br />
{$msgHTML}
			<form method="POST" name="join" action="{$_SERVER ['SCRIPT_NAME']}">
			
				<input type="hidden" name="aktion" value="login"> <input type="hidden" name="{session_name()}" value="{session_id()}">
				
				<table class="rm_no_borders" width="100%">
					<colgroup>
						<col width="20%">
						<col width="80%">
					</colgroup>
					<tr>
						<td class="td_1_r">Login:&nbsp;&nbsp;&nbsp;<img src="../../resources/graphics/icons/mem.gif"></td>
						<td class="td_1_l"><input type="text" name="login" value="{$login}"></td>
					</tr>
					<tr>
						<td class="td_1_r">Passwort:&nbsp;&nbsp;&nbsp;<img src="../../resources/graphics/icons/key.gif"></td>
						<td class="td_1_l"><input type="password" name="passwort" value="{$passwort}"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="td_1_l"><input type="submit" value="Login">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Passwort vergessen?&nbsp;<input type="button" value="Passwort neu setzen" onClick="self.location.href='pwchangemail.php'"></td>
					</tr>
				</table>
			</form>
		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Anmeldung', 'page', $renderedHTML);

?>
