<?php
require_once ('inc.php');
$renderedHTML = <<<EOT
	<div class=rm_h1>Festwirtschaft</div>
	<div class=big_block>
		<div class=rm_h2>Herzlich willkommen</div>
		<div class=rm_text>
			Das Team der Festwirtschaft an der {$rm_comp_type} Birrfeld freut sich auf deinen Besuch. Es wird an nichts mangeln.
		</div>
		
		<div class=rm_text>
		<table>
			<tr>
EOT;

switch ($rm_ausrichter) {
    case "AFG" :
        $renderedHTML .= <<<EOT
<!--
				<th class=td_1><b>Restauration</b><br />Paul Keller</th>
				<th class=td_1><b>Restauration</b><br />Walter Spatny</th>
				<th class=td_1><b>Restauration</b><br />Ruedi Bieri</th>
				<th class=td_1><b>Restauration</b><br />Ernst Kneuss</th>
				<th class=td_1><b>K&uuml;che</b><br />Kathrin Keller </th>
				<th class=td_1><b>K&uuml;che</b><br />Marlen Spatny</th>
				<th class=td_1><b>K&uuml;che</b><br />Katharina Schafroth </th>
-->
			</tr>
			<tr>
<!--
				<th class=td_1><img src="../../resources/photos/organisation/pak.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Walter_Spatny.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Ruedi_Bieri.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Ernst_Kneuss.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Kathrin_Keller.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Marlen_Spatny.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Katharina_Schafroth.jpg" height="150px;" border="0" /></th>
-->
			</tr>
		</table>
		</div>
		
		
		<div class=rm_h2>&Ouml;ffnungszeiten</div>
		<div class=rm_text>
			Die Festwirtschaft ist an den Wettbewerbsdaten von 08:00 bis 23:00 Uhr ge&ouml;ffnet.
		</div>
	
<!-- 		
		<div class=rm_h2>Fr&uuml;hst&uuml;ck</div>
		<div class=rm_text>
			Ab 08:00 Uhr offerieren wir ein Fr&uuml;hst&uuml;ck oder auch nur Kafi und Gipfeli.
		</div>

		<div class=rm_h2>S&uuml;ssigkeiten</div>
		<div class=rm_text>
			Nuss- und Mandelgipfel, Hausgemachte Kuchen, Torten und W&auml;hen, Eiscreme und vieles mehr.
		</div>

		<div class=rm_h2>Jederzeit</div>
		<div class=rm_text>
			W&uuml;rste, Steaks, Poulet-Schenkeli, -fl&uuml;geli und -spiessli, Pommes, sowie Schinken-, Salami und K&auml;sesandwiches und Salat vom Buffet.
		</div>
		
		<div class=rm_h2>Getr&auml;nke</div>
		<div class=rm_text>
			Die &uuml;blichen ges&uuml;ssten und unges&uuml;ssten Mineralwasser in 0.5- und 1.5-Liter Pet-Flaschen. Kein Offenausschank.<br />
			Zapfbier in 0.3- und 0.5-Litergl&auml;sern.<br />
			Kaffee frisch gemalen.
		</div>
		<div class=rm_text>
			Verschiedene Weiss- und Rotweine. Kein Offenausschank.
		</div>
		
		<div class=rm_h2>Die Tagesmenus</div>
		<table>
			<tr><td class=th_1>Datum</td><td class=th_1>Mittag</td><td class=th_1>Abend</td></tr>
			<tr><td class=th_1>Sa. 11. Mai</td><td class=td_1 colspan=2>tbd</td></tr>
			<tr><td class=th_1>So. 12. Mai</td><td class=td_1 colspan=2>tbd</td></tr>
			<tr><td class=th_1>Sa. 18. Mai</td><td class=td_1 colspan=2>tbd</td></tr>
			<tr><td class=th_1>So. 19. Mai</td><td class=td_1 colspan=2>tbd</td></tr>
		</table>
	
		<div class=rm_h2>Die Bar</div>
		<div class=rm_text>
			Ab etwa 20:00 Uhr ist die Bar ge&ouml;ffnet. StudentInnen servieren eigene exotische Kreationen mit und ohne Alkohol.
		</div>
-->
EOT;
        break;
    case "SFB" :
        $renderedHTML .= <<<EOT
<!--
				<th class=td_1><b>Restauration</b><br />Paul Keller</th>
				<th class=td_1><b>Restauration</b><br />Walter Spatny</th>
				<th class=td_1><b>Restauration</b><br />Ruedi Bieri</th>
				<th class=td_1><b>Restauration</b><br />Ernst Kneuss</th>
				<th class=td_1><b>K&uuml;che</b><br />Kathrin Keller </th>
				<th class=td_1><b>K&uuml;che</b><br />Marlen Spatny</th>
				<th class=td_1><b>K&uuml;che</b><br />Katharina Schafroth </th>
-->
			</tr>
			<tr>
<!--
				<th class=td_1><img src="../../resources/photos/organisation/pak.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Walter_Spatny.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Ruedi_Bieri.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Ernst_Kneuss.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Kathrin_Keller.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Marlen_Spatny.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Katharina_Schafroth.jpg" height="150px;" border="0" /></th>
-->
			</tr>
		</table>
		</div>
		
		
		<div class=rm_h2>&Ouml;ffnungszeiten</div>
		<div class=rm_text>
			Die Festwirtschaft ist an den Wettbewerbsdaten von 08:00 bis 23:00 Uhr ge&ouml;ffnet.
		</div>
	
<!-- 		
		<div class=rm_h2>Fr&uuml;hst&uuml;ck</div>
		<div class=rm_text>
			Ab 08:00 Uhr offerieren wir ein Fr&uuml;hst&uuml;ck oder auch nur Kafi und Gipfeli.
		</div>

		<div class=rm_h2>S&uuml;ssigkeiten</div>
		<div class=rm_text>
			Nuss- und Mandelgipfel, Hausgemachte Kuchen, Torten und W&auml;hen, Eiscreme und vieles mehr.
		</div>

		<div class=rm_h2>Jederzeit</div>
		<div class=rm_text>
			W&uuml;rste, Steaks, Poulet-Schenkeli, -fl&uuml;geli und -spiessli, Pommes, sowie Schinken-, Salami und K&auml;sesandwiches und Salat vom Buffet.
		</div>
		
		<div class=rm_h2>Getr&auml;nke</div>
		<div class=rm_text>
			Die &uuml;blichen ges&uuml;ssten und unges&uuml;ssten Mineralwasser in 0.5- und 1.5-Liter Pet-Flaschen. Kein Offenausschank.<br />
			Zapfbier in 0.3- und 0.5-Litergl&auml;sern.<br />
			Kaffee frisch gemalen.
		</div>
		<div class=rm_text>
			Verschiedene Weiss- und Rotweine. Kein Offenausschank.
		</div>
		
		<div class=rm_h2>Die Tagesmenus</div>
		<table>
			<tr><td class=th_1>Datum</td><td class=th_1>Mittag</td><td class=th_1>Abend</td></tr>
			<tr><td class=th_1>Sa. 11. Mai</td><td class=td_1 colspan=2>tbd</td></tr>
			<tr><td class=th_1>So. 12. Mai</td><td class=td_1 colspan=2>tbd</td></tr>
			<tr><td class=th_1>Sa. 18. Mai</td><td class=td_1 colspan=2>tbd</td></tr>
			<tr><td class=th_1>So. 19. Mai</td><td class=td_1 colspan=2>tbd</td></tr>
		</table>
	
		<div class=rm_h2>Die Bar</div>
		<div class=rm_text>
			Ab etwa 20:00 Uhr ist die Bar ge&ouml;ffnet. StudentInnen servieren eigene exotische Kreationen mit und ohne Alkohol.
		</div>
-->
EOT;
        break;
    case "SGL" :
        $renderedHTML .= <<<EOT
<!--
				<th class=td_1><b>Restauration</b><br />Paul Keller</th>
				<th class=td_1><b>Restauration</b><br />Walter Spatny</th>
				<th class=td_1><b>Restauration</b><br />Ruedi Bieri</th>
				<th class=td_1><b>Restauration</b><br />Ernst Kneuss</th>
				<th class=td_1><b>K&uuml;che</b><br />Kathrin Keller </th>
				<th class=td_1><b>K&uuml;che</b><br />Marlen Spatny</th>
				<th class=td_1><b>K&uuml;che</b><br />Katharina Schafroth </th>
-->
			</tr>
			<tr>
<!--
				<th class=td_1><img src="../../resources/photos/organisation/pak.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Walter_Spatny.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Ruedi_Bieri.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Ernst_Kneuss.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Kathrin_Keller.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Marlen_Spatny.jpg" height="150px;" border="0" /></th>
				<th class=td_1><img src="../../resources/photos/organisation/Katharina_Schafroth.jpg" height="150px;" border="0" /></th>
-->
			</tr>
		</table>
		</div>
		
		
		<div class=rm_h2>&Ouml;ffnungszeiten</div>
		<div class=rm_text>
			Die Festwirtschaft ist an den Wettbewerbsdaten von 08:00 bis 23:00 Uhr ge&ouml;ffnet.
		</div>
	
<!-- 		
		<div class=rm_h2>Fr&uuml;hst&uuml;ck</div>
		<div class=rm_text>
			Ab 08:00 Uhr offerieren wir ein Fr&uuml;hst&uuml;ck oder auch nur Kafi und Gipfeli.
		</div>

		<div class=rm_h2>S&uuml;ssigkeiten</div>
		<div class=rm_text>
			Nuss- und Mandelgipfel, Hausgemachte Kuchen, Torten und W&auml;hen, Eiscreme und vieles mehr.
		</div>

		<div class=rm_h2>Jederzeit</div>
		<div class=rm_text>
			W&uuml;rste, Steaks, Poulet-Schenkeli, -fl&uuml;geli und -spiessli, Pommes, sowie Schinken-, Salami und K&auml;sesandwiches und Salat vom Buffet.
		</div>
		
		<div class=rm_h2>Getr&auml;nke</div>
		<div class=rm_text>
			Die &uuml;blichen ges&uuml;ssten und unges&uuml;ssten Mineralwasser in 0.5- und 1.5-Liter Pet-Flaschen. Kein Offenausschank.<br />
			Zapfbier in 0.3- und 0.5-Litergl&auml;sern.<br />
			Kaffee frisch gemalen.
		</div>
		<div class=rm_text>
			Verschiedene Weiss- und Rotweine. Kein Offenausschank.
		</div>
		
		<div class=rm_h2>Die Tagesmenus</div>
		<table>
			<tr><td class=th_1>Datum</td><td class=th_1>Mittag</td><td class=th_1>Abend</td></tr>
			<tr><td class=th_1>Sa. 11. Mai</td><td class=td_1 colspan=2>tbd</td></tr>
			<tr><td class=th_1>So. 12. Mai</td><td class=td_1 colspan=2>tbd</td></tr>
			<tr><td class=th_1>Sa. 18. Mai</td><td class=td_1 colspan=2>tbd</td></tr>
			<tr><td class=th_1>So. 19. Mai</td><td class=td_1 colspan=2>tbd</td></tr>
		</table>
	
		<div class=rm_h2>Die Bar</div>
		<div class=rm_text>
			Ab etwa 20:00 Uhr ist die Bar ge&ouml;ffnet. StudentInnen servieren eigene exotische Kreationen mit und ohne Alkohol.
		</div>
-->
EOT;
        break;
}

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Restaurant - Einleitung', 'page', $renderedHTML);

?>
