<?php
require_once ('inc.php');

require_once ('functions.php');

$login = getRequestOrInit('login');
$index = getRequestOrInit('index');
$fsb = getRequestOrInit('fsb');
$id = getRequestOrInit('id');
$flarmid = getRequestOrInit('flarmid');
$sportlizenz = getRequestOrInit('sportlizenz');
$igc_id = getRequestOrInit('igc_id');

$teilnehmerListe = new Teilnehmer();

if ($login != '') {
	if ($index != '') {
		$teilnehmerListe->set_index_by_login($login, $index);
	}
	if ($fsb != '') {
		$teilnehmerListe->set_fsb_by_login($login, $fsb);
	}
	if ($id != '') {
		$teilnehmerListe->set_id_by_login($login, $id);
	}
	if ($flarmid != '') {
		$teilnehmerListe->set_flarmid_by_login($login, $flarmid);
	}
	if ($sportlizenz != '') {
		$teilnehmerListe->set_sportlizenz_by_login($login, $sportlizenz);
	}
	if ($igc_id != '') {
		$teilnehmerListe->set_igc_id_by_login($login, $igc_id);
	}
}

$renderedHTML = <<<EOT
		<div class=rm_h1>Verwaltung FSB-, Logger- und Flarm ID sowie Index</div>
		<div class=big_col_block>
EOT;

foreach ($class_key as $class) {
	if ($class != 0) {
		$renderedHTML .= <<<EOT
			<table bgcolor={$class_bgcolor[$class]} border=0>
				<colgroup>
					<col width="40px">
					<col width="100px">
					<col width="70px">
					<col width="80px">
					<col width="40px">
					<col width="80px">
					<col width="80px">
					<col width="80px">
					<col width="60px">                    
					<col width="60px">                    
					<col width="60px">                    
				</colgroup>
                <thead>
    				<tr><th colspan=9 text-align="left">$class_name[$class]</th></tr>
    				<tr>
    					<th class=th_1_c>FSB</td>
    					<th class=th_1  >Teilnehmer</td>
    					<th class=th_1_c>Immatr.</td>
    					<th class=th_1_c>Flz</td>
    					<th class=th_1_c>WK</td>
    					<th class=th_1_c>Logger</td>
    					<th class=th_1_c>Flarm</td>
    					<th class=th_1_c>Index</td>
    					<th class=th_1_c>Sportlizenz</td>
    					<th class=th_1_c>IGC Rank ID</td>
    					<th class=th_1_c>&nbsp;</td>
    				</tr>
                </thead>
                <tbody>
EOT;
		
		$teilnehmerListe->load_class_by_wk($class);
		foreach ($teilnehmerListe->list as $teilnehmer) {
			$pilot 					= $teilnehmer['vorname'] . " " . $teilnehmer['name'];
			$adresse 				= $teilnehmer['adresse'].", " .$teilnehmer['plz']." ".$teilnehmer['wohnort'];
			$loggerId 				= strtoupper($teilnehmer['logger1id']);
			$email		 			= $teilnehmer['email'];
			$flarmId 				= strtoupper($teilnehmer['FlarmId']);
			$fsb 					= $teilnehmer['fsb'];
			$flugz 					= $teilnehmer['flugzeug'];
			$immatrikulation 		= $teilnehmer['immatrikulation'];
			$loggerDisplayClass 	= (isLoggerIdString($loggerId)) 	? "td_1" : "td_e";
			$flarmDisplayClass 		= (isFlarmIdString($flarmId)) 		? "td_1" : "td_e";
			$fsbDisplayClass 		= ($fsb != '0') 					? "td_1" : "td_e";
			
			$renderedHTML .= <<<EOT
					<tr>
						<form method="POST" name="speichern" action="teilnehmerdaten.php">
							<td class=""{$fsbDisplayClass}_c"><input type="text" name="fsb" size="8" value="{$teilnehmer['fsb']}"/></td>
							<td class=td_1>{$pilot}</td>
							<td class=td_1_c>{$immatrikulation}</td>
							<td class=td_1_c>{$flugz}</td>
							<td class=td_1_c>{$teilnehmer['wk']}</td>
							<td class="{$loggerDisplayClass}_c"><input type="text" name="id" size="8" value="{$loggerId}"/></td>
							<td class="{$flarmDisplayClass}_c"><input type="text" name="flarmid" size="8" value="{$flarmId}"/></td>
							<td class=td_1_c><input type="text" name="index" size="8" value="{$teilnehmer['indexwert']}"/></td>
							<td class=td_1_c><input type="text" name="sportlizenz" size="8" value="{$teilnehmer['sportlizenz']}"/></td>
							<td class=td_1_c><input type="text" name="igc_id" size="8" value="{$teilnehmer['igc_rank_id']}"/></td>
							<td class=td_1_c><input type="submit" name="speichern" value="Update"/></td>
							<input type="hidden" name="login" value="{$teilnehmer['login']}"/>
						</form>
					</tr>
EOT;
		}
		
		$renderedHTML .= <<<EOT
                </tbody>
			</table>
			<hr />
EOT;
		
	}
}
$renderedHTML .= <<<EOT
		</div> <!-- big_col_block -->
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Auswertung - Verwaltung FlarmId, LoggerId und Index', 'page', $renderedHTML);

?>
