<?php
session_start();
error_reporting(E_ALL);
require_once ('dblib.inc.php');
$email = '';
$subject = "Passwort zu {$rm_url} $rm_name_lang";
$message = '';

$action = getPostOrInit('action');

if ($action == 'updatepw') {
	$email = strtolower(trim($_POST['email']));
	
	if (empty($email)) {
	    $message .= "Du musst Deine Email-Adresse eingeben.<br />\n";
	}
	elseif (! getRowByKeyValuePair($rm_tbl_teilnehmer, 'email', $email)) {
	    $message .= "<p /><span class='errormessage'>Unbekannte Email-Adresse. Bitte &uuml;berpr&uuml;fe Deine Eingabe!</span><p />";
	}
	else {
		$result = DBQuery("SELECT vorname, login FROM $rm_tbl_teilnehmer WHERE email ='$email'");
		
		$userdata = DBFetchAssoc($result);
		$token = bin2hex(random_bytes(50));
		$result = setPasswordReset($email, $token);		
		if ($result) {
		    $renderedHTML = <<<EOT
<table>
    <tr style="vertical-align: top">
        <td width="100px">
            <img src="{$rm_resources_url}/graphics/organisation/$rm_logo_file" width="80px">
        </td>
        <td>
            Hallo {$userdata['vorname']}!<p />
            <p />
            Dein Login f&uuml;r <a href="{$rm_url}">{$rm_name_lang}</a> ist: "{$userdata['login']}".<p /> 
            Um Dein Passwort zu setzen, klicke auf diesen <a href="{$rm_root_url}/for_everybody/teilnehmer/pwsetnewpw.php?token=$token">Link</a><p />
            <p />
            <p />
            Freundliche Gr&uuml;sse<p />
            {$rm_domain}<p />
            Administrator<p />
            <p />
            <hr><p />
            HINWEIS:<p />
            Diese Email wurde automatisch erstellt.<p />
            <p />
            Solltest Du nicht nach dem Passwort gefragt haben,<br />
            hat jemand anders versucht, an Dein Passwort zu gelangen.<br />
            <p />
            Keine Angst! Deine Daten wurden nur an Dich versandt.<p />
            <p>
            <hr><p />
            <br />
        </td>
    </tr>
</table>
EOT;
    		
    		$headers = "From: {$rm_mail_noreply}\r\n" .
    		  		"Reply-To: {$rm_mail_webmaster}\r\n" .
    		  		'Content-Type: text/html; charset=UTF-8\r\n' .
    		  		'X-Mailer: PHP/' . phpversion();
    		$result = mail($email, $subject, $renderedHTML, $headers);

    		if ($debug) {
    		    $debug_msg .= <<<EOT
    		    <p>to: {$email}
    		    <p>subject: {$subject}
                <p>body: {$renderedHTML}
                <p>result: 
EOT;
    		    $debug_msg .= '<pre>' . print_r($result, true) . '</pre>';  
    		}
    		$message .= '<p /><span class="errormessage">Wir haben Dir einen Password-Reset-Link zugeschickt. &Uuml;berpr&uuml;fe jetzt den Eingang Deiner E-Mails!</span><p />';
    		$email = '';
    	}
	}
}
$renderedHTML = <<<EOT
	<table class="body" cellspacing="0" cellpadding="0">
		<tr valign="middle" align="center">
			<td>
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					<div class=rm_h1>Passwort vergessen?</div>
					<br />
					<br />
					<div class=rm_text>Gib hier <b>Deine Email-Adresse</b> ein, welche
					Du damals bei Deiner Anmeldung eingegeben hast.<br />
					Wir senden Dir dann per Mail einen Link um Dein Passwort zu &auml;ndern!<br />
					<br />
					Falls diese Adresse nicht mehr stimmt, musst Du dem Webmaster eine <a href="mailto:webmaster@{$rm_domain}?subject={$rm_comp_type} Birrfeld:%20Account-Zugriff">Mitteilung</a>
					schicken.</div>
					<br />
					<b class="nachricht">$message</b>
					<form method="POST" name="updatepw" action="{$_SERVER['SCRIPT_NAME']}">
						<input type="hidden" name="action" value="updatepw">
						<input type="hidden" name="{session_name()}" value="{session_id()}">
						
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><img src="{$rm_icon_url}/mail2.gif" width="15" height="11" border="0" alt=""> &nbsp;Deine&nbsp;eMail-Adresse:</td>
								<td><input type="text" size="50" name="email" value="{$email}"></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td><input type="submit" value="Absenden"></td>
							</tr>
						</table>
						<hr />
						
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="170px">&nbsp;</td>
								<td align=center><br />
								<br />
								<input type="button" name="goback" value="zur&uuml;ck zur Auswahl"
									onClick="self.location.href='anmeld-start.php'"></td>
							</tr>
						</table>
						
					</form>
					</td>
				</tr>
			</table>
			
			</td>
		</tr>
	</table>
EOT;



// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Passwort vergessen', 'page', $renderedHTML);

?>
