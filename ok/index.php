<?php
if (! empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
	$uri = 'https://';
} else {
	$uri = 'http://';
}
$uri .= $_SERVER['HTTP_HOST'];

$request = $_SERVER['REQUEST_URI'];

if (substr($request, - 1) != '/') {
	$request = substr($request, 0, strrpos($request, '/') + 1);
}
$uri .= $request;

header('Location: ' . $uri . '../www/for_admin/frame_ok.php');
exit();

?>
