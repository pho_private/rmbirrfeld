<?php
require_once ('inc.php');
$renderedHTML = <<<EOT
	<div class=rm_h1>Landungen in Deutschland</div>
	<div class=big_col_block>
		<div class=rm_h2>Vorgehen</div>
		<div class=rm_text>
		Die Zollbeh&ouml;rden sind &uuml;ber Landungen in Deutschland unverz&uuml;glich zu unterrichten. </b>
		<br/><br/>
		F&uuml;r uns sind die Hauptzoll&auml;mter Karlsruhe, L&ouml;rrach, Singen und Ulm zust&auml;ndig. <br />
        Im Falle einer Aussenladung ruft bitte entsprechend dem Landeort eine der folgenden Nummern an und meldet Eure Landung:
		<br/><br/>

		<table>
			<tr><th class="th_1">Karlsruhe</th><td class="td_1">+49 7213 710 311</td><td class="td_1"></td><tr>
			<tr><th class="th_1">L&ouml;rrach</th><td class="td_1">+49 7621 6869 1600</td><td class="td_1"></td><tr>
			<tr><th class="th_1">Singen/Waldshut</th><td class="td_1">+49 7751 873 60</td><td class="td_1"></td><tr>
			<tr><th class="th_1">Singen/Konstanz</th><td class="td_1">+49 7531 124 260</td><td class="td_1"></td><tr>
			<tr><th class="th_1">Ulm</th><td class="td_1">+49 7319 6481 407</td><td class="td_1">oder</td><tr>
			<tr><th class="th_1">Ulm</th><td class="td_1">+49 7319 6481 440</td><td class="td_1"></td><tr>
			<tr><th class="th_1">Ulm</th><td class="td_1">+49 7541 284 625</td><td class="td_1">Ausserhalb der B&uuml;rozeiten</td><tr>
		</table>
		<br/><img border="0" src="../../resources/graphics/organisation/hza_map.png" width="500"><br/>
 		</div>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('Aussenlandungen in Deutschland', 'page', $renderedHTML);

?>
