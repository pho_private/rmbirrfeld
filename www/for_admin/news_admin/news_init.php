<?php
require_once ('inc.php');
require_once ('functions.php');


$id = getRequestOrInit('id', '');
$datum = getRequestOrInit('datum', '');
$zeit = getRequestOrInit('zeit', '');
$titel = getRequestOrInit('titel', '');
$text = getRequestOrInit('text', '');
$action = getRequestOrInit('action', '');

/*
INSERT INTO `news`(`datum`, `titel`, `released`) VALUES 
    ('$rm_new_year','Vorfeld','on'),
*/
$newsInitQuery = <<<EOT
    INSERT INTO `news`(`datum`, `titel`, `released`) VALUES 
    ('$rm_new_year','Vorfeld','on'),
EOT;
$i = 0;
foreach ($day in $rm_dates) {
    $newsDate = date(strtotime($date));
    $newsDataEntry = date('d. M', $newsDate);
    $newsQuery .= <<<EOT
        ('$day', '$newsDate', 'on'),
EOT;
    $i += 1;
}
$newsInitQuery = rtrim($newsInitQuery, ',') . ";";

$retval = DBQuery($newsInitQuery);

$renderedHTML = <<<EOT
        <br />
        <div class=\"rm_h1\">News Daten setzen</div>
EOT;

 
// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayPageAndSponsors('OK - News verwalten', 'page', $renderedHTML);

?>


