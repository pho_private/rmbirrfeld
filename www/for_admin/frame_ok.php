<?php
require_once ('inc.php');
require_once ('functions.php');


// --- prepare for news ---


$hnav_news = 'news_admin/news_admin_hnav.php';
$datum = NULL;
$hnav = getRequestOrInit('hnav', $hnav_news);
$datum = getRequestOrInit('datum', $datum);
$page = getRequestOrInit('page', "news_admin/news.php?datum={$datum}");

// avoid most of the calculations if not in news module.
if ($hnav == $hnav_news) {
	$newsItem = new NewsItems();
	$lastNewsDate = $newsItem->getLastNewsEntryDate();
	if ($lastNewsDate) {
	    if ($lastNewsDate < $rm_dates[0]) {
	        $lastNewsDate = $rm_new_year;
	    }
	    $newspage = "news.php?datum=" . $lastNewsDate;
	} else {
	    $newspage = "news.php?datum=" . $rm_new_year;
	}
}


$frameset = <<<EOT
<frameset cols="125,*" framespacing="2" frameborder="0">
	<frame name="vnav" noresize src="vnav_ok.php" marginwidth="2" marginheight="2" scrolling="auto" />
    <frameset rows="90,*" framespacing="2" frameborder="0">
    	<frame name="hnav"  marginwidth="5" marginheight="2" scrolling="no"   src="{$hnav}" noresize>
    	<frame name="page" marginwidth="5" marginheight="2" scrolling="auto" src="{$page}" noresize>
    </frameset>
</frameset>
EOT;

// ------------------------------------------------------------------------------------------------
// Common HTML Output
// ------------------------------------------------------------------------------------------------

rm_displayFrameset("ADMIN - {$rm_name_lang}", $frameset);
?>

