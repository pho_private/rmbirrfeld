<?php
require_once 'inc.php';

function encodeToUtf8(string $string) {
    return mb_convert_encoding($string, "UTF-8", mb_detect_encoding($string, "UTF-8, ISO-8859-1, ISO-8859-15", true));
}

function encodeToIso(string $string) {
    return mb_convert_encoding($string, "ISO-8859-1", mb_detect_encoding($string, "UTF-8, ISO-8859-1, ISO-8859-15", true));
}

function encodeHTML($myVar) {
    // print ("<br />encodeHTML('$myVar') = ");
    if (is_string($myVar)) {
        $myVar = encodeToUtf8(htmlentities($myVar));
        // print("'$myVar'");
    } elseif (is_array($myVar)) {
        foreach ($myVar as $key => $value) {
            $myVar[$key] = encodeHTML($value);
        }
    }
    return $myVar;
}

/**
 * recursively convert all string values in an array to contain native characters instead of characterset safe HTML entities like &auml;.
 *
 * @param array $myArray
 * @return array
 */
function decodeHTML($myVar) {
    // print ("<br />decodeHTML('$myVar') = ");
    if (is_string($myVar)) {
        $myVar = html_entity_decode($myVar);
        // print("'$myVar'");
    } elseif (is_array($myVar)) {
        foreach ($myVar as $key => $value) {
            $myVar[$key] = decodeHTML($value);
        }
    }
    return $myVar;
}

/**
 * return specified files in specified directory (just files)
 *
 * @param string $haystack
 * @param string $needle
 * @return array
 */
function filelist($haystack, $needle = '/./') {
    global $debug_msg;
    $files = array();
    try {
        foreach (new DirectoryIterator($haystack) as $file) {
            if ($file->isDir())
                continue;

            if (preg_match($needle, $file->getFilename())) {
                $files[] = $file->getFilename();
            }
        }
    } catch (UnexpectedValueException $e) {
        $debug_msg .= "Exception " . $e->getCode() . ":" . $e->getMessage() . "\nin " . $e->getFile() . " on line " . $e->getLine();
    }
    reset($files);
    return $files;
}

/**
 * recursively returns directories containing specified files under start directory
 *
 * @param string $startdir
 * @param string $lookfor
 * @return array
 */
function folderlist($startdir, $lookfor = '', $folders = array()) {
    // print("<div style=\"background-color:yellow;\">folderlist( startdir = " .$startdir . ", lookfor = {$lookfor});</div>");
    $currentdir = getcwd();
    if ($startdir) {
        chdir($startdir);
    }
    $d = opendir(".");
    $hits = 0;
    $file = readdir($d);
    while ($file) {

        if (is_dir($file)) {
            if ($file == ".." || $file == ".")
                continue;
            $folders = folderlist($file, $lookfor, $folders);
        }
        // print("<div style=\"background-color:yellow;\">preg_match( lookfor = {$lookfor}, file = {$file});</div>");

        if (preg_match($lookfor, $file))
            $hits ++;

        $file = readdir($d);
    }
    if ($hits > 0)
        $folders[] = getcwd();
    closedir($d);
    chdir($currentdir);
    return $folders;
}

/**
 * return list of directories under start directory
 *
 * @param string $startdir
 * @return array
 */
function foldersInFolder($startDir) {
    $folders = foldersInFolderContaining($startDir);
    if ($folders) {
        foreach ($folders as $folder) {
            $folder = $startDir . '/' . $folder;
        }
    }
    return $folders;
}

/**
 * return list of directories under start directory containing at least one of the file(s) specified by $needle
 *
 * @param string $startdir
 * @param string $needle
 *            - file(s) required
 * @return array $folders - directories (w/o path)
 */
function foldersInFolderContaining($haystack, $needle = NULL) {
    global $debug, $debug_msg;
    if ($debug) {
        $debug_msg .= "<br />foldersInFolderContaining(startdir={$haystack}, needle={$needle});";
    }
    $folders = array();

    try {
        foreach (new DirectoryIterator($haystack) as $dirCandidate) {
            if ($dirCandidate->isDot())
                continue;
            if ($dirCandidate->isDir()) {
                // got ourselves a directory, now check for desired contents.
                if (is_null($needle)) {
                    $folders[] = $dirCandidate->getPathname();
                } else {
                    foreach (new DirectoryIterator($dirCandidate->getPathname()) as $file) {
                        if ($file->isFile()) {
                            if (preg_match($needle, $file->getFilename())) {
                                $folders[] = $dirCandidate->getFilename();
                                continue;
                            }
                        }
                    }
                }
            }
        }
    } catch (UnexpectedValueException $e) {
        $debug_msg .= "<br />Exception " . $e->getCode() . ":" . $e->getMessage() . "<br >in " . $e->getFile() . " on line " . $e->getLine();
    }
    return $folders;
}

function deleteFilesLike(string $filename_scheme) {
    // delete existing files - do not check if successful or not.
    foreach( glob($filename_scheme) as $file ) {
        $status = unlink($file) ? "The file {$file} has been deleted" : "Error deleting {$file}";
    }
}

function qsort_multiarray($array, $num = 0, $order = "ASC", $left = 0, $right = - 1) {
    if ($right == - 1) {
        $right = count($array) - 1;
    }

    $links = $left;
    $rechts = $right;

    $mitte = $array[($left + $right) / 2][$num];
    if ($rechts > $links) {
        do {
            if ($order == "ASC") {
                while ($array[$links][$num] < $mitte)
                    $links ++;
                while ($array[$rechts][$num] > $mitte)
                    $rechts --;
            } else {
                while ($array[$links][$num] > $mitte)
                    $links ++;
                while ($array[$rechts][$num] < $mitte)
                    $rechts --;
            }
            if ($links <= $rechts) {
                $tmp = $array[$links];
                $array[$links ++] = $array[$rechts];
                $array[$rechts --] = $tmp;
            }
        } while ($links <= $rechts);
        if ($left < $rechts)
            $array = qsort_multiarray($array, $num, $order, $left, $rechts);
        if ($links < $right)
            $array = qsort_multiarray($array, $num, $order, $links, $right);
    }
    return $array;
}

function auswahlVereine() {
       return array(
        "w&auml;hle aus...",
        "AFG Z&uuml;rich",
        "Birrfeld",
        "SV Lenticularis",
        "Lenzburg",
        "Aktavia",
        "Veterano",
        "Albatros",
        "Olten",
        "Alpin",
        "Bad Ragaz",
        "Basel Fricktal",
        "Bern",
        "Bex les Martinets",
        "Biel",
        "Bleienbach",
        "Courtelary",
        "Cumulus",
        "Dittingen",
        "Fribourg",
        "Friedberg-Amlikon",
        "GVV Genevois",
        "Genevois de Montricher",
        "Glarnerland",
        "Grenchen",
        "Gruy &egrave;re",
        "Knonaueramt",
        "L&auml;gern",
        "Leventina",
        "Montagnes Neuch&acirc;teloises",
        "M&ouml;ve",
        "Muottas",
        "Neuch&acirc;tel GVV",
        "Nidwalden",
        "Oberaargau",
        "Oberwallis",
        "Obwalden",
        "OCS Sch&auml;nis",
        "Oldtimer R&auml;tikon",
        "Oldtimer Segelflug-Vereinigung OSV",
        "Pilatus-Luzern",
        "PowerGliders",
        "Randen",
        "Roche",
        "SAFE S&auml;ntis Aerob. Fox Enthusiasts",
        "SAGA Swiss Aerobatic Gliding Assoc.",
        "S&auml;ntis",
        "Sch&auml;nis",
        "Schaffhausen",
        "Skylark",
        "SG Solothurn",
        "Solothurn Sport",
        "Sud Alpin",
        "Thermik",
        "Thun",
        "Ticino",
        "Valais",
        "Vaudois de Montricher",
        "Verein Schweizer Segelfliegerinnen",
        "Winterthur",
        "Yverdon",
        "Z&uuml;rich",
        "Z&uuml;rcher Oberland",
        "Zweisimmen",
        "keine CH-Gruppe",
        "Belgique",
        "Czech Republic",
        "Deutschland",
        "France",
        "Italia",
        "Luxembourg",
        "&Ouml;sterreich",
        "other"
    );
}

function ausgabeHelferVereinOptionen($gruppe) {
    $select = "<select name=\"gruppe\" size=\"1\">";
    foreach (auswahlVereine() as $value) {
        $select .= "<option name=\"gruppe\" value=\"$value\"";
        if (htmlentities(encodeToIso($gruppe)) == $value) {
            $select .= " SELECTED";
        }
        $select .= ">$value</option>\n";
    }
    $select .= "</select>";
    return $select;
}

function formattedPhone($phone) {
    $phone = str_replace(" ", "", $phone);
    $phone = str_replace(",", "", $phone);
    $phone = str_replace("/", "", $phone);
    $phone = str_replace("(", "", $phone);
    $phone = str_replace(")", "", $phone);
    $phone = str_replace("-", "", $phone);
    if ($phone != '' && substr($phone, 0, 1) != '+')
        $phone = "+41" . $phone;
    $phone = str_replace("+410", "+41", $phone);
    $phone = substr($phone, 0, 3) . ' ' . substr($phone, 3, 2) . ' ' . substr($phone, 5, 3) . ' ' . substr($phone, 8, 4);
    return $phone;
}

/**
 * Date string including special weekdays like good friday, easter monday, ascension, pentecost monday.
 * All dates are datetimes with time as midnight of the day. Thus, daylight savings does make a difference since it
 * does change on the last weekend in March.
 * change
 *
 * @param
 *            int datetime $new_int_date
 * @return string
 */
function getWeekdayDateString(string $curDateString, bool $short = false) {
    $wochentage = array(
        "Sonntag",
        "Montag",
        "Dienstag",
        "Mittwoch",
        "Donnerstag",
        "Freitag",
        "Samstag",
        "Karfreitag",
        "Ostern",
        "Ostermontag",
        "Auffahrt",
        "Pfingsten",
        "Pfingstmontag"
    );
    $monate = array(
        "Januar",
        "Februar",
        "M&auml;rz",
        "April",
        "Mai",
        "Juni",
        "Juli",
        "August",
        "September",
        "Oktober",
        "November",
        "Dezember"
    );
    
    $curDate = strtotime($curDateString);
    $curDayInYear = date("z", $curDate);
    $this_year = date("Y", $curDate);
    $this_month = date("n", $curDate);
    $this_month_string = $monate[$this_month - 1];
    $easter_int = date("z", easter_date($this_year));

    switch (($curDayInYear - $easter_int)) {
        case 50:
            $compDay = 12; // Pfingstmontag
            break;
        case 49:
            $compDay = 11; // Pfingsten
            break;
        case 39:
            $compDay = 10; // Auffahrt
            break;
        case 1:
            $compDay = 9; // Ostermontag
            break;
        case 0:
            $compDay = 8; // Ostern
            break;
        case -2:
            $compDay = 7; // Karfreitag
            break;
        default: // just leave as is.
            $compDay = date("w", $curDate);
            break;
    }
    $today = date("j", $curDate) . ". {$this_month_string} {$this_year}";
    $wd = $wochentage[$compDay];
    // make short day name on normal weekdays
    if ($short) {
        if ($compDay < 7) {
            $wd = substr($wd,0,2);
        }
    }
    return "{$wd}, {$today}";
}

// holen oder initialisieren einer $_REQUEST Variablen
function getPostOrInit($var, $initialValue = '') {
    global $debug, $debug_msg;
    if (isset($_POST[$var])) {
        $val = $_POST[$var];
    } else {
        $val = $initialValue;
    }
    $val = html_entity_decode($val);
    if ($debug) {
        $debug_msg .= '<br />$_POST[' . "'{$var}'] = '{$val}'";
    }
    return $val;
}
function getRequestOrInit($var, $initialValue = '') {
    global $debug, $debug_msg;
    if (isset($_REQUEST[$var])) {
        $val = encodeToUtf8(html_entity_decode($_REQUEST[$var]));
    } else {
        $val = $initialValue;
    }
    if ($debug) {
        $debug_msg .= '<br />$_REQUEST[' . "'{$var}'] = '{$val}'";
    }
    return $val;
}

function getRequestOrInitHTML($var, $initialValue = '') {
    global $debug, $debug_msg;
    if (isset($_REQUEST[$var])) {
        $val = $_REQUEST[$var];
    } else {
        $val = $initialValue;
    }
    $val = encodeToUtf8($val);
    if ($debug) {
        $debug_msg .= '<br />$_REQUEST[' . "'{$var}'] = '{$val}'";
    }
    return $val;
}

function getSessionOrInit($var, $initialValue = '') {
    global $debug, $debug_msg;
    if (isset($_SESSION[$var])) {
        $val = $_SESSION[$var];
    } else {
        $val = $initialValue;
    }
    if ($debug) {
        $debug_msg .= '<br />$_SESSION[' . "'{$var}'] = '{$val}'";
    }
    return $val;
}
function isLoggerIdString($loggerIdString) {
    $loggerIds = explode(',', $loggerIdString);
    foreach ($loggerIds as $loggerId) {
        if (! isLoggerId($loggerId)) {
            return false;
        }
    }
    return true;
}

// see 
// https://www.fai.org/page/igc-approved-flight-recorders
// last update: 05-2024
$igc_manufacturercodes = 'ACDEFGHIKLMNPRSTVZ';
$igc_3_letter_codes =    array('ACT', 'CAM', 'CNI', 'DSX', 'EWA', 'FIL', 'FLA', 'GCS', 'IMI', 'LGS',
		                       'LXN', 'LXV', 'NAV', 'NKL', 'NTE', 'PFE', 'RCE', 'SCH', 'SDI', 'TRI', 'ZAN');


function isLoggerId($loggerId) {
    global $igc_manufacturerCodes;
    if ((strlen($loggerId) == 4) || (strlen($loggerId) == 6)) {
        return true;
    }
}

function isFlarmIdString($flarmIdString) {
    $flarmIds = explode(',', $flarmIdString);
    foreach ($flarmIds as $flarmId) {
        if (! isFlarmId($flarmId)) {
            return false;
        }
    }
    return true;
}

function isFlarmId($flarmId) {
	return (strlen($flarmId) == 6) && ctype_xdigit($flarmId);
}

function printArray($array) {
    $trace = debug_backtrace();
    $vLine = file(__FILE__);
    $fLine = $vLine[$trace[0]['line'] - 1];
    $x = '\w+)';
    $pattern = "/\\$(" . $x . "/";
    preg_match($pattern, $fLine, $match);
    $line = $match[1];
    print "<p />$line: ";
    print_r($array);
}

?>
